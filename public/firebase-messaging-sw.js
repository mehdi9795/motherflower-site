importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-messaging.js');

const config = {
  messagingSenderId: '358354824957',
};
firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(payload => {
  const notif = JSON.parse(payload.data.custom_notification);

  const title = notif.title;
  // const options = {
  //   body: notif.body,
  //   icon: notif.large_icon,
  // };
  console.log('notif', notif);
  const options = {
    body: notif.body,
    // icon: notif.large_icon,
    badge: '/favicon.ico',
    image: notif.large_icon,
    actions: [
      {
        action: 'coffee-action',
        title: 'بستن',
      },
    ],
  };

  return self.registration.showNotification(title, options);
});

self.addEventListener('notificationclick', event => {
  const clickedNotification = event.notification;
  clickedNotification.close();

  /* const promiseChain = clients
    .matchAll({
      type: 'window',
      includeUncontrolled: true,
    })
    .then(windowClients => {
      let matchingClient = null;
      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i];
        if (windowClient.url === feClickAction) {
          matchingClient = windowClient;
          break;
        }
      }
      if (matchingClient) {
        return matchingClient.focus();
      }
      return clients.openWindow(feClickAction);
    });
  event.waitUntil(promiseChain); */
});
