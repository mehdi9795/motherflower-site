import React from 'react';
import { BackTop } from 'antd';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './BackToTop.css';

class BackToTop extends React.Component {
  static propTypes = {
    onClick: PropTypes.func,
    visibilityHeight: PropTypes.number,
  };

  static defaultProps = {
    onClick: () => {},
    visibilityHeight: null,
  };

  render() {
    const { onClick, visibilityHeight } = this.props;
    return <BackTop onClick={onClick} visibilityHeight={visibilityHeight} />;
  }
}

export default withStyles(s)(BackToTop);
