import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cs from 'classnames';
import ReactGA from 'react-ga';
import s from './Footer.css';
import Link from '../../Link/Link';
import { baseCDN } from '../../../setting';

class Footer extends React.Component {
  static propTypes = {
    aboutUs1: PropTypes.string,
    aboutUs2: PropTypes.string,
    address: PropTypes.string,
    callNum: PropTypes.string,
    callNum2: PropTypes.string,
    fax: PropTypes.string,
    mail: PropTypes.string,
    webSite: PropTypes.string,
  };

  static defaultProps = {
    aboutUs1:
      'این شرکت با داشتن نیروهای متخصص داخلی و خارجی در گرایشهای مختلف توانایی ارائه خدمات،از ابتدا تا انتهای یک پروژه را داشته.',
    aboutUs2:
      'شرکت کاسپین شرکتی فعال و پویا است که در زمینه گل و گیاه فعالیت می کند.',
    address: 'تهران، جردن، خیابان آرش غربی، پلاک 24، طبقه دوم، واحد 1',
    callNum: '1876',
    callNum2: '91001877 - 021 ',
    fax: '88000000',
    mail: 'info@motherflower.com',
    webSite: 'www.motherflower.com',
  };

  constructor(props) {
    super(props);
    this.state = {
      aboutUs: false,
      working: false,
      lastProduct: false,
      contactUs: false,
      isWeb: false,
    };
  }

  componentDidMount() {
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
        navigator.userAgent,
      )
    ) {
      this.setState({
        aboutUs: true,
        working: true,
        lastProduct: true,
        contactUs: true,
      });
    }
    this.updatePredicate();
    const _Hasync = [];
    _Hasync.push(['Histats.start', '1,4206998,4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);

    const hs = document.createElement('script');
    hs.type = 'text/javascript';
    hs.async = true;
    hs.src = '//s10.histats.com/js15_as.js';
    (
      document.getElementsByTagName('head')[0] ||
      document.getElementsByTagName('body')[0]
    ).appendChild(hs);
  }

  onClick = name => {
    this.setState({
      [name]: !this.state[name],
    });
  };

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  goTo = url => {
    window.open(url, '_blank');
  };

  render() {
    const {
      aboutUs1,
      aboutUs2,
      address,
      callNum,
      callNum2,
      fax,
      mail,
      webSite,
    } = this.props;

    const { isWeb } = this.state;
    return (
      <footer className={cs(s.megaFooter, s.FooterBackground)} id="footer">
        <div className={s.footerContainer}>
          <div className={s.footerColumn}>
            <h1 className={s.footerHeader}>مادر فلاور</h1>
            <ul className={cs(s.footerListLink, s.footerListLinkBordered)}>
              {/* <li> */}
              {/* <Link to="#">درباره ما</Link> */}
              {/* </li> */}
              <li>
                <Link to="/cooperation">فرصت های شغلی (استخدام)</Link>
              </li>
              <li>
                <Link to="/vendorAffiliate">ثبت گلفروشی</Link>
              </li>
              <li>
                <Link to="/privacy">حریم خصوصی</Link>
              </li>
              <li>
                <Link to="/faq">پرسش های متداول</Link>
              </li>
              <li>
                <Link to="/app">دانلود اپلیکیشن</Link>
              </li>
              {/* <li> */}
              {/* <Link to="/rules">قوانین</Link> */}
              {/* </li> */}
            </ul>
          </div>
          <div className={cs(s.footerColumn, s.contactUs)}>
            <Link to="/contact">
              <h1 className={s.footerHeader}>تماس با ما</h1>
            </Link>
            <ul className={cs(s.footerListLink, s.footerListLinkBordered)}>
              <li>{address}</li>
              <li>
              ایمیل: {mail}
              </li>
              <li>
                تلفن تهران: ( بدون پیش شماره ) {callNum}
              </li>
              <li>
                تلفن شهرستان: {callNum2}
              </li>
              <li>
                <p>
                  <Link
                    to="https://www.telegram.org/motherflower_official/"
                    className="mdl-button mdl-js-button mdl-button--icon"
                    target="_blank"
                  >
                    <i className="mf-telegram" />
                  </Link>
                  <Link
                    to="https://www.instagram.com/motherflower.official/"
                    className="mdl-button mdl-js-button mdl-button--icon"
                    target="_blank"
                  >
                    <i className="mf-instagram" />
                  </Link>
                  <Link
                    to="#"
                    className="mdl-button mdl-js-button mdl-button--icon"
                    target="_blank"
                  >
                    <i className="mf-twitter" />
                  </Link>
                  <Link
                    to="https://www.facebook.com/motherflower.official/"
                    className="mdl-button mdl-js-button mdl-button--icon"
                    target="_blank"
                  >
                    <i className="mf-facebook" />
                  </Link>
                </p>
              </li>
            </ul>
            
          </div>
        
              <div className={cs(s.footerColumn, s.contactUs)}>
              <div className={s.footerWidget}>
              <p>ساعت کاری شنبه تا 5 شنبه </p>
              <p>از ساعت 10 الی 20</p>
            </div>
            <img
              height={95}
              className={s.footerImg}
              src={`${baseCDN}/Layer6.png`}
              alt="product thumbnail"
            />
            <img
              src="https://trustseal.enamad.ir/logo.aspx?id=87527&amp;p=HcjFUjtXPVTqTeXs"
              alt=""
              onClick={() => {
                window.open(
                  'https://trustseal.enamad.ir/Verify.aspx?id=87527&p=HcjFUjtXPVTqTeXs',
                  'Popup',
                  'toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width=580, height=600, top=30',
                );
              }}
              style={{ cursor: 'pointer' }}
              id="HcjFUjtXPVTqTeXs"
            />
              </div>
        </div>

        <noscript>
          <a href="/" target="_blank">
            <img
              src="//sstatic1.histats.com/0.gif?4206998&101"
              alt="website stat"
              border="0"
            />
          </a>
        </noscript>
      </footer>
    );
  }
}

export default withStyles(s)(Footer);
