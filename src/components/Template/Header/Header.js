import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Header.css';
import Navigation from '../Navigation/Navigation';
import RoundLogo from '../RoundLogo';
import HeaderSocialMenu from '../HeaderSocialMenu/HeaderSocialMenu';
import { hideAlertRequest } from '../../../redux/shared/action/errorAlert';


class Header extends React.Component {
  static propTypes = {
    isBackground: PropTypes.bool,
    navigationLoading: PropTypes.bool,
  };

  static defaultProps = {
    isBackground: false,
    navigationLoading: false,
  };

  render() {
    return (
      <div
        className={this.props.isBackground ? 'isBackground' : 'noBackground'}
        id="header"
      >
        <div className={s.row}>
          <Navigation />
          <HeaderSocialMenu />
        </div>
        <RoundLogo />
      </div>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(Header),
);
