import React from 'react';
import { bindActionCreators } from 'redux';
import { showLoading } from 'react-redux-loading-bar';
import { Menu, Dropdown } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Navigation.css';
import Link from '../../Link/Link';
import { deleteCookie, getCookie } from '../../../utils';
import { loginSuccess } from '../../../redux/identity/action/account';
import { LogOutApi } from '../../../services/identityApi';
import { BasketDto } from '../../../dtos/catalogDtos';
import { getDtoQueryString } from '../../../utils/helper';
import { GetUserBasketHistoriesApi } from '../../../services/samApi';
import { basketListSuccess } from '../../../redux/catalog/action/basket';
import history from '../../../history';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';

class Navigation extends React.Component {
  static propTypes = {
    loginData: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    loginData: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isWeb: false,
    };
    this.logOut = this.logOut.bind(this);
  }

  componentDidMount() {
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updatePredicate);
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  async logOut() {
    const response = await LogOutApi();
    if (response.status === 200) {
      if (window.location.pathname !== '/') this.props.actions.showLoading();

      const guestKey = getCookie('guestKey');
      deleteCookie('siteToken');
      deleteCookie('siteUserName');
      deleteCookie('siteDisplayName');
      deleteCookie('imageUser');
      this.props.actions.loginSuccess({});
      const responseGet = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new BasketDto({
                guestKey,
              }),
            )
            .buildJson(),
        ),
        null,
      );

      if (responseGet.status === 200) {
        this.props.actions.basketListSuccess(responseGet.data);
      }
    }
  }

  goToUrl = (e, url) => {
    e.preventDefault();
    this.props.actions.showLoading();
    history.push(url);
  };

  render() {
    const { loginData } = this.props;
    const { isWeb } = this.state;
    const menu = (
      <Menu>
        <Menu.Item>
          <Link to="/" onClick={e => this.goToUrl(e, '/profile')}>
            پروفایل
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/" onClick={e => this.goToUrl(e, '/profile?selectTab=order')}>
            لیست سفارشات
          </Link>
        </Menu.Item>
        <Menu.Item>
          <Link to="/#" onClick={this.logOut}>
            خروج
          </Link>
        </Menu.Item>
      </Menu>
    );

    return (
      <div className={s.navigation}>
        {loginData.token ? (
          <ul className={s.loginUser}>
            {isWeb ? (
              <li>
                {loginData.imageUser ? (
                  <img
                    src={`${loginData.imageUser}?dummy=${Math.random()}`}
                    className={s.userImage}
                    width={32}
                    height={32}
                  />
                ) : (
                  <i className="mf-user" />
                )}
                <b className={s.greeting}>سلام</b>
                <Dropdown overlay={menu} trigger={['click']}>
                  <b className={s.userName}>
                    {loginData.displayName} <i className="mf-down" />
                  </b>
                </Dropdown>
              </li>
            ) : (
              <li>
                <Dropdown overlay={menu} trigger={['click']}>
                  <b className={s.userName}>
                    {loginData.imageUser ? (
                      <img
                        src={`${loginData.imageUser}?dummy=${Math.random()}`}
                        className={s.userImage}
                        width={32}
                        height={32}
                      />
                    ) : (
                      <i className="mf-user" />
                    )}
                  </b>
                </Dropdown>
              </li>
            )}
          </ul>
        ) : (
          <ul className={s.userMenu}>
            <li>
              <Link
                className={s.link}
                to="/"
                onClick={e => this.goToUrl(e, '/login')}
              >
                ورود
              </Link>
            </li>
            <li>
              <Link
                className={s.link}
                to="/"
                onClick={e => this.goToUrl(e, '/register')}
              >
                عضویت
              </Link>
            </li>
          </ul>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loginData: state.account.loginData ? state.account.loginData : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginSuccess,
      basketListSuccess,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Navigation));
