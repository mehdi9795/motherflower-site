/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.


 */

import React from 'react';
import { showLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RoundLogo.css';
import logoUrl from './logo.png';
import Link from '../../Link/Link';
import history from '../../../history';

class RoundLogo extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  }

  goToUrl = (e, url) => {
    e.preventDefault();
    if (window.location.pathname !== '/') {
      this.props.actions.showLoading();
      history.push(url);
    }
  };

  render() {
    const { className } = this.props;
    return (
      <div className={cs(s.logoWrapper, className)}>
        <i className={s.lineRight} />
        <Link className={s.mainLogo} to="/" onClick={e => this.goToUrl(e, '/')}>
          <img className={s.logoColored} src={logoUrl} alt="MotherFlower" />
        </Link>
        <i className={s.lineLeft} />
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(RoundLogo),
);
