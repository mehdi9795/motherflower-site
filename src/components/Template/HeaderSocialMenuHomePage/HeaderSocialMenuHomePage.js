import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './HeaderSocialMenuHomePage.css';
import Link from '../../Link/Link';

class HeaderSocialMenuHomePage extends React.Component {
  render() {
    return (
      <div className={s.container}>
        <div className={s.login}>
      <label>تلفن: 1876 - 91001877 (021) </label>
      <ul className={s.socialMenu}>
        {/* <li>
          <Link
            className={s.link}
            to="https://www.facebook.com/motherflower.official/"
            target="_blank"
          >
            <i className="mf-facebook" />
          </Link>
        </li>
        <li>
          <Link className={s.link} to="/" target="_blank">
            <i className="mf-twitter" />
          </Link>
        </li> */}
        <li>
          <Link
            className={s.link}
            to="https://www.instagram.com/motherflower.official/"
            target="_blank"
          >
            <i className="mf-instagram" />
          </Link>
        </li>
        <li>
          <Link
            className={s.link}
            to="https://www.telegram.org/motherflower_official/"
            target="_blank"
          >
            <i className="mf-telegram" />
          </Link>
        </li>
      </ul>
      </div>
      </div>
    );
  }
}

export default withStyles(s)(HeaderSocialMenuHomePage);
