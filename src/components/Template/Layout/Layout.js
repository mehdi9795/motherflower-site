import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Dropdown, Layout as LayoutAnt, Menu } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import LoadingBar from 'react-redux-loading-bar';
import normalizeCss from '../../../../node_modules/normalize.css/normalize.css';
import Navigation from '../Navigation';
import Sidebar from '../Sidebar/Sidebar';
import s from './Layout.css';
import Link from '../../Link/Link';
import {baseCDN} from "../../../setting";

const { Header, Sider, Content } = LayoutAnt;

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    mainLogo: PropTypes.string.isRequired,
    vendorLogo: PropTypes.string.isRequired,
    loadingBar: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    mainLogo: `${baseCDN}/logo.png`,
    vendorLogo: `${baseCDN}/vendor_logo.png`,
    notification: [],
    loadingBar: 0,
  };

  constructor() {
    super();
    this.state = {
      collapsed: false,
    };
  }

  toggle = e => {
    e.preventDefault();
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    const { mainLogo, vendorLogo, loadingBar } = this.props;
    const menu = (
      <Menu>
        <div className={s.menu}>
          <ul className={s.tools}>
            <li>
              <Link to="/order/list">
                <i className="cp-list" />
                سفارشات
              </Link>
            </li>
            <li>
              <Link to="/product/list">
                <i className="cp-business" />
                محصولات
              </Link>
            </li>
            <li>
              <Link to="/promotion/list">
                <i className="cp-percentage" />
                پروموشن
              </Link>
            </li>
            <li>
              <Link to="/message/list">
                <i className="cp-file2" />
                درخواست ها
              </Link>
            </li>
            <li>
              <Link to="/user/list">
                <i className="cp-users" />
                کاربران
              </Link>
            </li>
          </ul>
        </div>
      </Menu>
    );

    return (
      <LayoutAnt
        style={{ flexDirection: 'row', minHeight: '100vh' }}
        className={s.layout}
      >
        <LoadingBar
          style={{ backgroundColor: 'yellow', height: '3px', zIndex: 10000, position: 'fixed' }}
          loading={loadingBar}

        />
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo">
            <img className={s.vendorLogo} src={vendorLogo} alt="" />
          </div>
          <Sidebar />
        </Sider>
        <LayoutAnt>
          <LoadingBar style={{ backgroundColor: 'yellow', height: '3px' }} />
          <Header className={s.header}>
            <div className={s.rightNav}>
              {this.state.collapsed ? (
                <Link className="navItems" to="/" onClick={e => this.toggle(e)}>
                  <i className={cs('cp-menu', s.trigger)} />
                </Link>
              ) : (
                <Link className="navItems" to="/" onClick={e => this.toggle(e)}>
                  <i className={cs('cp-menu', s.trigger)} />
                </Link>
              )}

              <Dropdown
                overlay={menu}
                trigger={['click']}
                placement="bottomRight"
              >
                <Link className="ant-dropdown-link navItems" to="#">
                  <i className="cp-app" />
                </Link>
              </Dropdown>
            </div>

            <div className={s.mainLogo}>
              <Link to="/#" className={s.link}>
                <img src={mainLogo} alt="" />
              </Link>
            </div>
            <Navigation />
          </Header>

          <Content className={s.content}>{this.props.children}</Content>
        </LayoutAnt>
      </LayoutAnt>
    );
  }
}

const mapStateToProps = state => ({
  notification: state.notificationNotification.notificationListData,
  loadingBar: state.loadingBar.default,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {},
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(normalizeCss, s)(Layout),
);
