import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { LoadingBar } from 'react-redux-loading-bar';
import { Spin } from 'antd';
import { bindActionCreators } from 'redux';
import $ from 'jquery';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import s from './CheckoutLayout.css';
import { hideAlertRequest } from '../../../../redux/shared/action/errorAlert';
import { showNotification } from '../../../../utils/helper';
import { baseCDN } from '../../../../setting';

class CheckoutLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    loadingAddressCheckout: PropTypes.bool,
    loadingBar: PropTypes.number,
    error: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    loadingAddressCheckout: false,
    loadingBar: 0,
    error: '',
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      showNotification('error', 'خطا', nextProps.error, 10, 'errorsBox');
      nextProps.actions.hideAlertRequest();
    }
  }

  componentDidMount() {
    // disable right click img
    $(document).on('contextmenu', 'img', () => false);
  }

  render() {
    const { loadingAddressCheckout, loadingBar } = this.props;

    return loadingAddressCheckout || loadingBar ? (
      <Spin spinning={loadingAddressCheckout}>
        <div
          className={s.content}
          style={{ backgroundImage: `url(${baseCDN}/home/1960.jpg)` }}
        >
          <LoadingBar
            style={{
              backgroundColor: 'yellow',
              height: '3px',
              zIndex: 10,
              position: 'fixed',
            }}
            loading={loadingBar}
          />
          {this.props.children}
        </div>
      </Spin>
    ) : (
      <div
        className={s.content}
        style={{ backgroundImage: `url(${baseCDN}/home/1960.jpg)` }}
      >
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadingAddressCheckout:
    state.identityAddress.addressListRequest ||
    state.identityAddress.addressPostRequest ||
    state.identityAddress.addressPutRequest ||
    state.identityAddress.addressDeleteRequest,
  error: state.errorAlert.message,
  navigationLoading: state.loadingSearch.navigationLoading,
  loadingBar: state.loadingBar.default,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CheckoutLayout));
