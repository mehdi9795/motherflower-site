import React from 'react';
import { LoadingBar } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import $ from 'jquery';
import { Spin } from 'antd';
import { bindActionCreators } from 'redux';
import { Drawer } from 'antd-mobile';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LoginLayout.css';
import { hideAlertRequest } from '../../../../redux/shared/action/errorAlert';
import CPBurgerMenu from '../../../CP/CPBurgerMenu';
import AddToCard from '../../../Shared/AddToCard';
import { showNotification } from '../../../../utils/helper';
import RoundLogo from '../../RoundLogo';
import {baseCDN} from "../../../../setting";

class LoginLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    error: PropTypes.string,
    loadingLogin: PropTypes.bool,
    loadingSignUp: PropTypes.bool,
    loadingBar: PropTypes.number,
    basketProductCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };
  static defaultProps = {
    error: '',
    loadingLogin: false,
    loadingSignUp: false,
    loadingBar: 0,
    basketProductCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };

    const classes = [
      `${baseCDN}/login/back1.jpg`,
      `${baseCDN}/login/back2.jpg`,
      `${baseCDN}/login/back3.jpg`,
      `${baseCDN}/login/back4.jpg`,
      `${baseCDN}/login/back5.jpg`,
      `${baseCDN}/login/back6.jpg`,
      `${baseCDN}/login/back7.jpg`,
      `${baseCDN}/login/back8.jpg`,
    ];

    this.Background = classes[Math.floor(Math.random() * classes.length)];
  }

  componentDidMount() {
    $('#chat').addClass('showFloatingBtn');
    $('.addToCard').addClass('showFloatingBtn');
    setTimeout(() => {
      $('#chat').addClass('shakeChatBtn');
      $('.addToCard').addClass('shakeChatBtn');
    }, 400);
    setTimeout(() => {
      $('#chat').removeClass('shakeChatBtn');
      $('.addToCard').removeClass('shakeChatBtn');
    }, 3000);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      showNotification('error', 'خطا', nextProps.error, 10, 'errorsBox');
      nextProps.actions.hideAlertRequest();
    }
  }

  onOpenChange = () => {
    this.setState({ open: !this.state.open });
    setTimeout(() => {
      $('.addToCard').addClass('showFloatingBtn');
    }, 500);
  };

  render() {
    const sidebar = <CPBurgerMenu />;
    const {
      basketProductCount,
      loadingSignUp,
      loadingLogin,
      loadingBar,
    } = this.props;

    return (
      <div>
        <Drawer
          className="my-drawer"
          enableDragHandle
          style={{ minHeight: '100vh' }}
          sidebar={sidebar}
          open={this.state.open}
          onOpenChange={this.onOpenChange}
        >
          <div />
        </Drawer>
        <LoadingBar
          style={{
            backgroundColor: 'yellow',
            height: '3px',
            zIndex: 10,
            position: 'fixed',
          }}
          loading={loadingBar}
        />
        <div
          className={s.content}
          style={{ backgroundImage: `url(${this.Background})` }}
        >
          <Spin spinning={loadingLogin || loadingSignUp}>
            <RoundLogo />
            {this.props.children}
          </Spin>
        </div>
        <AddToCard count={basketProductCount} onClick={this.onOpenChange} />
        {/* <Chat /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadingLogin: state.account.loginRequest,
  loadingSignUp: state.account.signUpRequest,
  error: state.errorAlert.message,
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
  navigationLoading: state.loadingSearch.navigationLoading,
  loadingBar: state.loadingBar.default,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(LoginLayout));
