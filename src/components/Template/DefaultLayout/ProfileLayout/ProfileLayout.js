import React from 'react';
import PropTypes from 'prop-types';
import { LoadingBar } from 'react-redux-loading-bar';
import $ from 'jquery';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import { bindActionCreators } from 'redux';
import { Drawer } from 'antd-mobile';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileLayout.css';
import Sidebar from '../../../profile/Sidebar';
import Footer from '../../Footer';
import Header from '../../Header';
import { hideAlertRequest } from '../../../../redux/shared/action/errorAlert';
import { showNotification } from '../../../../utils/helper';

class ProfileLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    onRef: PropTypes.func,
    error: PropTypes.string,
    loading: PropTypes.bool,
    loadingBar: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    onRef: () => {},
    error: '',
    loading: false,
    loadingBar: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  componentDidMount() {
    this.props.onRef(this);
    // $('#chat').addClass('showFloatingBtn');
    setTimeout(() => {
      // $('#chat').addClass('shakeChatBtn');
    }, 400);
    setTimeout(() => {
      // $('#chat').removeClass('shakeChatBtn');
      $('.addToCard').removeClass('shakeChatBtn');
    }, 3000);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      showNotification('error', 'خطا', nextProps.error, 10, 'errorsBox');
      nextProps.actions.hideAlertRequest();
    }
  }

  onOpenChange = () => {
    this.setState({ open: !this.state.open });
    setTimeout(() => {
      $('.addToCard').addClass('showFloatingBtn');
    }, 500);
  };

  render() {
    const { loading, loadingBar } = this.props;
    const sidebar = <Sidebar />;

    return (
      <div>
        <Drawer
          className="my-drawer"
          enableDragHandle
          style={{ minHeight: '100vh' }}
          position="right"
          sidebar={sidebar}
          open={this.state.open}
          onOpenChange={this.onOpenChange}
        >
          <div />
        </Drawer>
        <LoadingBar
          style={{
            backgroundColor: 'yellow',
            height: '3px',
            zIndex: 10,
            position: 'fixed',
          }}
          loading={loadingBar}
        />
        <Header isBackground />
        <div className={s.content}>
          <Spin spinning={loading}>{this.props.children}</Spin>
        </div>
        <Footer />
        {/* <Chat /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loadingSearch.loadingSearch,
  error: state.errorAlert.message,
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
  navigationLoading: state.loadingSearch.navigationLoading,
  loadingBar: state.loadingBar.default,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProfileLayout));
