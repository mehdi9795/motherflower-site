import React from 'react';
import { LoadingBar } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Drawer } from 'antd-mobile';
import $ from 'jquery';
import { LocaleProvider, Spin } from 'antd';
import fa_IR from 'antd/lib/locale-provider/fa_IR';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Layout.css';
import Footer from '../../Footer/Footer';
import AddToCard from '../../../Shared/AddToCard/AddToCard';
import BackToTop from '../../BackToTop/BackToTop';
import CPBurgerMenu from '../../../CP/CPBurgerMenu';
import { hideAlertRequest } from '../../../../redux/shared/action/errorAlert';
import Header from '../../Header';
import { getCookie } from '../../../../utils';
import AppInstall from '../../../Home/AppInstall';
import ReactGA from 'react-ga';

const DEFAULT_CONFIG = {
  trackingId: 'UA-138063597-1',
  debug: true,
  gaOptions: {
    cookieDomain: 'none',
  },
};

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    error: PropTypes.string,
    basketProductCount: PropTypes.number,
    visibleAddToCard: PropTypes.bool,
    showFooter: PropTypes.bool,
    loadingBar: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    error: '',
    basketProductCount: 0,
    visibleAddToCard: false,
    showFooter: true,
    loadingBar: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      showComponent: false,
      showAppInstall: false,
      os: '',
    };
    this.firstTime = true;
  }

  componentDidMount() {
    this.showComponent();
    this.checkInstallApp();
    this.initReactGA();
  }

  onOpenChange = () => {
    this.setState({ open: !this.state.open });
    setTimeout(() => {
      $('.addToCard').addClass('showFloatingBtn');
    });
  };

  onClose = () => {
    this.props.actions.hideAlertRequest();
  };

  showComponent = () => {
    this.setState({ showComponent: true }, () => {
      // $('#chat').addClass('showFloatingBtn');
      $('.addToCard').addClass('showFloatingBtn');
      setTimeout(() => {
        // $('#chat').addClass('shakeChatBtn');
        $('.addToCard').addClass('shakeChatBtn');
      }, 400);
      setTimeout(() => {
        // $('#chat').removeClass('shakeChatBtn');
        $('.addToCard').removeClass('shakeChatBtn');
      }, 3000);
    });

    // disable right click img
    $(document).on('contextmenu', 'img', () => false);
  };

  checkInstallApp = () => {
    if (navigator.userAgent.match(/Android/i)) {
      this.setState({ os: 'android' });
    } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i))
      this.setState({ os: 'ios' });

    this.setState({ showAppInstall: getCookie('C-App') !== 'true' });
  };

  initReactGA = () => {
    if (window) ReactGA.initialize(DEFAULT_CONFIG);
    ReactGA.pageview(window.location.pathname);
  };

  render() {
    const { showComponent, os, showAppInstall } = this.state;
    const {
      basketProductCount,
      error,
      children,
      visibleAddToCard,
      loadingBar,
      showFooter,
    } = this.props;
    const sidebar = <CPBurgerMenu />;
    return showComponent ? (
      <div>
        <Drawer
          className="my-drawer"
          enableDragHandle
          style={{ minHeight: '100vh' }}
          sidebar={sidebar}
          open={this.state.open}
          onOpenChange={this.onOpenChange}
        >
          <div />
        </Drawer>
        <div>
          <LoadingBar
            style={{
              backgroundColor: 'yellow',
              height: '3px',
              zIndex: 10000,
              position: 'fixed',
            }}
            loading={loadingBar}
          />
        </div>
        <div onClick={this.onClose}>{error}</div>
        {showAppInstall &&
          os && (
            <AppInstall
              androidLink="http://bit.ly/2FcbyNG"
              iosLink="/IOS"
            />
          )}
        <Header isBackground />
        <LocaleProvider locale={fa_IR}>
          <div className={s.content}>{children}</div>
        </LocaleProvider>
        {showFooter && <Footer />}
        {visibleAddToCard && (
          <AddToCard count={basketProductCount} onClick={this.onOpenChange} />
        )}
        {/* <Chat /> */}
        <BackToTop visibilityHeight={200} />
      </div>
    ) : (
      <div className={s.spinner}>
        <Spin loading />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.errorAlert.message,
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
  visibleAddToCard: state.sharedAddToCard.showAddToCardRequest,
  loadingBar: state.loadingBar.default,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Layout));
