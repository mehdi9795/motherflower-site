import React, { Component } from 'react';
import { showLoading } from 'react-redux-loading-bar';
import cs from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CPRate from '../../CP/CPRate/CPRate';
import Link from '../../Link/Link';
import s from './SpecialProductCard.css';
import { DISCOUNT, TOMAN } from '../../../Resources/Localization';
import { baseCDN } from '../../../setting';

class SpecialProductCard extends Component {
  static propTypes = {
    imageUrl1: PropTypes.string,
    imageUrl2: PropTypes.string,
    ProductName: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.string,
    discount: PropTypes.string,
    value: PropTypes.number,
    percent: PropTypes.string,
    lazyLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    styles: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    price: '',
    ProductName: '',
    url: '',
    discount: '',
    value: 4,
    percent: '',
    imageUrl1: 'https://cdn.motherflower.com:7090/SiteAsset/ImageNotFound.png',
    imageUrl2: 'https://cdn.motherflower.com:7090/SiteAsset/ImageNotFound.png',
    lazyLoading: false,
    styles: {},
  };

  goToUrl = (e, url) => {
    // e.preventDefault();
    if (!window.location.pathname.includes('product-details')) {
      this.props.actions.showLoading();
    }
    // history.push(url);
  };

  render() {
    const {
      imageUrl1,
      imageUrl2,
      price,
      discount,
      url,
      percent,
      value,
      lazyLoading,
      styles,
      ProductName,
    } = this.props;
    
    return (
      <div className={cs(s.cardProduct, 'animate')} style={styles}>
        <div className={s.cardProductMedia}>
          <Link
            to={url}
            onClick={e => this.goToUrl(e, url)}
            className={s.productLink}
          >
              <div
                className={s.cardProductImg}
                style={{ backgroundImage: `url(${imageUrl1})` }}
              />
            <label className={s.productName}>{ProductName}</label>
            <div className={cs(s.rate)}>
              <CPRate value={value} disabled />
           </div>
            {percent > 0 && (
              <span className={cs(s.cardProductBadge, s.badge)}>
                {/* <img
                  src={`${baseCDN}/label.png`}
                  className={s.badgeImg}
                  alt=""
                /> */}
              
                <small>
                  <span>{`${percent}%`}</span> {DISCOUNT}
                </small>
              
                <small>
                  Pick-Up
                </small>
              </span>
            )}
          </Link>
        </div>
        <div className={s.cardProductAction}>
          <strong className={s.productPrice}>
            <ins>
              <b>{discount}</b>
            </ins>
            {percent > 0 && (
              <del>
                <span className={s.deletePrice}>{price}</span>
              </del>
            )}
            <b className={s.currency}>{TOMAN}</b>
          </strong>{' '}
        </div>
        
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SpecialProductCard));
