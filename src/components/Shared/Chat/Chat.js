import React from 'react';
import $ from 'jquery';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Chat.css';
import CPButton from '../../CP/CPButton/CPButton';
import {baseCDN} from "../../../setting";

class Chat extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  onClick = () => {
    setTimeout(() => {
      $('#chat').addClass('showFloatingBtn');
    }, 500);
  };

  render() {
    return (
      <CPButton
        className={s.chatBtn}
        id="chat"
        type="circle"
        onClick={this.onClick}
      >
        <img src={`${baseCDN}/chat.png`} alt="" />
      </CPButton>
    );
  }
}

export default withStyles(s)(Chat);
