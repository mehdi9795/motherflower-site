import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import cs from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import s from './AddToCard.css';
import CPButton from '../../CP/CPButton/CPButton';
import { GetUserBasketHistoriesApi } from '../../../services/samApi';
import { getDtoQueryString } from '../../../utils/helper';
import { getCookie, setCookie } from '../../../utils';
import { basketListSuccess } from '../../../redux/catalog/action/basket';
import { BasketDto } from '../../../dtos/catalogDtos';
import { UserGuestKeyApi } from '../../../services/identityApi';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';

class AddToCard extends React.Component {
  static propTypes = {
    count: PropTypes.number,
    // className: PropTypes.string,
    onClick: PropTypes.func,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    count: 0,
    // className: '',
    onClick: () => {},
  };

  async componentDidMount() {
    const token = getCookie('siteToken');
    const guestKey = getCookie('guestKey');
    if (!token && !guestKey) {
      const response = await UserGuestKeyApi();

      if (response.data && response.data.guestKey)
        setCookie('guestKey', response.data.guestKey, 8760);
    }

    const responseG = await GetUserBasketHistoriesApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BasketDto({
              guestKey: !token ? guestKey : undefined,
            }),
          )
          .buildJson(),
      ),
      token,
    );
    if (responseG.status === 200) {
      this.props.actions.basketListSuccess(responseG.data);
    }
  }

  render() {
    const { onClick, count } = this.props;
    return (
      <div className={cs(s.addBtn, 'add-to-card-icon')}>
        <CPButton onClick={() => onClick()} type="circle" className="addToCard">
          <i className="mf-addToCard" />
          {count > 0 && <small>{this.props.count}</small>}
        </CPButton>
        {/* {count && <CPBadge className={s.badgeCount} count={count} />} */}
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      basketListSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(AddToCard));
