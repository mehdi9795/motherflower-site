import PropTypes from 'prop-types';
import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Card, Divider, Tooltip, Button, Icon } from 'antd';
import s from './VendorCard.css';
import Link from '../../Link/Link';

class VendorCard extends React.Component {
  static propTypes = {
    vendorName: PropTypes.string.isRequired,
    address: PropTypes.string,
    workField: PropTypes.string.isRequired,
    hours: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    tooltipTxt: PropTypes.string.isRequired,
    rate: PropTypes.string.isRequired,
    rateNum: PropTypes.string.isRequired,
    call: PropTypes.string.isRequired,
  };

  static defaultProps = {
    address: 'جردن، آرش غربی، پلاک 24',
    // workField: 'دسته گل عروس، تاج گل، گلدان',
    // hours: '8 صبح تا 12 شب',
    // call: 'تماس',
    // productList: 'لیست محصولات',
    // tooltipTxt: 'عالی',
    // rate: '4.8',
    // rateNum: '46 رای',
  };

  render() {
    const {
      vendorName,
      address,
      hours,
      workField,
      tooltipTxt,
      rate,
      rateNum,
      call,
      productList,
      imageUrl,
    } = this.props;

    return (
      <div className={s.vendorCard}>
        <Card bordered={false} className={s.vendorCardddddddddddddddd}>
          <div className={s.rowWrapper}>
            <div className={s.firstRow}>
              <div className={s.proImage}>
                <a
                  className={s.imageCard}
                  style={{ backgroundImage: `url(${imageUrl})` }}
                />
              </div>
              <div className={s.proInfo}>
                <Link to="/login">
                  <h5>{vendorName}</h5>
                </Link>
                <Link to="/login">{address}</Link>
              </div>

              <div className={s.rating}>
                <Tooltip placement="bottom" title={tooltipTxt}>
                  <Button>{rate}</Button>
                </Tooltip>
                <small>{rateNum}</small>
              </div>
            </div>

            <Divider />

            <div className={s.secondRow}>
              <div className={s.lable}>
                <p>حوزه کاری</p>
                <p>شیفت کاری</p>
              </div>
              <div className={s.field}>
                <p>{workField}</p>
                <p>{hours}</p>
              </div>
            </div>
          </div>

          <div className={s.buttons}>
            <a className={s.links} href={'/'}>
              <Icon type="phone" />
              {call}
            </a>
            <a className={s.links} href={'/'}>
              <Icon type="appstore-o" />
              {productList}
            </a>
          </div>
        </Card>
      </div>
    );
  }
}

export default withStyles(s)(VendorCard);
