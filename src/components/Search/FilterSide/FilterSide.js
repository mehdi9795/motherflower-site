import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { AutoComplete, Collapse, Select } from 'antd';
import PropTypes from 'prop-types';
import s from './FilterSide.css';
import {
  DELIVERY_TYPE,
  CATEGORY,
  FILTERS,
  FLOWER_TYPE,
  CHOOSE_COLOR,
  TAGS,
  PRICE_PERIOD,
  ZONE,
  VENDOR_NAME,
  CHOOSE_FLOWER,
  IN_SHOPPING,
  IN_PLACE,
  APPLY_FILTER,
  CONFIRMATION,
  CITY,
} from '../../../Resources/Localization';
import CPMultiSelect from '../../CP/CPMultiSelect';
import CPTag from '../../CP/CPTag';
import CPButton from '../../CP/CPButton';
import CPColorPicker from '../../CP/CPColorPicker';
import CPPriceSlider from '../../CP/CPPriceSlider';
import CPPanel from '../../CP/CPPanel';
import { formatNum, setCookie } from '../../../utils';
import { SearchColorOptionDto } from '../../../dtos/catalogDtos';
import { searchSpecificationAttributeRequest } from '../../../redux/catalog/action/searchSpecificationAttribute';
import { searchColorOptionRequest } from '../../../redux/catalog/action/searchColorOption';
import CPRadio from '../../CP/CPRadio';
import { CityDto, DistrictDto, ZoneDto } from '../../../dtos/commonDtos';
import {
  districtOptionListRequest,
  districtOptionListSuccess,
} from '../../../redux/common/action/district';
import { vendorBranchZoneListSuccess } from '../../../redux/vendor/action/vendorBranchZone';
import CPBadge from '../../CP/CPBadge';
import CPTree from '../../CP/CPTree';
import { treeModel } from '../../../utils/model';
import {
  isEmptyProductCardArray,
  productSearchRequest,
  productSearchSuccess,
} from '../../../redux/catalog/action/product';
import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../../redux/shared/action/loading';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import { VendorBranchDto, VendorBranchZoneDto } from '../../../dtos/vendorDtos';
import { getVendorBranchZoneApi } from '../../../services/vendorApi';
import {
  categoryListRequest,
  categoryListSuccess,
} from '../../../redux/catalog/action/category';
import {
  getSearchVendorBranchCategoriesApi,
  getTagVendorBranchApi,
} from '../../../services/catalogApi';
import { tagListSuccess } from '../../../redux/catalog/action/tag';
import CPModal from '../../CP/CPModal';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';
import CPSelect from '../../CP/CPSelect';

const { Option } = Select;
const { Panel } = Collapse;
const classNameOne = 'touchMode';

class FilterSide extends React.Component {
  static propTypes = {
    categoryData: PropTypes.arrayOf(PropTypes.object),
    districts: PropTypes.arrayOf(PropTypes.object),
    vendorBranches: PropTypes.arrayOf(PropTypes.object),
    searchColorOptionData: PropTypes.arrayOf(PropTypes.object),
    searchSpecificationAttributeData: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    tags: PropTypes.arrayOf(PropTypes.object),
    params: PropTypes.objectOf(PropTypes.any).isRequired,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    applySearch: PropTypes.func,
    loadingSearch: PropTypes.bool,
  };

  static defaultProps = {
    categoryData: null,
    districts: null,
    vendorBranches: null,
    searchColorOptionData: null,
    searchSpecificationAttributeData: null,
    tags: null,
    loadingSearch: false,
    cities: [],
    applySearch: () => {},
  };

  constructor(props) {
    super(props);
    this.priceValue = {
      min: 20000,
      max: 3000000,
    };

    this.state = {
      treeSelectedCheckBox:
        props.params.categoryIds && props.params.categoryIds !== '0'
          ? props.params.categoryIds.split(',')
          : [],
      multiSelect: props.params.specificationIds
        ? props.params.specificationIds.split(',')
        : [],
      minPrice: props.params.minPrice
        ? props.params.minPrice
        : this.priceValue.min,
      maxPrice: props.params.maxPrice
        ? props.params.maxPrice
        : this.priceValue.max,
      isWeb: false,
      show: false,
      priceOption: props.params.priceOption || '2',
      colorPicker: props.params.colorOptionIds
        ? props.params.colorOptionIds.split(',')
        : [],
      dataSource: [],
      districtName: props.params.districtName,
      vendorBranchName: props.params.vendorBranchName,
      vendorBranchId: `${props.params.vendorBranchId}*${
        props.params.vendorBranchName
      }`,
      tagIds: props.params.tagIds
        ? props.params.tagIds.split(',').map(item => parseInt(item, 10))
        : [],
      visibleCategoryModal: false,
      categoryIds: props.params.categoryIds
        ? props.params.categoryIds.split(',')
        : [],
      categoryModalArray: [],
      cityId: props.params.cityId,
    };
    this.toggleMe = this.toggleMe.bind(this);
    this.applySearch = this.applySearch.bind(this);
    this.onSelectDistrict = this.onSelectDistrict.bind(this);
    this.handleChangeInput = this.handleChangeInput.bind(this);
    this.categoryAllArray = [];
  }

  componentWillMount() {
    const { params } = this.props;
    this.state.dataSource.push({
      text: params.districtName,
      value: params.districtId,
    });
  }

  componentDidMount() {
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.districts) {
      // this.setState({ dataSource: [] });
      const dataSourceArray = [];
      nextProps.districts.map(item =>
        dataSourceArray.push({ text: item.name, value: item.id }),
      );
      this.setState({ dataSource: dataSourceArray });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updatePredicate);
  }

  onChangeTreeSelectCheckBox = (value, from = 'web') => {
    this.setState({ treeSelectedCheckBox: value });
    const ids = [];
    value.map(item => ids.push(item.split('*')[0]));

    const listJson = new BaseGetDtoBuilder()
      .dto(
        new SearchColorOptionDto({
          categoryIds: ids.length > 0 ? ids.toString().replace(/,/g, '-') : 0,
          vendorBranchId: this.props.params.vendorBranchId,
        }),
      )
      .fromCache(true)
      .buildJson();

    this.props.actions.searchColorOptionRequest(listJson);
    this.props.actions.searchSpecificationAttributeRequest(listJson);
    this.setState(
      {
        multiSelect: [],
        colorPicker: [],
      },
      () => {
        if (from === 'web') this.applySearch();
      },
    );
  };

  onChangeMultiSelect = (value, from = 'web') => {
    this.setState({ multiSelect: value }, () => {
      if (from === 'web') this.applySearch();
    });
  };

  onChangeColorPicker = (value, from = 'web') => {
    this.setState({ colorPicker: value }, () => {
      if (from === 'web') this.applySearch();
    });
  };

  onSelect = value => {
    this.setState({ districtName: value });
  };

  async onSelectDistrict(value, option) {
    this.setState({ districtName: option.props.children });
    /**
     * get vendor branch for product search
     */
    const VBZone = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchZoneDto({
          zoneDto: new ZoneDto({
            districtDtos: [
              new DistrictDto({
                name: option.props.children,
                cityDto: new CityDto({ id: this.state.cityId }),
              }),
            ],
          }),
        }),
      )
      .searchMode(true)
      .fromCache(true)
      .buildJson();

    const vendorBranchData = await getVendorBranchZoneApi(
      getDtoQueryString(VBZone),
    );

    if (
      vendorBranchData.data &&
      vendorBranchData.data.items &&
      vendorBranchData.data.items.length > 0
    ) {
      setCookie('districtId', value, 8760);
      setCookie('districtName', encodeURIComponent(option.props.children), 8760);
      this.props.actions.vendorBranchZoneListSuccess(vendorBranchData.data);
      let index = 0;
      for (let i = 0; i < vendorBranchData.data.items.length; i += 1) {
        if (vendorBranchData.data.items[i].vendorBranchDto.active) break;
        else index += 1;
      }
      this.setState({
        // districtId: value,
        vendorBranchId:
          vendorBranchData.data.items.length > 0
            ? `${vendorBranchData.data.items[index].vendorBranchDto.id}*${
                vendorBranchData.data.items[index].vendorBranchDto.name
              }`
            : 0,
        vendorBranchName:
          vendorBranchData.data.items.length > 0
            ? vendorBranchData.data.items[index].vendorBranchDto.name
            : '',
      });

      const pathName = window.location.pathname.split('/');
      pathName.splice(2, 1, value.toString());
      pathName.splice(
        3,
        1,
        vendorBranchData.data.items.length > 0
          ? vendorBranchData.data.items[index].vendorBranchDto.id
          : 0,
      );
      const newurl = `${window.location.protocol}//${
        window.location.host
      }${pathName.toString().replace(/,/g, '/')}${window.location.search}`;
      window.history.pushState({ path: newurl }, '', newurl);
      // this.props.applySearch();
      this.handleChangeInput(
        'vendorBranchId',
        vendorBranchData.data.items.length > 0
          ? `${vendorBranchData.data.items[index].vendorBranchDto.id}*${
              vendorBranchData.data.items[index].vendorBranchDto.name
            }`
          : 0,
      );
    } else {
      showNotification(
        'error',
        '',
        'فروشگاهی در این محله موجود نیست',
        10,
        'errorsBox',
      );
    }
  }

  onCheckCategory = checkedKeys => {
    this.setState({ categoryIds: checkedKeys });
  };

  getParamsURL = () => {
    const queryStringUrl = window.location.search.substring(1);
    const params = {};
    let temp;
    let i;
    let l;
    // Split into key/value pairs1
    const queries = queryStringUrl.split('&');
    // Convert the array of strings into an object
    for (i = 0, l = queries.length; i < l; i += 1) {
      temp = queries[i].split('=');
      params[temp[0]] = temp[1];
    }
    return params;
  };

  async applySearch(from = 'web') {
    const {
      treeSelectedCheckBox,
      multiSelect,
      minPrice,
      maxPrice,
      colorPicker,
      priceOption,
    } = this.state;

    const ids = [];
    treeSelectedCheckBox.map(item => ids.push(item.split('*')[0]));

    if (from === 'app') {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
      this.toggleMe();
    }

    this.props.actions.loadingSearchShowRequest();
    const { sort } = this.getParamsURL();

    let queryString = ids.length > 0 ? `categoryIds=${ids}&` : '';
    queryString =
      multiSelect.length > 0
        ? `${queryString}specificationIds=${multiSelect}&`
        : queryString;
    queryString =
      colorPicker.length > 0
        ? `${queryString}colorOptionIds=${colorPicker}&`
        : queryString;

    queryString = `${queryString}priceOption=${priceOption}&`;

    queryString = `${queryString}minPrice=${minPrice}&maxPrice=${maxPrice}`;
    queryString = `${queryString}&sort=${sort}`;

    const newurl = `${window.location.protocol}//${window.location.host}${
      window.location.pathname
    }?${queryString}`;
    window.history.pushState({ path: newurl }, '', newurl);

    this.props.applySearch();
  }

  priceSlider = (value, from = 'web') => {
    this.setState(
      {
        minPrice: value[0],
        maxPrice: value[1],
      },
      () => {
        if (from === 'web') this.applySearch();
      },
    );
  };

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  handelChangeInput = (inputName, value, from = 'web') => {
    this.setState({ [inputName]: value }, () => {
      if (from === 'web') this.applySearch();
    });
  };

  handleSearch = value => {
    this.setState({ districtName: value, districtId: 0 });
    if (value.length > 1) {
      const districtList = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityDto: new CityDto({
              id: this.state.cityId,
            }),
            name: value,
          }),
        )
        .fromCache(true)
        .buildJson();

      this.props.actions.districtOptionListRequest(districtList);
    }
  };

  async handleChangeInput(inputName, value, from = 'web') {
    this.props.params.vendorBranchId = value.split('*')[0];
    const pathName = window.location.pathname.split('/');
    pathName.splice(3, 1, value.split('*')[0].toString());
    const newurl = `${window.location.protocol}//${
      window.location.host
    }${pathName.toString().replace(/,/g, '/')}${window.location.search}`;
    window.history.pushState({ path: newurl }, '', newurl);

    this.setState({
      [inputName]: `${value.split('*')[0]}*${value.split('*')[1]}`,
      vendorBranchName: value.split('*')[1],
    });

    const responseCategory = await getSearchVendorBranchCategoriesApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchDto({
              id: value.split('*')[0],
            }),
          )
          .buildJson(),
      ),
    );
    // this.props.actions.categoryListRequest(JSON.stringify(baseGetDtoTemp));
    if (responseCategory.status === 200)
      this.props.actions.categoryListSuccess(responseCategory.data);

    const listJson = new BaseGetDtoBuilder()
      .dto(
        new SearchColorOptionDto({
          categoryIds: 0,
          vendorBranchId: value.split('*')[0],
        }),
      )
      .fromCache(true)
      .buildJson();

    this.props.actions.searchColorOptionRequest(listJson);
    this.props.actions.searchSpecificationAttributeRequest(listJson);

    const tagData = await getTagVendorBranchApi(
      getDtoQueryString(JSON.stringify({ dto: { id: value.split('*')[0] } })),
    );

    if (tagData.status === 200) {
      this.props.actions.tagListSuccess(tagData.data);
    }

    this.setState(
      {
        treeSelectedCheckBox: [],
        categoryIds: [],
        categoryModalArray: [],
        multiSelect: [],
        colorPicker: [],
      },
      () => {
        if (from === 'web') this.applySearch();
      },
    );
  }

  click = () => {
    this.setState({ visibleCategoryModal: true });
  };

  async toggleMe() {
    document.getElementsByClassName('enableSelect')[0].onclick = this.click;
    let cityName = '';
    const elementCityBox = document.getElementsByClassName('cityBox');
    if (elementCityBox)
      cityName = elementCityBox[0].getElementsByClassName(
        'ant-select-selection-selected-value',
      )[0].title;

    this.setState({
      show: !this.state.show,
      cityName,
    });

    if (!this.state.show) {
      document.body.classList.add(classNameOne);
    } else {
      document.body.className = document.body.className.replace(
        classNameOne,
        '',
      );
    }
  }

  traverseTree(node) {
    if (node.children && node.children.length > 0) {
      let tempItem = {};
      node.children.map(item => {
        tempItem = item;
        tempItem.link = `/`;
        this.categoryAllArray.push(
          <Option key={item.id}>{item.description}</Option>,
        );
        return this.traverseTree(tempItem);
      });
    }
  }

  tagChange = value => {
    const { minPrice, maxPrice, priceOption } = this.state;
    const { sort } = this.getParamsURL();
    this.setState({
      treeSelectedCheckBox: [],
      multiSelect: [],
      colorPicker: [],
    });

    let queryString = value.length > 0 ? `tagIds=${value}&` : '';

    queryString = `${queryString}priceOption=${priceOption}&`;

    queryString = `${queryString}minPrice=${minPrice}&maxPrice=${maxPrice}`;
    queryString = `${queryString}&sort=${sort}`;

    const newurl = `${window.location.protocol}//${window.location.host}${
      window.location.pathname
    }?${queryString}`;
    window.history.pushState({ path: newurl }, '', newurl);

    this.props.applySearch();
  };

  handleCancelCategoryModal = () => {
    this.setState({ visibleCategoryModal: false });
  };

  handleOkCategoryModal = () => {
    const { categoryIds } = this.state;
    this.setState({
      visibleCategoryModal: false,
      treeSelectedCheckBox: categoryIds,
    });
  };

  categoryChange = value => {
    this.setState({
      treeSelectedCheckBox: value,
    });
  };

  handleChangeCity = (inputName, value) => {
    this.setState({ [inputName]: value, districtName: '', dataSource: [] });

    setTimeout(() => {
      const element = document
        .getElementsByClassName('cityBox')[0]
        .getElementsByClassName('ant-select-selection-selected-value')[0];
      this.setState({ cityName: element.title });
    }, 50);
  };

  render() {
    const { isWeb } = this.state;
    const {
      priceOption,
      minPrice,
      maxPrice,
      colorPicker,
      treeSelectedCheckBox,
      multiSelect,
      dataSource,
      districtName,
      vendorBranchId,
      vendorBranchName,
      tagIds,
      visibleCategoryModal,
      categoryIds,
      cityId,
      cityName,
    } = this.state;
    const {
      categoryData,
      searchColorOptionData,
      searchSpecificationAttributeData,
      vendorBranches,
      params,
      tags,
      loadingSearch,
      cities,
    } = this.props;

    const objMultiSelect = ['1', '2', '3', '4'];
    const tagData = [];
    const cityArray = [];
    this.categoryAllArray = [];

    tags.map(item => tagData.push({ key: item.id, name: item.title }));

    if (cities)
      cities.map(item =>
        cityArray.push(
          <Option key={item.id.toString()} value={item.id}>
            {item.name}
          </Option>,
        ),
      );

    const priceOptionModel = [
      {
        value: '2',
        name: IN_PLACE,
        defaultChecked: priceOption === '2',
      },
      {
        value: '1',
        name: IN_SHOPPING,
        defaultChecked: priceOption === '1',
      },
    ];

    treeModel.items = [];
    categoryData.map(item => {
      const treeNodeModel = {
        id: item.id,
        name: item.name,
        link: `/`,
        children: item.children,
      };
      this.categoryAllArray.push(
        <Option key={item.id}>{item.description}</Option>,
      );
      return treeModel.items.push(treeNodeModel);
    });
    categoryData.map(item => this.traverseTree(item));
    treeModel.allowLastLeafSelect = true;

    const searchSpecificationAttributeDataArray = [];
    if (searchSpecificationAttributeData) {
      searchSpecificationAttributeData.map(item =>
        searchSpecificationAttributeDataArray.push(
          <Option key={item.id}>{item.name}</Option>,
        ),
      );
    }

    const vendorBranchArray = [];

    vendorBranches.map(item =>
      vendorBranchArray.push({
        value: `${item.vendorBranchDto.id}*${item.vendorBranchDto.name}`,
        name: item.vendorBranchDto.name,
        defaultChecked: params.vendorBranchId === item.vendorBranchDto.id,
        disabled: !item.vendorBranchDto.active,
      }),
    );
    return (
      <div>
        {isWeb ? (
          <div className={s.webFilter} id="filterSide">
            <h3 className={s.webTitle}>
              <i className="mf-filters" />
              {FILTERS}
            </h3>
            <div>
              <div className={s.filterSide}>
                <div className={s.zoneName}>
                  <CPPanel defaultActiveKey="1" header={`${CITY} و ${ZONE}`}>
                    <div className={s.location}>
                      <CPSelect
                        value={cityId}
                        onChange={value =>
                          this.handleChangeCity('cityId', value)
                        }
                        showSearch
                        className={cs(s.cityLocation, 'cityBox')}
                      >
                        {cityArray}
                      </CPSelect>
                      <AutoComplete
                        // className={s.areaInput}
                        dataSource={dataSource}
                        onSelect={this.onSelectDistrict}
                        onSearch={this.handleSearch}
                        placeholder="محله"
                        value={districtName}
                      />
                    </div>
                  </CPPanel>
                </div>
                <div className={s.vendorName}>
                  <CPPanel header={VENDOR_NAME}>
                    <CPRadio
                      model={vendorBranchArray}
                      size="small"
                      onChange={value =>
                        this.handleChangeInput(
                          'vendorBranchId',
                          value.target.value,
                        )
                      }
                      defaultValue={vendorBranchId}
                    />
                  </CPPanel>
                </div>
                <div className={s.deliveryType}>
                  <CPPanel defaultActiveKey="1" header={DELIVERY_TYPE}>
                    <CPRadio
                      model={priceOptionModel}
                      size="small"
                      onChange={value =>
                        this.handelChangeInput(
                          'priceOption',
                          value.target.value,
                        )
                      }
                      defaultValue={priceOption}
                    />
                  </CPPanel>
                </div>
                <div className={s.pricePeriod}>
                  <CPPanel header={PRICE_PERIOD}>
                    <div className={s.wrapp}>
                      <CPPriceSlider
                        onChange={value => this.priceSlider(value)}
                        defaultValue={[
                          parseInt(minPrice, 0),
                          parseInt(maxPrice, 0),
                        ]}
                        max={this.priceValue.max}
                        min={this.priceValue.min}
                        step={10000}
                      />
                    </div>
                    <div className={s.label}>
                      <span className={s.price} style={{ textAlign: 'right' }}>
                        <span>
                          <span>{formatNum(maxPrice)}</span>
                          <small> تومان</small>
                        </span>
                      </span>
                      <span className={s.price}>
                        <span>
                          <span>{formatNum(minPrice)}</span>
                          <small> تومان</small>
                        </span>
                      </span>
                    </div>
                  </CPPanel>
                </div>

                <div className={s.category}>
                  <CPPanel header={CATEGORY}>
                    <CPTree
                      model={treeModel}
                      checkable
                      checkedKeys={treeSelectedCheckBox}
                      onCheck={value => this.onChangeTreeSelectCheckBox(value)}
                    />
                  </CPPanel>
                </div>
                <div className={s.chooseFlower}>
                  <CPPanel defaultActiveKey="1" header={FLOWER_TYPE}>
                    <CPMultiSelect
                      placeholder={CHOOSE_FLOWER}
                      defaultValue={objMultiSelect}
                      onChange={value => this.onChangeMultiSelect(value)}
                      value={multiSelect}
                    >
                      {searchSpecificationAttributeDataArray}
                    </CPMultiSelect>
                  </CPPanel>
                </div>
                <div className={s.color}>
                  <CPPanel defaultActiveKey="1" header={CHOOSE_COLOR}>
                    <CPColorPicker
                      multiSelect
                      data={searchColorOptionData}
                      defaultValues={colorPicker}
                      onChange={value => this.onChangeColorPicker(value)}
                    />
                  </CPPanel>
                </div>
                {tagData.length > 0 && (
                  <div>
                    <h3 className={s.tagTitle}>
                      <i className="mf-tag" />
                      {TAGS}
                    </h3>

                    <div className={s.tags}>
                      <div className={s.tag}>
                        <CPTag
                          tagSelected={tagIds}
                          checkable
                          data={tagData}
                          onChange={this.tagChange}
                        />
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        ) : (
          <div className={s.mobileFilter}>
            <div
              className={
                this.state.show
                  ? 'hideMobileFilter showMobileFilter'
                  : 'hideMobileFilter'
              }
            >
              <div className={s.closeBtn}>
                <CPButton
                  onClick={this.toggleMe}
                  type="circle"
                  className={s.filterButton}
                >
                  <i className="mf-close-o" />
                </CPButton>
              </div>
              <div className={s.applySearch}>
                <CPButton
                  className="pinkBtn"
                  type="primary"
                  onClick={() => this.applySearch('app')}
                >
                  {APPLY_FILTER}
                  <i className="mf-filter" />
                </CPButton>
                {/* <CPButton className={s.deleteBtn} type="primary"> */}
                {/* {DELETE} */}
                {/* <i className="mf-trash" /> */}
                {/* </CPButton> */}
              </div>
              <h3 className={s.webTitle}>
                <i className="mf-filters">
                  <CPButton className={s.filterBtn}>{FILTERS}</CPButton>
                </i>
              </h3>
              <Collapse
                accordion
                className="scroll-collapse"
                defaultActiveKey={['2']}
              >
                <Panel
                  header={
                    <span>
                      {DELIVERY_TYPE}{' '}
                      {priceOption === '1' ? IN_SHOPPING : IN_PLACE}
                    </span>
                  }
                  key="1"
                >
                  <CPRadio
                    model={priceOptionModel}
                    size="small"
                    onChange={value =>
                      this.handelChangeInput(
                        'priceOption',
                        value.target.value,
                        'app',
                      )
                    }
                    defaultValue={priceOption}
                  />
                </Panel>
                <Panel
                  header={
                    <span>
                      {CITY} و {ZONE}{' '}
                      <span>
                         ( {cityName} - {districtName} )
                      </span>
                    </span>
                  }
                  key="2"
                >
                  <div className={s.location}>
                    <CPSelect
                      value={cityId}
                      onChange={value => this.handleChangeCity('cityId', value)}
                      showSearch
                      className="cityBox"
                    >
                      {cityArray}
                    </CPSelect>
                    <AutoComplete
                      // className={s.areaInput}
                      dataSource={dataSource}
                      onSelect={this.onSelectDistrict}
                      onSearch={this.handleSearch}
                      placeholder="محله"
                      value={districtName}
                    />
                  </div>
                </Panel>
                <Panel
                  header={
                    <span>
                      {VENDOR_NAME}
                      <span> {vendorBranchName}</span>
                    </span>
                  }
                  key="3"
                >
                  <CPRadio
                    model={vendorBranchArray}
                    size="small"
                    onChange={value =>
                      this.handleChangeInput(
                        'vendorBranchId',
                        value.target.value,
                        'app',
                      )
                    }
                    defaultValue={vendorBranchId}
                  />
                </Panel>
                <Panel
                  header={
                    <span>
                      {PRICE_PERIOD}
                      <span> {`${maxPrice} - ${minPrice}`}</span>
                    </span>
                  }
                  key="4"
                >
                  <div className={s.pricePeriod}>
                    <div className={s.wrapp}>
                      <CPPriceSlider
                        onChange={value => this.priceSlider(value, 'app')}
                        defaultValue={[
                          parseInt(minPrice, 0),
                          parseInt(maxPrice, 0),
                        ]}
                        max={this.priceValue.max}
                        min={this.priceValue.min}
                        step={10000}
                      />
                    </div>
                    <div className={s.label}>
                      {/* <b className={s.from}>{FROM}</b> */}
                      {/* <b className={s.to}>{TO}</b> */}
                      <span className={s.price} style={{ textAlign: 'right' }}>
                        <span>
                          <span>{formatNum(maxPrice)}</span>
                          <small> تومان</small>
                        </span>
                      </span>
                      <span className={s.price}>
                        <span>
                          <span>{formatNum(minPrice)}</span>
                          <small> تومان</small>
                        </span>
                      </span>
                    </div>
                  </div>
                </Panel>

                <Panel
                  header={
                    <CPBadge count={treeSelectedCheckBox.length}>
                      {CATEGORY}
                    </CPBadge>
                  }
                  key="5"
                  forceRender
                >
                  {/* <CPTreeSelect
                    placeholder={CHOOSE_CATEGORY}
                    model={categoryData}
                    checkBox
                    onChange={value =>
                      this.onChangeTreeSelectCheckBox(value, 'app')
                    }
                    values={treeSelectedCheckBox}
                  /> */}
                  <CPMultiSelect
                    placeholder="انتخاب گروه محصول"
                    onChange={this.categoryChange}
                    value={treeSelectedCheckBox}
                    disabled
                    showSearch={false}
                    // onMouseEnter={() =>
                    //   this.setState({ visibleCategoryModal: true })
                    // }
                    className="enableSelect"
                  >
                    {this.categoryAllArray}
                  </CPMultiSelect>
                </Panel>
                <Panel
                  header={
                    <CPBadge count={multiSelect.length}>{FLOWER_TYPE}</CPBadge>
                  }
                  key="6"
                >
                  <CPMultiSelect
                    placeholder={CHOOSE_FLOWER}
                    defaultValue={objMultiSelect}
                    onChange={value => this.onChangeMultiSelect(value, 'app')}
                    value={multiSelect}
                  >
                    {searchSpecificationAttributeDataArray}
                  </CPMultiSelect>
                </Panel>
                <Panel
                  header={
                    <CPBadge count={colorPicker.length}>{CHOOSE_COLOR}</CPBadge>
                  }
                  key="7"
                >
                  <CPColorPicker
                    multiSelect
                    data={searchColorOptionData}
                    onChange={value => this.onChangeColorPicker(value, 'app')}
                    defaultValues={colorPicker}
                  />
                </Panel>
              </Collapse>

              <CPModal
                visible={visibleCategoryModal}
                handleCancel={this.handleCancelCategoryModal}
                handleOk={this.handleOkCategoryModal}
                className="max_modal mobile"
                title="انتخاب دسته بندی"
                textSave={CONFIRMATION}
                footerClassName={s.footerModal}
              >
                <CPTree
                  model={treeModel}
                  onCheck={this.onCheckCategory}
                  checkable
                  checkedKeys={categoryIds}
                  defaultExpandAll
                />
              </CPModal>
            </div>
          </div>
        )}

        <div
          className={cs(
            s.mobileTitle,
            loadingSearch ? 'hideFilterIcon' : 'showFilterIcon',
          )}
        >
          <CPButton
            onClick={this.toggleMe}
            type="circle"
            className={s.filterButton}
          >
            <span>{vendorBranchName}</span>
            <i className="mf-filter" />
          </CPButton>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  vendorBranches: state.vendorVendorBranchZone.vendorBranchZoneData.items,
  categoryData: state.catalogCategory.categoryListData.items,
  searchColorOptionData: state.searchColorOption.searchColorOptionData.items,
  districts: state.commonDistrict.districtOptionData.items,
  searchSpecificationAttributeData:
    state.searchSpecificationAttribute.searchSpecificationAttributeData.items,
  tags: state.catalogTag.tagListData.items,
  loadingSearch: state.loadingSearch.loadingSearch,

  cities: state.commonCity.cityListData
    ? state.commonCity.cityListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      searchColorOptionRequest,
      searchSpecificationAttributeRequest,
      districtOptionListRequest,
      vendorBranchZoneListSuccess,
      districtOptionListSuccess,
      productSearchRequest,
      isEmptyProductCardArray,
      loadingSearchShowRequest,
      productSearchSuccess,
      loadingSearchHideRequest,
      categoryListRequest,
      tagListSuccess,
      categoryListSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(FilterSide));
