import React from 'react';
import { connect } from 'react-redux';
import { showLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPBurgerMenu.css';
import CPButton from '../CPButton';
import CPAvatar from '../CPAvatar';
import { getCookie } from '../../../utils';
import {
  DeleteUserBasketHistoriesApi,
  GetUserBasketHistoriesApi,
} from '../../../services/samApi';
import { BasketDto } from '../../../dtos/catalogDtos';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  basketListRequest,
  basketListSuccess,
} from '../../../redux/catalog/action/basket';
import {
  TOTAL_PRICE,
  PAYMENT,
  TOMAN,
  CART,
} from '../../../Resources/Localization';
import history from '../../../history';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';

class CPBurgerMenu extends React.Component {
  static propTypes = {
    basketProducts: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    basketProducts: [],
  };

  constructor(props) {
    super(props);
    this.state = { buttonDisable: false, loading: false };
    this.deleteOnBasket = this.deleteOnBasket.bind(this);
  }

  componentDidMount() {
    setTimeout(() => this.paymentInSafari(), 1000);
  }

  componentDidUpdate() {
    setTimeout(() => this.paymentInSafari(), 1000);
  }

  paymentInSafari = () => {
    const safari = navigator.userAgent.toLowerCase();
    if (safari.indexOf('safari') !== -1) {
      if (safari.indexOf('chrome') > -1) {
        // Chrome
      } else {
        const element = document.getElementById('ul-payment');
        if (element) element.classList.add('paymentToUp');
      }
    }
  }

  async deleteOnBasket(id) {
    this.setState({ loading: true });
    const response = await DeleteUserBasketHistoriesApi(id);

    if (response.status === 200) {
      showNotification(
        'success',
        '',
        'محصول مورد نظر با موفقیت از سبد خرید حذف شد.',
        5,
      );

      const token = getCookie('siteToken');
      const guestKey = getCookie('guestKey');

      const responseGet = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(new BasketDto({ guestKey: !token ? guestKey : undefined }))
            .buildJson(),
        ),
        token,
      );
      if (responseGet.status === 200) {
        this.props.actions.basketListSuccess(responseGet.data);
      }
      this.setState({ loading: false });
    } else {
      showNotification(
        'error',
        '',
        'متاسفانه در حذف محصول از سبد خرید خطایی رخ داده است.',
        5,
      );
      this.setState({ loading: false });
    }
  }

  goToCheckout = () => {
    this.props.actions.showLoading();
    this.setState({ buttonDisable: true });
    history.push('/checkout');
  };

  goToCard = () => {
    this.props.actions.showLoading();
    this.setState({ buttonDisable: true });
    window.location.href = '/Card';
  };

  render() {
    const { basketProducts } = this.props;
    const { buttonDisable, loading } = this.state;

    return (
      <div className={s.burgerMenu}>
        {basketProducts &&
        basketProducts.length > 0 &&
        basketProducts[0].userBasketHistoryDetailDtos &&
        basketProducts[0].userBasketHistoryDetailDtos.length > 0 ? (
          <div>
            <ul className={s.basketList}>
              <Spin spinning={loading} className={s.loading}>
                {basketProducts[0].userBasketHistoryDetailDtos.map(item => (
                  <li key={item.id}>
                    <CPAvatar
                      src={
                        item.vendorBranchProductDto.productDto.imageDtos
                          ? item.vendorBranchProductDto.productDto.imageDtos[0]
                              .url
                          : ''
                      }
                    />
                    <div className={s.proDetail}>
                      <p className={s.proName}>
                        {item.vendorBranchProductDto.productDto.name}
                      </p>
                      <p>
                        <b>
                          {item.priceOption === 'Pickup'
                            ? item.vendorBranchProductDto.productDto
                                .vendorBranchProductPriceDtos[0]
                                .stringPickupPriceAfterDiscount
                            : item.vendorBranchProductDto.productDto
                                .vendorBranchProductPriceDtos[0]
                                .stringDeliveryPriceAfterDiscount}
                        </b>
                        <i className="mf-close" />
                        <b>{item.count}</b>
                      </p>
                    </div>
                    <CPButton
                      className={s.deleteProduct}
                      onClick={() => this.deleteOnBasket(item.id)}
                      type="circle"
                    >
                      <i className="mf-close" />
                    </CPButton>
                  </li>
                ))}
              </Spin>
            </ul>

            <ul className={s.basketListFooter} id="ul-payment">
              <li>
                <b>
                  {TOTAL_PRICE} :{' '}
                  <b>
                    {basketProducts[0]
                      ? basketProducts[0].stringPayableAmount
                      : 0}{' '}
                    {TOMAN}
                  </b>
                </b>
              </li>
              <li>
                <CPButton disabled={buttonDisable} onClick={this.goToCard}>
                  {CART}
                </CPButton>
                <CPButton
                  className={s.cart}
                  disabled={buttonDisable}
                  onClick={this.goToCheckout}
                >
                  {PAYMENT}
                </CPButton>
              </li>
            </ul>
          </div>
        ) : (
          <div>
            {/* <p className={s.empty}>سبد خرید شما خالیست.</p>
            <ul className={s.basketListFooter} id="ul-payment" /> */}
            <div className={s.emptyBasket}>
              <i className="mf-addToCard">
                <i className="mf-close-o" />
              </i>
              <div>سبد خرید شما خالی است !</div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  basketProducts: state.basket.basketListData
    ? state.basket.basketListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      basketListSuccess,
      basketListRequest,
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CPBurgerMenu));
