/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPInputMask.css';

class CPInputMask extends React.Component {
  static propTypes = {
    mask: PropTypes.string.isRequired,
    direction: PropTypes.string,
    value: PropTypes.string,
  };

  static defaultProps = {
    direction: 'ltr',
    value: '',
  };

  changeValue = value => {
    const data = value.target.value.replace(/ /g, '');
    this.props.onChange(data);
  };

  render() {
    const { mask, direction, value } = this.props;
    return (
      <InputMask
        onChange={this.changeValue}
        className={s[direction]}
        mask={mask}
        value={value}
      />
    );
  }
}

export default withStyles(s)(CPInputMask);
