import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { Card, Button } from 'antd';
import s from './EditCard.css';
import CPLoading from '../CPLoading/CPLoading';
import CPButton from '../CPButton/CPButton';
import Link from '../../Link/Link';
import { NO, YES } from '../../../Resources/Localization';
import CPPopConfirm from '../CPPopConfirm';
import CPAlert from '../CPAlert';
import { AlertHideRequest } from '../../../redux/shared/action/alert';

const { Group } = Button;

class EditCard extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    title: PropTypes.string.isRequired,
    isEdit: PropTypes.bool,
    loading: PropTypes.bool,
    errorAlert: PropTypes.bool,
    backUrl: PropTypes.string,
    onDelete: PropTypes.func,
    deleteTitle: PropTypes.string,
    errorMessage: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    title: 'عنوان کارت',
    isEdit: false,
    loading: false,
    errorAlert: false,
    backUrl: '',
    deleteTitle: '',
    errorMessage: '',
    onDelete: () => {},
  };

  onDelete = () => {
    this.props.onDelete();
  };

  onCloseErrorMessage = () => {
    this.props.actions.AlertHideRequest();
  };

  renderActions = () => (
    <div>
      <div>
        <a href="/#">rferfer</a>
      </div>
      <div>
        <a href="/#">rferfer</a>
      </div>
      <div>
        <a href="/#">rferfer</a>
      </div>
      <div>
        <a href="/#">rferfer</a>
      </div>
    </div>
  );

  render() {
    const {
      title,
      backUrl,
      isEdit,
      deleteTitle,
      loading,
      errorAlert,
      errorMessage,
    } = this.props;
    return (
      <CPLoading spinning={loading}>
        <div className={s.editCard}>
          <Card title={title}>
            <div className={s.topbtns}>
              <Group>
                {/* <CPPopover elements={this.renderActions()}>
                  <CPButton shape="circle">
                    <i className="cp-dot" />
                  </CPButton>
                </CPPopover> */}
                <CPButton shape="circle">
                  <Link to={backUrl}>
                    <i className="cp-arrow-left" />
                  </Link>
                </CPButton>
              </Group>
            </div>
            {errorAlert && (
              <CPAlert
                showIcon
                closable
                onClose={this.onCloseErrorMessage}
                type="error"
                message="خطا"
                description={errorMessage}
              />
            )}
            {isEdit && (
              <div className={s.bottombtns}>
                <Group>
                  <CPPopConfirm
                    title={deleteTitle}
                    okText={YES}
                    cancelText={NO}
                    okType="primary"
                    onConfirm={() => this.onDelete()}
                  >
                    <CPButton shape="circle">
                      <i className="cp-trash" />
                    </CPButton>
                  </CPPopConfirm>
                </Group>
              </div>
            )}
            {this.props.children}
          </Card>
        </div>
      </CPLoading>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.loading.editFormLoading,
  errorAlert: state.alert.editFormAlert,
  errorMessage: state.alert.editFormData,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      AlertHideRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(EditCard));
