import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, LocaleProvider } from 'antd';
import faIR from 'antd/lib/locale-provider/fa_IR';
import s from './CPList.css';
import CPTable from '../CPTable/CPTable';
import SearchBox from '../SearchBox';
import CPButton from '../CPButton';

class CPList extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    columns: PropTypes.arrayOf(PropTypes.object).isRequired,
    flatPermission: PropTypes.objectOf(PropTypes.any).isRequired,
    count: PropTypes.number,
    loading: PropTypes.bool,
    children: PropTypes.node,
    onAddClick: PropTypes.func,
    onChange: PropTypes.func,
    rowClassName: PropTypes.func,
    width: PropTypes.node,
    searchBoxName: PropTypes.string,
    hideSearchBox: PropTypes.bool,
    hideAdd: PropTypes.bool,
    showTopAdd: PropTypes.bool,
    rowSelection: PropTypes.objectOf(PropTypes.any),
    withCheckBox: PropTypes.bool,
    pagination: PropTypes.bool,
  };
  static defaultProps = {
    data: [],
    columns: [],
    onAddClick: () => {},
    onChange: () => {},
    rowClassName: () => {},
    count: 0,
    loading: false,
    children: '',
    width: '',
    searchBoxName: '',
    hideSearchBox: false,
    hideAdd: false,
    showTopAdd: false,
    rowSelection: {},
    withCheckBox: false,
    pagination: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      hiddenSearcBox: this.props.hideSearchBox,
    };
  }

  componentWillMount() {
    const { flatPermission } = this.props;
    const { searchBoxName } = this.props;
    Object.keys(flatPermission).map(item => {
      if (flatPermission[item].resourceValue === searchBoxName) {
        this.setState({ hiddenSearcBox: true });
      }
    });
  }

  render() {
    const {
      data,
      count,
      columns,
      loading = false,
      onAddClick,
      width,
      hideAdd,
      showTopAdd,
      rowSelection,
      withCheckBox,
      pagination,
      onChange,
      rowClassName,
    } = this.props;
    const { hiddenSearcBox } = this.state;
    return (
      <LocaleProvider locale={faIR}>
        <Card className={s.cardTable}>
          {!hiddenSearcBox && <SearchBox>{this.props.children}</SearchBox>}
          {showTopAdd && (
            <CPButton
              onClick={onAddClick}
              type="primary"
              className={s.createRecordLocationTop}
              shape="circle"
            >
              <i className="cp-add" />
            </CPButton>
          )}
          {withCheckBox && (
            <CPTable
              width={width}
              data={data}
              count={count}
              columns={columns}
              loading={loading}
              rowSelection={rowSelection}
              pagination={pagination}
              onChange={onChange}
              rowClassName={rowClassName}
            />
          )}
          {!withCheckBox && (
            <CPTable
              width={width}
              data={data}
              count={count}
              columns={columns}
              loading={loading}
              pagination={pagination}
              onChange={onChange}
              rowClassName={rowClassName}
            />
          )}
          {!hideAdd && (
            <CPButton
              onClick={onAddClick}
              type="primary"
              className={s.createRecord}
              shape="circle"
            >
              <i className="cp-add" />
            </CPButton>
          )}
        </Card>
      </LocaleProvider>
    );
  }
}

const mapStateToProps = state => ({
  flatPermission:
    state.identityPermission.permissionListData &&
    state.identityPermission.permissionListData.items.length > 0
      ? state.identityPermission.permissionListData.items[0].flatermission
      : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(CPList),
);
