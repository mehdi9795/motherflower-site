import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Select, Switch } from 'antd';
import s from './CPUserListSearchBox.css';
import CPButton from '../CPButton';
import CPInput from '../CPInput';
import * as lg from '../../../Resources/Localization';
import { FetchData } from '../../../utils/index';
import * as actions from '../../actions/userList'

class CPUserListSearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: [],
      active: true,
    };
    this.submitSearch = this.submitSearch.bind(this);
  }

  handleChange = value => {
    this.setState({
      selectedItem: value,
    });
  };

  submitSearch()
  {
    this.props.actions.vendorListLoading();
    const container = `{dto:{active:${
      this.state.vendorStatus
      }}pageIndex:0,pageSize:0,disabledCount:false,orderByFelds:[Id]}`;
    const data = await FetchData(
    `http://192.168.110.12:4060/Vendor/Vendors/Vendors?container=${container}`,
    fetch,
  );
    this.props.actions.vendorListLoaded(data);
  }

  render() {

  }
}

export default withStyles(s)(CPUserListSearchBox);
