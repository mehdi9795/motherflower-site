import React from 'react';
import {showLoading} from "react-redux-loading-bar";
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Breadcrumb } from 'antd';
import s from './CPBreadcrumb.css';
import Link from '../../Link/Link';
import history from '../../../history';

class CPBreadcrumb extends React.Component {
  static propTypes = {
    links: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    links: [
      { key: 1, name: 'صفحه نخست', url: '/' },
      { key: 2, name: 'محصولات', url: '/' },
      { key: 3, name: 'جزئیات محصول', url: '/' },
    ],
  };

  goToUrl = (e, url) => {
    e.preventDefault();
    this.props.actions.showLoading();
    history.push(url);
  };

  render() {
    const { links } = this.props;

    return (
      <Breadcrumb>
        {links.map(item => (
          <Breadcrumb.Item key={item.key}>
            {item.url ? (
              <Link to={item.url} onClick={e => this.goToUrl(e, item.url)}>
                {item.name}
              </Link>
            ) : (
              item.name
            )}
          </Breadcrumb.Item>
        ))}
      </Breadcrumb>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(CPBreadcrumb),
);
