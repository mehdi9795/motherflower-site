import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Input } from 'antd';
import s from './CPTextArea.css';

const { TextArea } = Input;

class CPTextArea extends React.Component {
  static propTypes = {
    autoSize: PropTypes.bool,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    // defaultValue: PropTypes.string,
    value: PropTypes.string,
    onPressEnter: PropTypes.func,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    autoSize: false,
    disabled: false,
    // defaultValue: '',
    value: '',
    className: '',
    placeholder: '',
    onPressEnter: () => {},
    onChange: () => {},
  };

  render() {
    const {
      autoSize,
      value,
      // defaultValue,
      onPressEnter,
      placeholder,
      onChange,
      disabled,
      className,
    } = this.props;
    return (
      <TextArea
        autosize={autoSize}
        // defaultValue={defaultValue}
        value={value}
        onPressEnter={onPressEnter}
        placeholder={placeholder}
        onChange={onChange}
        disabled={disabled}
        className={className}
      />
    );
  }
}

export default withStyles(s)(CPTextArea);
