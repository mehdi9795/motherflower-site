import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import IntlTelInput from 'react-intl-tel-input';
import s from './CPIntlPhoneInput.css';
import utilsScript from '../../../../public/js/libphonenumber';

class CPIntlPhoneInput extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    className: PropTypes.string,
    preferredCountries: PropTypes.arrayOf(PropTypes.string),
  };

  static defaultProps = {
    className: '',
    value: null,
    preferredCountries: ['ir', 'us', 'ca', 'au', 'gb'],
    onChange: () => {},
  };

  handler = (status, value, countryData, number, id) => {
    this.props.onChange(status, value, countryData, number, id);
  };

  render() {
    const { preferredCountries, className, value } = this.props;
    return (
      <IntlTelInput
        className={className}
        onPhoneNumberChange={this.handler}
        preferredCountries={preferredCountries}
        utilsScript={utilsScript}
        value={value}
      />
    );
  }
}

export default withStyles(s)(CPIntlPhoneInput);
