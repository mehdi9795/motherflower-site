import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Checkbox } from 'antd';
import s from './CPCheckbox.css';

class CPCheckbox extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    defaultChecked: PropTypes.bool,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    children: '',
    defaultChecked: false,
    checked: false,
    disabled: false,
    onChange: () => {},
  };

  render() {
    const { onChange, defaultChecked, disabled, checked } = this.props;
    return (
      <Checkbox
        checked={checked}
        defaultChecked={defaultChecked}
        disabled={disabled}
        onChange={onChange}
      >
        {this.props.children}
      </Checkbox>
    );
  }
}

export default withStyles(s)(CPCheckbox);
