import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './CPWizard.css';
import CPButton from '../CPButton';

class CPWizard extends React.Component {
  static propTypes = {
    selectedStep: PropTypes.number,
    steps: PropTypes.arrayOf(PropTypes.object),
    availableSteps: PropTypes.arrayOf(PropTypes.number),
    disableSteps: PropTypes.arrayOf(PropTypes.number),
    passedSteps: PropTypes.arrayOf(PropTypes.number),
    showButtonNavigation: PropTypes.bool,
  };

  static defaultProps = {
    steps: [],
    selectedStep: 0,
    availableSteps: [],
    disableSteps: [],
    passedSteps: [],
    showButtonNavigation: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedStep: this.initSelectedStep(),
      availableNext: this.nextPrevAvailability('next', this.props.selectedStep),
      availablePrev: this.nextPrevAvailability('prev', this.props.selectedStep),
      isValid: true,
      passedSteps: props.passedSteps,
    };
  }
  setPassedStep = steps => {
    this.setState({passedSteps: steps});
  };

  hidePrev = () => {
    this.setState({
      availablePrev: false,
    });
  };
  hideNext = () => {
    this.setState({
      availableNext: false,
    });
  };


  validationTrue = () => {
    this.setState({
      isValid: true,
    });
  };

  validationFalse = () => {
    this.setState({
      isValid: false,
    });
  };

  initSelectedStep = () => {
    const { steps, selectedStep } = this.props;
    const step = steps.find(element => element.key === selectedStep);
    return step;
  };

  nextPrevAvailability = (status, selectedStep) => {
    const { availableSteps, disableSteps } = this.props;
    const sortAvailableSteps = availableSteps.sort();

    disableSteps.map(item => {
      const disableIndex = sortAvailableSteps.indexOf({ item });
      if (disableIndex > -1) {
        sortAvailableSteps.splice(disableIndex, 1);
      }
      return null;
    });

    const index = sortAvailableSteps.indexOf(selectedStep);
    if (status === 'next') {
      return index !== sortAvailableSteps.length - 1;
    } else if (status === 'prev') {
      return index !== 0;
    }
    return null;
  };

  checkStepAvailability = key => {
    const { availableSteps } = this.props;
    if (availableSteps.length === 0) return true;
    const step = availableSteps.find(element => element === key);

    return step !== undefined;
  };

  checkStepDisableStatus = key => {
    const { disableSteps } = this.props;
    if (disableSteps.length === 0) return false;
    const step = disableSteps.find(element => element === key);
    return step !== undefined;
  };

  stepNavigationHandler = (e, key) => {
    e.preventDefault();
    const { steps } = this.props;

    if (!this.state.isValid) {
      const currentStepIndex = steps.indexOf(this.state.selectedStep);
      const selectedStep = steps.find(element => element.key === key);
      const selectedStepIndex = steps.indexOf(selectedStep);
      if (selectedStepIndex > currentStepIndex) return false;
    }

    if (this.checkStepDisableStatus(key)) return false;

    this.stepNavigation(key);
    return null;
  };

  stepCssHandler = key => {
    const { passedSteps } = this.state;
    if (this.state.selectedStep.key === key) {
      return 'stepsKey selectedStep';
    }

    const passedIndex = passedSteps.indexOf(key);
    if (passedIndex > -1) return 'stepsKey passedStep';
    return 'stepsKey';
  };

  stepNavigation = key => {
    const { steps } = this.props;
    const step = steps.find(element => element.key === key);
    this.setState({
      selectedStep: step,
      availableNext: this.nextPrevAvailability('next', key),
      availablePrev: this.nextPrevAvailability('prev', key),
    });
  };

  nextPrevHandler = (e, status) => {
    e.preventDefault();
    this.nextPrev(status);
  };

  nextPrev = (status, disableCurrentStep = false, newStepName = '') => {
    const { availableSteps, disableSteps, steps } = this.props;
    const { passedSteps } = this.state;
    const sortAvailableStep = availableSteps.sort();
    const index = sortAvailableStep.indexOf(this.state.selectedStep.key);
    const selectedKey = this.state.selectedStep.key;
    const passedIndex = passedSteps.indexOf(this.state.selectedStep.key);

    if (newStepName !== '') {
      const currentStepIndex = steps.indexOf(this.state.selectedStep);
      steps[currentStepIndex].title = newStepName;
    }
    if (passedIndex === -1) {
      passedSteps.push(selectedKey);
    }

    if (disableCurrentStep) {
      disableSteps.push(selectedKey);
    }

    if (status === 'next') {
      this.stepNavigation(sortAvailableStep[index + 1]);
    } else if (status === 'prev') {
      this.stepNavigation(sortAvailableStep[index - 1]);
    }
  };

  render() {
    const { steps, showButtonNavigation } = this.props;
    return (
      <div>
        <div className={s.wizardWrapper}>
          <ul className={s.stepContainer}>
            {steps.map(item => (
              <li key={item.key} className={s.list}>
                {/* circle box */}
                <div className={s.steps}>
                  <span className={this.stepCssHandler(item.key)}>
                    <i className="mf-checked" />
                    {item.key}
                  </span>
                </div>
                {this.checkStepAvailability(item.key) && (
                  <div
                    className={s.title}
                    disabled={this.checkStepDisableStatus(item.key)}
                    onClick={e => this.stepNavigationHandler(e, item.key)}
                  >
                    {item.title}
                  </div>
                )}
              </li>
            ))}
          </ul>
          <div className={this.state.selectedStep.className}>
            <div className={s.wizardContent}>
              {React.cloneElement(this.state.selectedStep.components, {
                validationTrue: this.validationTrue,
                validationFalse: this.validationFalse,
                isValid: this.nextPrev,
                hideNext: this.hideNext,
                hidePrev: this.hidePrev,
                setPassedStep: this.setPassedStep,
              })}
            </div>
          </div>
          {showButtonNavigation && (
            <div className={s.nextPrevBtns}>
              {this.state.availableNext && (
                <CPButton
                  disabled={!this.state.isValid}
                  onClick={e => this.nextPrevHandler(e, 'next')}
                >
                  بعدی
                </CPButton>
              )}

              {this.state.availablePrev && (
                <CPButton onClick={e => this.nextPrevHandler(e, 'prev')}>
                  قبلی
                </CPButton>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(s)(CPWizard);
