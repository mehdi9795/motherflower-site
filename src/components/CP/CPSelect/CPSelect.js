import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPSelect.css';

class CPSelect extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    onChange: PropTypes.func,
    showSearch: PropTypes.bool,
    value: PropTypes.node,
    defaultValue: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    className: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    placeholder: '',
    onChange: () => {},
    showSearch: false,
    value: null,
    defaultValue: null,
    disabled: false,
    className: '',
  };

  render() {
    const {
      onChange,
      placeholder,
      children,
      showSearch,
      value,
      defaultValue,
      disabled,
      className,
    } = this.props;
    return value !== null ? (
      <Select
        showSearch={showSearch}
        placeholder={placeholder}
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={onChange}
        disabled={disabled}
        value={value}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        className={className}
      >
        {children}
      </Select>
    ) : (
      <Select
        placeholder={placeholder}
        showSearch={showSearch}
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={onChange}
        disabled={disabled}
        defaultValue={defaultValue}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        className={className}
      >
        {children}
      </Select>
    );
  }
}

export default withStyles(s)(CPSelect);
