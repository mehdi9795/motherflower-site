import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Upload } from 'antd';
import s from './CPAntdUpload.css';
import CPButton from '../CPButton';

class CPAntdUpload extends React.Component {
  static propTypes = {
    accept: PropTypes.string,
    action: PropTypes.string,
    name: PropTypes.string,
    onChange: PropTypes.func,
    beforeUpload: PropTypes.func,
    onRemove: PropTypes.bool,
  };

  static defaultProps = {
    accept: '',
    action: '',
    name: '',
    onChange: () => {},
    beforeUpload: () => {},
    onRemove: () => {},
  };

  render() {
    const {
      accept,
      action,
      name,
      onChange,
      onRemove,
      beforeUpload,
    } = this.props;
    return (
      <Upload
        accept={accept}
        action={action}
        name={name}
        onChange={onChange}
        onRemove={onRemove}
        beforeUpload={beforeUpload}
      >
        <CPButton icon="upload">آپلود تصویر</CPButton>
      </Upload>
    );
  }
}

export default withStyles(s)(CPAntdUpload);
