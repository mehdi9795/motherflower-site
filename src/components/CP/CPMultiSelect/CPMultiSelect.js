import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPMultiSelect.css';

class CPMultiSelect extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    onChange: PropTypes.func,
    onMouseEnter: PropTypes.func,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    showSearch: PropTypes.bool,
    value: PropTypes.arrayOf(PropTypes.string),
    className: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    className: '',
    onMouseEnter: () => {},
    onChange: () => {},
    placeholder: '',
    value: [],
    disabled: false,
    showSearch: true,
  };

  render() {
    const {
      onChange,
      children,
      placeholder,
      value,
      disabled,
      onMouseEnter,
      className,
      showSearch,
    } = this.props;
    return (
      <Select
        mode="tags"
        style={{ width: '100%' }}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        disabled={disabled}
        onMouseEnter={onMouseEnter}
        className={className}
        showSearch={showSearch}
      >
        {children}
      </Select>
    );
  }
}

export default withStyles(s)(CPMultiSelect);
