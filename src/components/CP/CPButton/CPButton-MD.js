import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPButton.css';

class CPButton extends React.Component {
  static propTypes = {
    labelPosition: PropTypes.string,
    label: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    icon: PropTypes.node,
  };

  static defaultProps = {
    label: null,
    labelPosition: 'before',
    disabled: false,
    onClick: () => {},
    icon: null,
  };

  render() {
    const { label, disabled, onClick, icon, labelPosition } = this.props;
    return (
      <Button
        label={label}
        primary
        className={s.button}
        onClick={onClick}
        icon={icon}
        labelPosition={labelPosition}
        disabled={disabled}
      />
    );
  }
}

export default withStyles(s)(CPButton);
