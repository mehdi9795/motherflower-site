import React from 'react';
import PropTypes from 'prop-types';
import { Slider } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPPriceSlider.css';

class CPPriceSlider extends React.Component {
  static propTypes = {
    defaultValue: PropTypes.arrayOf(PropTypes.number),
    min: PropTypes.number,
    max: PropTypes.number,
    range: PropTypes.bool,
    onChange: PropTypes.func,
    step: PropTypes.number,
  };

  static defaultProps = {
    defaultValue: {},
    min: null,
    max: null,
    range: true,
    step: 1,
    onChange: () => {},
  };

  render() {
    const { defaultValue, min, max, range, onChange, step } = this.props;
    return (
      <Slider
        defaultValue={defaultValue}
        min={min}
        max={max}
        range={range}
        onAfterChange={onChange}
        step={step}
      />
    );
  }
}

export default withStyles(s)(CPPriceSlider);
