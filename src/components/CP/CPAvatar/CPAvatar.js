import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Avatar } from 'antd';
import s from './CPAvatar.css';

class CPAvatar extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    icon: PropTypes.string,
    shape: PropTypes.string,
    size: PropTypes.number,
    src: PropTypes.string,
    style: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    children: '',
    icon: '',
    shape: 'circle',
    size: 64,
    src: '',
    style: null,
  };

  render() {
    const { icon, shape, size, src, style } = this.props;
    return (
      <Avatar icon={icon} shape={shape} size={size} src={src} style={style}>
        {this.props.children}
      </Avatar>
    );
  }
}

export default withStyles(s)(CPAvatar);
