import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Radio } from 'antd';
import s from './CPRadio.css';

const { Group } = Radio;

class CPRadio extends React.Component {
  static propTypes = {
    model: PropTypes.arrayOf(PropTypes.object),
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    defaultValue: PropTypes.string,
  };

  static defaultProps = {
    disabled: false,
    onChange: () => {},
    defaultValue: 0,
    model: [],
  };

  loop = (data, disabled) =>
    data.map(item => (
      <Radio disabled={disabled || item.disabled} value={item.value} key={item.value}>
        {item.name}
      </Radio>
    ));

  render() {
    const { model, disabled, onChange, defaultValue } = this.props;
    return (
      <Group value={defaultValue} onChange={onChange}>
        {this.loop(model, disabled)}
      </Group>
    );
  }
}

export default withStyles(s)(CPRadio);
