import React from 'react';
import PropTypes from 'prop-types';
import { TreeSelect } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPTreeSelect.css';

const { SHOW_PARENT } = TreeSelect;

class CPTreeSelect extends React.Component {
  static propTypes = {
    model: PropTypes.arrayOf(PropTypes.object),
    checkBox: PropTypes.bool,
    withParentId: PropTypes.bool,
    onChange: PropTypes.func,
    value: PropTypes.string,
    values: PropTypes.array,
    placeholder: PropTypes.string,
    defaultValue: PropTypes.string,
    parentDisabled: PropTypes.bool,
  };

  static defaultProps = {
    model: [],
    checkBox: false,
    onChange: () => {},
    value: '',
    placeholder: '',
    defaultValue: '',
    values: [],
    withParentId: false,
    parentDisabled: false,
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  traverseTree(node) {
    const { withParentId, parentDisabled } = this.props;
    const child = [];

    if (node.children && node.children.length > 0) {
      node.children.map(item => {
        const childNodeModel = {
          value: withParentId
            ? `${item.id}*${item.parentCategoryId}`
            : `${item.id}`,
          label: item.name,
          disabled: false,
          children: [],
        };
        if (item.children && item.children.length > 0) {
          childNodeModel.disabled = parentDisabled;
          childNodeModel.children = this.traverseTree(item);
        }

        return child.push(childNodeModel);
      });
    }
    return child;
  }

  render() {
    const {
      model,
      checkBox,
      onChange,
      value,
      values,
      withParentId,
      parentDisabled,
      placeholder,
    } = this.props;
    const treeData = [];

    model.map(item => {
      const treeNodeModel = {
        value: withParentId
          ? `${item.id}*${item.parentCategoryId}`
          : `${item.id}`,
        label: item.name,
        disabled: parentDisabled,
        children: [],
      };
      treeNodeModel.children = this.traverseTree(item);

      return treeData.push(treeNodeModel);
    });

    // noinspection JSAnnotator
    const tProps = {
      treeData,
      value: checkBox ? values : value,
      defaultValue: '1',
      onChange,
      treeCheckable: checkBox,
      showCheckedStrategy: SHOW_PARENT,
      searchPlaceholder: '',
      // treeCheckStrictly: true,
      treeDefaultExpandAll: parentDisabled,
      style: {
        width: '100%',
      },
    };

    return (
      <div>
        <TreeSelect {...tProps} placeholder={placeholder} />
      </div>
    );
  }
}
export default withStyles(s)(CPTreeSelect);
