import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { InputNumber } from 'antd';
import s from './CPInputNumber.css';

class CPInputNumber extends React.Component {
  static propTypes = {
    value: PropTypes.number,
    // defaultValue: PropTypes.number,
    disabled: PropTypes.bool,
    max: PropTypes.number,
    min: PropTypes.number,
    size: PropTypes.string,
    format: PropTypes.string,
    onChange: PropTypes.func,
    // step: PropTypes.node,
  };

  static defaultProps = {
    value: null,
    // defaultValue: '',
    disabled: false,
    max: 9999999999,
    min: null,
    size: 'small',
    format: '',
    step: '',
    onChange: () => {},
  };

  render() {
    const {
      disabled,
      max,
      min,
      size,
      onChange,
      format,
      // defaultValue,
      value,
      // step,
    } = this.props;

    if (format === 'Currency') {
      return (
        <InputNumber
          value={value}
          disabled={disabled}
          max={max}
          min={min}
          size={size}
          onChange={onChange}
          formatter={value2 =>
            `${value2}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
          }
          // parser={value => value.replace(/تومان\s?|(,*)/g, '')}
        />
      );
    } else if (format === 'Percent') {
      return (
        <InputNumber
          value={value}
          max={100}
          min={0}
          size={size}
          formatter={value2 => `${value2}%`}
          parser={value2 => value2.replace('%', '')}
          onChange={onChange}
        />
      );
    }
    return (
      <InputNumber
        value={value}
        disabled={disabled}
        max={max}
        min={min}
        size={size}
        onChange={onChange}
      />
    );
  }
}

export default withStyles(s)(CPInputNumber);
