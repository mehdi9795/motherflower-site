import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Popover } from 'antd';
import s from './CPPopover.css';

class CPPopover extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    placement: PropTypes.string,
    trigger: PropTypes.string,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    title: '',
    content: '',
    placement: 'top',
    trigger: '',
  };

  render() {
    const { title, content, placement, trigger } = this.props;

    return (
      <Popover
        content={content}
        title={title}
        trigger={trigger}
        placement={placement}
      >
        {this.props.children}
      </Popover>
    );
  }
}

export default withStyles(s)(CPPopover);
