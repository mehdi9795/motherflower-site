import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPInput.css';

class CPInput extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    hintText: PropTypes.string,
    fullWidth: PropTypes.bool,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onPressEnter: PropTypes.func,
    onKeyUp: PropTypes.func,
    name: PropTypes.string,
    isValidClass: PropTypes.string,
    value: PropTypes.string,
    type: PropTypes.string,
    maxLength: PropTypes.number,
    autoFocus: PropTypes.bool,
    underLine: PropTypes.string,
    style: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    hintText: null,
    fullWidth: true,
    disabled: false,
    onChange: () => {},
    onBlur: () => {},
    onPressEnter: () => {},
    onKeyUp: () => {},
    label: '',
    className: '',
    name: '',
    isValidClass: 'bar',
    value: '',
    maxLength: 99999999999999999,
    autoFocus: false,
    type: 'text',
    underLine: 'block',
    style: {},
  };

  onKeyPress = e => {
    if (e.key === 'Enter') {
      this.props.onPressEnter();
    }
  };

  onKeyUp = e => {
    this.props.onKeyUp(e);
  };

  render() {
    const {
      label,
      hintText,
      fullWidth,
      disabled,
      name,
      onChange,
      className,
      isValidClass,
      onBlur,
      value,
      maxLength,
      autoFocus,
      type,
      underLine,
      style,
    } = this.props;
    return (
      <div
        className={classnames('form-group', className, {
          fullWidth,
        })}
      >
        <input
          className={className}
          disabled={disabled}
          name={name}
          type={type}
          required="required"
          onChange={onChange}
          placeholder={hintText}
          onBlur={onBlur}
          value={value}
          maxLength={maxLength}
          autoFocus={autoFocus}
          onKeyPress={this.onKeyPress}
          onKeyUp={this.onKeyUp}
          style={style}
        />
        <label className="control-label" htmlFor="input">
          {label}
        </label>
        <i className={isValidClass} style={{ display: underLine }} />
      </div>
    );
  }
}

export default withStyles(s)(CPInput);
