import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import Coverflow from 'react-coverflow';
import s from './CPCoverFlow.css';

class CPCoverFlow extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    className: PropTypes.string,
  };

  static defaultProps = {
    data: [],
    className: 'coverFlow',
  };

  render() {
    const { data, className } = this.props;

    return (
      <Coverflow
        className={className}
        displayQuantityOfSide={1}
        navigation={false}
        enableScroll
        active={0}
        clickable={false}
        enableHeading={false}
        currentFigureScale={0.8}
        otherFigureScale={0.6}
      >
        {data.map(item => <img src={item.imgUrl} alt="" key={item.imgUrl} />)}
      </Coverflow>
    );
  }
}

export default withStyles(s)(CPCoverFlow);
