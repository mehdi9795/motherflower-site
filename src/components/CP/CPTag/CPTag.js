import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Tag } from 'antd';
import PropTypes from 'prop-types';
import s from './CPTag.css';
import { TAGS } from '../../../resources/Localization';

const { CheckableTag } = Tag;

class CPTag extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    tagSelected: PropTypes.arrayOf(PropTypes.number),
    onClose: PropTypes.func,
    onChange: PropTypes.func,
    closable: PropTypes.bool,
    checkable: PropTypes.bool,
    tagLabel: PropTypes.bool,
    color: PropTypes.string,
  };

  static defaultProps = {
    data: [],
    tagSelected: [],
    closable: false,
    checkable: false,
    tagLabel: false,
    color: '',
    onClose: () => {},
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedTags: props.tagSelected,
    };
  }

  onClick = id => {
    this.props.onChange(id);
  };

  handleChange = (tag, checked) => {
    const { selectedTags } = this.state;
    const nextSelectedTags = checked
      ? [...selectedTags, tag]
      : selectedTags.filter(t => t !== tag);
    this.setState({ selectedTags: nextSelectedTags });
    this.props.onChange(nextSelectedTags);
  };

  render() {
    const { data, onClose, closable, color, checkable, tagLabel } = this.props;
    const { selectedTags } = this.state;
    return (
      <div className="tagWrapper">
        {tagLabel && <b className="tagLabel">{TAGS} :</b>}
        {checkable
          ? data.map(tag => (
              <CheckableTag
                key={tag.key}
                checked={selectedTags.indexOf(tag.key) > -1}
                onChange={check => this.handleChange(tag.key, check)}
              >
                {tag.name}
              </CheckableTag>
            ))
          : data.map(tag => (
              <Tag
                key={tag.key}
                onClose={onClose}
                closable={closable}
                color={color}
                onClick={() => this.onClick(tag.key)}
              >
                {tag.name}
              </Tag>
            ))}
      </div>
    );
  }
}

export default withStyles(s)(CPTag);
