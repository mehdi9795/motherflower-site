import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Badge } from 'antd';
import s from './CPBadge.css';

class CPBadge extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    count: PropTypes.node,
    status: PropTypes.string,
    text: PropTypes.string,
    className: PropTypes.string,
  };

  static defaultProps = {
    children: '',
    count: '',
    status: '',
    text: '',
    className: '',
  };

  render() {
    const { count, status, text, className } = this.props;
    return (
      <Badge
        className={cs(s.badge, className)}
        count={count}
        status={status}
        text={text}
      >
        {this.props.children}
      </Badge>
    );
  }
}

export default withStyles(s)(CPBadge);
