import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import s from './CPTable.css';

class CPTable extends React.Component {
  static propTypes = {
    pagination: PropTypes.bool,
    loading: PropTypes.bool,
    footer: PropTypes.func,
    columns: PropTypes.arrayOf(PropTypes.any),
    rowSelection: PropTypes.objectOf(PropTypes.any),
    data: PropTypes.arrayOf(PropTypes.object),
  };
  static defaultProps = {
    pagination: false,
    loading: false,
    columns: [],
    rowSelection: null,
    footer: () => {},
    data: [],
  };

  render() {
    const {
      data,
      columns,
      loading,
      rowSelection,
      footer,
      pagination,
    } = this.props;
    return (
      <Table
        footer={footer}
        columns={columns}
        dataSource={data}
        pagination={pagination}
        loading={loading}
        rowSelection={rowSelection}
        key="1"

      />
    );
  }
}

export default withStyles(s)(CPTable);
