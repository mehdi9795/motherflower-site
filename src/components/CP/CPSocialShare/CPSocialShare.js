import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import $ from 'jquery';
import {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
  TelegramShareButton,
} from 'react-share';
import s from './CPSocialShare.css';

class CPSocialShare extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    telegram: PropTypes.string,
    facebook: PropTypes.string,
    twitter: PropTypes.string,
    googlePlus: PropTypes.string,
    defaultSelect: PropTypes.string,
  };

  static defaultProps = {
    className: 'share-button',
    telegram: '',
    facebook: '',
    twitter: '',
    googlePlus: '',
    defaultSelect: '',
  };

  componentDidMount() {
    $('.share-button').hover(
      () => {
        $('.SocialNameDefault').addClass('SocialNameDefaultHide');
      },
      () => {
        $('.SocialNameDefault').removeClass('SocialNameDefaultHide');
      },
    );
  }

  render() {
    const {
      className,
      telegram,
      facebook,
      twitter,
      googlePlus,
      defaultSelect,
    } = this.props;

    return (
      <div className={s.socialShare}>
        <h4 className={s.label}>اشتراک گذاری در </h4>
        <div className={s.socialShare}>
          {facebook && (
            <FacebookShareButton url={facebook} className={className}>
              <i className="mf-facebook" />
              <b className={s.SocialName}>فیسبوک</b>
            </FacebookShareButton>
          )}
          {googlePlus && (
            <GooglePlusShareButton url={googlePlus} className={className}>
              <i className="mf-googlePlus" />
              <b className={s.SocialName}>گوگل پلاس</b>
            </GooglePlusShareButton>
          )}
          {twitter && (
            <TwitterShareButton url={twitter} className={className}>
              <i className="mf-twitter" />
              <b className={s.SocialName}>توئیتر</b>
            </TwitterShareButton>
          )}
          {telegram && (
            <TelegramShareButton url={telegram} className={className}>
              <i className="mf-telegram" />
              <b className={cs(s.SocialName, 'SocialNameDefault')}>
                {defaultSelect}
              </b>
              <b className={s.SocialName}>تلگرام</b>
            </TelegramShareButton>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(s)(CPSocialShare);
