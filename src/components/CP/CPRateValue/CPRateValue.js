import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './CPRateValue.css';

class CPRateValue extends React.Component {
  static propTypes = {
    value: PropTypes.number,
  };

  static defaultProps = {
    value: 0,
  };

  colorChange = () => {
    let className = '';

    if (this.props.value >= 2) {
      className = 'black';
    }
    if (this.props.value >= 3) {
      className = 'blue';
    }
    if (this.props.value >= 4) {
      className = 'red';
    }
    if (this.props.value >= 5) {
      className = 'white';
    }

    return className;
  };

  render() {
    const { value } = this.props;
    return <div className={this.colorChange()}>{value}</div>;
  }
}

export default withStyles(s)(CPRateValue);
