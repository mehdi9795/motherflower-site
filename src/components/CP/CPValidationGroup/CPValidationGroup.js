import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './CPValidationGroup.css';

const cx = require('classnames');

class CPValidationGroup extends React.Component {
  static propTypes = {
    validationSummary: PropTypes.bool,
    showMessage: PropTypes.bool,
    children: PropTypes.node,
  };

  static defaultProps = {
    validationSummary: false,
    showMessage: false,
    children: '',
  };

  constructor(props) {
    super(props);
    this.childrenWithProps = props.children;
    this.message = '';
  }

  cloneElement = child => {
    try {
      let result = '';
      if (child.props.isValid && child.props.value.length !== 0) {
        if (child.props.validationEmail) {
          if (
            !/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(child.props.value)
          ) {
            result = this.validation(
              child.props,
              'bar_noValid',
              'لطفا ایمیل را صحیح وارد کنید',
              'false',
            );
          }
        }

        if (
          child.props.minLength > child.props.value.length &&
          child.props.value.length !== 0
        ) {
          result = this.validation(
            child.props,
            'bar_noValid',
            `${child.props.name} حداقل باید ${
              child.props.minLength
            } کاراکتر باشد.`,
            'false',
          );
        }

        if (result) throw result;
        else throw this.validation(child.props, 'bar_isValid', '', 'true');
      }
      if (
        this.props.showMessage &&
        child.props.isRequired &&
        child.props.value.length === 0
      ) {
        result = this.validation(
          child.props,
          'bar_noValid',
          `${child.props.name} اجباری است.`,
          'false',
        );
        throw result;
      }
      throw this.validation(child.props, 'bar', '', '');
    } catch (e) {
      return e;
    }
  };

  validation = (props, className, message, isValid) => {
    const { showMessage, validationSummary } = this.props;
    const child = React.cloneElement(props.children, {
      isValidClass: cx(props.children.props.isValidClass, className),
      isValid: cx(props.children.props.isValid, isValid),
      errorMessage: cx(props.errorMessage, message),
      validationSummary: cx(props.validationSummary, 'true'),
    });
    const p = React.createElement(
      'p',
      { style: { color: 'red', fontSize: 12 } },
      message,
    );
    if (message && validationSummary && showMessage)
      this.message = `${this.message} \n ${message}`;
    return (
      <div>
        {child}
        {!validationSummary && showMessage && p}
      </div>
    );
  };

  render() {
    this.message = '';
    this.childrenWithProps = React.Children.map(this.props.children, child =>
      this.cloneElement(child),
    );
    return (
      <div>
        <div className={s.messageValidation}>{this.message}</div>
        {this.childrenWithProps}
      </div>
    );
  }
}

export default withStyles(s)(CPValidationGroup);
