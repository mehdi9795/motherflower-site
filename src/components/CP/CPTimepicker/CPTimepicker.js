import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { TimePicker } from 'antd';
import moment from 'moment';
import s from './CPTimepicker.css';

class CPTimepicker extends React.Component {
  static propTypes = {
    value: PropTypes.objectOf(PropTypes.any),
    format: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    value: moment('10:00', 'HH:mm'),
    format: 'HH:mm',
    placeholder: '',
    onChange: () => {},
  };

  render() {
    const { value, format, placeholder, onChange } = this.props;
    return (
      <TimePicker
        style={{ width: '100%' }}
        placeholder={placeholder}
        value={value}
        format={format}
        onChange={onChange}
      />
    );
  }
}

export default withStyles(s)(CPTimepicker);
