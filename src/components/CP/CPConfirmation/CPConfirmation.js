import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Modal } from 'antd';
import CPButton from '../CPButton';
import s from './CPConfirmation.css';
import { ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT } from '../../../resources/Localization';
import {baseCDN} from "../../../setting";

class CPConfirmation extends React.Component {
  static propTypes = {
    visible: PropTypes.bool,
    closable: PropTypes.bool,
    handleOk: PropTypes.func,
    handleCancel: PropTypes.func,
    footer: PropTypes.node,
    content: PropTypes.string,
  };

  static defaultProps = {
    visible: false,
    closable: false,
    handleOk: () => {},
    handleCancel: () => {},
    footer: null,
    content: '',
  };

  handleOk = e => {
    this.props.handleOk(e);
  };

  handleCancel = e => {
    this.props.handleCancel(e);
  };

  render() {
    const { closable, footer, handleOk, handleCancel, visible, content } = this.props;
    return (
      <Modal closable={closable} visible={visible} footer={footer}>
        <div className={s.modalContent}>
          <div className={s.content}>
            <h3>
              <img className={s.logo} src={`${baseCDN}/fly.png`} />
              {content}
            </h3>

            <CPButton className={s.yes} onClick={handleOk}>
              بله
            </CPButton>
            <CPButton className={s.no} onClick={handleCancel}>
              خیر
            </CPButton>
          </div>
          <img className={s.background} src={`${baseCDN}/confirm.jpg`} />
        </div>
      </Modal>
    );
  }
}

export default withStyles(s)(CPConfirmation);
