import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CPCarouselSlider.css';

class CPCarouselSlider extends React.Component {
  static propTypes = {
    responsive: PropTypes.arrayOf(PropTypes.object),
    children: PropTypes.node,
    infinite: PropTypes.bool,
    slidesToShow: PropTypes.number,
    slidesToScroll: PropTypes.number,
    autoplay: PropTypes.bool,
    autoplaySpeed: PropTypes.number,
    initialSlide: PropTypes.number,
    speed: PropTypes.number,
    rtl: PropTypes.bool,
    arrows: PropTypes.bool,
    dots: PropTypes.bool,
    centerMode: PropTypes.bool,
    accessibility: PropTypes.bool,
    className: PropTypes.string,
    centerMode:  PropTypes.bool,
  };

  static defaultProps = {
    children: '',
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    autoplay: false,
    autoplaySpeed: null,
    initialSlide: 0,
    speed: 500,
    rtl: true,
    centerMode: false,
    accessibility: false,
    accessibicenterModelity: false,
    className: {},
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  render() {
    const {
      dots,
      accessibility,
      arrows,
      centerMode,
      infinite,
      slidesToShow,
      slidesToScroll,
      autoplay,
      autoplaySpeed,
      rtl,
      initialSlide,
      responsive,
      speed,
      className
    } = this.props;
    return (
      <Slider
        dots={dots}
        accessibility={accessibility}
        arrows={arrows}
        responsive={responsive}
        initialSlide={initialSlide}
        infinit={infinite}
        slidesToShow={slidesToShow}
        slidesToScroll={slidesToScroll}
        autoplay={autoplay}
        autoplaySpeed={autoplaySpeed}
        rtl={rtl}
        className={className}
        centerMode={centerMode}
        speed={speed}
      >
        {this.props.children}
      </Slider>
    );
  }
}

export default withStyles(s)(CPCarouselSlider);
