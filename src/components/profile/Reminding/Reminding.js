import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Reminding.css';
import {
  YES,
  NO,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT,
  REMINDING,
} from '../../../Resources/Localization';
import CPButton from '../../CP/CPButton';
import CPTable from '../../CP/CPTable';
import CPPopConfirm from '../../CP/CPPopConfirm';
import Link from '../../Link';

class Reminding extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      isWeb: false,
    };
  }

  componentDidMount() {
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
  }

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 992 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 992 });
    });
  };

  render() {
    const { isWeb } = this.state;
    const dataSource = [
      {
        key: '1',
        event: 'تولد همسرم (عشقم)',
        date: '23 فروردین',
        re_days: '1 روز',
        description: 'یادم باشه گل رز بخرم',
      },
      {
        key: '1',
        event: 'تولد مادرم',
        date: '24 فروردین',
        re_days: '2 روز',
        description: 'یادم باشه گل رز نخرم',
      },
    ];

    const columns = [
      {
        title: 'رویداد',
        dataIndex: 'event',
        key: 'event',
      },
      {
        title: 'تاریخ',
        dataIndex: 'date',
        key: 'date',
      },
      {
        title: 'روزهای مانده',
        dataIndex: 're_days',
        key: 're_days',
      },
      {
        title: 'توضیحات',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <div>
            <CPPopConfirm
              title={ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT}
              okText={YES}
              cancelText={NO}
              okType="primary"
              onConfirm={() => this.onDelete(record.key)}
            >
              <CPButton className="delete_action">
                <i className="mf-trash" />
              </CPButton>
            </CPPopConfirm>
          </div>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: () => (
          <div>
            <Link to="/#" className="edit_action">
              <i className="mf-edit" />
            </Link>
          </div>
        ),
      },
    ];

    return (
      <div className={s.reminding}>
        <h3 className="mobileViewTitle">
          <i className="mf-calendar" />
          {REMINDING}
        </h3>
        <div className={s.wrapper}>
          <h3 className={s.title}>{REMINDING}</h3>
          {isWeb ? (
            <CPTable data={dataSource} columns={columns} footer={false} />
          ) : (
            <ul className={s.mobileViewList}>
              <li>
                <label>رویداد</label>
                <b>تولد همسرم</b>
              </li>
              <li>
                <label>تاریخ</label>
                <b>23 فروردین</b>
              </li>
              <li>
                <label>روزهای مانده</label>
                <b>8 روز</b>
              </li>
              <li>
                <label>توضیحات</label>
                <b>لیلیوم سفید</b>
              </li>
              <li>
                <CPButton className={s.btn}>
                  <i className="mf-trash" />
                </CPButton>
              </li>
            </ul>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Reminding);
