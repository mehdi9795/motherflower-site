import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './BankAccount.css';
import CPInput from '../../CP/CPInput';
import CPSelect from '../../CP/CPSelect';
import {
  ACCOUNT_OWNER_NAME_AND_FAMILY,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_ADDRESS,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_CARD_NUMBER,
  BANK_ACCOUNT_NUMBER,
  CANCEL,
  CARD_NUMBER,
  CHOOSE_BANK,
  NO,
  OPTIONAL,
  SAVE,
  SHEBA_NUMBER,
  YES,
} from '../../../Resources/Localization';
import CPButton from '../../CP/CPButton';
import { BankDto, UserBankHistoryDto } from '../../../dtos/samDtos';
import { getCookie } from '../../../utils';
import {
  DeleteUserBankHistoriesApi,
  PostUserBankHistoriesApi,
  PutUserBankHistoriesApi,
  UserBankHistoriesApi,
} from '../../../services/samApi';
import {
  bankListSuccess,
  cardNumberListSuccess,
} from '../../../redux/sam/action/bank';
import {
  ellipsisString,
  getDtoQueryString,
  showNotification,
} from '../../../utils/helper';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import CPPopConfirm from '../../CP/CPPopConfirm';
import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../../redux/shared/action/loading';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';

const { Option } = Select;

class BankAccount extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    cardNumber: PropTypes.arrayOf(PropTypes.object),
    banks: PropTypes.arrayOf(PropTypes.object),
  };
  static defaultProps = {
    banks: [],
    cardNumber: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      code1: '',
      code2: '',
      code3: '',
      code4: '',
      code5: '',
      code6: '',
      code7: '',
      code8: '',
      code9: '',
      code10: '',
      code11: '',
      code12: '',
      code13: '',
      code14: '',
      code15: '',
      code16: '',
      sheba: '',
      name: '',
      bankId: props.banks && props.banks.length > 0 ? props.banks[0].id : '',
      id: 0,
      messageCardNumber: '',
      messageSheba: '',
      messageName: '',
      isWeb: false,
      numberCard: '',
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.updatePredicate);
    this.updatePredicate();
  }

  async onDelete(id) {
    this.props.actions.loadingSearchShowRequest();
    const token = getCookie('siteToken');
    const response = await DeleteUserBankHistoriesApi(id, token);
    this.props.actions.loadingSearchHideRequest();
    if (response.status === 200) {
      const responsePost = await UserBankHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder().dto(new BankDto()).buildJson(),
        ),
        token,
      );

      if (responsePost.status === 200) {
        this.props.actions.cardNumberListSuccess(responsePost.data);
        showNotification(
          'success',
          '',
          'َشماره کارت بانکی با موفقیت حذف شد.',
          10,
        );
      } else {
        this.props.actions.showAlertRequest(response.data.errorMessage);
      }
    }
  }

  onCancel = () => {
    this.setState({
      code1: '',
      code2: '',
      code3: '',
      code4: '',
      code5: '',
      code6: '',
      code7: '',
      code8: '',
      code9: '',
      code10: '',
      code11: '',
      code12: '',
      code13: '',
      code14: '',
      code15: '',
      code16: '',
      sheba: '',
      name: '',
      bankId: this.props.banks ? this.props.banks[0].id : '',
      id: 0,
      messageCardNumber: '',
      messageSheba: '',
      messageName: '',
    });

    const element = document.getElementById('bankAccount');
    const elementButton = document.getElementById('add-button');
    elementButton.className = 'addButton';
    element.className = 'BankAccount-bankAccount-21Ewe hideBankAccount';
  };

  async onSubmit() {
    const {
      code1,
      code2,
      code3,
      code4,
      code5,
      code6,
      code7,
      code8,
      code9,
      code10,
      code11,
      code12,
      code13,
      code14,
      code15,
      code16,
      name,
      sheba,
      bankId,
      id,
      numberCard,
      isWeb,
    } = this.state;
    this.props.actions.loadingSearchShowRequest();
    const token = getCookie('siteToken');
    if (!isWeb && numberCard.length !== 16) {
      this.setState({
        messageCardNumber: 'شماره کارت باید 16 رقم باشد.',
      });
      this.props.actions.loadingSearchHideRequest();
    } else if (name.length > 2)
      if (sheba.length === 0 || sheba.length === 24)
        if (
          (code1 &&
            code2 &&
            code3 &&
            code4 &&
            code5 &&
            code6 &&
            code7 &&
            code8 &&
            code9 &&
            code10 &&
            code11 &&
            code12 &&
            code13 &&
            code14 &&
            code15 &&
            code16) ||
          numberCard
        ) {
          const userBankHistoryCrud = new BaseCRUDDtoBuilder()
            .dto(
              new UserBankHistoryDto({
                bankDto: new BankDto({ id: bankId }),
                id,
                name,
                shabaNo: sheba,
                cardNo: isWeb
                  ? `${code1}${code2}${code3}${code4}${code5}${code6}${code7}${code8}${code9}${code10}${code11}${code12}${code13}${code14}${code15}${code16}`
                  : numberCard,
              }),
            )
            .build();

          let response;
          if (id === 0)
            response = await PostUserBankHistoriesApi(
              userBankHistoryCrud,
              token,
            );
          else
            response = await PutUserBankHistoriesApi(
              userBankHistoryCrud,
              token,
            );

          this.props.actions.loadingSearchHideRequest();
          if (response.status === 200) {
            const responsePost = await UserBankHistoriesApi(
              getDtoQueryString(
                new BaseGetDtoBuilder().dto(new BankDto()).buildJson(),
              ),
              token,
            );

            if (responsePost.status === 200) {
              this.props.actions.cardNumberListSuccess(responsePost.data);

              const element = document.getElementById('bankAccount');
              const elementButton = document.getElementById('add-button');
              elementButton.className = 'addButton';
              element.className =
                'BankAccount-bankAccount-21Ewe hideBankAccount';
            }

            if (id === 0)
              showNotification(
                'success',
                '',
                'شماره کارت بانکی با موفقیت اضافه شد.',
                10,
              );
            else
              showNotification(
                'success',
                '',
                'شماره کارت بانکی با موفقیت ویرایش شد.',
                10,
              );
          } else {
            this.props.actions.showAlertRequest(response.data.errorMessage);
          }
        } else {
          this.setState({
            messageCardNumber: 'شماره کارت الزامی است',
          });
          this.props.actions.loadingSearchHideRequest();
        }
      else {
        this.setState({
          messageSheba: 'شماره شبا باید 24 رقم باشد.',
        });
        this.props.actions.loadingSearchHideRequest();
      }
    else {
      this.setState({
        messageName: 'نام و نام خانوادگی الزامی است.',
      });
      this.props.actions.loadingSearchHideRequest();
    }
  }

  async onEdit(record) {
    const cardNumber = record.cardNo.split('');

    this.setState({
      numberCard: record.cardNo,
      code1: cardNumber[0],
      code2: cardNumber[1],
      code3: cardNumber[2],
      code4: cardNumber[3],
      code5: cardNumber[4],
      code6: cardNumber[5],
      code7: cardNumber[6],
      code8: cardNumber[7],
      code9: cardNumber[8],
      code10: cardNumber[9],
      code11: cardNumber[10],
      code12: cardNumber[11],
      code13: cardNumber[12],
      code14: cardNumber[13],
      code15: cardNumber[14],
      code16: cardNumber[15],
      name: record.name,
      sheba: record.shabaNo.replace('IR',''),
      bankId: record.bankDto.id,
      id: record.key,
      messageCardNumber: '',
      messageSheba: '',
      messageName: '',
    });

    const elementForm = document.getElementById('bankAccount');
    elementForm.className = 'BankAccount-bankAccount-21Ewe showBankAccount';

    const elementButton = document.getElementById('add-button');
    elementButton.className = 'hide-button';
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  handleInput = (inputName, value, keyFocus) => {
    const { code16 } = this.state;
    const re = /^[0-9\b]+$/;

    if (value === 'Backspace') {
      if (inputName === 'code16' && code16.length > 0) {
        this.setState({ [inputName]: '' });
        return;
      } else if (inputName !== 'code1') {
        let name = 'code';
        if (inputName.length === 6 && inputName !== 'code10') name = 'code1';
        const beforeInput =
          inputName !== 'code10'
            ? `${name}${inputName.charAt(inputName.length - 1) - 1}`
            : 'code9';
        this.setState({ [beforeInput]: '' });
        const input = document.getElementsByName(beforeInput);
        input[0].focus();
      }
      return;
    }

    if (inputName === 'code1' && value === '0') return;
    if (inputName === 'numberCard') {
      this.setState({ [inputName]: value });
      if (value.length === 16) {
        const input = document.getElementsByName(keyFocus);
        input[0].focus();
      }
    } else if (value === '' || re.test(value)) {
      this.setState({ [inputName]: value });
      if (!(!this.state.isWeb && inputName === 'code1' && value.length === 24))
        if (keyFocus !== 'submit') {
          const input = document.getElementsByName(keyFocus);
          input[0].focus();
        } else {
          // document.getElementById(keyFocus).focus();
        }
    }
    return null;
  };

  handleInput2 = (inputName, value) => {
    const re = /^[a-zA-Z][a-zA-Z\-'\s]{0,43}$/;
    const regFa = /^[\u0600-\u06FF\s]+$/;
    if (
      (value === '' || (regFa.test(value) || re.test(value))) &&
      inputName === 'name'
    )
      this.setState({
        [inputName]: value,
        messageCardNumber: '',
        messageSheba: '',
        messageName: '',
      });

    if (inputName === 'bankId')
      this.setState({
        [inputName]: value,
        messageCardNumber: '',
        messageSheba: '',
        messageName: '',
      });
  };

  handleInputSheba = (inputName, value) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        [inputName]: value,
        messageCardNumber: '',
        messageSheba: '',
        messageName: '',
      });
    }
  };

  showForm = () => {
    this.onCancel();

    const elementForm = document.getElementById('bankAccount');
    elementForm.className = 'BankAccount-bankAccount-21Ewe showBankAccount';

    const elementButton = document.getElementById('add-button');
    elementButton.className = 'hide-button';
  };

  render() {
    const { banks, cardNumber } = this.props;
    const {
      bankId,
      messageCardNumber,
      messageSheba,
      messageName,
      isWeb,
    } = this.state;

    const bankArray = [];
    banks.map(item =>
      bankArray.push(
        <Option value={item.id} key={item.id}>
          {item.name}
        </Option>,
      ),
    );

    return (
      <div className={s.wrapper}>
        <h3 className="mobileViewTitle">
          <i className="mf-pin" />
          {BANK_ACCOUNT_NUMBER}
        </h3>
        {isWeb ? (
          <div>
            <ul className={s.tableHeader}>
              <li>
                <b>نام دارنده حساب</b>
              </li>
              <li>
                <b>شماره شبا</b>
              </li>
              <li>
                <b>شماره کارت</b>
              </li>
              <li />
              <li />
            </ul>
            <ul>
              {cardNumber.map(item => (
                <div key={item.id} className={s.tableData}>
                  <li>
                    <p>{item.name}</p>
                  </li>
                  <li>
                    <p>{item.shabaNo}</p>
                  </li>
                  <li>
                    <p>{item.cardNo}</p>
                  </li>
                  <li>
                    <CPPopConfirm
                      title={ARE_YOU_SURE_WANT_TO_DELETE_THE_CARD_NUMBER}
                      okText={YES}
                      cancelText={NO}
                      okType="primary"
                      onConfirm={() => this.onDelete(item.id)}
                    >
                      <CPButton>
                        <i className="mf-trash" />
                      </CPButton>
                    </CPPopConfirm>
                  </li>
                  <li>
                    <CPButton onClick={() => this.onEdit(item)}>
                      <i className="mf-edit" />
                    </CPButton>
                  </li>
                </div>
              ))}
            </ul>
          </div>
        ) : (
          cardNumber.map(item => (
            <div className={s.mobileView}>
              <ul>
                <li>
                  <b>نام دارنده حساب</b>
                </li>
                <li>
                  <b>شماره شبا</b>
                </li>
                <li>
                  <b>شماره کارت</b>
                </li>
                <li>
                  <CPButton
                    className={s.editButton}
                    onClick={() => this.onEdit(item)}
                  >
                    <i className="mf-edit" />
                  </CPButton>
                </li>
              </ul>
              <ul>
                <li>
                  {item.name}
                </li>
                <li className={item.shabaNo.length === 0 && s.emptyColumn}>
                  {item.shabaNo}
                </li>
                <li>
                  {item.cardNo}
                </li>
                <li>
                  <CPPopConfirm
                    title={ARE_YOU_SURE_WANT_TO_DELETE_THE_CARD_NUMBER}
                    okText={YES}
                    cancelText={NO}
                    okType="primary"
                    onConfirm={() => this.onDelete(item.id)}
                  >
                    <CPButton  className="delete_action">
                      <i className="mf-trash" />
                    </CPButton>
                  </CPPopConfirm>
                </li>
              </ul>
            </div>
          ))
        )}
        {/* <CPTable data={dataSource} columns={columns} footer={null} /> */}

        <div className="addButton" id="add-button">
          <CPButton className={s.addBtn} shape="circle" onClick={this.showForm}>
            <i className="mf-add" />
          </CPButton>
        </div>

        <div className={s.bankAccount} id="bankAccount">
          <div className="col-sm-12">
            <b className={s.label}>{CARD_NUMBER}</b>
            {isWeb ? (
              <div className={s.cardNumber}>
                <div className={s.inputs}>
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code1', value.key, 'code2')
                    }
                    value={this.state.code1}
                    maxLength={1}
                    name="code1"
                    autoFocus
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code2', value.key, 'code3')
                    }
                    value={this.state.code2}
                    maxLength={1}
                    name="code2"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code3', value.key, 'code4')
                    }
                    value={this.state.code3}
                    maxLength={1}
                    name="code3"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code4', value.key, 'code5')
                    }
                    value={this.state.code4}
                    maxLength={1}
                    name="code4"
                    type="number"
                  />
                </div>
                <i className="mf-minus" />
                <div className={s.inputs}>
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code5', value.key, 'code6')
                    }
                    value={this.state.code5}
                    maxLength={1}
                    name="code5"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code6', value.key, 'code7')
                    }
                    value={this.state.code6}
                    maxLength={1}
                    name="code6"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code7', value.key, 'code8')
                    }
                    value={this.state.code7}
                    maxLength={1}
                    name="code7"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code8', value.key, 'code9')
                    }
                    value={this.state.code8}
                    maxLength={1}
                    name="code8"
                    type="number"
                  />
                </div>
                <i className="mf-minus" />
                <div className={s.inputs}>
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code9', value.key, 'code10')
                    }
                    value={this.state.code9}
                    maxLength={1}
                    name="code9"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code10', value.key, 'code11')
                    }
                    value={this.state.code10}
                    maxLength={1}
                    name="code10"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code11', value.key, 'code12')
                    }
                    value={this.state.code11}
                    maxLength={1}
                    name="code11"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code12', value.key, 'code13')
                    }
                    value={this.state.code12}
                    maxLength={1}
                    name="code12"
                    type="number"
                  />
                </div>
                <i className="mf-minus" />
                <div className={s.inputs}>
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code13', value.key, 'code14')
                    }
                    value={this.state.code13}
                    maxLength={1}
                    name="code13"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code14', value.key, 'code15')
                    }
                    value={this.state.code14}
                    maxLength={1}
                    name="code14"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code15', value.key, 'code16')
                    }
                    value={this.state.code15}
                    maxLength={1}
                    name="code15"
                    type="number"
                  />
                  <CPInput
                    onKeyUp={value =>
                      this.handleInput('code16', value.key, 'sheba')
                    }
                    value={this.state.code16}
                    maxLength={1}
                    name="code16"
                    type="number"
                  />
                </div>
              </div>
            ) : (
              <div className={s.cardNumber}>
                <CPInput
                  onChange={value =>
                    this.handleInput('numberCard', value.target.value, 'sheba')
                  }
                  value={this.state.numberCard}
                  maxLength={16}
                  name="code1"
                  autoFocus
                  type="number"
                />
              </div>
            )}
            {messageCardNumber && (
              <p className={s.errorMessage}>{messageCardNumber}</p>
            )}
          </div>
          <div className="col-sm-12">
            <b className={s.label}>
              {SHEBA_NUMBER} <b>{OPTIONAL}</b>
            </b>
            <div className={s.shebaNumber}>
              <CPInput
                onChange={value =>
                  this.handleInputSheba('sheba', value.target.value)
                }
                value={this.state.sheba}
                name="sheba"
                maxLength={24}
                type="number"
              />
              <span className={s.ir}>IR</span>
            </div>
            {messageSheba && <p className={s.errorMessage}>{messageSheba}</p>}
          </div>
          <div className={s.row}>
            <div className="col-sm-6">
              <b className={s.label}>{ACCOUNT_OWNER_NAME_AND_FAMILY}</b>
              <CPInput
                onChange={value =>
                  this.handleInput2('name', value.target.value)
                }
                value={this.state.name}
                placeholder="IR"
              />
              {messageName && <p className={s.errorMessage}>{messageName}</p>}
            </div>
            <div className="col-sm-6">
              <b className={s.label}>{CHOOSE_BANK}</b>
              <CPSelect
                onChange={value => this.handleInput2('bankId', value)}
                value={bankId}
                className={s.bankId}
              >
                {bankArray}
              </CPSelect>
            </div>
          </div>
          <div className="text-left hiddenBoxBtns">
            <CPButton className={s.save} onClick={this.onSubmit}>
              {SAVE}
            </CPButton>
            <CPButton className={s.cancel} onClick={this.onCancel}>
              {CANCEL}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  banks: state.samBank.bankListData ? state.samBank.bankListData.items : [],
  cardNumber: state.samBank.cardNumberListData
    ? state.samBank.cardNumberListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      bankListSuccess,
      cardNumberListSuccess,
      showAlertRequest,
      loadingSearchHideRequest,
      loadingSearchShowRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(BankAccount));
