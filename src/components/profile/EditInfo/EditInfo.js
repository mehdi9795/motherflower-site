import React from 'react';
import { Select } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditInfo.css';
import {
  EMAIL,
  FAMILY_NAME,
  SEXUAL,
  NAME,
  BIRTHDAY_DATE,
  MOBILE,
  SAVE,
  EDIT_INFORMATION,
  CITY, NEW_PASSWORD,
} from '../../../Resources/Localization';
import CPInput from '../../CP/CPInput';
import CPSelect from '../../CP/CPSelect';
import CPRadio from '../../CP/CPRadio';
import CPButton from '../../CP/CPButton';
import { UserDto } from '../../../dtos/identityDtos';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import { getCookie, setCookie } from '../../../utils';
import { GetUser, PutUser } from '../../../services/identityApi';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../../redux/shared/action/loading';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import { CityDto } from '../../../dtos/commonDtos';
import CPValidator from "../../CP/CPValidator";

const { Option } = Select;

class EditInfo extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    cities: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    cities: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      gender: 'M',
      firstName: '',
      lastName: '',
      phone: '',
      dayOfBirthDate: 1,
      monthOfBirthDate: 1,
      email: '',
      countryCode: '',
      errorValid: false,
      cityId: props.cities && props.cities[0] ? props.cities[0].id : '',
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.validation = [];
  }

  async componentWillMount() {
    const token = getCookie('siteToken');
    const response = await GetUser(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new UserDto({
              fromMobile: true,
            }),
          )
          .buildJson(),
      ),
      true,
    );
    if (response.status === 200) {
      this.setState({
        firstName:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].firstName
            : '',
        lastName:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].lastName
            : '',
        phone:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].phone
            : '',
        dayOfBirthDate:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].dayOfBirthDate
            : '',
        monthOfBirthDate:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].monthOfBirthDate.toString()
            : '1',
        email:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].email
            : '',
        countryCode:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].countryCode
            : '',
        cityId:
            response.data.items && response.data.items.length > 0 && response.data.items[0].cityDto
              ? response.data.items[0].cityDto.id
              : 1,
        gender:
          response.data.items && response.data.items.length > 0
            ? response.data.items[0].genderType
            : 'M',
      });
    }
  }

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  async onSubmit() {
    const {
      gender,
      firstName,
      lastName,
      phone,
      dayOfBirthDate,
      monthOfBirthDate,
      email,
      countryCode,
      cityId,
    } = this.state;
    if (this.validation.length === 3) {
      this.props.actions.loadingSearchShowRequest();
      const token = getCookie('siteToken');

      const userCrud = new BaseCRUDDtoBuilder()
        .dto(
          new UserDto({
            firstName,
            lastName,
            phone,
            countryCode,
            dayOfBirthDate,
            monthOfBirthDate,
            email,
            genderType: gender,
            fromMobile: true,
            cityDto: new CityDto({id: cityId}),
          }),
        )
        .build();

      const response = await PutUser(token, userCrud);
      this.props.actions.loadingSearchHideRequest();
      if (response.status === 200) {
        setCookie(
          'siteDisplayName',
          encodeURIComponent(`${firstName} ${lastName}`),
          8760,
        );
        showNotification(
          'success',
          '',
          'ویرایش اطلاعات کاربری با موفقیت انجام شد',
          10,
        );
      } else {
        this.props.actions.showAlertRequest(response.data.errorMessage);
      }
    } else {
      this.setState({ errorValid: true });
    }
  }

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  render() {
    const { cities } = this.props;
    const {
      gender,
      firstName,
      lastName,
      phone,
      dayOfBirthDate,
      monthOfBirthDate,
      email,
      cityId,
      errorValid,
    } = this.state;
    const sexual = [
      {
        value: 'M',
        name: 'مرد',
        defaultChecked: true,
      },
      {
        value: 'F',
        name: 'زن',
        defaultChecked: false,
      },
    ];
    const dayArray = [];
    for (let i = 1; i < 32; i += 1) {
      dayArray.push(
        <Option value={i} key={i}>
          {i}
        </Option>,
      );
    }
    const cityArray = [];
    cities.map(item =>
      cityArray.push(
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>,
      ),
    );

    return (
      <div className={s.userInfo}>
        <h3 className="mobileViewTitle">
          <i className="mf-edit" />
          {EDIT_INFORMATION}
        </h3>
        <div className={s.wrapper}>
          <div className="col-md-12">
            <b className={s.label}>{SEXUAL}</b>
            <CPRadio
              model={sexual}
              size="small"
              onChange={value => this.onChange('gender', value.target.value)}
              defaultValue={gender}
            />
          </div>
          <div className="col-md-6">
            <CPValidator
              value={firstName}
              checkValidation={this.checkValidation}
              minLength={2}
              showMessage={errorValid}
              name={NAME}
            >
              <CPInput
                onChange={value =>
                  this.handleInput('firstName', value.target.value)
                }
                label={NAME}
                value={firstName}
              />
            </CPValidator>
          </div>
          <div className="col-md-6">
            <CPValidator
              value={lastName}
              checkValidation={this.checkValidation}
              minLength={2}
              showMessage={errorValid}
              name={FAMILY_NAME}
            >
              <CPInput
                onChange={value =>
                  this.handleInput('lastName', value.target.value)
                }
                label={FAMILY_NAME}
                value={lastName}
              />
            </CPValidator>
          </div>
          <div className="col-md-6">
            <CPValidator
              validationEmail
              value={email}
              checkValidation={this.checkValidation}
              showMessage={errorValid}
              name={EMAIL}
            >
              <CPInput
                onChange={value => this.handleInput('email', value.target.value)}
                label={EMAIL}
                value={email}
              />
            </CPValidator>
          </div>

          <div className="col-md-6">
            <div className="row">
              <div className="col-12">
                <label className={s.dateOfBirth}>{BIRTHDAY_DATE}</label>
              </div>
              <div className="col-6">
                <CPSelect
                  onChange={value =>
                    this.handleInput('monthOfBirthDate', value)
                  }
                  value={monthOfBirthDate}
                >
                  <Option value="1" key="1">
                    فروردین
                  </Option>
                  <Option value="2" key="2">
                    اردیبهشت
                  </Option>
                  <Option value="3" key="3">
                    خرداد
                  </Option>
                  <Option value="4" key="4">
                    تیر
                  </Option>
                  <Option value="5" key="5">
                    مرداد
                  </Option>
                  <Option value="6" key="6">
                    شهریور
                  </Option>
                  <Option value="7" key="7">
                    مهر
                  </Option>
                  <Option value="8" key="8">
                    آبان
                  </Option>
                  <Option value="9" key="9">
                    آذر
                  </Option>
                  <Option value="10" key="10">
                    دی
                  </Option>
                  <Option value="11" key="11">
                    بهمن
                  </Option>
                  <Option value="12" key="12">
                    اسفند
                  </Option>
                </CPSelect>
              </div>
              <div className="col-6">
                <CPSelect
                  onChange={value => this.handleInput('dayOfBirthDate', value)}
                  value={dayOfBirthDate}
                >
                  {dayArray}
                </CPSelect>
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <CPInput label={MOBILE} value={phone.replace('+98', '0')} />
          </div>

          <div className="col-md-6">
            <label>{CITY}</label>
            <CPSelect
              onChange={value => this.handleInput('cityId', value)}
              value={cityId}
              showSearch
            >
              {cityArray}
            </CPSelect>
          </div>

          <div className="col-md-12 text-left">
            <CPButton className={s.saveBtn} onClick={this.onSubmit}>
              {SAVE}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  loginData: state.account.loginData ? state.account.loginData : [],
  cities: state.commonCity.cityListData
    ? state.commonCity.cityListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showAlertRequest,
      loadingSearchShowRequest,
      loadingSearchHideRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(EditInfo));
