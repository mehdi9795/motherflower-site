import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Spin, Tabs } from 'antd';
import s from './Orders2.css';
import {
  ADDRESS,
  CANCELED_ORDERS,
  DELIVERY_TYPE,
  ORDER_STATUS,
  ORDERS_IN_PROGRESS,
  PAST_ORDERS,
  TOMAN,
  YOUR_ORDERS,
} from '../../../Resources/Localization';
import CPAvatar from '../../CP/CPAvatar';
import CPButton from '../../CP/CPButton';
import {
  cancelOrderListFailure,
  cancelOrderListRequest,
  cancelOrderListSuccess,
  deliveredOrderListFailure,
  deliveredOrderListRequest,
  deliveredOrderListSuccess,
  inProgressOrderListFailure,
  inProgressOrderListRequest,
  inProgressOrderListSuccess,
} from '../../../redux/sam/action/order';
import { getDtoQueryString } from '../../../utils/helper';
import { BillApi } from '../../../services/samApi';
import { BillDto } from '../../../dtos/samDtos';
import { getCookie } from '../../../utils';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';
import Link from '../../Link';

const { TabPane } = Tabs;

class Orders2 extends React.Component {
  static propTypes = {
    inProgressOrders: PropTypes.arrayOf(PropTypes.object),
    deliveredOrders: PropTypes.arrayOf(PropTypes.object),
    cancelOrders: PropTypes.arrayOf(PropTypes.object),
    inProgressOrderCount: PropTypes.number,
    inProgressOrderLoading: PropTypes.bool,
    deliveredOrderLoading: PropTypes.bool,
    cancelOrderLoading: PropTypes.bool,
    cancelOrderCount: PropTypes.number,
    deliveredOrderCount: PropTypes.number,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    inProgressOrders: [],
    deliveredOrders: [],
    cancelOrders: [],
    inProgressOrderCount: 0,
    inProgressOrderLoading: false,
    deliveredOrderLoading: false,
    cancelOrderLoading: false,
    deliveredOrderCount: 0,
    cancelOrderCount: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
    this.toggleMe = this.toggleMe.bind(this);
    this.getOrders = this.getOrders.bind(this);
    this.getAfterOrderInProgress = this.getAfterOrderInProgress.bind(this);
    this.getAfterCancelOrder = this.getAfterCancelOrder.bind(this);
    this.getAfterDeliveredOrder = this.getAfterDeliveredOrder.bind(this);
  }

  async toggleMe2() {
    this.setState({
      show: !this.state.show,
    });
  }

  toggleMe = id => {
    const elements = document.getElementsByName(id);
    const elementI = document.getElementById(id);
    const elementIApp = document.getElementById(`${id}app`);

    if (elementI.className === 'mf-double-down-arrow') {
      for (let i = 0; i < elements.length; i += 1) {
        elements[i].className = 'toggleBox show';
      }

      elementI.className = 'mf-double-up-arrow';
      elementIApp.className = 'mf-double-up-arrow';
    } else {
      // elementDiv.className = 'toggleBox hide';
      for (let i = 0; i < elements.length; i += 1) {
        elements[i].className = 'toggleBox hide';
      }
      elementI.className = 'mf-double-down-arrow';
      elementIApp.className = 'mf-double-down-arrow';
    }
  };

  async getOrders(key) {
    const { deliveredOrders, cancelOrders } = this.props;
    const token = getCookie('siteToken');
    if (key === '2' && deliveredOrders.length === 0) {
      const responseDeliveredOrder = await BillApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new BillDto({
                searchBillStatus: ['Finished'],
              }),
            )
            .includes(['addressDto', 'billItemDtos', 'vendorBranchDto'])
            .pageSize(4)
            .pageIndex(0)
            .buildJson(),
        ),
        token,
      );

      if (responseDeliveredOrder.status === 200)
        this.props.actions.deliveredOrderListSuccess(
          responseDeliveredOrder.data,
        );
      else this.props.actions.deliveredOrderListFailure();
    } else if (key === '3' && cancelOrders.length === 0) {
      const responseCancelOrder = await BillApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new BillDto({
                searchBillStatus: ['CancelBySystem'],
              }),
            )
            .includes(['addressDto', 'billItemDtos', 'vendorBranchDto'])
            .pageSize(4)
            .pageIndex(0)
            .buildJson(),
        ),
        token,
      );

      if (responseCancelOrder.status === 200)
        this.props.actions.cancelOrderListSuccess(responseCancelOrder.data);
      else this.props.actions.cancelOrderListFailure();
    }
  }

  async getAfterOrderInProgress() {
    const { inProgressOrders } = this.props;
    const token = getCookie('siteToken');
    this.props.actions.inProgressOrderListRequest();

    const responseInProgressOrder = await BillApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BillDto({
              searchBillStatus: ['None', 'Confirmed', 'Sending', 'Prepared'],
            }),
          )
          .includes(['addressDto', 'billItemDtos', 'vendorBranchDto'])
          .pageSize(4)
          .pageIndex(inProgressOrders.length / 4)
          .buildJson(),
      ),
      token,
    );
    if (responseInProgressOrder.status === 200) {
      const array = this.props.inProgressOrders;
      responseInProgressOrder.data.items.map(item => array.push(item));

      this.props.actions.inProgressOrderListSuccess({
        items: array,
        count: responseInProgressOrder.data.count,
      });
    } else this.props.actions.inProgressOrderListFailure();
  }

  async getAfterDeliveredOrder() {
    const { deliveredOrders } = this.props;
    const token = getCookie('siteToken');
    this.props.actions.deliveredOrderListRequest();
    const responseDeliveredOrder = await BillApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BillDto({
              searchBillStatus: ['Finished'],
            }),
          )
          .includes(['addressDto', 'billItemDtos', 'vendorBranchDto'])
          .pageSize(4)
          .pageIndex(deliveredOrders.length / 4)
          .buildJson(),
      ),
      token,
    );

    if (responseDeliveredOrder.status === 200) {
      const array = this.props.deliveredOrders;
      responseDeliveredOrder.data.items.map(item => array.push(item));
      this.props.actions.deliveredOrderListSuccess({
        items: array,
        count: responseDeliveredOrder.data.count,
      });
    } else this.props.actions.deliveredOrderListFailure();
  }

  async getAfterCancelOrder() {
    const { cancelOrders } = this.props;
    const token = getCookie('siteToken');
    this.props.actions.cancelOrderListRequest();

    const responseCancelOrder = await BillApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BillDto({
              searchBillStatus: ['CancelBySystem'],
            }),
          )
          .includes(['addressDto', 'billItemDtos', 'vendorBranchDto'])
          .pageSize(4)
          .pageIndex(cancelOrders.length / 4)
          .buildJson(),
      ),
      token,
    );

    if (responseCancelOrder.status === 200) {
      const array = this.props.cancelOrders;
      responseCancelOrder.data.items.map(item => array.push(item));
      this.props.actions.cancelOrderListSuccess({
        items: array,
        count: responseCancelOrder.data.count,
      });
    } else this.props.actions.cancelOrderListFailure();
  }

  render() {
    const {
      inProgressOrders,
      deliveredOrders,
      cancelOrders,
      inProgressOrderCount,
      inProgressOrderLoading,
      deliveredOrderLoading,
      cancelOrderLoading,
      deliveredOrderCount,
      cancelOrderCount,
    } = this.props;

    return (
      <div className={s.orders}>
        <h3 className="mobileViewTitle">
          <i className="mf-squares" />
          {YOUR_ORDERS}
        </h3>
        <div className={s.wrapper}>
          <Tabs
            defaultActiveKey="1"
            animated
            style={{ width: '100%' }}
            onTabClick={value => {
              this.getOrders(value);
            }}
          >
            <TabPane tab={ORDERS_IN_PROGRESS} key="1">
              {inProgressOrders.map(item => (
                <div key={item.id} className={s.ordersList}>
                  <div className={s.orderDetail}>
                    <ul className={s.header}>
                      <li>شماره سفارش</li>
                      <li>تاریخ سفارش</li>
                      <li>تخفیف</li>
                      <li>هزینه حمل</li>
                      <li>قیمت کل</li>
                      <li />
                    </ul>

                    <ul className={s.data}>
                      <li>{item.id}</li>
                      <li>{item.persianCreatedDateTimeUtc}</li>
                      <li>{item.stringTotalDiscount}</li>
                      <li>{item.stringTotalShipmentAmount}</li>
                      <li>{item.stringPayableBillingAmount}</li>
                      <li>
                        <CPButton
                          type="circle"
                          className={s.upAndDownBtn}
                          onClick={() => this.toggleMe(item.id)}
                        >
                          <i className="mf-double-down-arrow" id={item.id} />
                        </CPButton>
                      </li>
                    </ul>
                    <CPButton
                      type="circle"
                      name={item.id}
                      className={s.upDownBtn}
                      onClick={() => this.toggleMe(item.id)}
                    >
                      <i
                        className="mf-double-down-arrow"
                        id={`${item.id}app`}
                      />
                    </CPButton>
                    <span className={s.ordersAvatar}>
                      {item.billItemDtos.map(item2 => (
                        <CPAvatar
                          src={
                            item2.vendorBranchProductDto.productDto.imageDtos
                              ? item2.vendorBranchProductDto.productDto
                                  .imageDtos[0].url
                              : ''
                          }
                          key={item2.id}
                        />
                      ))}
                    </span>
                  </div>
                  {item.billItemDtos.map(item2 => (
                    <div
                      key={item2.id}
                      className="toggleBox hide"
                      name={item.id}
                    >
                      <div className={s.productDetail}>
                        <div className={s.rightContent}>
                          <Link
                            to={`/product-details/${
                              item2.vendorBranchProductDto.productDto.id
                            }/${
                              item2.vendorBranchProductDto.vendorBranchDto.id
                            }`}
                            target="_blank"
                          >
                            <CPAvatar
                              src={
                                item2.vendorBranchProductDto.productDto
                                  .imageDtos
                                  ? item2.vendorBranchProductDto.productDto
                                      .imageDtos[0].url
                                  : ''
                              }
                              key={item2.id}
                            />
                          </Link>
                          <span>
                            <h4>
                              {item2.vendorBranchProductDto.productDto.name}
                            </h4>
                            <p>
                              فروشنده:
                              {
                                item2.vendorBranchProductDto.vendorBranchDto
                                  .name
                              }
                            </p>
                            <p> تعداد: {item2.count}</p>
                          </span>
                        </div>
                        <div className={s.leftContent}>
                          <ul>
                            <li>
                              <label>تخفیف :</label>
                              <p>
                                {item2.stringTotalDiscount} {TOMAN}
                              </p>
                            </li>
                            <li>
                              <label>هزینه حمل :</label>
                              <p>
                                {item2.stringShipmentAmount} {TOMAN}
                              </p>
                            </li>
                            <li>
                              <label>قیمت :</label>
                              <p>
                                {item2.stringTotalAmount} {TOMAN}
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className={s.deliveryType}>
                        <span>
                          <label>{ORDER_STATUS} :</label>
                          <b className={s.status}>
                            {item2.billItemStatusTitle}
                          </b>
                        </span>
                        <span>
                          <label>{DELIVERY_TYPE} :</label>
                          <b>{item2.priceOption}</b>
                        </span>
                      </div>
                      {item2.priceOption === 'Pickup' && (
                        <div className={s.deliveryType}>
                          <span>
                            <label>{ADDRESS} :</label>
                            <b className={s.status}>
                              {item2.vendorBranchDto &&
                                item2.vendorBranchDto.address}
                            </b>
                          </span>
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              ))}
              {inProgressOrders.length < inProgressOrderCount && (
                <CPButton
                  className={s.moreButton}
                  onClick={this.getAfterOrderInProgress}
                  disabled={inProgressOrderLoading}
                >
                  {inProgressOrderLoading ? (
                    <Spin spinning={inProgressOrderLoading} />
                  ) : (
                    <label className={s.moreCursor}>بیشتر</label>
                  )}
                </CPButton>
              )}
            </TabPane>
            <TabPane tab={PAST_ORDERS} key="2">
              {deliveredOrders.map(item => (
                <div key={item.id} className={s.ordersList}>
                  <div className={s.orderDetail}>
                    <ul className={s.header}>
                      <li>شماره سفارش</li>
                      <li>تاریخ سفارش</li>
                      <li>تخفیف</li>
                      <li>هزینه حمل</li>
                      <li>قیمت کل</li>
                      <li />
                    </ul>

                    <ul className={s.data}>
                      <li>{item.id}</li>
                      <li>{item.persianCreatedDateTimeUtc}</li>
                      <li>{item.stringTotalDiscount}</li>
                      <li>{item.stringTotalShipmentAmount}</li>
                      <li>{item.stringPayableBillingAmount}</li>
                      <li>
                        <CPButton
                          type="circle"
                          className={s.upAndDownBtn}
                          onClick={() => this.toggleMe(item.id)}
                        >
                          <i className="mf-double-down-arrow" id={item.id} />
                        </CPButton>
                      </li>
                    </ul>
                    <CPButton
                      type="circle"
                      name={item.id}
                      className={s.upDownBtn}
                      onClick={() => this.toggleMe(item.id)}
                    >
                      <i
                        className="mf-double-down-arrow"
                        id={`${item.id}app`}
                      />
                    </CPButton>
                    <span className={s.ordersAvatar}>
                      {item.billItemDtos.map(item2 => (
                        <CPAvatar
                          src={
                            item2.vendorBranchProductDto.productDto.imageDtos
                              ? item2.vendorBranchProductDto.productDto
                                  .imageDtos[0].url
                              : ''
                          }
                          key={item2.id}
                        />
                      ))}
                    </span>
                  </div>
                  {item.billItemDtos.map(item2 => (
                    <div
                      key={item2.id}
                      className="toggleBox hide"
                      name={item.id}
                    >
                      <div className={s.productDetail}>
                        <div className={s.rightContent}>
                          <Link
                            to={`/product-details/${
                              item2.vendorBranchProductDto.productDto.id
                            }/${
                              item2.vendorBranchProductDto.vendorBranchDto.id
                            }`}
                            target="_blank"
                          >
                            <CPAvatar
                              src={
                                item2.vendorBranchProductDto.productDto
                                  .imageDtos
                                  ? item2.vendorBranchProductDto.productDto
                                      .imageDtos[0].url
                                  : ''
                              }
                              key={item2.id}
                            />
                          </Link>
                          <span>
                            <h4>
                              {item2.vendorBranchProductDto.productDto.name}
                            </h4>
                            <p>
                              فروشنده:
                              {
                                item2.vendorBranchProductDto.vendorBranchDto
                                  .name
                              }
                            </p>
                            <p> تعداد: {item2.count}</p>
                          </span>
                        </div>
                        <div className={s.leftContent}>
                          <ul>
                            <li>
                              <label>تخفیف :</label>
                              <p>
                                {item2.stringTotalDiscount} {TOMAN}
                              </p>
                            </li>
                            <li>
                              <label>هزینه حمل :</label>
                              <p>
                                {item2.stringShipmentAmount} {TOMAN}
                              </p>
                            </li>
                            <li>
                              <label>قیمت :</label>
                              <p>
                                {item2.stringTotalAmount} {TOMAN}
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className={s.deliveryType}>
                        <span>
                          <label>{ORDER_STATUS} :</label>
                          <b className={s.status}>
                            {item2.billItemStatusTitle}
                          </b>
                        </span>
                        <span>
                          <label>{DELIVERY_TYPE} :</label>
                          <b>{item2.priceOption}</b>
                        </span>
                      </div>
                      {item2.priceOption === 'Pickup' && (
                        <div className={s.deliveryType}>
                          <span>
                            <label>{ADDRESS} :</label>
                            <b className={s.status}>
                              {item2.vendorBranchDto &&
                              item2.vendorBranchDto.address}
                            </b>
                          </span>
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              ))}
              {deliveredOrders.length < deliveredOrderCount && (
                <CPButton
                  className={s.moreButton}
                  onClick={this.getAfterDeliveredOrder}
                  disabled={deliveredOrderLoading}
                >
                  {deliveredOrderLoading ? (
                    <Spin spinning={deliveredOrderLoading} />
                  ) : (
                    <label className={s.moreCursor}>بیشتر</label>
                  )}
                </CPButton>
              )}
            </TabPane>
            <TabPane tab={CANCELED_ORDERS} key="3">
              {cancelOrders.map(item => (
                <div key={item.id} className={s.ordersList}>
                  <div className={s.orderDetail}>
                    <ul className={s.header}>
                      <li>شماره سفارش</li>
                      <li>تاریخ سفارش</li>
                      <li>تخفیف</li>
                      <li>هزینه حمل</li>
                      <li>قیمت کل</li>
                      <li />
                    </ul>

                    <ul className={s.data}>
                      <li>{item.id}</li>
                      <li>{item.persianCreatedDateTimeUtc}</li>
                      <li>{item.stringTotalDiscount}</li>
                      <li>{item.stringTotalShipmentAmount}</li>
                      <li>{item.stringPayableBillingAmount}</li>
                      <li>
                        <CPButton
                          type="circle"
                          className={s.upAndDownBtn}
                          onClick={() => this.toggleMe(item.id)}
                        >
                          <i className="mf-double-down-arrow" id={item.id} />
                        </CPButton>
                      </li>
                    </ul>
                    <CPButton
                      type="circle"
                      name={item.id}
                      className={s.upDownBtn}
                      onClick={() => this.toggleMe(item.id)}
                    >
                      <i
                        className="mf-double-down-arrow"
                        id={`${item.id}app`}
                      />
                    </CPButton>
                    <span className={s.ordersAvatar}>
                      {item.billItemDtos.map(item2 => (
                        <CPAvatar
                          src={
                            item2.vendorBranchProductDto.productDto.imageDtos
                              ? item2.vendorBranchProductDto.productDto
                                  .imageDtos[0].url
                              : ''
                          }
                          key={item2.id}
                        />
                      ))}
                    </span>
                  </div>
                  {item.billItemDtos.map(item2 => (
                    <div
                      key={item2.id}
                      className="toggleBox hide"
                      name={item.id}
                    >
                      <div className={s.productDetail}>
                        <div className={s.rightContent}>
                          <Link
                            to={`/product-details/${
                              item2.vendorBranchProductDto.productDto.id
                            }/${
                              item2.vendorBranchProductDto.vendorBranchDto.id
                            }`}
                            target="_blank"
                          >
                            <CPAvatar
                              src={
                                item2.vendorBranchProductDto.productDto
                                  .imageDtos
                                  ? item2.vendorBranchProductDto.productDto
                                      .imageDtos[0].url
                                  : ''
                              }
                              key={item2.id}
                            />
                          </Link>
                          <span>
                            <h4>
                              {item2.vendorBranchProductDto.productDto.name}
                            </h4>
                            <p>
                              فروشنده:
                              {
                                item2.vendorBranchProductDto.vendorBranchDto
                                  .name
                              }
                            </p>
                            <p> تعداد: {item2.count}</p>
                          </span>
                        </div>
                        <div className={s.leftContent}>
                          <ul>
                            <li>
                              <label>تخفیف :</label>
                              <p>
                                {item2.stringTotalDiscount} {TOMAN}
                              </p>
                            </li>
                            <li>
                              <label>هزینه حمل :</label>
                              <p>
                                {item2.stringShipmentAmount} {TOMAN}
                              </p>
                            </li>
                            <li>
                              <label>قیمت :</label>
                              <p>
                                {item2.stringTotalAmount} {TOMAN}
                              </p>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className={s.deliveryType}>
                        <span>
                          <label>{ORDER_STATUS} :</label>
                          <b className={s.status}>
                            {item2.billItemStatusTitle}
                          </b>
                        </span>
                        <span>
                          <label>{DELIVERY_TYPE} :</label>
                          <b>{item2.priceOption}</b>
                        </span>
                      </div>
                      {item2.priceOption === 'Pickup' && (
                        <div className={s.deliveryType}>
                          <span>
                            <label>{ADDRESS} :</label>
                            <b className={s.status}>
                              {item2.vendorBranchDto &&
                              item2.vendorBranchDto.address}
                            </b>
                          </span>
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              ))}
              {cancelOrders.length < cancelOrderCount && (
                <CPButton
                  className={s.moreButton}
                  onClick={this.getAfterCancelOrder}
                  disabled={cancelOrderLoading}
                >
                  {cancelOrderLoading ? (
                    <Spin spinning={cancelOrderLoading} />
                  ) : (
                    <label className={s.moreCursor}>بیشتر</label>
                  )}
                </CPButton>
              )}
            </TabPane>
          </Tabs>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  inProgressOrders: state.samOrder.inProgressOrderListData
    ? state.samOrder.inProgressOrderListData.items
    : [],
  inProgressOrderCount: state.samOrder.inProgressOrderListData
    ? state.samOrder.inProgressOrderListData.count
    : 0,
  inProgressOrderLoading: state.samOrder.inProgressOrderListLoading,
  deliveredOrders: state.samOrder.deliveredOrderListData
    ? state.samOrder.deliveredOrderListData.items
    : [],
  deliveredOrderCount: state.samOrder.deliveredOrderListData
    ? state.samOrder.deliveredOrderListData.count
    : 0,
  deliveredOrderLoading: state.samOrder.deliveredOrderListLoading,
  cancelOrders: state.samOrder.cancelOrderListData
    ? state.samOrder.cancelOrderListData.items
    : [],
  cancelOrderCount: state.samOrder.cancelOrderListData
    ? state.samOrder.cancelOrderListData.count
    : 0,
  cancelOrderLoading: state.samOrder.cancelOrderListLoading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      deliveredOrderListSuccess,
      deliveredOrderListFailure,
      cancelOrderListSuccess,
      cancelOrderListFailure,
      inProgressOrderListSuccess,
      inProgressOrderListFailure,
      inProgressOrderListRequest,
      deliveredOrderListRequest,
      cancelOrderListRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Orders2));
