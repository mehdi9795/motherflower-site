import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Event.css';
import {
  SAVE,
  CANCEL,
  YES,
  NO,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT,
  NAME,
  EVENT_LIST,
  EVENT,
  RELATION,
  DATE,
  DAYS_LEFT,
  EVENT_TYPE,
  BIRTHDAY_MONTH,
  BIRTHDAY_DAY,
  MOBILE,
} from '../../../Resources/Localization';
import CPButton from '../../CP/CPButton';
import CPInput from '../../CP/CPInput';
import CPSelect from '../../CP/CPSelect';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  deleteUserEventApi,
  getUserEventApi,
  getUserEventTypeApi,
  getUserRelationShipApi,
  postUserEventApi,
  putUserEventApi,
} from '../../../services/identityApi';
import { getCookie } from '../../../utils';
import CPPopConfirm from '../../CP/CPPopConfirm';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import {
  userEventSuccess,
  userEventTypeSuccess,
  userRelationShipSuccess,
} from '../../../redux/identity/action/user';
import {
  UserEvent,
  UserEventType,
  UserRelationShip,
} from '../../../dtos/identityDtos';
import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../../redux/shared/action/loading';
import $ from 'jquery';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';

const { Option } = Select;

class Event extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    userEventTypes: PropTypes.arrayOf(PropTypes.object),
    userEvents: PropTypes.arrayOf(PropTypes.object),
    userRelationShips: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    userEventTypes: [],
    userEvents: [],
    userRelationShips: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      eventType: '',
      userRelation: '',
      name: '',
      day: 1,
      month: '1',
      isWeb: false,
      id: 0,
      phoneNumberFinal: '',
      phoneNumber: '',
      isValidPhoneNumber: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }

  async componentWillMount() {
    const token = getCookie('siteToken');
    const responseEventType = await getUserEventTypeApi(token, { dto: {} });
    if (responseEventType.status === 200) {
      this.props.actions.userEventTypeSuccess(responseEventType.data);
    } else this.props.actions.userEventTypeFailure();

    const responseUserRelationShip = await getUserRelationShipApi(token,
      getDtoQueryString(new BaseGetDtoBuilder().dto({published: true}).orderByFields(['DisplayOrder']).buildJson()),
    );
    if (responseUserRelationShip.status === 200)
      this.props.actions.userRelationShipSuccess(responseUserRelationShip.data);
    else this.props.actions.userRelationShipFailure();
    this.setState({
      eventType: responseEventType.data.items[0].id.toString(),
      userRelation: responseUserRelationShip.data.items[0].id.toString(),
    });
    this.getUserEvent();
  }

  componentDidMount() {
    this.updatePredicate();
  }

  async getUserEvent() {
    const token = getCookie('siteToken');
    const responseUserEvent = await getUserEventApi(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto({published: true})
          .includes(['userEventTypeDto', 'userRelationShipDto'])
          .buildJson(),
      ),
    );
    if (responseUserEvent.status === 200)
      this.props.actions.userEventSuccess(responseUserEvent.data);
    else this.props.actions.userEventFailure();
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  onCancel = () => {
    const element = document.getElementById('addNewAddress');
    const elementButton = document.getElementById('add-button');
    elementButton.className = 'addButton';
    element.className = 'AddressList-addNewAddress-31y5A hideAddNewAddress';
  };

  async onSubmit() {
    const {
      eventType,
      userRelation,
      name,
      day,
      month,
      id,
      phoneNumberFinal,
      isValidPhoneNumber,
    } = this.state;
    const { userEventTypes, userRelationShips } = this.props;

    // if (!isValidPhoneNumber) {
    //   showNotification(
    //     'error',
    //     '',
    //     'لطفا تلفن همراه را صحیح وارد کنید!',
    //     7,
    //     'errorBox',
    //   );
    //   return;
    // }
    const token = getCookie('siteToken');
    this.props.actions.loadingSearchShowRequest();
    const eventCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserEvent({
          userEventTypeDto: new UserEventType({ id: eventType }),
          userRelationShipDto: new UserRelationShip({ id: userRelation }),
          name,
          eventDay: day,
          eventMonth: month,
          phone: phoneNumberFinal,
          id,
        }),
      )
      .build();

    if (id === 0) {
      const response = await postUserEventApi(token, eventCrud);
      this.props.actions.loadingSearchHideRequest();
      if (response.status === 200) {
        this.getUserEvent();
        showNotification(
          'success',
          '',
          'رویداد مورد نظر با موفقیت اضافه شد.',
          7,
        );
      } else
        showNotification(
          'error',
          '',
          'در ایجاد رویداد خطایی رخ داده است.',
          7,
          'errorBox',
        );
    } else {
      const response = await putUserEventApi(token, eventCrud);
      this.props.actions.loadingSearchHideRequest();
      if (response.status === 200) {
        this.getUserEvent();
        showNotification(
          'success',
          '',
          'رویداد مورد نظر با موفقیت ویرایش شد.',
          7,
        );
      } else
        showNotification(
          'error',
          '',
          'در ویرایش رویداد خطایی رخ داده است.',
          7,
          'errorBox',
        );
    }
    this.setState({
      eventType: userEventTypes ? userEventTypes[0].id.toString() : '',
      userRelation: userRelationShips ? userRelationShips[0].id.toString() : '',
      id: 0,
      name: '',
      day: 1,
      month: '1',
    });
    this.onCancel();
  }

  async onDelete(id) {
    this.props.actions.loadingSearchShowRequest();
    const token = getCookie('siteToken');
    const response = await deleteUserEventApi(token, id, true);
    this.props.actions.loadingSearchHideRequest();
    if (response.status === 200) {
      this.getUserEvent();
      showNotification('success', '', 'رویداد با موفقیت حذف شد.', 7);
    } else {
      showNotification(
        'error',
        '',
        'در حذف رویداد خطایی رخ داده است.',
        7,
        'errorBox',
      );
    }
  }

  async onEdit(record) {
    const elementForm = document.getElementById('addNewAddress');
    elementForm.className = 'AddressList-addNewAddress-31y5A showAddNewAddress';

    const elementButton = document.getElementById('add-button');
    elementButton.className = 'hide-button';

    const footerHeight = document.getElementById('footer').offsetHeight;
    const formHeight = document.getElementById('addNewAddress').offsetHeight;
    $('html, body').animate(
      {
        scrollTop: document.body.scrollHeight - formHeight - footerHeight - 250,
      },
      1000,
    );

    this.setState({
      name: record.name,
      day: record.eventDay,
      month: record.eventMonth.toString(),
      eventType: record.userEventTypeDto.id.toString(),
      userRelation: record.userRelationShipDto.id.toString(),
      id: record.id,
    });
  }

  showForm = () => {
    this.onCancel();

    const elementForm = document.getElementById('addNewAddress');
    elementForm.className = 'AddressList-addNewAddress-31y5A showAddNewAddress';

    const elementButton = document.getElementById('add-button');
    elementButton.className = 'hide-button';
  };

  handleChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneNumberFinal: number.replace(/ /g, ''),
        phoneNumber: value,
        isValidPhoneNumber: status,
      });
    }
  };

  render() {
    const { userRelationShips, userEventTypes, userEvents } = this.props;
    const {
      eventType,
      userRelation,
      name,
      day,
      month,
      isWeb,
      phoneNumber,
    } = this.state;

    const userEventTypeDataSource = [];
    const userRelationShipDataSource = [];
    const dayDataSource = [];

    /**
     * map district for comboBox Datasource
     */
    userRelationShips.map(item =>
      userRelationShipDataSource.push(
        <Option key={item.id.toString()}>{item.title}</Option>,
      ),
    );

    /**
     * map cities for comboBox Datasource
     */
    userEventTypes.map(item =>
      userEventTypeDataSource.push(
        <Option key={item.id.toString()}>{item.title}</Option>,
      ),
    );

    for (let item = 1; item <= 31; item += 1) {
      dayDataSource.push(<Option key={item.toString()}>{item}</Option>);
    }

    return isWeb ? (
      <div className={s.addressList}>
        <h3 className="mobileViewTitle">
          <i className="mf-pin" />
          {EVENT_LIST}
        </h3>
        <div className={s.wrapper}>
          <ul className={s.tableHeader}>
            <li>
              <b>{EVENT}</b>
            </li>
            <li>
              <b>{RELATION}</b>
            </li>
            <li>
              <b>{NAME}</b>
            </li>
            <li>
              <b>{MOBILE}</b>
            </li>
            <li>
              <b>{DATE}</b>
            </li>
            <li>
              <b>{DAYS_LEFT}</b>
            </li>
            <li />
            <li />
          </ul>

          <ul>
            {userEvents.map(item => (
              <div key={item.id} className={s.tableData}>
                <li>
                  <p>{item.userEventTypeDto.title}</p>
                </li>
                <li>
                  <p>{item.userRelationShipDto.title}</p>
                </li>
                <li>
                  <p>{item.name}</p>
                </li>
                <li>
                  <p>{item.phone ? item.phone.replace('+98', '0') : ''}</p>
                </li>
                <li>
                  <p>{item.persinaDayMonth}</p>
                </li>
                <li>
                  <p>{item.leftOverDay}</p>
                </li>
                <li>
                   <CPButton onClick={() => this.onEdit(item)}>
                    <i className="mf-edit" />
                  </CPButton>
                </li>
                <li>
                  <CPPopConfirm
                    title={ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT}
                    okText={YES}
                    cancelText={NO}
                    okType="primary"
                    onConfirm={() => this.onDelete(item.id)}
                  >
                    <CPButton className="delete_action">
                      <i className="mf-trash" />
                    </CPButton>
                  </CPPopConfirm>
                </li>
              </div>
            ))}
          </ul>
        </div>
        <div className="addButton" id="add-button">
          <CPButton className={s.addBtn} shape="circle" onClick={this.showForm}>
            <i className="mf-add" />
          </CPButton>
        </div>
        <div className={s.addNewAddress} id="addNewAddress">
          <div className="eventForm">
            <div>
              <label>{EVENT_TYPE}</label>
              <CPSelect
                value={eventType}
                onChange={value => this.handleChangeInput('eventType', value)}
                className={s.comboBox}
              >
                {userEventTypeDataSource}
              </CPSelect>
            </div>
            <div>
              <label>{RELATION}</label>
              <CPSelect
                value={userRelation}
                onChange={value =>
                  this.handleChangeInput('userRelation', value)
                }
                className={s.comboBox}
              >
                {userRelationShipDataSource}
              </CPSelect>
            </div>
            <div>
              <label>{NAME}</label>
              <CPInput
                value={name}
                onChange={value =>
                  this.handleChangeInput('name', value.target.value)
                }
                className={s.inputName}
              />
            </div>
          </div>
          <div className="eventForm">
            <div>
              <label>{BIRTHDAY_DAY}</label>
              <CPSelect
                value={day}
                onChange={value => this.handleChangeInput('day', value)}
                className={s.comboBox}
              >
                {dayDataSource}
              </CPSelect>
            </div>
            <div>
              <label>{BIRTHDAY_MONTH}</label>
              <CPSelect
                value={month}
                onChange={value => this.handleChangeInput('month', value)}
                className={s.comboBox}
              >
                <Option value="1" key="1">
                  فروردین
                </Option>
                <Option value="2" key="2">
                  اردیبهشت
                </Option>
                <Option value="3" key="3">
                  خرداد
                </Option>
                <Option value="4" key="4">
                  تیر
                </Option>
                <Option value="5" key="5">
                  مرداد
                </Option>
                <Option value="6" key="6">
                  شهریور
                </Option>
                <Option value="7" key="7">
                  مهر
                </Option>
                <Option value="8" key="8">
                  آبان
                </Option>
                <Option value="9" key="9">
                  آذر
                </Option>
                <Option value="10" key="10">
                  دی
                </Option>
                <Option value="11" key="11">
                  بهمن
                </Option>
                <Option value="12" key="12">
                  اسفند
                </Option>
              </CPSelect>
            </div>
            <div>
              <label>{MOBILE}</label>
              <CPIntlPhoneInput
                value={phoneNumber}
                onChange={this.changeNumber}
              />
            </div>
          </div>
          <div className={s.eventBtn}>
            <CPButton onClick={this.onSubmit}>{SAVE}</CPButton>
            <CPButton onClick={this.onCancel}>{CANCEL}</CPButton>
          </div>
        </div>
      </div>
    ) : (
      <div className={s.addressList}>
        <h3 className="mobileViewTitle">{EVENT_LIST}</h3>
        {userEvents.map(item => (
          <div className={s.mobileView}>
            <ul>
              <li>
                <b>{EVENT}</b>
              </li>
              <li>
                <b>{RELATION}</b>
              </li>
              <li>
                <b>{NAME}</b>
              </li>
              <li>
                <b>{MOBILE}</b>
              </li>
              <li>
                <b>{DATE}</b>
              </li>
              <li>
                <b>{DAYS_LEFT}</b>
              </li>
              <li>
                 <CPButton
                  className={s.editButton}
                  onClick={() => this.onEdit(item)}
                >
                  <i className="mf-edit" />
                </CPButton>
              </li>
            </ul>
            <ul>
              <li>{item.userEventTypeDto.title}</li>
              <li>{item.userRelationShipDto.title}</li>
              <li>{item.name}</li>
              <li>{item.phone ? item.phone.replace('+98', '0') : ''}</li>
              <li>{item.persinaDayMonth}</li>
              <li>{item.leftOverDay}</li>
              <li>
                <CPPopConfirm
                  title={ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT}
                  okText={YES}
                  cancelText={NO}
                  okType="primary"
                  onConfirm={() => this.onDelete(item.id)}
                >
                  <CPButton className="delete_action">
                    <i className="mf-trash" />
                  </CPButton>
                </CPPopConfirm>
              </li>
            </ul>
          </div>
        ))}

        <div className="addButton" id="add-button">
          <CPButton className={s.addBtn} shape="circle" onClick={this.showForm}>
            <i className="mf-add" />
          </CPButton>
        </div>
        <div className={s.addNewAddress} id="addNewAddress">
          <div className={s.mobileViewForm}>
            <div>
              <label>{EVENT_TYPE}</label>
              <CPSelect
                value={eventType}
                onChange={value => this.handleChangeInput('eventType', value)}
                className={s.comboBox}
              >
                {userEventTypeDataSource}
              </CPSelect>
            </div>
            <div>
              <label>{RELATION}</label>
              <CPSelect
                value={userRelation}
                onChange={value =>
                  this.handleChangeInput('userRelation', value)
                }
                className={s.comboBox}
              >
                {userRelationShipDataSource}
              </CPSelect>
            </div>
            <div>
              <label>{BIRTHDAY_DAY}</label>
              <CPSelect
                value={day}
                onChange={value => this.handleChangeInput('day', value)}
                className={s.comboBox}
              >
                {dayDataSource}
              </CPSelect>
            </div>
            <div>
              <label>{BIRTHDAY_MONTH}</label>
              <CPSelect
                value={month}
                onChange={value => this.handleChangeInput('month', value)}
                className={s.comboBox}
              >
                <Option value="1" key="1">
                  فروردین
                </Option>
                <Option value="2" key="2">
                  اردیبهشت
                </Option>
                <Option value="3" key="3">
                  خرداد
                </Option>
                <Option value="4" key="4">
                  تیر
                </Option>
                <Option value="5" key="5">
                  مرداد
                </Option>
                <Option value="6" key="6">
                  شهریور
                </Option>
                <Option value="7" key="7">
                  مهر
                </Option>
                <Option value="8" key="8">
                  آبان
                </Option>
                <Option value="9" key="9">
                  آذر
                </Option>
                <Option value="10" key="10">
                  دی
                </Option>
                <Option value="11" key="11">
                  بهمن
                </Option>
                <Option value="12" key="12">
                  اسفند
                </Option>
              </CPSelect>
            </div>
            <div>
              <label>{NAME}</label>
              <CPInput
                value={name}
                onChange={value =>
                  this.handleChangeInput('name', value.target.value)
                }
                className={s.inputName}
              />
            </div>
            <div>
              <label>{MOBILE}</label>
              <CPIntlPhoneInput
                value={phoneNumber}
                onChange={this.changeNumber}
              />
            </div>

          </div>
          <div className={s.eventBtn}>
            <CPButton onClick={this.onSubmit}>{SAVE}</CPButton>
            <CPButton onClick={this.onCancel}>{CANCEL}</CPButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userEvents: state.identityUser.userEventData
    ? state.identityUser.userEventData.items
    : [],
  userEventTypes: state.identityUser.userEventTypeData
    ? state.identityUser.userEventTypeData.items
    : [],
  userRelationShips: state.identityUser.userRelationShipData
    ? state.identityUser.userRelationShipData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      userEventTypeSuccess,
      userEventSuccess,
      userRelationShipSuccess,
      loadingSearchShowRequest,
      loadingSearchHideRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Event));
