import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd/lib/index';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import $ from 'jquery';
import s from './AddressList.css';
import {
  ADDRESS_LIST,
  RECEIVER_NAME,
  ENTER_THE_EXACT_ADDRESS,
  POSTAL_ADDRESS,
  DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP,
  SAVE,
  CANCEL,
  YES,
  NO,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_ADDRESS,
} from '../../../Resources/Localization';
import CPButton from '../../CP/CPButton';
import CPMap from '../../CP/CPMap';
import CPInput from '../../CP/CPInput';
import CPSelect from '../../CP/CPSelect';
import {
  getCityApi,
  getDistrictApi,
  getNearestLocationsApi,
} from '../../../services/commonApi';
import {
  ellipsisString,
  getDtoQueryString,
  showNotification,
} from '../../../utils/helper';
import { CityDto, DistrictDto } from '../../../dtos/commonDtos';
import { districtOptionListSuccess } from '../../../redux/common/action/district';
import { AddressDto } from '../../../dtos/identityDtos';
import {
  DeleteAddressUser,
  GetAddressUser,
  PostAddressUser,
  PutAddressUser,
} from '../../../services/identityApi';
import { getCookie } from '../../../utils';
import {
  addressDeleteFailure,
  addressDeleteRequest,
  addressDeleteSuccess,
  addressListFailure,
  addressListRequest,
  addressListSuccess,
  addressPostFailure,
  addressPostRequest,
  addressPostSuccess,
  addressPutFailure,
  addressPutRequest,
  addressPutSuccess,
} from '../../../redux/identity/action/address';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import CPPopConfirm from '../../CP/CPPopConfirm';
import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../../redux/shared/action/loading';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import CPValidator from '../../CP/CPValidator';

const { Option } = Select;

class AddressList extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    addresses: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object).isRequired,
    districtOptions: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    addresses: null,
    districtOptions: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      receiveName: '',
      phone: '',
      cityId:
        props.cities && props.cities[0] ? props.cities[0].id.toString() : '',
      districtId:
        props.districtOptions && props.districtOptions[0]
          ? `${props.districtOptions[0].id.toString()}*${
              props.districtOptions[0].lat
            }*${props.districtOptions[0].lng}`
          : '',
      content: '',
      lat:
        props.districtOptions && props.districtOptions[0]
          ? props.districtOptions[0].lat
          : 35.6891975,
      lng:
        props.districtOptions && props.districtOptions[0]
          ? props.districtOptions[0].lng
          : 51.3889736,
      id: 0,
      showMessagePhone: false,
      messagePhoneNumber: '',
      // userNameFinal: '',
      phoneNumber: '',
      isWeb: false,
    };
    this.getAddress = this.getAddress.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.setLatAndLng = this.setLatAndLng.bind(this);
    this.validation = [];
  }

  componentDidMount() {
    if (this.state.cityId) this.cityChange('cityId', this.state.cityId);
    this.updatePredicate();
  }

  onCancel = () => {
    const { cities, districtOptions } = this.props;
    this.setState({
      receiveName: '',
      phone: '',
      cityId: cities && cities[0] ? cities[0].id.toString() : '',
      districtId:
        districtOptions && districtOptions[0]
          ? `${districtOptions[0].id.toString()}*${districtOptions[0].lat}*${
              districtOptions[0].lng
            }`
          : '',
      content: '',
      lat:
        districtOptions && districtOptions[0]
          ? districtOptions[0].lat
          : 35.6891975,
      lng:
        districtOptions && districtOptions[0]
          ? districtOptions[0].lng
          : 51.3889736,
      id: 0,
      errorValid: false,
      showMessagePhone: true,
    });

    const element = document.getElementById('addNewAddress');
    const elementButton = document.getElementById('add-button');
    elementButton.className = 'addButton';
    element.className = 'AddressList-addNewAddress-31y5A hideAddNewAddress';
  };

  async onSubmit() {
    const {
      receiveName,
      phone,
      cityId,
      districtId,
      content,
      lat,
      lng,
      id,
      showMessagePhone,
    } = this.state;
    if (this.validation.length === 2 && !showMessagePhone) {
      const token = getCookie('siteToken');
      this.props.actions.loadingSearchShowRequest();

      const addressCrud = new BaseCRUDDtoBuilder()
        .dto(
          new AddressDto({
            id,
            content,
            receiverName: receiveName,
            phone,
            cityId,
            districtId: districtId.split('*')[0],
            lat,
            lng,
          }),
        )
        .build();

      let response;
      if (id === 0) response = await PostAddressUser(token, addressCrud, true);
      else response = await PutAddressUser(token, addressCrud, true);
      this.props.actions.loadingSearchHideRequest();
      if (response.status === 200) {
        this.props.actions.addressPostSuccess();
        this.getAddress();
        if (id === 0)
          showNotification('success', '', 'آدرس با موفقیت اضافه شد.', 10);
        else showNotification('success', '', 'آدرس با موفقیت ویرایش شد.', 10);
        this.setState({ phoneNumber: '', receiveName: '', content: '' });
      } else {
        this.props.actions.addressPostFailure();
        this.props.actions.showAlertRequest(response.data.errorMessage);
      }

      const element = document.getElementById('addNewAddress');
      const elementButton = document.getElementById('add-button');
      elementButton.className = 'addButton';
      element.className = 'AddressList-addNewAddress-31y5A hideAddNewAddress';
    } else {
      this.setState({
        errorValid: true,
        messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  }

  async onEdit(record) {
    const elementForm = document.getElementById('addNewAddress');
    elementForm.className = 'AddressList-addNewAddress-31y5A showAddNewAddress';

    const elementButton = document.getElementById('add-button');
    elementButton.className = 'hide-button';

    const footerHeight = document.getElementById('footer').offsetHeight;
    const formHeight = document.getElementById('addNewAddress').offsetHeight;
    $('html, body').animate(
      { scrollTop: document.body.scrollHeight - formHeight - footerHeight - 90 },
      1000,
    );

    this.cityChange('cityId', record.cityId);
    setTimeout(() => {
      this.setState({
        receiveName: record.receiverName,
        phoneNumber: record.phone.replace('+98', '0'),
        phone: record.phone.replace(/ /g, ''),
        cityId: record.cityId.toString(),
        districtId: `${record.districtId}*${record.districtDto.lat}*${
          record.districtDto.lng
        }`,
        content: record.content,
        lat: record.lat,
        lng: record.lng,
        id: record.id,
        showMessagePhone: false,
      });
    }, 2500);
  }

  async onDelete(id) {
    this.props.actions.loadingSearchShowRequest();
    const token = getCookie('siteToken');
    const response = await DeleteAddressUser(token, id, true);
    this.props.actions.loadingSearchHideRequest();
    if (response.status === 200) {
      this.getAddress();
      showNotification('success', '', 'آدرس با موفقیت حذف شد.', 10);
    } else {
      this.props.actions.showAlertRequest(response.data.errorMessage);
    }
  }

  async setLatAndLng(value) {
    this.setState({ lat: value.lat, lng: value.lng });
    const response = await getNearestLocationsApi(
      getDtoQueryString(
        JSON.stringify({
          dto: { lat: value.lat, lng: value.lng },
          fromCache: true,
        }),
      ),
    );

    if (
      response.status === 200 &&
      response.data.items &&
      response.data.items.length > 0
    ) {
      this.cityChange(
        'cityId',
        response.data.items[0].cityDto.id.toString(),
        `${response.data.items[0].id.toString()}*${
          response.data.items[0].lat
        }*${response.data.items[0].lng}`,
      );
      // this.setState({
      //   cityId: response.data.items[0].cityDto.id.toString(),
      //   districtId: `${response.data.items[0].id.toString()}*${
      //     response.data.items[0].lat
      //   }*${response.data.items[0].lng}`,
      // });
    } else {
      showNotification(
        'error',
        '',
        'محله مورد نظر تحت پوشش نیست',
        15,
        'errorBox',
      );
    }
  }

  async getAddress() {
    const token = getCookie('siteToken');
    this.props.actions.addressListRequest();

    const response = await GetAddressUser(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new AddressDto({ active: true }))
          .includes(['districtDto'])
          .buildJson(),
      ),
      true,
    );
    if (response.status === 200) {
      this.props.actions.addressListSuccess(response.data);
    } else {
      this.props.actions.addressListFailure();
    }
  }

  async cityChange(inputName, value, districtId) {
    const response = await getDistrictApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new DistrictDto({
              cityDto: new CityDto({ id: value }),
            }),
          )
          .buildJson(),
      ),
    );

    if (response.status === 200) {
      this.props.actions.districtOptionListSuccess(response.data);

      if (response.data.items.length === 0) {
        /**
         * get single city by id
         */
        const responseCity = await getCityApi(
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(
                new CityDto({
                  id: value,
                }),
              )
              .buildJson(),
          ),
        );

        if (responseCity.status === 200) {
          this.setState({
            lat: responseCity.data.items[0].lat,
            lng: responseCity.data.items[0].lng,
          });
        }
      } else {
        this.setState({
          [inputName]: value,
          districtId: districtId !== undefined ? districtId : '0',
        });
      }

      this.setState({
        [inputName]: value,
      });
    }
  }

  showForm = () => {
    this.onCancel();

    const elementForm = document.getElementById('addNewAddress');
    elementForm.className = 'AddressList-addNewAddress-31y5A showAddNewAddress';

    const elementButton = document.getElementById('add-button');
    elementButton.className = 'hide-button';
  };

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value });
    if (inputName === 'districtId') {
      const position = value.split('*');
      this.setState({
        lat: parseFloat(position[1]),
        lng: parseFloat(position[2]),
      });
    }
  };

  changeNumber = (status, value, countryCode, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phone: number.replace(/ /g, ''),
        phoneNumber: value,
        showMessagePhone: !status,
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  render() {
    const { addresses, cities, districtOptions } = this.props;
    const {
      receiveName,
      phoneNumber,
      cityId,
      districtId,
      content,
      lat,
      lng,
      showMessagePhone,
      messagePhoneNumber,
      errorValid,
      isWeb,
    } = this.state;

    const citiesDataSource = [];
    const districtDataSource = [];

    /**
     * map district for comboBox Datasource
     */
    districtOptions.map(item =>
      districtDataSource.push(
        <Option key={`${item.id}*${item.lat}*${item.lng}`}>{item.name}</Option>,
      ),
    );

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(
        <Option key={item.id.toString()}>{item.name}</Option>,
      ),
    );

    return (
      <div className={s.addressList}>
        <h3 className="mobileViewTitle">
          <i className="mf-pin" />
          {ADDRESS_LIST}
        </h3>
        {isWeb ? (
          <div className={s.wrapper}>
            <ul className={s.tableHeader}>
              <li>
                <b>آدرس</b>
              </li>
              <li>
                <b>نام گیرنده</b>
              </li>
              <li>
                <b>تلفن</b>
              </li>
              <li />
              <li />
            </ul>

            <ul>
              {addresses.map(item => (
                <div key={item.id} className={s.tableData}>
                  <li>
                    <p>{item.content}</p>
                  </li>
                  <li>
                    <p>{item.receiverName}</p>
                  </li>
                  <li>
                    <p>{item.phone ? item.phone.replace('+98', '0') : ''}</p>
                  </li>
                  <li>
                    <CPPopConfirm
                      title={ARE_YOU_SURE_WANT_TO_DELETE_THE_ADDRESS}
                      okText={YES}
                      cancelText={NO}
                      okType="primary"
                      onConfirm={() => this.onDelete(item.id)}
                    >
                      <CPButton className="delete_action">
                        <i className="mf-trash" />
                      </CPButton>
                    </CPPopConfirm>
                  </li>
                  <li>
                    <CPButton onClick={() => this.onEdit(item)}>
                      <i className="mf-edit" />
                    </CPButton>
                  </li>
                </div>
              ))}
            </ul>
          </div>
        ) : (
          addresses.map(item => (
            <div className={s.mobileView}>
              <ul>
                <li>
                  <b>آدرس</b>
                </li>
                <li>
                  <b>نام گیرنده</b>
                </li>
                <li>
                  <b>تلفن</b>
                </li>
                <li>
                  <CPButton
                    className={s.editButton}
                    onClick={() => this.onEdit(item)}
                  >
                    <i className="mf-edit" />
                  </CPButton>
                </li>
              </ul>
              <ul>
                <li>{ellipsisString(item.content)}</li>
                <li>{ellipsisString(item.receiverName)}</li>
                <li>{item.phone ? item.phone.replace('+98', '0') : ''}</li>
                <li>
                  <CPPopConfirm
                    title={ARE_YOU_SURE_WANT_TO_DELETE_THE_ADDRESS}
                    okText={YES}
                    cancelText={NO}
                    okType="primary"
                    onConfirm={() => this.onDelete(item.id)}
                  >
                    <CPButton className="delete_action">
                      <i className="mf-trash" />
                    </CPButton>
                  </CPPopConfirm>
                </li>
              </ul>
            </div>
          ))
        )}
        <div className="addButton" id="add-button">
          <CPButton className={s.addBtn} shape="circle" onClick={this.showForm}>
            <i className="mf-add" />
          </CPButton>
        </div>
        <div className={s.addNewAddress} id="addNewAddress">
          <div className="col-md-6 col-sm-12">
            <h5 className={s.modalTitle}>{ENTER_THE_EXACT_ADDRESS}</h5>
            <div className={s.flagBox}>
              <CPIntlPhoneInput
                value={phoneNumber}
                onChange={this.changeNumber}
              />
              <p
                className={s.errorMessage}
                style={{ display: `${!showMessagePhone}` }}
              >
                {messagePhoneNumber}
              </p>
            </div>
            <CPValidator
              value={receiveName}
              checkValidation={this.checkValidation}
              minLength={3}
              showMessage={errorValid}
              name={RECEIVER_NAME}
            >
              <CPInput
                label={RECEIVER_NAME}
                value={receiveName}
                onChange={value =>
                  this.handleInput('receiveName', value.target.value)
                }
              />
            </CPValidator>
            <div className={s.city}>
              <CPSelect
                onChange={value => this.cityChange('cityId', value)}
                value={cityId}
                showSearch
              >
                {citiesDataSource}
              </CPSelect>
            </div>
            <div className={s.city}>
              <CPSelect
                onChange={value => this.handleInput('districtId', value)}
                value={districtId}
                showSearch
              >
                {districtDataSource}
              </CPSelect>
            </div>
            <CPValidator
              value={content}
              checkValidation={this.checkValidation}
              minLength={10}
              showMessage={errorValid}
              name={POSTAL_ADDRESS}
            >
              <CPInput
                label={POSTAL_ADDRESS}
                value={content}
                onChange={value =>
                  this.handleInput('content', value.target.value)
                }
              />
            </CPValidator>
          </div>
          <div className="col-md-6 col-sm-12">
            <h5 className={s.mapTitle}>
              {DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP}
            </h5>
            <CPMap
              containerElement="231px"
              // defaultCenter={{ lat, lng}}
              defaultZoom={15}
              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
              mapElement="242px"
              loadingElement="100%"
              draggable
              onDragEnd={value => this.setLatAndLng(value)}
              lat={lat}
              lng={lng}
            />
            <div className="text-left hiddenBoxBtns">
              <CPButton className={s.save} onClick={this.onSubmit}>
                {SAVE}
              </CPButton>
              <CPButton className={s.cancel} onClick={this.onCancel}>
                {CANCEL}
              </CPButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  addresses: state.identityAddress.addressListData.items,
  cities: state.commonCity.cityListData.items,
  districtOptions: state.commonDistrict.districtOptionData.items,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      districtOptionListSuccess,
      addressListSuccess,
      addressListRequest,
      addressListFailure,
      addressPostFailure,
      addressPostSuccess,
      addressPostRequest,
      addressPutFailure,
      addressPutSuccess,
      addressPutRequest,
      addressDeleteFailure,
      addressDeleteSuccess,
      addressDeleteRequest,
      showAlertRequest,
      loadingSearchHideRequest,
      loadingSearchShowRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(AddressList));
