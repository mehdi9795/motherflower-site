import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Sidebar.css';
import Link from '../../Link';
import {
  ADDRESS_LIST,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_IMAGE,
  BANK_ACCOUNT_NUMBER,
  CHANGE_PASSWORD,
  EDIT_INFORMATION,
  NO,
  YES,
  YOUR_ORDERS,
  EVENT,
  WALLET, ORDERS,
} from '../../../Resources/Localization';
import { changeMenuProfileSuccess } from '../../../redux/shared/action/changeMenuProfile';
import { BankApi, UserBankHistoriesApi } from '../../../services/samApi';
import {
  bankListFailure,
  bankListSuccess,
  cardNumberListFailure,
  cardNumberListSuccess,
} from '../../../redux/sam/action/bank';
import { BankDto } from '../../../dtos/samDtos';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import { getCookie, setCookie } from '../../../utils';
import { AddressDto, UserDto } from '../../../dtos/identityDtos';
import {
  addressListFailure,
  addressListSuccess,
} from '../../../redux/identity/action/address';
import { GetAddressUser, GetUser } from '../../../services/identityApi';
import {
  districtOptionListFailure,
  districtOptionListSuccess,
} from '../../../redux/common/action/district';
import {
  cityListFailure,
  cityListSuccess,
} from '../../../redux/common/action/city';
import { CityDto, DistrictDto } from '../../../dtos/commonDtos';
import { getCityApi, getDistrictApi } from '../../../services/commonApi';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import CPModal from '../../CP/CPModal';
import CPImageEditor from '../../CP/CPImageEditor';
import { ImageDto } from '../../../dtos/cmsDtos';
import {
  deleteImageApi,
  getImageApi,
  postImageApi,
  putImageApi,
} from '../../../services/cmsApi';
import {
  singleUserFailure,
  singleUserSuccess,
} from '../../../redux/identity/action/user';
import { loginSuccess } from '../../../redux/identity/action/account';

class Sidebar extends React.Component {
  static propTypes = {
    loginData: PropTypes.objectOf(PropTypes.any),
    user: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    selectedMenu: PropTypes.string,
  };
  static defaultProps = {
    loginData: {},
    selectedMenu: 'editInfo',
    user: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      menuSelected: props.selectedMenu ? props.selectedMenu : 'wallet',
      showModalImage: false,
      binariesImage: '',
      extention: '',
      id: 0,
      imageBase64: '',
    };
    this.imageEditorFunc = null;
    this.onChangeImage = this.onChangeImage.bind(this);
    this.showModalImage = this.showModalImage.bind(this);
    this.deleteProfileImage = this.deleteProfileImage.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedMenu !== nextProps.selectedMenu) {
      this.setState({ menuSelected: nextProps.selectedMenu });
    }
  }

  async onClick(e, value) {
    e.preventDefault();
    if (value === 'accountNumber') {
      this.getCardNumber();
    } else if (value === 'address') {
      this.getAddress();
    } else if (value === 'editInfo') {
      // this.getUserInfo();
    }

    this.props.actions.changeMenuProfileSuccess(value);
  }

  async onChangeImage(value) {
    this.setState(
      { binariesImage: value.split(',')[1], extention: 'png' },
      () => {
        this.saveImage();
      },
    );
  }

  async getCardNumber() {
    const token = getCookie('siteToken');

    const responseCardNumber = await UserBankHistoriesApi(
      getDtoQueryString(new BaseGetDtoBuilder().dto(new BankDto()).buildJson()),
      token,
    );

    if (responseCardNumber.status === 200) {
      this.props.actions.cardNumberListSuccess(responseCardNumber.data);
    } else this.props.actions.cardNumberListFailure();

    const responseBank = await BankApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new BankDto({
              usedForProfile: true,
            }),
          )
          .buildJson(),
      ),
      token,
    );

    if (responseBank.status === 200) {
      this.props.actions.bankListSuccess(responseBank.data);
    } else this.props.actions.bankListFailure();
  }

  async getAddress() {
    const token = getCookie('siteToken');

    const responseAddress = await GetAddressUser(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new AddressDto({ active: true }))
          .includes(['districtDto'])
          .buildJson(),
      ),
      true,
    );
    if (responseAddress.status === 200) {
      this.props.actions.addressListSuccess(responseAddress.data);
    } else this.props.actions.addressListFailure();

    /**
     * get all cities for first time with default clause
     */
    const responseCity = await getCityApi(
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );
    if (responseCity.status === 200) {
      this.props.actions.cityListSuccess(responseCity.data);

      const responseDistrict = await getDistrictApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new DistrictDto({
                cityDto: new CityDto({ id: responseCity.data.items[0].id }),
              }),
            )
            .buildJson(),
        ),
      );

      if (responseDistrict.status === 200) {
        this.props.actions.districtOptionListSuccess(responseDistrict.data);
      } else this.props.actions.districtOptionListFailure();
    } else this.props.actions.cityListFailure();
  }

  async saveImage() {
    const { user } = this.props;
    const { binariesImage, extention, id } = this.state;
    const token = getCookie('siteToken');
    this.setState({ showModalImage: false });
    const jsonCrud = new BaseCRUDDtoBuilder()
      .dto(
        new ImageDto({
          entityId: user.id,
          binaries: binariesImage,
          extention,
          isCover: true,
          imagetype: 'User',
          id,
        }),
      )
      .build();

    let response = null;
    if (id === 0) response = await postImageApi(token, jsonCrud);
    else response = await putImageApi(token, jsonCrud);

    if (response.status === 200) {
      const responseUser = await GetUser(
        token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new UserDto({
                fromMobile: true,
              }),
            )
            .includes(['imageDto'])
            .buildJson(),
        ),
        true,
      );

      if (responseUser.status === 200) {
        this.props.actions.singleUserSuccess(responseUser.data);
        this.setLoginData(
          responseUser.data.items.length > 0
            ? responseUser.data.items[0].imageDto.url
            : '',
        );
      } else this.props.actions.singleUserFailure();
    }
  }

  async showModalImage() {
    const { user } = this.props;
    const token = getCookie('siteToken');
    if (user.imageDto) {
      const json = new BaseGetDtoBuilder()
        .dto(
          new ImageDto({
            id: user.imageDto.id,
            imageSizeType: 'small',
          }),
        )
        .includes(['binaries'])
        .buildJson();
      const responseImage = await getImageApi(token, getDtoQueryString(json));
      if (responseImage.status === 200)
        this.setState({
          showModalImage: true,
          id: user.imageDto.id,
          imageBase64:
            responseImage.data.items.length > 0
              ? `data:image/jpeg;base64,${responseImage.data.items[0].binaries}`
              : '',
        });
    } else this.setState({ showModalImage: true, id: 0 });
  }

  closeModalImage = () => {
    this.setState({ showModalImage: false });
  };

  okModalImage = () => {
    this.imageEditorFunc.onClickSave();
  };

  async deleteProfileImage() {
    const { user } = this.props;

    const token = getCookie('siteToken');
    const response = await deleteImageApi(token, user.imageDto.id);
    if (response.status === 200) {
      showNotification('success', '', 'تصویر با موفقیت حذف شد.', 10);
      const responseUser = await GetUser(
        token,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new UserDto({
                fromMobile: true,
              }),
            )
            .includes(['imageDto'])
            .buildJson(),
        ),
        true,
      );

      if (responseUser.status === 200)
        this.props.actions.singleUserSuccess(responseUser.data);
      else this.props.actions.singleUserFailure();
      this.setLoginData('');
      this.setState({ showModalImage: false });
    }
  }

  setLoginData = value => {
    const { loginData } = this.props;
    const obj = {
      token: loginData.token,
      userName: loginData.userName,
      displayName: loginData.displayName,
      imageUser: value,
    };

    this.props.actions.loginSuccess(obj);
    setCookie('imageUser', value, 8760);
  };

  render() {
    const { menuSelected, imageBase64 } = this.state;
    const { loginData, user } = this.props;
    return (
      <ul className={s.profileMenu}>
        <li className={s.proItems}>
          {user.imageDto ? (
            <div className={s.parentAvatar}>
              <img
                className={s.avatar}
                alt="avatar"
                width={64}
                height={64}
                src={`${user.imageDto.url}?dummy=${Math.random()}`}
                onClick={this.showModalImage}
              />
            </div>
          ) : (
            <i className="mf-user" onClick={this.showModalImage} />
          )}
          <b className={s.userName}>{loginData.displayName}</b>
        </li>

        <li className={cs(s.proItems, menuSelected === 'editInfo' && s.active)}>
          <Link to="/" onClick={e => this.onClick(e, 'editInfo')}>
            <i className="mf-edit" />
            {EDIT_INFORMATION}
          </Link>
        </li>
        <li className={cs(s.proItems, menuSelected === 'order' && s.active)}>
          <Link to="/" onClick={e => this.onClick(e, 'order')}>
            <i className="mf-squares" />
            {ORDERS}
          </Link>
        </li>
        <li className={cs(s.proItems, menuSelected === 'address' && s.active)}>
          <Link to="/" onClick={e => this.onClick(e, 'address')}>
            <i className="mf-pin" />
            {ADDRESS_LIST}
          </Link>
        </li>
        <li className={cs(s.proItems, menuSelected === 'event' && s.active)}>
          <Link to="/" onClick={e => this.onClick(e, 'event')}>
            <i className="mf-calendar" />
            {EVENT}
          </Link>
        </li>
        <li className={cs(s.proItems, menuSelected === 'wallet' && s.active)}>
          <Link to="/" onClick={e => this.onClick(e, 'wallet')}>
            <i className="mf-wallet" />
            {WALLET}
          </Link>
        </li>
        <li
          className={cs(
            s.proItems,
            menuSelected === 'accountNumber' && s.active,
          )}
        >
          <Link to="/" onClick={e => this.onClick(e, 'accountNumber')}>
            <i className="mf-credit-card" />
            {BANK_ACCOUNT_NUMBER}
          </Link>
        </li>
        <li className={cs(s.proItems, menuSelected === 'password' && s.active)}>
          <Link to="/" onClick={e => this.onClick(e, 'password')}>
            <i className="mf-lock" />
            {CHANGE_PASSWORD}
          </Link>
        </li>
        {/* <li className={cs(s.proItems, menuSelected === 'favorite' && s.active)}>
          <Link onClick={e => this.onClick(e, 'favorite')}>
            <i className="mf-favorite-o" />
            {FAVORITE_LIST}
          </Link>
        </li> */}
        {/* <li className={cs(s.proItems, menuSelected === 'comment' && s.active)}>
          <Link onClick={e => this.onClick(e, 'comment')}>
            <i className="mf-chat" />
            {YOUR_COMMENTS}
          </Link>
        </li> */}

        {/* <li
          className={cs(s.proItems, menuSelected === 'reminding' && s.active)}
        >
          <Link onClick={e => this.onClick(e, 'reminding')}>
            <i className="mf-calendar" />
            {REMINDING}
          </Link>
        </li> */}
        <CPModal
          // title="تصویر کاربر"
          handleCancel={this.closeModalImage}
          visible={this.state.showModalImage}
          showFooter={false}
        >
          <CPImageEditor
            image={user.imageDto ? imageBase64 : ''}
            onRef={ref => {
              this.imageEditorFunc = ref;
              return false;
            }}
            onChangeImage={this.onChangeImage}
            width={250}
            height={250}
            onSave={this.okModalImage}
            onDelete={this.deleteProfileImage}
          />
        </CPModal>
      </ul>
    );
  }
}
const mapStateToProps = state => ({
  loginData: state.account.loginData ? state.account.loginData : [],
  user:
    state.identityUser.singleUserData && state.identityUser.singleUserData.items
      ? state.identityUser.singleUserData.items[0]
      : [],
  selectedMenu:
    state.sharedChangeMenu.value === ''
      ? undefined
      : state.sharedChangeMenu.value,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      changeMenuProfileSuccess,
      bankListFailure,
      bankListSuccess,
      cardNumberListFailure,
      cardNumberListSuccess,
      addressListFailure,
      addressListSuccess,
      districtOptionListFailure,
      districtOptionListSuccess,
      cityListFailure,
      cityListSuccess,
      singleUserSuccess,
      singleUserFailure,
      loginSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Sidebar));
