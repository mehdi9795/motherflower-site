import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cs from 'classnames';
import { message } from 'antd';
import s from './Wallet.css';
import {
  APPLY_CODE,
  BALANCE,
  INCREASE_CREDIT,
  HEZAR_TOMAN,
  TOMAN,
  PAYMENT,
  GIFT_CODE,
} from '../../../Resources/Localization';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import {
  loginFailure,
  loginRequest,
  loginSuccess,
} from '../../../redux/identity/action/account';
import {
  walletListFailure,
  walletListSuccess,
} from '../../../redux/sam/action/wallet';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../../redux/shared/action/loading';
import { PersianNumber, showNotification } from '../../../utils/helper';
import {
  BankPaymentRequestsDto,
  WalletDepositHistoriesDto,
} from '../../../dtos/samDtos';
import {
  BankPaymentRequestsApi,
  BankPaymentWalletRequestsApi,
  GetWalletsApi,
  PostWalletDepositHistoriesApi,
} from '../../../services/samApi';
import { getCookie } from '../../../utils';
import { BaseCRUDDtoBuilder } from '../../../dtos/dtoBuilder';

class Wallet extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    walletUser: PropTypes.objectOf(PropTypes.any),
    banks: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    walletUser: {},
    banks: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      isWeb: false,
      paymentPrice: '',
      discountCode: '',
      applyCodeDisable: true,
      paymentDisable: true,
      balance:
        props.walletUser && props.walletUser.totalAmount
          ? props.walletUser.totalAmount
          : 0,
      afterPayment:
        props.walletUser && props.walletUser.totalAmount
          ? props.walletUser.totalAmount
          : 0,
      bankId: props.banks && props.banks.length ? props.banks[0].id : 2,
    };

    this.payment = this.payment.bind(this);
    this.applyCode = this.applyCode.bind(this);
  }

  componentDidMount() {
    this.updatePredicate();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      balance: nextProps.walletUser ? nextProps.walletUser.totalAmount : 0,
      afterPayment: nextProps.walletUser ? nextProps.walletUser.totalAmount : 0,
    });
  }

  async payment() {
    const { paymentPrice, bankId } = this.state;
    const token = getCookie('siteToken');
    const amount = paymentPrice.replace(/,/g, '');

    const bankPaymentRequestCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BankPaymentRequestsDto({
          bankId,
          amount: parseInt(amount, 0),
        }),
      )
      .build();

    const response = await BankPaymentWalletRequestsApi(
      bankPaymentRequestCrud,
      token,
    );

    if (response.status === 200)
      window.location.href = `https://api.motherflower.com/SAM/Payment/BankPage/wallet/${
        response.data
      }/false`;
  }

  async applyCode() {
    const { discountCode } = this.state;
    const token = getCookie('siteToken');
    const response = await PostWalletDepositHistoriesApi(
      token,
      new BaseCRUDDtoBuilder()
        .dto(
          new WalletDepositHistoriesDto({
            discountCode,
          }),
        )
        .build(),
    );

    if (response.status === 200) {
      showNotification('success', '', 'کد هدیه با موفقیت اعمال شد.', 7);
      const responseWalletUser = await GetWalletsApi(token, { dto: {} });
      if (responseWalletUser.status === 200)
        this.props.actions.walletListSuccess(responseWalletUser.data);
      else this.props.actions.walletListFailure();
      this.setState({
        paymentPrice: '',
        discountCode: '',
        applyCodeDisable: true,
        paymentDisable: true,
      });
    } else
      showNotification('error', '', response.data.errorMessage, 7, 'errorsBox');
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  handleInput = (inputName, value, buttonClick = false) => {
    const { balance } = this.state;
    const re = /^[0-9\b]+$/;

    if (inputName === 'paymentPrice') {
      if (re.test(value.replace(/,/g, '')) || value.length === 0) {
        this.setState({
          [inputName]: PersianNumber(value.replace(/,/g, '')),
          paymentDisable: !(value.length > 0),
          afterPayment:
            value.length > 0
              ? parseInt(value.replace(/,/g, ''), 0) + parseInt(balance, 0)
              : parseInt(balance, 0),
        });

        const activeButton = document.getElementsByClassName('activeButton');
        while (activeButton.length > 0) {
          activeButton[0].classList.remove('activeButton');
        }

        if (value.replace(/,/g, '') === '30000') {
          document.getElementById('30hezar').classList.add('activeButton');
        } else if (value.replace(/,/g, '') === '50000') {
          document.getElementById('50hezar').classList.add('activeButton');
        } else if (value.replace(/,/g, '') === '70000') {
          document.getElementById('70hezar').classList.add('activeButton');
        } else if (value.replace(/,/g, '') === '200000') {
          document.getElementById('200hezar').classList.add('activeButton');
        } else if (value.replace(/,/g, '') === '300000') {
          document.getElementById('300hezar').classList.add('activeButton');
        } else if (value.replace(/,/g, '') === '500000') {
          document.getElementById('500hezar').classList.add('activeButton');
        }

        if (buttonClick) {
          const input = document.getElementsByName('inputPayment');
          input[0].focus();
        }
      }
    } else if (inputName === 'discountCode')
      this.setState({
        [inputName]: value,
        applyCodeDisable: !(value.length > 0),
      });
    else
      this.setState({
        [inputName]: value,
      });
  };

  separateThreeDigit = value => {
    for (let i = 0; i < value.length; i += 1) {}
  };

  render() {
    const {
      isWeb,
      paymentPrice,
      discountCode,
      applyCodeDisable,
      paymentDisable,
      balance,
      afterPayment,
    } = this.state;

    return (
      <div>
        <div className={s.container}>
          <div>
            <div className={s.afterAddedCredit}>
              <label>{BALANCE}:</label>
              <label>{PersianNumber(balance)} تومان</label>
            </div>
            <div className={s.addedCredit}>
              <label>اعتبار افزوده شده :</label>

              <label>
                <span>{paymentPrice === '' ? '0' : paymentPrice}</span> {TOMAN}
              </label>
            </div>
            <div className={s.afterAddedCredit}>
              <label>مبلغ کل اعتبار پس از افزایش :</label>
              <label>
                {PersianNumber(afterPayment)} {TOMAN}
              </label>
            </div>
          </div>
        </div>
        <hr />
        <div className={s.container}>
          <div className={s.increaseCredit}>
            <label>{INCREASE_CREDIT}:</label>
            <div>
              <div className={cs('row', s.buttons)}>
                <div className="col-xs-4 col-sm-4">
                  <label
                    id="30hezar"
                    onClick={() =>
                      this.handleInput('paymentPrice', '30000', true)
                    }
                  >
                    30 {HEZAR_TOMAN}
                  </label>
                </div>
                <div className="col-xs-4 col-sm-4">
                  <label
                    id="50hezar"
                    onClick={() =>
                      this.handleInput('paymentPrice', '50000', true)
                    }
                  >
                    {' '}
                    50 {HEZAR_TOMAN}
                  </label>
                </div>
                <div className="col-xs-4 col-sm-4">
                  <label
                    id="70hezar"
                    onClick={() =>
                      this.handleInput('paymentPrice', '70000', true)
                    }
                  >
                    {' '}
                    70 {HEZAR_TOMAN}
                  </label>
                </div>
                <div className="col-xs-4 col-sm-4">
                  <label
                    id="200hezar"
                    onClick={() =>
                      this.handleInput('paymentPrice', '200000', true)
                    }
                  >
                    {' '}
                    200 {HEZAR_TOMAN}
                  </label>
                </div>
                <div className="col-xs-4 col-sm-4">
                  <label
                    id="300hezar"
                    onClick={() =>
                      this.handleInput('paymentPrice', '300000', true)
                    }
                  >
                    300 {HEZAR_TOMAN}
                  </label>
                </div>
                <div className="col-xs-4 col-sm-4">
                  <label
                    id="500hezar"
                    onClick={() =>
                      this.handleInput('paymentPrice', '500000', true)
                    }
                  >
                    {' '}
                    500 {HEZAR_TOMAN}
                  </label>
                </div>
              </div>

              <div className={s.payment}>
                <CPInput
                  hintText="مبلغ دلخواه خود را به تومان وارد نمایید"
                  onChange={value =>
                    this.handleInput('paymentPrice', value.target.value)
                  }
                  value={paymentPrice}
                  name="inputPayment"
                />

                <CPButton
                  onClick={this.payment}
                  className={s.submitButton}
                  disabled={paymentDisable}
                >
                  {PAYMENT}
                </CPButton>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className={s.container}>
          <div className={s.increaseCredit}>
            <label>{GIFT_CODE}:</label>
            <div className={cs(s.payment, s.giftCode)}>
              <CPInput
                hintText="کد هدیه خود را وارد کنید"
                onChange={value =>
                  this.handleInput('discountCode', value.target.value)
                }
                value={discountCode}
                className={s.input}
              />

              <CPButton
                onClick={this.applyCode}
                className={s.submitButton}
                disabled={applyCodeDisable}
              >
                {APPLY_CODE}
              </CPButton>
            </div>
          </div>
          <label className={s.description}>
            نکته: کد هدیه برای شارژ حساب میباشد و با کد تخفیف تفاوت دارد.
          </label>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  walletUser:
    state.samWallet.walletListData &&
    state.samWallet.walletListData.items &&
    state.samWallet.walletListData.items.length > 0
      ? state.samWallet.walletListData.items[0]
      : {},

  banks: state.samBank.bankListData ? state.samBank.bankListData.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginRequest,
      loginFailure,
      loginSuccess,
      showAlertRequest,
      loadingSearchHideRequest,
      loadingSearchShowRequest,
      walletListFailure,
      walletListSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Wallet));
