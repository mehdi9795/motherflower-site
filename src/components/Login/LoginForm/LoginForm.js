import React from 'react';
import { Spin } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import { setCookie, getCookie } from '../../../utils/index';
import Link from '../../Link';
import s from './LoginForm.css';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import {
  PASSWORD,
  REGISTER,
  LOGIN,
  FORGOT_PASSWORD,
  PHONE_NUMBER,
} from '../../../Resources/Localization';
import {
  AddressDto,
  AuthenticateDto,
  PermissionDto,
  SendVerificationCodeDto,
  UserAppInfo,
  UserDto,
} from '../../../dtos/identityDtos';

import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  authPostAppUserInfo,
  GetAddressUser,
  getPermissionApi,
  postAuthenticateApi,
  SendVerificationCodeApi,
} from '../../../services/identityApi';
import {
  permissionListFailure,
  permissionListSuccess,
} from '../../../redux/identity/action/permission';
import {
  loginFailure,
  loginRequest,
  loginSuccess, signUpSuccess,
} from '../../../redux/identity/action/account';
import history from '../../../history';
import CPValidator from '../../CP/CPValidator';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import {
  GetUserBasketHistoriesApi,
  PutUserBasketHistoriesApi,
} from '../../../services/samApi';
import { BasketDto } from '../../../dtos/catalogDtos';
import { basketListSuccess } from '../../../redux/catalog/action/basket';
import { getCityApi, getDistrictApi } from '../../../services/commonApi';
import { addressListSuccess } from '../../../redux/identity/action/address';
import { CityDto, DistrictDto } from '../../../dtos/commonDtos';
import { districtOptionListSuccess } from '../../../redux/common/action/district';
import { cityListSuccess } from '../../../redux/common/action/city';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';

class LoginForm extends React.Component {
  static propTypes = {
    changeStep: PropTypes.func,
    isValid: PropTypes.func,
    hideNext: PropTypes.func,
    hidePrev: PropTypes.func,
    validationFalse: PropTypes.func,
    fromCheckout: PropTypes.bool,
    loadingLogin: PropTypes.bool,
    fromUrl: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    changeStep: () => {},
    isValid: () => {},
    hideNext: () => {},
    hidePrev: () => {},
    validationFalse: () => {},
    fromCheckout: false,
    loadingLogin: false,
    fromUrl: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      // isCaptchaValid: false,
      userName: '',
      password: '',
      showMessage: false,
      captchaNC: '',
      userNameFinal: '',
      isValidPassword: false,
      showMessagePhone: 'none',
      messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
      buttonDisable: false,
    };

    if (props.hideNext) {
      props.hideNext();
      props.hidePrev();
      props.validationFalse();
    }
    this.login = this.login.bind(this);
    this.validation = [];
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  toggleMeOne = () => {
    this.props.changeStep('register');
  };

  forgetPassword = e => {
    e.preventDefault();
    this.props.changeStep('forgetPassword');
    // this.props.changeStep('changePassword');
  };

  async login() {
    const {
      userNameFinal,
      password,
      captchaNC,
      isValidPassword,
      messagePhoneNumber,
      userName,
      countryCode,
    } = this.state;
    const { fromCheckout, fromUrl } = this.props;
    if (this.validation.length === 1 && isValidPassword) {
      this.setState({ buttonDisable: true });

      if (fromCheckout) this.props.actions.loginRequest();
      else this.props.actions.showLoading();

      const response = await postAuthenticateApi(
        new AuthenticateDto({
          UserName: userNameFinal,
          Password: password,
          nc: captchaNC,
          State: 'True',
        }),
      );

      if (response.status === 645) {
        const verificationCodeCrud = new BaseCRUDDtoBuilder()
          .dto(
            new SendVerificationCodeDto({
              userDto: new UserDto({ phone: userNameFinal, countryCode }),
              verificationType: 'Signup',
              resend: true,
            }),
          )
          .build();

        await SendVerificationCodeApi(verificationCodeCrud);
        const obj = {
          countryCode,
          phone: userNameFinal,
        };
        this.props.actions.signUpSuccess(obj);
        this.props.changeStep('verification');
      }

      if (response.data.sessionId) {
        const authenticationCookieExpireHours = 8760;
        setCookie(
          'siteToken',
          response.data.sessionId,
          authenticationCookieExpireHours,
        );
        setCookie(
          'siteUserName',
          response.data.userName,
          authenticationCookieExpireHours,
        );
        setCookie(
          'siteDisplayName',
          encodeURIComponent(response.data.displayName),
          authenticationCookieExpireHours,
        );
        setCookie(
          'imageUser',
          response.data.meta.imageUrl,
          authenticationCookieExpireHours,
        );

        const obj = {
          token: response.data.sessionId,
          userName: response.data.userName,
          displayName: response.data.displayName,
          imageUser: response.data.meta.imageUrl,
        };

        this.props.actions.loginSuccess(obj);

        const token = getCookie('siteToken');
        const guestKey = getCookie('guestKey');
        const fcmToken = getCookie('fcmToken');

        const basketCrud = new BaseCRUDDtoBuilder()
          .dto(
            new BasketDto({
              guestKey,
            }),
          )
          .build();

        await PutUserBasketHistoriesApi(basketCrud, token);

        const permissions = await getPermissionApi(
          response.data.sessionId,
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(
                new PermissionDto({
                  resourceType: 'UI',
                  permissionStatus: 'Deny',
                }),
              )
              .buildJson(),
          ),
        );

        if (permissions.data) {
          this.props.actions.permissionListSuccess(permissions.data.listDto);
        } else {
          this.props.actions.permissionListFailure();
        }

        const responseGet = await GetUserBasketHistoriesApi(
          getDtoQueryString(
            new BaseGetDtoBuilder().dto(new BasketDto()).buildJson(),
          ),
          token,
        );

        if (responseGet.status === 200) {
          this.props.actions.basketListSuccess(responseGet.data);
        }

        authPostAppUserInfo(
          new BaseCRUDDtoBuilder()
            .dto(
              new UserAppInfo({
                token: fcmToken,
                os: 'web',
                appType: 'Customer',
              }),
            )
            .build(),
          token,
        );

        if (fromUrl === 'Card') history.push('/card');
        else if (fromCheckout) {
          if (responseGet.data.items && responseGet.data.items.length > 0) {
            responseGet.data.items[0].userBasketHistoryDetailDtos.map(item => {
              if (item.expired) history.push('/card');
              return null;
            });
          }
          if (token) {
            const responseGetAddress = await GetAddressUser(
              token,
              getDtoQueryString(
                new BaseGetDtoBuilder()
                  .dto(new AddressDto({ active: true }))
                  .buildJson(),
              ),
            );
            if (responseGetAddress.status === 200) {
              this.addresses = responseGetAddress.data;
              this.props.actions.addressListSuccess(responseGetAddress.data);
            }

            /**
             * get all cities for first time with default clause
             */
            const responseCity = await getCityApi(
              getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
            );
            if (responseCity.status === 200) {
              this.props.actions.cityListSuccess(responseCity.data);

              const responseDistrict = await getDistrictApi(
                getDtoQueryString(
                  new BaseGetDtoBuilder()
                    .dto(
                      new DistrictDto({
                        cityDto: new CityDto({
                          id: responseCity.data.items[0].id,
                        }),
                      }),
                    )
                    .buildJson(),
                ),
              );

              if (responseDistrict.status === 200) {
                this.props.actions.districtOptionListSuccess(
                  responseDistrict.data,
                );
              }
            }
          }

          this.props.isValid('next', true);
        } else {
          this.props.actions.hideLoading();
          history.push('/');
        }
      } else {
        this.props.actions.hideLoading();
        this.props.actions.loginFailure();
        this.props.actions.showAlertRequest(response.data.errorMessage);
        this.setState({ buttonDisable: false });
      }
    } else {
      if (!isValidPassword) {
        showNotification('success', 'خطا', messagePhoneNumber, 10);
      }
      this.setState({
        showMessage: true,
        // showMessagePhone: this.state.messagePhoneNumber ? 'block' : 'none',
      });
    }
  }

  verifyCallback = response => {
    this.setState({
      // isCaptchaValid: true,
      captchaNC: response,
    });
  };

  handleInput = (inputName, value) => {
    this.setState({ [inputName]: value, showMessage: false });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length === 3) this.disable = false;
    else this.disable = true;
  };
  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        userNameFinal: number.replace(/ /g, ''),
        userName: value,
        isValidPassword: status,
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
        showMessage: false,
        countryCode: countryData,
      });
    }
  };

  render() {
    const { loadingLogin, fromCheckout } = this.props;
    const {
      // isCaptchaValid,
      password,
      userName,
      showMessage,
      // showMessagePhone,
      // messagePhoneNumber,
      buttonDisable,
    } = this.state;
    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          {/* <i className="mf-login" /> */}
          <h3 className={s.title}>{LOGIN}</h3>
          <div className={s.formContainer}>
            <div className={s.loginFields}>
              <div className={s.flagBox}>
                <b className={s.label}>{PHONE_NUMBER}</b>
                <CPIntlPhoneInput
                  value={userName}
                  onChange={this.changeNumber}
                />
                {/* <p
                  className={s.errorMessage}
                  style={{ display: `${showMessagePhone}` }}
                >
                  {messagePhoneNumber}
                </p> */}
              </div>
              <CPValidator
                isRequired
                value={password}
                checkValidation={this.checkValidation}
                showNotification={showMessage}
                name={PASSWORD}
              >
                <CPInput
                  onChange={value =>
                    this.handleInput('password', value.target.value)
                  }
                  className={s.loginInput}
                  label={PASSWORD}
                  value={password}
                  type="password"
                  onPressEnter={this.login}
                />
              </CPValidator>
              <CPButton
                onClick={this.login}
                disabled={buttonDisable}
                className="loginBtn greenBtn"
              >
                {loadingLogin && fromCheckout ? (
                  <Spin spinning={loadingLogin} />
                ) : (
                  LOGIN
                )}
              </CPButton>
              <Link
                to="/"
                onClick={e => this.forgetPassword(e)}
                className={s.forgot}
              >
                {FORGOT_PASSWORD}
              </Link>
              <div className={s.captcha}>
                {/* <Recaptcha */}
                {/* sitekey="6LfqdU8UAAAAAB1s_ajGeaM5E0XnEpkwkIqyGySy" */}
                {/* onloadCallback={this.captcha} */}
                {/* verifyCallback={this.verifyCallback} */}
                {/* /> */}
              </div>
            </div>
            <CPButton className={s.registerBtn} onClick={this.toggleMeOne}>
              {REGISTER}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadingLogin: state.account.loginRequest,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
      hideLoading,
      loginSuccess,
      loginRequest,
      loginFailure,
      permissionListSuccess,
      permissionListFailure,
      showAlertRequest,
      basketListSuccess,
      addressListSuccess,
      districtOptionListSuccess,
      cityListSuccess,
      signUpSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(LoginForm));
