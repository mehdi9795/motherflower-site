import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ForgetPassword.css';
import CPButton from '../../CP/CPButton';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import {
  PHONE_NUMBER,
  ACTIVATION_CODE,
  SPECIFICATION, REGISTER,
} from '../../../Resources/Localization';
import { SendVerificationCodeDto, UserDto } from '../../../dtos/identityDtos';
import { SendVerificationCodeApi } from '../../../services/identityApi';
import {
  signUpFailure,
  signUpRequest,
  signUpSuccess,
} from '../../../redux/identity/action/account';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import { BaseCRUDDtoBuilder } from '../../../dtos/dtoBuilder';

class ForgetPassword extends React.Component {
  static propTypes = {
    changeStep: PropTypes.func,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    changeStep: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      countryCode: '98',
      messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
      showMessagePhone: 'none',
      phoneFinal: '',
    };
    this.toggleMeTwo = this.toggleMeTwo.bind(this);
    this.validation = [];
  }

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phone: value,
        countryCode: countryData.dialCode,
        phoneFinal: number.replace(/ /g, ''),
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
      });
    }
  };

  async toggleMeTwo() {
    const { countryCode, phoneFinal } = this.state;

    const verificationCodeCrud = new BaseCRUDDtoBuilder()
      .dto(
        new SendVerificationCodeDto({
          userDto: new UserDto({ phone: phoneFinal, countryCode }),
          verificationType: 'SignIn',
          // verificationType: 'ForgetPassword',
          // resend: false,
        }),
      )
      .build();
    this.props.actions.signUpRequest();
    const response = await SendVerificationCodeApi(verificationCodeCrud);
    console.log(response);
    if (response.status === 200) {
      const obj = {
        countryCode,
        phone: phoneFinal,
        fromForgetPassword: true,
      };
      this.props.actions.signUpSuccess(obj);
      this.props.changeStep('verification');
    } else {
      this.props.actions.signUpFailure();
      this.props.actions.showAlertRequest(response.data.errorMessage);
    }
  }

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length === 3) this.disable = false;
    else this.disable = true;
  };

  toggleMeOne = () => {
    this.props.changeStep('register');
  };

  render() {
    const { messagePhoneNumber, showMessagePhone, phone } = this.state;

    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          <i className="mf-user" />
          <h3 className={s.title}>{SPECIFICATION}</h3>
          <div className={s.formContainer}>
            <div className={s.registerInputs}>
              <div className={s.flagBox}>
                <b className={s.label}>{PHONE_NUMBER}</b>
                <CPIntlPhoneInput value={phone} onChange={this.changeNumber} />
                <p
                  className={s.errorMessage}
                  style={{ display: `${showMessagePhone}` }}
                >
                  {messagePhoneNumber}
                </p>
              </div>
              <CPButton className={s.loginBtn} onClick={this.toggleMeTwo}>
                {ACTIVATION_CODE}
              </CPButton>
            </div>


            <CPButton className={s.registerBtn} onClick={this.toggleMeOne}>
              {REGISTER}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      signUpSuccess,
      signUpRequest,
      signUpFailure,
      showAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ForgetPassword));
