import React from 'react';
import moment from 'moment';
import Countdown from 'react-countdown-moment';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ConfirmationForm.css';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import {
  CONFIRMATION,
  REMINDING_TIME,
  RESEND,
  ENTER_THE_CONFIRMATION_CODE,
  SENDING_CODE,
  REGISTER,
} from '../../../Resources/Localization';
import {
  loginFailure,
  loginRequest,
  loginSuccess,
} from '../../../redux/identity/action/account';
import {
  PermissionDto,
  SendVerificationCodeDto,
  UserDto,
  UserVerificationDto,
} from '../../../dtos/identityDtos';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  getPermissionApi,
  SendVerificationCodeApi,
  UserVerificationApi,
} from '../../../services/identityApi';
import history from '../../../history';
import { getCookie, setCookie } from '../../../utils';
import {
  permissionListFailure,
  permissionListSuccess,
} from '../../../redux/identity/action/permission';
import { BasketDto } from '../../../dtos/catalogDtos';
import { PutUserBasketHistoriesApi } from '../../../services/samApi';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';

class ConfirmationForm extends React.Component {
  static propTypes = {
    validationFalse: PropTypes.func,
    isValid: PropTypes.func,
    hideNext: PropTypes.func,
    hidePrev: PropTypes.func,
    fromCheckout: PropTypes.bool,
    loadingLogin: PropTypes.bool,
    signUpData: PropTypes.objectOf(PropTypes.any),
    changeStep: PropTypes.func,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    fromCheckout: false,
    loadingLogin: false,
    signUpData: {},
    validationFalse: () => {},
    changeStep: () => {},
    isValid: () => {},
    hideNext: () => {},
    hidePrev: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      code1: '',
      code2: '',
      code3: '',
      code4: '',
      btnResend: true,
    };
    if (this.props.hideNext) {
      this.props.hideNext();
      this.props.hidePrev();
      this.props.validationFalse();
    }
    this.verify = this.verify.bind(this);
    this.onResend = this.onResend.bind(this);
    this.sendVerivicationCodeHandler();
    this.endDate = moment().add(30, 'seconds');
  }

  async onResend() {
    const { phone, countryCode } = this.props.signUpData;
    this.endDate = moment().add(30, 'seconds');
    this.setState({ btnResend: true });
    this.sendVerivicationCodeHandler();

    const verificationCodeCrud = new BaseCRUDDtoBuilder()
      .dto(
        new SendVerificationCodeDto({
          userDto: new UserDto({ phone, countryCode }),
          verificationType: 'Signup',
          resend: true,
        }),
      )
      .build();

    await SendVerificationCodeApi(verificationCodeCrud);
  }

  async verify() {
    const { code1, code2, code3, code4 } = this.state;
    const { fromCheckout, signUpData } = this.props;

    const verificationCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserVerificationDto({
          id: signUpData.id,
          userName: signUpData.phone,
          verificationType: signUpData.fromForgetPassword ? 'SignIn' : 'Signup',
          // ? 'ForgetPassword'

          verificationCode: `${code1}${code2}${code3}${code4}`.toString(),
          fromWeb: true,
        }),
      )
      .build();

    this.props.actions.loginRequest();
    const response = await UserVerificationApi(verificationCrud);

    if (response.status === 603) {
      this.props.actions.loginFailure();
      showNotification(
        'error',
        'خطا',
        response.data.errorMessage,
        15,
        'errorsBox',
      );
      // } else if (response.data.sessionId && !signUpData.fromForgetPassword) {
    } else if (response.data.bearerToken) {
    // } else if (response.data.sessionId) {
      const authenticationCookieExpireHours = 8760;
      setCookie(
        'siteToken',
        response.data.bearerToken,
        authenticationCookieExpireHours,
        PutUserBasketHistoriesApi,
      );
      setCookie(
        'siteUserName',
        response.data.userName,
        authenticationCookieExpireHours,
      );
      setCookie(
        'siteDisplayName',
        encodeURIComponent(response.data.displayName),
        authenticationCookieExpireHours,
      );

      const obj = {
        token: response.data.bearerToken,
        userName: response.data.userName,
        displayName: response.data.displayName,
      };

      this.props.actions.loginSuccess(obj);

      const permissions = await getPermissionApi(
        response.data.bearerToken,
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new PermissionDto({
                resourceType: 'UI',
                permissionStatus: 'Deny',
              }),
            )
            .buildJson(),
        ),
      );

      if (permissions.data) {
        this.props.actions.permissionListSuccess(permissions.data.listDto);
      } else {
        this.props.actions.permissionListFailure();
      }

      const token = getCookie('siteToken');
      const guestKey = getCookie('guestKey');

      const basketCrud = new BaseCRUDDtoBuilder()
        .dto(
          new BasketDto({
            guestKey,
          }),
        )
        .build();

      await PutUserBasketHistoriesApi(basketCrud, token);

      if (fromCheckout) {
        this.props.isValid('next', true);
      } else {
        history.push('/');
      }
    } else if (response.data.bearerToken && signUpData.phone) {
      // this.props.changeStep('changePassword');
      this.props.actions.loginFailure();
    } else this.props.actions.loginFailure();
  }

  handleInput = (inputName, value, keyFocus) => {
    const { code4 } = this.state;
    const re = /^[0-9\b]+$/;

    if (value === 'Backspace') {
      if (inputName === 'code4' && code4.length > 0) {
        this.setState({ [inputName]: '' });
        return;
      } else if (inputName !== 'code1') {
        const beforeInput = `code${inputName.charAt(4) - 1}`;
        this.setState({ [beforeInput]: '' });
        const input = document.getElementsByName(beforeInput);
        input[0].focus();
      }
      return;
    }
    if (value === '' || re.test(value)) {
      this.setState({ [inputName]: value });

      if (keyFocus !== 'submit') {
        const input = document.getElementsByName(keyFocus);
        input[0].focus();
      } else {
        // document.getElementById(keyFocus).focus();
        setTimeout(() => this.verify(), 100);
      }
    }
  };

  sendVerivicationCodeHandler = () => {
    const thiz = this;
    setTimeout(() => {
      thiz.setState({ btnResend: false });
    }, 30000);
  };

  render() {
    const { loadingLogin } = this.props;
    const { btnResend } = this.state;
    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          <i className="mf-mobile" />
          <h3 className={s.title}>{SENDING_CODE}</h3>
          <div className={s.formContainer}>
            <div className={s.confirmationFields}>
              <b className={s.label}>{ENTER_THE_CONFIRMATION_CODE}</b>
              <div className={s.confirmationInputs}>
                <CPInput
                  // onChange={value =>
                  //   this.handleInput('code4', value.target.value, 'submit', value)
                  // }
                  className={s.confirmationInput}
                  value={this.state.code4}
                  maxLength={1}
                  type="number"
                  name="code4"
                  onKeyUp={value =>
                    this.handleInput('code4', value.key, 'submit')
                  }
                  onPressEnter={this.verify}
                />
                <CPInput
                  // onChange={value =>
                  //   this.handleInput('code3', value.target.value, 'code4', value)
                  // }
                  className={s.confirmationInput}
                  value={this.state.code3}
                  maxLength={1}
                  name="code3"
                  type="number"
                  onKeyUp={value =>
                    this.handleInput('code3', value.key, 'code4')
                  }
                  onPressEnter={this.verify}
                />
                <CPInput
                  // onChange={value =>
                  //   this.handleInput('code2', value.target.value, 'code3', value)
                  // }
                  className={s.confirmationInput}
                  value={this.state.code2}
                  maxLength={1}
                  name="code2"
                  type="number"
                  onKeyUp={value =>
                    this.handleInput('code2', value.key, 'code3')
                  }
                  onPressEnter={this.verify}
                />
                <CPInput
                  // onChange={value =>
                  //   this.handleInput('code1', value.target.value, 'code2', value)
                  // }
                  className={s.confirmationInput}
                  value={this.state.code1}
                  maxLength={1}
                  name="code1"
                  type="number"
                  autoFocus
                  onKeyUp={value =>
                    this.handleInput('code1', value.key, 'code2')
                  }
                  onPressEnter={this.verify}
                />
              </div>
              <CPButton
                disabled={loadingLogin}
                className={s.confirmationBtn}
                onClick={this.verify}
                id="submit"
              >
                {loadingLogin ? <Spin spinning={loadingLogin} /> : CONFIRMATION}
              </CPButton>
            </div>
            <div className={s.reminder}>
              <b className={s.label}>
                {REMINDING_TIME} :{' '}
                <b>
                  <Countdown endDate={this.endDate} />
                </b>
              </b>
              <CPButton
                disabled={btnResend}
                onClick={this.onResend}
                className={s.resendBtn}
              >
                {RESEND}
              </CPButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  signUpData: state.account.signUpData,
  loadingLogin: state.account.loginRequest,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginSuccess,
      loginRequest,
      loginFailure,
      permissionListSuccess,
      permissionListFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ConfirmationForm));
