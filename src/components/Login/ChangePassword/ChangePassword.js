import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { setCookie, getCookie } from '../../../utils/index';
import s from './ChangePassword.css';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import {
  LOGIN,
  CONFIRMATION,
  NEW_PASSWORD,
  REENTER_NEW_PASSWORD,
} from '../../../Resources/Localization';
import { ChangePasswordDto, PermissionDto } from '../../../dtos/identityDtos';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  ChangePasswordApi,
  getPermissionApi,
} from '../../../services/identityApi';
import {
  permissionListFailure,
  permissionListSuccess,
} from '../../../redux/identity/action/permission';
import {
  loginFailure,
  loginRequest,
  loginSuccess,
} from '../../../redux/identity/action/account';
import history from '../../../history';
import CPValidator from '../../CP/CPValidator';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import { BasketDto } from '../../../dtos/catalogDtos';
import { PutUserBasketHistoriesApi } from '../../../services/samApi';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';

class ChangePassword extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      confirmPasswordClass: 'bar',
      showMessage: false,
      // showMessageConfirm: false,
    };

    this.changePassword = this.changePassword.bind(this);
    this.validation = [];
  }

  async changePassword() {
    const { confirmPassword, password } = this.state;
    const { signUpData } = this.props;
    if (this.validation.length === 1 && confirmPassword === password) {
      const ChangePasswordCrud = new BaseCRUDDtoBuilder()
        .dto(
          new ChangePasswordDto({
            userName: signUpData.phone,
            newPassword: password,
            newConfirmPassword: confirmPassword,
            fromWeb: true,
          }),
        )
        .build();
      this.props.actions.loginRequest();
      const response = await ChangePasswordApi(ChangePasswordCrud);
      if (response.data.bearerToken) {
        const authenticationCookieExpireHours = 8;
        setCookie(
          'siteToken',
          response.data.bearerToken,
          authenticationCookieExpireHours,
        );
        setCookie(
          'siteUserName',
          response.data.userName,
          authenticationCookieExpireHours,
        );
        setCookie(
          'siteDisplayName',
          encodeURIComponent(response.data.displayName),
          authenticationCookieExpireHours,
        );

        const obj = {
          token: response.data.bearerToken,
          userName: response.data.userName,
          displayName: response.data.displayName,
        };

        this.props.actions.loginSuccess(obj);

        const permissionList = new BaseGetDtoBuilder()
          .dto(
            new PermissionDto({
              resourceType: 'UI',
              permissionStatus: 'Deny',
            }),
          )
          .buildJson();

        const permissions = await getPermissionApi(
          response.data.bearerToken,
          getDtoQueryString(permissionList),
        );

        if (permissions.data) {
          this.props.actions.permissionListSuccess(permissions.data.listDto);
        } else {
          this.props.actions.permissionListFailure();
        }

        const token = getCookie('siteToken');
        const guestKey = getCookie('guestKey');

        const basketCrud = new BaseCRUDDtoBuilder()
          .dto(
            new BasketDto({
              guestKey,
            }),
          )
          .build();

        await PutUserBasketHistoriesApi(basketCrud, token);

        history.push('/');
      } else {
        this.props.actions.loginFailure();
      }
    } else {
      this.setState({
        showMessage: this.validation.length !== 1,
        // showMessageConfirm: confirmPassword !== password,
      });
      if (confirmPassword !== password) {
        showNotification(
          'error',
          'خطا',
          'رمز عبور جدید با تکرار آن برابر نیست',
          10,
        );
      }
    }
  }

  handleInput = (inputName, value) => {
    const { password, confirmPassword } = this.state;
    if (inputName === 'confirmPassword')
      this.setState({
        [inputName]: value,
        confirmPasswordClass:
          value === password ? 'bar_isValid' : 'bar_noValid',
        showMessage: false,
      });
    else
      this.setState({
        [inputName]: value,
        confirmPasswordClass:
          value === confirmPassword ? 'bar_isValid' : 'bar_noValid',
        showMessage: false,
      });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length === 3) this.disable = false;
    else this.disable = true;
  };

  render() {
    const {
      password,
      confirmPassword,
      showMessage,
      // showMessageConfirm,
      confirmPasswordClass,
    } = this.state;
    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          <i className="mf-login" />
          <h3 className={s.title}>{LOGIN}</h3>
          <div className={s.formContainer}>
            <div className={s.loginFields}>
              <CPValidator
                minLength={5}
                value={password}
                checkValidation={this.checkValidation}
                showNotification={showMessage}
                name={NEW_PASSWORD}
              >
                <CPInput
                  onChange={value =>
                    this.handleInput('password', value.target.value)
                  }
                  className={s.loginInput}
                  label={NEW_PASSWORD}
                  value={password}
                  type="password"
                  autoFocus
                />
              </CPValidator>
              <CPInput
                onChange={value =>
                  this.handleInput('confirmPassword', value.target.value)
                }
                className={s.loginInput}
                label={REENTER_NEW_PASSWORD}
                value={confirmPassword}
                isValidClass={confirmPasswordClass}
                type="password"
              />
              {/* {showMessageConfirm && (
                <p className={s.errorMessage}>
                  رمز عبور جدید با تکرار آن برابر نیست
                </p>
              )} */}
            </div>
            <CPButton className={s.registerBtn} onClick={this.changePassword}>
              {CONFIRMATION}
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  signUpData: state.account.signUpData,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      loginSuccess,
      loginRequest,
      loginFailure,
      permissionListSuccess,
      permissionListFailure,
      showAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ChangePassword));
