import React from 'react';
import { Select } from 'antd';
import { hideLoading } from 'react-redux-loading-bar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RegisterForm.css';
import CPRadio from '../../CP/CPRadio';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import CPSelect from '../../CP/CPSelect';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import {
  NAME,
  FAMILY_NAME,
  PASSWORD,
  CITY,
  PHONE_NUMBER,
  EMAIL,
  SEXUAL,
  ACTIVATION_CODE,
  BIRTHDAY_DATE,
  SPECIFICATION,
} from '../../../Resources/Localization';

import { UserDto } from '../../../dtos/identityDtos';
import { SignUpApi } from '../../../services/identityApi';
import {
  signUpFailure,
  signUpRequest,
  signUpSuccess,
} from '../../../redux/identity/action/account';
import CPValidator from '../../CP/CPValidator';
import { showAlertRequest } from '../../../redux/shared/action/errorAlert';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import { getCityApi } from '../../../services/commonApi';
import { cityListSuccess } from '../../../redux/common/action/city';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import { CityDto } from '../../../dtos/commonDtos';

const { Option } = Select;

class RegisterForm extends React.Component {
  static propTypes = {
    changeStep: PropTypes.func,
    fromCheckout: PropTypes.bool,
    loadingSignUp: PropTypes.bool,
    reagentUser: PropTypes.string,
    cities: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    changeStep: () => {},
    fromCheckout: false,
    loadingSignUp: false,
    cities: [],
    reagentUser: '',
  };

  constructor(props) {
    super(props);
    const re = /^(\+98|98|0)?9\d{9}$/g;
    const reagentValid = re.test(props.reagentUser);
    this.state = {
      gender: 'M',
      name: '',
      family: '',
      email: '',
      password: '',
      day: '1',
      month: '1',
      city: props.cities && props.cities[0] ? props.cities[0].id : '',
      phone: '',
      countryCode: '98',
      messagePhoneNumber: 'لطفا شماره همراه را صحیح وارد کنید.',
      messageFamily: false,
      messageName: false,
      messagePassword: false,
      messageEmail: false,
      // showMessagePhone: 'none',
      // isCaptchaValid: false,
      captchaNC: '',
      phoneFinal: '',
      isValidPhoneNumber: false,
      buttonDisable: false,
      reagentUserPhone: reagentValid ? props.reagentUser.replace('+', '') : '',
      reagentUserIsValidPhoneNumber: reagentValid,
      reagentUserMessagePhoneNumber: reagentValid
        ? ''
        : 'لطفا شماره همراه معرف را صحیح وارد کنید.',
      reagentUserPhoneFinal: reagentValid
        ? `+${props.reagentUser.replace(/ /g, '')}`
        : '',
    };

    this.toggleMeTwo = this.toggleMeTwo.bind(this);
    this.validation = [];
  }

  async componentWillMount() {
    /**
     * get all city for first time with default clause
     */
    const response = await getCityApi(
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );
    if (response.status === 200)
      this.props.actions.cityListSuccess(response.data);
    this.setState({ city: response.data.items[0].id });
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cities)
      this.setState({
        city:
          nextProps.cities && nextProps.cities[0] ? nextProps.cities[0].id : '',
      });
  }

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phone: value,
        countryCode: countryData.dialCode,
        phoneFinal: number.replace(/ /g, ''),
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
        isValidPhoneNumber: status,
        // showMessagePhone: this.state.messagePhoneNumber ? 'block' : 'none',
        messageFamily: false,
        messageName: false,
        messagePassword: false,
        messageEmail: false,
      });
    }
  };

  changeNumberReagentUser = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        reagentUserPhone: value,
        reagentUserPhoneFinal: number.replace(/ /g, ''),
        reagentUserMessagePhoneNumber: status
          ? ''
          : 'لطفا شماره همراه معرف را صحیح وارد کنید.',
        reagentUserIsValidPhoneNumber: status,
      });
    }
  };

  async toggleMeTwo() {
    const {
      gender,
      name,
      family,
      email,
      password,
      day,
      month,
      city,
      countryCode,
      captchaNC,
      phoneFinal,
      isValidPhoneNumber,
      messagePhoneNumber,
      reagentUserPhoneFinal,
      reagentUserMessagePhoneNumber,
      reagentUserIsValidPhoneNumber,
    } = this.state;

    if (
      this.validation.length === 2 &&
      isValidPhoneNumber &&
      (reagentUserPhoneFinal.length === 0 || reagentUserIsValidPhoneNumber)
    ) {
      this.setState({ buttonDisable: true });

      const userCrud = new BaseCRUDDtoBuilder()
        .dto(
          new UserDto({
            firstName: name,
            lastName: family,
            phone: phoneFinal,
            email,
            password,
            genderType: gender,
            monthOfBirthDate: month,
            dayOfBirthDate: day,
            cityId: city,
            cityDto: new CityDto({ id: city }),
            active: true,
            countryCode,
            nc: captchaNC,
            reagentUserDto:
              reagentUserPhoneFinal.length === 0 ||
              reagentUserIsValidPhoneNumber
                ? new UserDto({ phone: reagentUserPhoneFinal })
                : undefined,
          }),
        )
        .build();

      this.props.actions.signUpRequest();
      const response = await SignUpApi(userCrud);

      if (response.status === 200) {
        const obj = {
          countryCode,
          phone: phoneFinal,
          id: response.data.id,
        };
        this.props.actions.signUpSuccess(obj);
        this.props.changeStep('verification');
      } else {
        this.props.actions.signUpFailure();

        this.props.actions.showAlertRequest(response.data.errorMessage);
        this.setState({ buttonDisable: false });
      }
    } else {
      this.setState({
        messageFamily: true,
        messageName: true,
        messagePassword: true,
        messageEmail: true,
      });
      if (!isValidPhoneNumber) {
        showNotification('error', '', messagePhoneNumber, 10, 'errorsBox');
      }

      if (
        reagentUserPhoneFinal.length !== 0 &&
        !reagentUserIsValidPhoneNumber
      ) {
        showNotification(
          'error',
          '',
          reagentUserMessagePhoneNumber,
          10,
          'errorsBox',
        );
      }
    }
  }

  handleInput = (inputName, value) => {
    this.setState({
      [inputName]: value,
      messageFamily: false,
      messageName: false,
      messagePassword: false,
      messageEmail: false,
    });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length < 2) this.disable = false;
    else this.disable = true;
  };

  verifyCallback = response => {
    this.setState({
      // isCaptchaValid: true,
      captchaNC: response,
    });
  };

  render() {
    const { cities } = this.props;

    const {
      gender,
      day,
      month,
      city,
      // messagePhoneNumber,
      name,
      family,
      password,
      email,
      messageFamily,
      messageName,
      messagePassword,
      messageEmail,
      // showMessagePhone,
      phone,
      // isCaptchaValid,
      buttonDisable,
      reagentUserPhone,
    } = this.state;
    const sexual = [
      {
        value: 'M',
        name: 'مرد',
        defaultChecked: true,
      },
      {
        value: 'F',
        name: 'زن',
        defaultChecked: false,
      },
    ];
    const days = [];
    for (let i = 1; i <= 31; i += 1) {
      days.push(
        <Option value={i.toString()} key={i}>
          {i}
        </Option>,
      );
    }
    const cityArray = [];
    cities.map(item =>
      cityArray.push(
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>,
      ),
    );

    return (
      <div className={s.loginForm}>
        <div className={s.formWrapper}>
          {/* <i className="mf-user" /> */}
          <h3 className={s.title}>{SPECIFICATION}</h3>
          <div className={s.formContainer}>
            <div className={s.registerInputs}>
              {/* <div className={s.sexual}>
                <b className={s.label}>{SEXUAL}</b>
                <CPRadio
                  model={sexual}
                  size="small"
                  onChange={value =>
                    this.onChange('gender', value.target.value)
                  }
                  defaultValue={gender}
                />
              </div> */}
              <div className="row">
                <div className="col-6">
                  <CPValidator
                    isRequired
                    value={name}
                    checkValidation={this.checkValidation}
                    showNotification={messageName}
                    name={NAME}
                  >
                    <CPInput
                      onChange={value =>
                        this.handleInput('name', value.target.value)
                      }
                      className={s.registerInput}
                      label={NAME}
                      value={name}
                      autoFocus
                    />
                  </CPValidator>
                </div>
                <div className="col-6">
                  <CPValidator
                    isRequired
                    value={family}
                    checkValidation={this.checkValidation}
                    showNotification={messageFamily}
                    name={FAMILY_NAME}
                  >
                    <CPInput
                      onChange={value =>
                        this.handleInput('family', value.target.value)
                      }
                      className={s.registerInput}
                      label={FAMILY_NAME}
                      value={family}
                    />
                  </CPValidator>
                </div>
              </div>
              <div className={s.flagBox}>
                <b className={s.label}>{PHONE_NUMBER}</b>
                <CPIntlPhoneInput value={phone} onChange={this.changeNumber} />
                {/* <p
                  className={s.errorMessage}
                  style={{ display: `${showMessagePhone}` }}
                >
                  {messagePhoneNumber}
                </p> */}
              </div>
              <CPValidator
                validationEmail
                value={email}
                checkValidation={this.checkValidation}
                name={EMAIL}
              >
              <CPInput
                onChange={value =>
                  this.handleInput('email', value.target.value)
                }
                className={s.registerInput}
                label={EMAIL}
                value={email}
              />
              </CPValidator>
              {/* <CPValidator
                isRequired
                value={password}
                checkValidation={this.checkValidation}
                showNotification={messagePassword}
                name={PASSWORD}
              >
                <CPInput
                  onChange={value =>
                    this.handleInput('password', value.target.value)
                  }
                  className={s.registerInput}
                  label={PASSWORD}
                  value={password}
                  type="password"
                />
              </CPValidator> */}
              <b className={s.label}>{BIRTHDAY_DATE}</b>
              <div className="row">
                <div className="col-6">
                  <CPSelect
                    onChange={value => this.handleInput('month', value)}
                    defaultValue={month}
                  >
                    <Option value="1" key="1">
                      فروردین
                    </Option>
                    <Option value="2" key="2">
                      اردیبهشت
                    </Option>
                    <Option value="3" key="3">
                      خرداد
                    </Option>
                    <Option value="4" key="4">
                      تیر
                    </Option>
                    <Option value="5" key="5">
                      مرداد
                    </Option>
                    <Option value="6" key="6">
                      شهریور
                    </Option>
                    <Option value="7" key="7">
                      مهر
                    </Option>
                    <Option value="8" key="8">
                      آبان
                    </Option>
                    <Option value="9" key="9">
                      آذر
                    </Option>
                    <Option value="10" key="10">
                      دی
                    </Option>
                    <Option value="11" key="11">
                      بهمن
                    </Option>
                    <Option value="12" key="12">
                      اسفند
                    </Option>
                  </CPSelect>
                </div>
                <div className="col-6">
                  <CPSelect
                    onChange={value => this.handleInput('day', value)}
                    defaultValue={day}
                  >
                    {days}
                  </CPSelect>
                </div>
              </div>
              <b className={s.label}>{CITY}</b>
              <div className={s.city}>
                <CPSelect
                  onChange={value => this.handleInput('city', value)}
                  value={city}
                  showSearch
                >
                  {cityArray}
                </CPSelect>
              </div>
              <div className={cs(s.flagBox, 'reagentUserPhoneNumber')}>
                <b className={s.label}>{PHONE_NUMBER} معرف </b>
                <CPIntlPhoneInput
                  value={reagentUserPhone}
                  onChange={this.changeNumberReagentUser}
                />
              </div>
              {/* <div className={s.captcha}>
                <Recaptcha
                  sitekey="6LfqdU8UAAAAAB1s_ajGeaM5E0XnEpkwkIqyGySy"
                  onloadCallback={this.captcha}
                  verifyCallback={this.verifyCallback}
                />
              </div> */}
              <CPButton
                disabled={!this.disable || buttonDisable}
                className="loginBtn greenBtn"
                onClick={this.toggleMeTwo}
              >
                {ACTIVATION_CODE}
              </CPButton>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadingSignUp: state.account.signUpRequest,
  cities: state.commonCity.cityListData
    ? state.commonCity.cityListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      signUpSuccess,
      signUpRequest,
      signUpFailure,
      showAlertRequest,
      hideLoading,
      cityListSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(RegisterForm));
