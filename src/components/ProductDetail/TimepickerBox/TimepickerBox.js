import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import cs from 'classnames';
import s from './TimepickerBox.css';
import CPModal from '../../CP/CPModal';
import CPDatepicker from '../../CP/CPDatepicker';
import { ProductDto } from '../../../dtos/catalogDtos';
import {
  AgentDto,
  AvailableProductShiftDto,
  VendorBranchDto,
} from '../../../dtos/vendorDtos';
import { availableProductShiftRequest } from '../../../redux/vendor/action/availableProductShift';

const { Option } = Select;

class TimepickerBox extends React.Component {
  static propTypes = {
    data: PropTypes.objectOf(PropTypes.any),
    priceType: PropTypes.string.isRequired,
    calendarData: PropTypes.objectOf(PropTypes.any),
    title: PropTypes.string,
    clearDate: PropTypes.bool,
    inventoryStatus: PropTypes.bool,
    availableProductShift: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    onClick: PropTypes.func,
    params: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    data: {},
    title: 'تاریخ و ساعت تحویل',
    clearDate: '',
    calendarData: {},
    availableProductShift: null,
    onClick: () => {},
    params: {},
    inventoryStatus: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      calendarDate: null,
    };
    this.availableProductShiftArray = [];
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.availableProductShift) {
      this.availableProductShiftArray = [];
      if (nextProps.priceType === 'pickUp') {
        if (
          nextProps.availableProductShift &&
          nextProps.availableProductShift.pickupItems
        )
          Object.values(nextProps.availableProductShift.pickupItems).map(item =>
            this.availableProductShiftArray.push(
              <Option key={item} value={item}>
                {item}
              </Option>,
            ),
          );
      } else if (nextProps.priceType === 'delivery') {
        if (
          nextProps.availableProductShift &&
          nextProps.availableProductShift.deliveryItems
        )
          Object.values(nextProps.availableProductShift.deliveryItems).map(
            item =>
              this.availableProductShiftArray.push(
                <Option key={item} value={item}>
                  {item}
                </Option>,
              ),
          );
      }
      this.props.onClick(
        this.availableProductShiftArray,
        this.state.calendarDate,
      );
    }
    if (this.props.clearDate !== nextProps.clearDate) {
      this.setState({ calendarDate: null });
    }
  }

  changeCalenderDate = value => {
    this.setState({
      nextCalenderData: value,
    });
  };

  handelChangeInput = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOkModal = () => {
    const { params } = this.props;
    this.setState({
      visible: false,
      calendarDate: this.state.nextCalenderData,
    });

    /**
     * get available product shift
     */

    const availableProductShift = new AvailableProductShiftDto({
      persianSelectedDate: this.state.nextCalenderData,
      productDto: new ProductDto({
        id: params.id,
        vendorDto: new AgentDto({
          vendorBranchDtos: [
            new VendorBranchDto({ id: params.vendorBranchId }),
          ],
        }),
      }),
    });

    this.props.actions.availableProductShiftRequest(
      JSON.stringify(availableProductShift),
    );
  };

  handleCancelModal = () => {
    this.setState({
      visible: false,
    });
  };

  renderHourSelect = (hours, day, disabled) => {
    if (!disabled) {
      const hoursArray = [];
      hours.map(hour =>
        hoursArray.push(
          <Option key={hour} value={hour}>
            {hour}
          </Option>,
        ),
      );
      this.props.onClick(hoursArray, day);
      const thiz = this;
      setTimeout(() => {
        thiz.setState({ calendarDate: day });
      }, 50);
    }
  };
  // renderHourSelect(hours) {
  //   return (
  //     <CPSelect>
  //       {hours.map((hour, index) => (
  //         <Option key={index} value={hour}>
  //           {hour}
  //         </Option>
  //       ))}
  //     </CPSelect>
  //   );
  // }

  render() {
    const {
      title,
      priceType,
      data,
      calendarData,
      inventoryStatus,
    } = this.props;
    const { calendarDate } = this.state;
    if (priceType === 'pickUp') {
      return (
        <div className={s.timepickerBox}>
          <h3 className={s.title}>{title}</h3>
          <div className={s.contentWrap}>
            {Object.keys(data.pickupItems).map(days => (
              <div
                key={days}
                className={s.dateTime}
                onClick={() => {
                  this.renderHourSelect(
                    data.pickupItems[days].hourPeriod,
                    data.pickupItems[days].persianDate,
                    data.pickupItems[days].disabled || !inventoryStatus,
                  );
                }}
              >
                <span
                  className={cs(
                    s.date,
                    calendarDate === data.pickupItems[days].persianDate &&
                      s.dateSelect,
                    (data.pickupItems[days].disabled || !inventoryStatus) &&
                      'disabled',
                  )}
                >
                  <p>{days}</p>
                </span>
              </div>
            ))}

            <div className={s.dateTime}>
              <div
                className={cs(
                  s.date,
                  s.lastDate,
                  data.disableCalendar || (!inventoryStatus && 'disabled'),
                )}
                onClick={!data.disableCalendar ? this.showModal : null}
              >
                <i className="mf-calendar" />
                <p>
                  {this.state.calendarDate
                    ? this.state.calendarDate
                    : 'تمام تقویم'}
                </p>
              </div>
            </div>
          </div>
          <CPModal
            className={s.cpDatepiker}
            width={650}
            closable={false}
            visible={this.state.visible}
            handleOk={this.handleOkModal}
            handleCancel={this.handleCancelModal}
          >
            <CPDatepicker
              calendarData={calendarData}
              onChange={this.changeCalenderDate}
            />
          </CPModal>
        </div>
      );
    }

    return (
      <div className={s.timepickerBox}>
        <h3 className={s.title}>{title}</h3>
        <div className={s.contentWrap}>
          {Object.keys(data.deliveryItems).map(days => (
            <div
              key={days}
              className={s.dateTime}
              onClick={() => {
                this.renderHourSelect(
                  data.deliveryItems[days].deliveryShiftIds,
                  data.deliveryItems[days].persianDate,
                  data.deliveryItems[days].disabled || !inventoryStatus,
                );
              }}
            >
              <span
                className={cs(
                  s.date,
                  calendarDate === data.deliveryItems[days].persianDate &&
                    s.dateSelect,
                  (data.deliveryItems[days].disabled || !inventoryStatus) &&
                    'disabled',
                )}
              >
                <p>{days}</p>
              </span>
            </div>
          ))}

          <div className={s.dateTime}>
            <div
              className={cs(
                s.date,
                s.lastDate,
                data.disableCalendar || (!inventoryStatus && 'disabled'),
              )}
              onClick={!data.disableCalendar ? this.showModal : null}
            >
              <i className="mf-calendar" />
              <p>
                {this.state.calendarDate
                  ? this.state.calendarDate
                  : 'تمام تقویم'}
              </p>
            </div>
          </div>
        </div>
        <CPModal
          className={s.cpDatepiker}
          closable={false}
          width={650}
          visible={this.state.visible}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
        >
          <CPDatepicker
            calendarData={calendarData}
            onChange={this.changeCalenderDate}
          />
        </CPModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  availableProductShift: state.availableProductShift.availableProductShiftData,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      availableProductShiftRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(TimepickerBox));
