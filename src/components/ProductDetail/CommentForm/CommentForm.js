import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './CommentForm.css';
import CPRate from '../../CP/CPRate';
import CPInput from '../../CP/CPInput';
import CPButton from '../../CP/CPButton';
import { CommentDto, RateDto } from '../../../dtos/cmsDtos';
import { commentPostRequest } from '../../../redux/cms/action/comment';
import { BaseCRUDDtoBuilder } from '../../../dtos/dtoBuilder';
import { getCookie } from '../../../utils';
import { showNotification } from '../../../utils/helper';
import CPValidator from '../../CP/CPValidator';
import { EMAIL, NAME } from '../../../Resources/Localization';

class CommentForm extends React.Component {
  static propTypes = {
    label: PropTypes.string,
    comment: PropTypes.string,
    name: PropTypes.string,
    email: PropTypes.string,
    commentType: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    entityId: PropTypes.number.isRequired,
  };

  static defaultProps = {
    label: 'ارسال نظر',
    comment: 'نظر شما',
    name: 'نام',
    email: 'ایمیل',
    commentType: 'Product',
  };

  constructor(props) {
    super(props);
    this.state = {
      rateValue: 0,
      body: '',
      userId: '',
      userFullName: '',
      userEmail: '',
      errorValid: false,
    };
    this.validation = [];
    this.isLogin = getCookie('siteToken')
      ? getCookie('siteToken').length > 0
      : false;
  }

  handleChange = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  postComment = () => {
    const { entityId, commentType } = this.props;
    const { rateValue, body, userId, userFullName, userEmail } = this.state;

    if (
      body.length > 0 &&
      ((userFullName.length > 0 && userEmail.length > 0) || this.isLogin)
    ) {
      const commentCrud = new BaseCRUDDtoBuilder()
        .dto(
          new CommentDto({
            rateDto: new RateDto({
              value: rateValue,
              entityId,
              userFullName,
              userEmail,
            }),
            commentType,
            body,
            entityId,
            userId,
            userFullName,
            userEmail,
          }),
        )
        .build();

      const data = {
        data: commentCrud,
        isLogin: userFullName.length === 0,
      };

      this.props.actions.commentPostRequest(data);
    } else this.setState({ errorValid: true });
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
    if (this.validation.length === 3) this.disable = false;
    else this.disable = true;
  };

  render() {
    const { label, comment, name, email } = this.props;
    const { rateValue, userEmail, body, userFullName, errorValid } = this.state;
    return (
      <div className={s.form}>
        <h4 className={s.title}>
          نظر خود را در مورد این محصول با ما در میان بگذارید.
        </h4>
        <div className={s.rateBox}>
          <p>به این محصول امتیاز دهید</p>
          <CPRate
            value={rateValue}
            onChange={value => this.handleChange('rateValue', value)}
          />
          {rateValue >= 1 && <b className="rate-text">({rateValue})</b>}
        </div>
        <CPValidator
          isRequired
          showMessage={errorValid}
          checkValidation={this.checkValidation}
          value={body}
          name={comment}
        >
          <CPInput
            label={comment}
            value={body}
            onChange={value => this.handleChange('body', value.target.value)}
          />
        </CPValidator>
        {!this.isLogin && (
          <div>
            <CPValidator
              showMessage={errorValid}
              checkValidation={this.checkValidation}
              isRequired
              value={userFullName}
              name={NAME}
            >
              <CPInput
                label={name}
                value={userFullName}
                onChange={value =>
                  this.handleChange('userFullName', value.target.value)
                }
              />
            </CPValidator>
            <CPValidator
              showMessage={errorValid}
              checkValidation={this.checkValidation}
              isRequired
              name={EMAIL}
              value={userEmail}
            >
              <CPInput
                label={email}
                value={userEmail}
                onChange={value =>
                  this.handleChange('userEmail', value.target.value)
                }
              />
            </CPValidator>
          </div>
        )}
        <CPButton className={s.button} onClick={this.postComment}>
          {label}
        </CPButton>
      </div>
    );
  }
}
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      commentPostRequest,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CommentForm));
