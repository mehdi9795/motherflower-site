import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductDetail.css';
import CPTable from '../../CP/CPTable';

class ProductDetail extends React.Component {
  static propTypes = {
    specificationAttribute: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    specificationAttribute: [],
  };

  render() {
    const { specificationAttribute } = this.props;
    const dataSource = [];
    const properties = [];

    const firstColumn = [
      {
        title: 'نوع گل',
        dataIndex: 'flowerType',
        key: 'flowerType',
      },
      {
        title: 'رنگ',
        dataIndex: 'color',
        key: 'color',
      },
      {
        title: 'تعداد',
        dataIndex: 'number',
        key: 'number',
      },
    ];

    specificationAttribute.map(item => {
      if (item.customValue !== 'true') {
        const obj = {
          flowerType: item.specificationAttributeDto.name,
          color: item.colorOptionDto.name,
          number: `${item.customValueDescription} عدد `,
          key: item.id,
        };
        dataSource.push(obj);
      } else {
        properties.push(item.specificationAttributeDto.name);
      }
      return null;
    });

    return (
      <div className={s.productDetail}>
        <CPTable data={dataSource} columns={firstColumn} footer={null}  />
        <div className={s.properties}>
          <h3>سایر متعلقات</h3>
          <ul>
            {properties.map(item => (
              <li key={item}>
                <i className="mf-circle" />
                <b>{item}</b>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(ProductDetail);
