import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ShareBox.css';
import CPSocialShare from '../../CP/CPSocialShare/CPSocialShare';
import WishList from '../../ProductDetail/WishList/WishList';

class ShareBox extends React.Component {
  render() {
    return (
      <div className={s.shareBox}>
        <WishList />
        <CPSocialShare
          facebook="http://google.com"
          googlePlus="http://google.com"
          twitter="http://google.com"
          telegram="http://google.com"
          defaultSelect="تلگرام "
        />
      </div>
    );
  }
}

export default withStyles(s)(ShareBox);
