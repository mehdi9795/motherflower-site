import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './LocationBox.css';
import CPRate from '../../CP/CPRate/CPRate';

class LocationBox extends React.Component {
  static propTypes = {
    location: PropTypes.string,
    vendorName: PropTypes.string,
    rate: PropTypes.number,
  };

  static defaultProps = {
    location: 'جهان کودک',
    vendorName: 'بهرام عظیمی و برادران',
    rate: 0,
  };

  render() {
    const { location, vendorName, rate } = this.props;
    return (
      <div className={s.rateBox}>
        <div className={s.rate}>
          <CPRate value={rate} className={s.roundRate} disabled />
          <b className="rate-text">{rate}</b>
          <h6 className={s.vendorName}>{vendorName}</h6>
          <p className={s.vendorLoc}>
            <i className="fa fa-location-arrow" />
            {location}
          </p>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(LocationBox);
