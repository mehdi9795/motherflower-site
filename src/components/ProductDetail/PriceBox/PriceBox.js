import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PriceBox.css';
import CPRadio from '../../CP/CPRadio';
import PickupPrice from '../PickupPrice';
import DeliveryPrice from '../DeliveryPrice';

class PriceBox extends React.Component {
  static propTypes = {
    priceData: PropTypes.objectOf(PropTypes.any),
    deliveryEnable: PropTypes.bool,
    pickupEnable: PropTypes.bool,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    onChange: () => {},
    priceData: {},
    deliveryEnable: true,
    pickupEnable: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      priceMode: props.pickupEnable ? 'pickUp' : 'delivery',
    };
  }

  onChange = (name, value) => {
    this.setState({ [name]: value });
    this.props.onChange(value);
  };

  render() {
    const { priceData, deliveryEnable, pickupEnable } = this.props;
    const { priceMode } = this.state;

    const deliveryPrice = priceData.stringDeliveryBasePrice;
    const pickUpPrice = priceData.stringPickupBasePrice;
    let deliveryPriceDiscount = '';
    let pickUpPriceDiscount = '';
    let deliveryAmount = '';
    let PickUpAmount = '';

    if (priceData.deliveryPriceDiscount) {
      deliveryPriceDiscount = priceData.stringDeliveryPriceAfterDiscount;
      deliveryAmount = priceData.stringDeliveryPriceDiscount;
    }

    if (priceData.pickupPriceDiscount) {
      pickUpPriceDiscount = priceData.stringPickupPriceAfterDiscount;
      PickUpAmount = priceData.stringPickupPriceDiscount;
    }

    const priceModel = [
      
      {
        value: 'delivery',
        name: (
          <div className={cs(s.radioData, !deliveryEnable && 'disabled')}>
            <div className={s.label}>
              <label>قیمت ارسال محصول</label>
              <b>(Delivery)</b>
            </div>
            <DeliveryPrice
              price={deliveryPrice}
              discount={`${deliveryAmount}%`}
              lastPrice={deliveryPriceDiscount}
            />
          </div>
        ),
        disabled: !deliveryEnable,
      },
      {
        value: 'pickUp',
        name: (
          <div className={cs(s.radioData, !pickupEnable && 'disabled')}>
            <div className={s.label}>
              <label>قیمت محصول برای تحویل در گلفروشی</label>
              <b>(Pick-Up)</b>
            </div>
            <PickupPrice
              discount={`${PickUpAmount}%`}
              lastPrice={pickUpPriceDiscount}
              price={pickUpPrice}
            />
          </div>
        ),
        disabled: !pickupEnable,
      },
    ];

    return (
      <div className={s.priceBox}>
        <CPRadio
          model={priceModel}
          onChange={value => this.onChange('priceMode', value.target.value)}
          defaultValue={priceMode}
        />
      </div>
    );
  }
}

export default withStyles(s)(PriceBox);
