import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './PickupPrice.css';
import Discount from '../Discount/Discount';

class PickupPrice extends React.Component {
  static propTypes = {
    lastPrice: PropTypes.string,
    label: PropTypes.string,
    price: PropTypes.string,
    discount: PropTypes.string,
  };

  static defaultProps = {
    lastPrice: '',
    price: '',
    label: 'تومان',
    discount: '',
  };

  render() {
    const { lastPrice, label, price, discount } = this.props;
    return lastPrice ? (
      <div className={s.pickupPrice}>
        <Discount className={s.percent} discount={discount} />
        <div className={s.lastPrice}>
          <b>{lastPrice}</b>
          <p>{label}</p>
        </div>

        <div className={s.price}>
          <b>
            {price} <p>{label}</p>
          </b>
        </div>
      </div>
    ) : (
      <div className={s.pickupPrice}>
        <div className={s.lastPrice}>
          <b>{price}</b>
          <p>{label}</p>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(PickupPrice);
