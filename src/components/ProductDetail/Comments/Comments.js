import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './Comments.css';
import CPRate from '../../CP/CPRate';

class Comments extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  render() {
    const { data } = this.props;

    return (
      <div>
        <h4 className={s.title}>نظرات ثبت شده</h4>
        {Object.values(data)
          .slice(0, 2)
          .map(item => (
            <div key={item.key} className={s.comment}>
              <h4 className={s.userName}>{item.userName}</h4>
              <div className={s.rateBox}>
                <CPRate value={item.value} disabled />
              </div>
              <p className={s.date}>{item.date}</p>
              <p>{item.comment}</p>
            </div>
          ))}
      </div>
    );
  }
}

export default withStyles(s)(Comments);
