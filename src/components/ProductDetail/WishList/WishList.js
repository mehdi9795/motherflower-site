import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WishList.css';

class WishList extends React.Component {
  render() {
    return (
      <span className={s.wishList}>
        <i className="mf-favorite-o" />
      </span>
    );
  }
}

export default withStyles(s)(WishList);
