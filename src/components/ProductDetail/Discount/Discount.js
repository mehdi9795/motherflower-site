import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import PropTypes from 'prop-types';
import s from './Discount.css';
import {baseCDN} from "../../../setting";

class Discount extends React.Component {
  static propTypes = {
    discount: PropTypes.string,
    discountImg: PropTypes.string,
    className: PropTypes.string,
  };

  static defaultProps = {
    discount: '',
    discountImg: `${baseCDN}/discount.png`,
    className: 'percent',
  };

  render() {
    const { discount, discountImg, className } = this.props;
    return (
      <span className={className}>
        <img src={discountImg} alt="" />
        <b className={s.discount}>{discount}</b>
      </span>
    );
  }
}

export default withStyles(s)(Discount);
