import React from 'react';
import { Collapse } from 'antd';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Faq.css';

const { Panel } = Collapse;

class Faq extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  render() {
    const { data } = this.props;
    return (
      <Collapse defaultActiveKey={['0']}>
        {data.map(faq => (
          <Panel header={faq.question} key={faq.id}>
            <p>{faq.answer}</p>
          </Panel>
        ))}
      </Collapse>
    );
  }
}

export default withStyles(s)(Faq);
