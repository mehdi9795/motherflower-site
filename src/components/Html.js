/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import serialize from 'serialize-javascript';
import config from '../config';

class Html extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    styles: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        cssText: PropTypes.string.isRequired,
      }).isRequired,
    ),
    scripts: PropTypes.arrayOf(PropTypes.string.isRequired),
    app: PropTypes.object, // eslint-disable-line
    children: PropTypes.string.isRequired,
  };

  static defaultProps = {
    styles: [],
    scripts: [],
  };

  render() {
    const { title, description, styles, scripts, app, children, hamlet } = this.props;

    return (
      <html className="no-js" lang="en">
        <head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
            <meta name="apple-mobile-web-app-capable" content="yes" />
            <meta name="apple-mobile-web-app-status-bar-style" content="black" />
            <link rel="apple-touch-icon" href="apple-touch-icon-57x57.png" />
            <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png" />
            <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png" />
            <link rel="apple-touch-startup-image" href="apple-touch-startup-image-320x460.png" />
            <link rel="apple-touch-startup-image" sizes="768x1004" href="apple-touch-startup-image-768x1004.png" />
          <link rel="stylesheet" type="text/css" href="/css/antd.min.css" />
          <link rel="stylesheet" type="text/css" href="/css/slick.min.css" />
          <link
            rel="stylesheet"
            type="text/css"
            href="/css/slick-theme.min.css"
          />
          {/* <link rel="stylesheet" type="text/css" href="/css/style.css" /> */}
          <link rel="stylesheet" type="text/css" href="/css/antd-rtl.css" />
          <link rel="stylesheet" type="text/css" href="/css/public.css" />
          <link rel="stylesheet" type="text/css" href="/css/ant-mobile.css" />
          <link rel="stylesheet" type="text/css" href="/css/material-rtl.css" />
          <link
            rel="stylesheet"
            type="text/css"
            href="/css/confirm-alert.css"
          />
          <link rel="stylesheet" type="text/css" href="/css/grid.css" />
          <link rel="stylesheet" type="text/css" href="/css/icons.css" />
          {scripts.map(script => (
            <link key={script} rel="preload" href={script} as="script" />
          ))}
          <link rel="apple-touch-icon" href="apple-touch-icon.png" />
          {styles.map(style => (
            <style
              key={style.id}
              id={style.id}
              dangerouslySetInnerHTML={{ __html: style.cssText }}
            />
          ))}

          {/* <script src="https://www.google.com/recaptcha/api.js?hl=fa" /> */}
          {/* <script src="https://www.google.com/recaptcha/api.js" async defer /> */}
        </head>
        <body>
          <div id="app" dangerouslySetInnerHTML={{ __html: children }} />
          <script
            dangerouslySetInnerHTML={{ __html: `window.App=${serialize(app)}` }}
          />
          {scripts.map(script => <script key={script} src={script} />)}
          {config.analytics.googleTrackingId && (
            <script
              dangerouslySetInnerHTML={{
                __html:
                  'window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;' +
                  `ga('create','${
                    config.analytics.googleTrackingId
                  }','auto');ga('send','pageview')`,
              }}
            />
          )}
          {config.analytics.googleTrackingId && (
            <script
              src="https://www.google-analytics.com/analytics.js"
              async
              defer
            />
          )}
        </body>
      </html>
    );
  }
}

export default Html;
