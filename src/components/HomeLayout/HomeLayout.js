import React from 'react';
import { LoadingBar } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import $ from 'jquery';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Spin } from 'antd';
import { bindActionCreators } from 'redux';
import { Drawer } from 'antd-mobile';
import { connect } from 'react-redux';
import normalizeCss from 'normalize.css';
import s from './HomeLayout.css';
import Footer from '../Template/Footer';
import AddToCard from '../Shared/AddToCard';
import BackToTop from '../Template/BackToTop';
import CPBurgerMenu from '../CP/CPBurgerMenu';
import { hideAlertRequest } from '../../redux/shared/action/errorAlert';
import ReactGA from 'react-ga';

const DEFAULT_CONFIG = {
  trackingId: 'UA-138063597-1',
  debug: true,
  gaOptions: {
    cookieDomain: 'none',
  },
};

class HomeLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    basketProductCount: PropTypes.number,
    loadingBar: PropTypes.number,
  };

  static defaultProps = {
    basketProductCount: 0,
    loadingBar: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      showComponent: false,
    };
  }

  componentDidMount() {
    this.showComponent();
    this.initReactGA();
  }

  onOpenChange = () => {
    this.setState({ open: !this.state.open });
    setTimeout(() => {
      $('.addToCard').addClass('showFloatingBtn');
    }, 500);
  };

  showComponent = () => {
    this.setState({ showComponent: true }, () => {
      // $('#chat').addClass('showFloatingBtn');
      $('.addToCard').addClass('showFloatingBtn');
      setTimeout(() => {
        // $('#chat').addClass('shakeChatBtn');
        $('.addToCard').addClass('shakeChatBtn');
      }, 400);
      setTimeout(() => {
        // $('#chat').removeClass('shakeChatBtn');
        $('.addToCard').removeClass('shakeChatBtn');
      }, 3000);
    });
    $(document).on('contextmenu', 'img', () => false);
  };

  initReactGA = () => {
    if (window) ReactGA.initialize(DEFAULT_CONFIG);
    ReactGA.pageview(window.location.pathname);
  };

  render() {
    const { loadingBar } = this.props;
    const { showComponent } = this.state;
    const sidebar = <CPBurgerMenu />;
    return showComponent ? (
      <div className={s.PageWrapper}>
        <Drawer
          className="my-drawer"
          enableDragHandle
          style={{ minHeight: '100vh' }}
          sidebar={sidebar}
          open={this.state.open}
          onOpenChange={this.onOpenChange}
        >
          <div />
        </Drawer>
        <LoadingBar
          style={{
            backgroundColor: 'yellow',
            height: '3px',
            zIndex: 10,
            position: 'fixed',
          }}
          loading={loadingBar}
          updateTime={200}
        />
        {this.props.children}
        {/* <div className={s.topFooter}>
          <img alt="img" src={`${baseCDN}/topFooter.png`} />
        </div> */}
        <Footer />
        <AddToCard
          count={this.props.basketProductCount}
          onClick={this.onOpenChange}
        />
        {/* <Chat /> */}
        <BackToTop visibilityHeight={200} />
      </div>
    ) : (
      <div className={s.spinner}>
        <Spin loading />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  error: state.errorAlert.message,
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
  loadingBar: state.loadingBar.default,
  navigationLoading: state.loadingSearch.navigationLoading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideAlertRequest,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(normalizeCss, s)(HomeLayout));
