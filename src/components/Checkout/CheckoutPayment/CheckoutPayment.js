import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Radio, Spin, Select } from 'antd';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cs from 'classnames';
import s from './CheckoutPayment.css';
import {
  APPLY_CODE,
  YOUR_ORDERS,
  PAYMENT,
  DO_YOU_HAVE_DISCOUNT,
  TOMAN,
  TOTAL_PRICE_TO_PAY,
  DISCOUNT_CODE,
  E_PAYMENT,
  CHOOSE_YOUR_TERMINAL,
  SHIPPING_PRICE,
  TOTAL_SAVE,
  VALUE_OF_ORDER,
  READY_TO_DELIVER,
  IN_SHOPPING,
  FROM_TIME,
  THIS_PRODUCT_IN_DATE,
  PRODUCT,
  COUNT,
  UNITE_PRICE,
  TOTAL_PRICE,
  DISCOUNT,
  COMPLETE_PAYMENT,
  DELIVERY_TYPE,
  DISCOUNT_SAVE,
  SEND_TO_YOU,
  DELETE_CODE,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT,
  DO_NOT_SUBSCRIBE_TO_THE_PRODUCT_NOTES_DO_YOU_AGREE_TO_CONTINUE,
  PREVIEW,
  IN_TIME,
  PAY_BY_WALLET,
  WALLET,
} from '../../../Resources/Localization';
import CPAvatar from '../../CP/CPAvatar';
import CPButton from '../../CP/CPButton';
import CPCard from '../../CP/CPCard';
import { basketListSuccess } from '../../../redux/catalog/action/basket';
import { BasketDto, UserBasketHistoryDto } from '../../../dtos/catalogDtos';
import { getCookie } from '../../../utils';
import {
  BankPaymentRequestsApi,
  GetUserBasketHistoriesApi,
  PutUserBasketHistoriesApi,
  PutUserBasketHistoryDetailOccasionTypesApi,
} from '../../../services/samApi';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  checkoutListFailure,
  checkoutListRequest,
  checkoutListSuccess,
} from '../../../redux/sam/action/checkout';
import {
  BankPaymentRequestsDto,
  UserBasketHistoryDetailOccasionTypesDto,
} from '../../../dtos/samDtos';
import history from '../../../history';
import CPInput from '../../CP/CPInput';
import CPModal from '../../CP/CPModal';
import CPSelect from '../../CP/CPSelect';
import CPTextArea from '../../CP/CPTextArea';
import { OccasionTypeDto } from '../../../dtos/commonDtos';
import { getOccasionTypeApi } from '../../../services/commonApi';
import {
  occasionTypeListFailure,
  occasionTypeListSuccess,
} from '../../../redux/common/action/occasionType';
import CPConfirmation from '../../CP/CPConfirmation';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import { baseCDN } from '../../../setting';

const { Group } = Radio;
const { Option } = Select;

class CheckoutPayment extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    checkoutProducts: PropTypes.objectOf(PropTypes.any),
    validationFalse: PropTypes.func,
    banks: PropTypes.arrayOf(PropTypes.object),
    occasionTypes: PropTypes.arrayOf(PropTypes.object),
    userWallet: PropTypes.objectOf(PropTypes.any),
    isValid: PropTypes.func,
    setPassedStep: PropTypes.func,
  };

  static defaultProps = {
    checkoutProducts: [],
    validationFalse: () => {},
    banks: [],
    occasionTypes: [],
    isValid: () => {},
    userWallet: {},
    setPassedStep: () => {},
  };

  constructor(props) {
    super(props);
    let value = -1;
    if (props.checkoutProducts.payableAmount > 0 && props.banks.length > 0)
      value = props.banks[0].id;

    this.state = {
      value,
      webView: false,
      discountCode: props.checkoutProducts.discountCode,
      messageDiscountCode: '',
      activeDiscountCode:
        props.checkoutProducts &&
        props.checkoutProducts.totalDiscountCodeAmount > 0,
      buttonDisable: false,
      visibleModal: false,
      occasionTypeId: 0,
      occasionText: '',
      occasionPlaceHolder: '',
      checkoutProductId: 0,
      validation: true,
      visibleConfirm: false,
      isOccasion: false,
      occasionArray: [],
      visibleConfirmExistEvent: false,
      descriptionOrder: '',
      fromOccasion: '',
      toOccasion: '',
      checkoutFullMessage: false,
      textValidation: 'متن ارسالی را پر کنید',
    };
    this.props.validationFalse();
    this.applyDiscountCode = this.applyDiscountCode.bind(this);
    this.deleteDiscountCode = this.deleteDiscountCode.bind(this);
    this.onCompletePayment = this.onCompletePayment.bind(this);
    this.handleOkModal = this.handleOkModal.bind(this);
    this.handleOkConfirm = this.handleOkConfirm.bind(this);
  }

  async componentWillMount() {
    const occasionTypeList = new BaseGetDtoBuilder()
      .dto(
        new OccasionTypeDto({
          published: true,
        }),
      )
      .includes(['occasionTypeSampleDtos'])
      .buildJson();

    const response = await getOccasionTypeApi(
      getDtoQueryString(occasionTypeList),
    );

    if (response.status === 200) {
      this.props.actions.occasionTypeListSuccess(response.data);
    } else {
      this.props.actions.occasionTypeListFailure();
    }
  }

  componentDidMount() {
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.occasionTypes && nextProps.occasionTypes[0]) {
      this.setState({
        occasionTypeId: `${nextProps.occasionTypes[0].id}*${
          nextProps.occasionTypes[0].occasionTypeSampleDtos[0].name
        }`,
        occasionPlaceHolder:
          nextProps.occasionTypes[0].occasionTypeSampleDtos[0].name,
      });
    }
    this.state.occasionArray = [];
    nextProps.checkoutProducts.userBasketHistoryDetailDtos.map(item => {
      if (item.occasionTypeId) this.state.occasionArray.push(item.id);
      return null;
    });

    if (nextProps.checkoutProducts.payableAmount <= 0)
      this.setState({ value: -1 });
    else
      this.setState({
        value: nextProps.banks.length > 0 ? nextProps.banks[0].id : '',
      });
  }

  onChange = e => {
    this.setState({
      value: e.target.value,
    });
  };

  async onCompletePayment() {
    const { isOccasion, occasionArray, descriptionOrder } = this.state;
    const { checkoutProducts } = this.props;

    if (
      isOccasion ||
      occasionArray.length ===
        checkoutProducts.userBasketHistoryDetailDtos.length
    ) {
      const token = getCookie('siteToken');
      this.setState({ buttonDisable: true });

      const basketCrud = new BaseCRUDDtoBuilder()
        .dto(
          new BasketDto({
            userBasketHistoryDto: new UserBasketHistoryDto({
              description: descriptionOrder,
            }),
          }),
        )
        .build();

      const responsePut = await PutUserBasketHistoriesApi(basketCrud, token);

      if (responsePut.status === 200) {
        const bankPaymentRequestCrud = new BaseCRUDDtoBuilder()
          .dto(new BankPaymentRequestsDto({ bankId: this.state.value }))
          .build();

        const response = await BankPaymentRequestsApi(
          bankPaymentRequestCrud,
          token,
        );

        if (response.status === 200) {
          if (this.state.value === -1) {
            // this.setState({ buttonDisable: false });
            window.open(response.data, '_self');
          } else {
            window.location.href = `https://api.motherflower.com/SAM/Payment/BankPage/${
              response.data
            }/false`;

            const basketList = new BaseGetDtoBuilder()
              .dto(new BasketDto())
              .buildJson();

            const responseGet = await GetUserBasketHistoriesApi(
              getDtoQueryString(basketList),
              token,
            );
            if (responseGet.status === 200) {
              this.props.actions.basketListSuccess(responseGet.data);
              // history.push(`/returnBank/status?form=${response.data}`);
            }
          }
        } else if (response.status === 401) {
          this.props.isValid('prev');
          this.props.isValid('prev');
          this.props.setPassedStep([]);
        } else if (response.status === 604)
          showNotification(
            'error',
            '',
            response.data.errorMessage,
            7,
            'errorsBox',
          );
        else {
          history.push('/returnBank/error');
        }
      } else {
        showNotification(
          'error',
          '',
          'در ثبت توضیحات خطایی رخ داده است.',
          10,
          'errorsBox',
        );
        this.setState({ buttonDisable: false });
      }
    } else {
      this.setState({ visibleConfirmExistEvent: true });
    }
  }

  async applyDiscountCode() {
    const { discountCode } = this.state;
    const token = getCookie('siteToken');
    this.props.actions.checkoutListRequest();

    const userBasketHistoryCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserBasketHistoryDto({
          discountCode,
        }),
      )
      .build();

    const response = await PutUserBasketHistoriesApi(
      userBasketHistoryCrud,
      token,
    );
    if (response.status === 200) {
      this.setState({ activeDiscountCode: true });

      const basketList = new BaseGetDtoBuilder()
        .dto(new BasketDto({ fromCheckout: true }))
        .buildJson();

      const responseGet = await GetUserBasketHistoriesApi(
        getDtoQueryString(basketList),
        token,
      );
      if (responseGet.status === 200) {
        this.props.actions.checkoutListSuccess(responseGet.data);
      } else {
        this.props.actions.checkoutListFailure();
      }
    } else {
      this.setState({
        messageDiscountCode: response.data.errorMessage,
        discountCode: '',
      });
      this.props.actions.checkoutListFailure();
    }
  }

  async deleteDiscountCode() {
    const { discountCode } = this.state;
    const token = getCookie('siteToken');
    this.props.actions.checkoutListRequest();

    const userBasketHistoryCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserBasketHistoryDto({
          discountCode,
          deleteDiscountCode: true,
        }),
      )
      .build();

    const response = await PutUserBasketHistoriesApi(
      userBasketHistoryCrud,
      token,
    );
    if (response.status === 200) {
      this.setState({ activeDiscountCode: false });
      const basketList = new BaseGetDtoBuilder()
        .dto(new BasketDto({ fromCheckout: true }))
        .buildJson();

      const responseGet = await GetUserBasketHistoriesApi(
        getDtoQueryString(basketList),
        token,
      );
      if (responseGet.status === 200) {
        this.props.actions.checkoutListSuccess(responseGet.data);
      } else {
        this.props.actions.checkoutListFailure();
      }
    } else {
      this.props.actions.checkoutListFailure();
    }
  }

  handleInput(inputName, value) {
    this.setState({ [inputName]: value, messageDiscountCode: '' });
  }

  handleInputChange(inputName, value) {
    this.setState({ [inputName]: value });
    if (inputName === 'occasionTypeId') {
      this.setState({ occasionPlaceHolder: value.split('*')[1] });
    }
  }

  updatePredicate = () => {
    this.setState({ webView: window.innerWidth >= 992 });
    window.addEventListener('resize', () => {
      this.setState({ webView: window.innerWidth >= 992 });
    });
  };

  showModal = (
    checkoutProductId,
    occasionText = '',
    fromOccasionText = '',
    toOccasionText = '',
    checkoutFullMessage = false,
  ) => {
    this.setState({
      visibleModal: true,
      checkoutProductId,
      occasionText,
      fromOccasion: fromOccasionText,
      toOccasion: toOccasionText,
      checkoutFullMessage,
      validation: true,
    });
  };

  async handleOkModal() {
    const {
      occasionText,
      occasionTypeId,
      checkoutProductId,
      checkoutFullMessage,
      fromOccasion,
      toOccasion,
    } = this.state;
    const token = getCookie('siteToken');
    if (checkoutFullMessage && fromOccasion.length === 0) {
      this.setState({
        validation: false,
        textValidation: 'از طرف و متن پیام را پر کنید.',
      });
      return;
    }
    if (occasionText.length > 0) {
      const occasionCrud = new BaseCRUDDtoBuilder()
        .dto(
          new UserBasketHistoryDetailOccasionTypesDto({
            id: checkoutProductId,
            occasionTypeId: occasionTypeId.split('*')[0],
            occasionText,
            fromOccasionText: fromOccasion,
            toOccasionText: toOccasion,
          }),
        )
        .build();

      const response = await PutUserBasketHistoryDetailOccasionTypesApi(
        occasionCrud,
        token,
      );

      if (response.data.isSuccess) {
        const basketList = new BaseGetDtoBuilder()
          .dto(new BasketDto({ fromCheckout: true }))
          .buildJson();

        const responseCheckout = await GetUserBasketHistoriesApi(
          getDtoQueryString(basketList),
          token,
        );
        if (responseCheckout.status === 200) {
          this.props.actions.checkoutListSuccess(responseCheckout.data);
        } else {
          this.props.actions.checkoutListFailure();
        }
        if (this.state.occasionArray.indexOf(checkoutProductId) === -1)
          this.state.occasionArray.push(checkoutProductId);
      }
      this.setState({
        visibleModal: false,
        textValidation: 'متن پیام را پر کنید',
      });
    }
    this.setState({ validation: false, textValidation: 'متن پیام را پر کنید' });
  }

  handleCancelModal = () => {
    this.setState({ visibleModal: false });
  };

  async handleOkConfirm() {
    const { checkoutProductId } = this.state;
    const token = getCookie('siteToken');

    const occasionCrud = new BaseCRUDDtoBuilder()
      .dto(
        new UserBasketHistoryDetailOccasionTypesDto({
          id: checkoutProductId,
          occasionText: null,
          occasionTypeId: null,
        }),
      )
      .build();

    const response = await PutUserBasketHistoryDetailOccasionTypesApi(
      occasionCrud,
      token,
    );

    if (response.data.isSuccess) {
      const basketList = new BaseGetDtoBuilder()
        .dto(new BasketDto({ fromCheckout: true }))
        .buildJson();

      const responseCheckout = await GetUserBasketHistoriesApi(
        getDtoQueryString(basketList),
        token,
      );
      if (responseCheckout.status === 200) {
        this.props.actions.checkoutListSuccess(responseCheckout.data);
      } else {
        this.props.actions.checkoutListFailure();
      }
    }

    this.setState({ visibleConfirm: false });
  }

  handleCancelConfirm = () => {
    this.setState({ visibleConfirm: false });
  };

  showConfirm = checkoutProductId => {
    this.setState({ visibleConfirm: true, checkoutProductId });
  };

  handleOkConfirmExistEvent = () => {
    this.setState({ isOccasion: true, visibleConfirmExistEvent: false }, () =>
      this.onCompletePayment(),
    );
  };

  handleCancelConfirmExistEvent = () => {
    this.setState({ visibleConfirmExistEvent: false });
  };

  showPreviewModal = () => {
    this.setState({ previewModal: true });
  };

  cancelPreviewModal = () => {
    this.setState({ previewModal: false });
  };

  render() {
    const { checkoutProducts, banks, occasionTypes, userWallet } = this.props;
    const {
      webView,
      discountCode,
      messageDiscountCode,
      activeDiscountCode,
      buttonDisable,
      visibleModal,
      occasionTypeId,
      occasionText,
      occasionPlaceHolder,
      validation,
      visibleConfirm,
      visibleConfirmExistEvent,
      descriptionOrder,
      fromOccasion,
      toOccasion,
      textValidation,
      previewModal,
    } = this.state;

    const occasionTypeArray = [];

    occasionTypes.map(item =>
      occasionTypeArray.push(
        <Option
          key={item.id}
          value={`${item.id}*${item.occasionTypeSampleDtos &&
            item.occasionTypeSampleDtos[0].name}`}
        >
          {item.name}
        </Option>,
      ),
    );

    return (
      <div className={s.checkoutContainer}>
        {webView ? (
          <div className={s.checkoutPayment}>
            <div className={s.formWrapper}>
              {/* <i className="mf-credit-card" /> */}
              <h3 className={s.title}>{PAYMENT}</h3>
              <div className={s.orders}>
                <h4 className={s.orderTitle}>{YOUR_ORDERS}</h4>
                <div className={s.productDetail}>
                  <ul className={s.productHeader}>
                    <li className="col-md-3">
                      <b>{PRODUCT}</b>
                    </li>
                    <li className="col-md-1">
                      <b>{COUNT}</b>
                    </li>
                    <li className="col-md-2">
                      <b>{UNITE_PRICE}</b>
                    </li>
                    <li className="col-md-2">
                      <b>{DISCOUNT}</b>
                    </li>
                    <li className="col-md-2">
                      <b>{SHIPPING_PRICE}</b>
                    </li>
                    <li className="col-md-2">
                      <b>{TOTAL_PRICE}</b>
                    </li>
                  </ul>
                  {checkoutProducts.userBasketHistoryDetailDtos.map(item => (
                    <div
                      key={item.id}
                      className={
                        item.priceOption === 'Pickup'
                          ? s.pickupList
                          : s.deliveryList
                      }
                    >
                      {item.expired && (
                        <span className={s.expired}>
                          <p>زمان ارسال / تحویل به اتمام رسید</p>
                        </span>
                      )}
                      <ul className={s.productList}>
                        <li className="col-md-3">
                          <CPAvatar
                            src={
                              item.vendorBranchProductDto.productDto.imageDtos
                                ? item.vendorBranchProductDto.productDto
                                    .imageDtos[0].url
                                : ''
                            }
                          />
                          <p className={s.productName}>
                            {item.vendorBranchProductDto.productDto.name}
                          </p>
                          <p>
                            {item.vendorBranchProductDto.vendorBranchDto.name}
                          </p>
                          <p className={s.type}>
                            {item.priceOption === 'Pickup'
                              ? 'تحویل درب گل فروشی (Pick up)'
                              : 'تحویل در محل (Delivery)'}
                          </p>
                        </li>
                        <li className="col-md-1">
                          <div className={s.counter}>{item.count}</div>
                        </li>
                        <li className="col-md-2">
                          <span className={s.unitePrice}>
                            <b>
                              {item.priceOption === 'Pickup'
                                ? item.vendorBranchProductDto.productDto
                                    .vendorBranchProductPriceDtos[0]
                                    .stringPickupBasePrice
                                : item.vendorBranchProductDto.productDto
                                    .vendorBranchProductPriceDtos[0]
                                    .stringDeliveryBasePrice}
                            </b>
                            <small>{TOMAN}</small>
                          </span>
                        </li>
                        <li className="col-md-2">
                          <span className={s.save}>
                            <b>{item.stringTotalDiscount}</b>
                            {item.totalDiscount !== 0 && (
                              <small> {TOMAN}</small>
                            )}
                          </span>
                        </li>
                        <li className="col-md-2">
                          <span className={s.save}>
                            <b>{item.stringShipmentAmount}</b>
                            {item.shipmentAmount !== 0 && (
                              <small> {TOMAN}</small>
                            )}
                          </span>
                        </li>
                        <li className="col-md-2">
                          <b>{item.stringPayableAmount}</b>
                          {item.payableAmount !== 0 && <small> {TOMAN}</small>}
                        </li>
                      </ul>
                      <div className={s.description}>
                        <div className={s.widthDescription}>
                          <img
                            className={s.giftImg}
                            src={
                              item.priceOption === 'Pickup'
                                ? `${baseCDN}/present1.png`
                                : `${baseCDN}/gps.png`
                            }
                            alt=""
                          />
                          <b>{THIS_PRODUCT_IN_DATE} </b>
                          <b className={s.date}> {item.requestedDate} </b>
                          <b>
                            {item.priceOption === 'Pickup'
                              ? FROM_TIME
                              : IN_TIME}{' '}
                          </b>
                          <b className={s.time}> {item.requestedHour} </b>
                          <b>
                            {' '}
                            {item.priceOption === 'Pickup' && IN_SHOPPING}{' '}
                          </b>
                          <b>
                            {item.priceOption === 'Pickup' &&
                              item.vendorBranchProductDto.vendorBranchDto.name}
                          </b>
                          <b>
                            {item.priceOption === 'Pickup'
                              ? READY_TO_DELIVER
                              : SEND_TO_YOU}
                          </b>
                        </div>
                        {!item.occasionTypeId ? (
                          <CPButton
                            className={s.btnEvent}
                            onClick={() =>
                              this.showModal(
                                item.id,
                                '',
                                '',
                                '',
                                item.checkoutFullMessage,
                              )
                            }
                          >
                            یادداشت مناسبت <i className="mf-pen" />
                          </CPButton>
                        ) : (
                          <div className={s.btnEventOk}>
                            <i className="mf-check" />
                            پیام ثبت شد
                            <div>
                              <i
                                className="mf-trash"
                                onClick={() => this.showConfirm(item.id)}
                              />
                              <i
                                className="mf-pen"
                                onClick={() =>
                                  this.showModal(
                                    item.id,
                                    item.occasionText,
                                    item.fromOccasionText,
                                    item.toOccasionText,
                                    item.checkoutFullMessage,
                                  )
                                }
                              />
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  ))}
                </div>

                <div className={s.userDescription}>
                  <h4>
                    <img className={s.penImg} src={`${baseCDN}/pen.png`} />چنانچه
                    توضیحاتی برای فروشنده دارید، وارد نمایید.
                  </h4>
                  <CPInput
                    hintText="توضیحات"
                    onChange={value =>
                      this.handleInput('descriptionOrder', value.target.value)
                    }
                    value={descriptionOrder}
                    maxLength={500}
                  />
                </div>

                <div className={s.coupon}>
                  <div className="col-sm-6">
                    <div className={s.couponForm}>
                      <img
                        className={s.couponImg}
                        src={`${baseCDN}/gift.png`}
                        alt=""
                      />
                      <h4>{DO_YOU_HAVE_DISCOUNT}</h4>
                      {!activeDiscountCode && (
                        <div>
                          <input
                            className={
                              messageDiscountCode ? s.inputError : s.input
                            }
                            value={discountCode}
                            onChange={value =>
                              this.handleInput(
                                'discountCode',
                                value.target.value,
                              )
                            }
                            placeholder={
                              messageDiscountCode ||
                              'لطفا کد تخفیف خود را وارد نمایید ...'
                            }
                          />

                          <CPButton
                            className="greenBtn discountBtn"
                            disabled={!discountCode}
                            onClick={() => this.applyDiscountCode()}
                          >
                            {APPLY_CODE}
                          </CPButton>
                        </div>
                      )}
                      {activeDiscountCode && (
                        <CPButton
                          className={s.yellowBtn}
                          onClick={() => this.deleteDiscountCode()}
                        >
                          {DELETE_CODE}
                        </CPButton>
                      )}
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <ul className={s.receiptPrices}>
                      <li>
                        <b>{VALUE_OF_ORDER} :</b>
                        <span className={s.price}>
                          <b>{checkoutProducts.stringTotalAmount}</b>
                        </span>
                      </li>
                      <li>
                        <b>{DISCOUNT} :</b>
                        <span className={s.price}>
                          <b>{checkoutProducts.stringTotalDiscount}</b>
                        </span>
                      </li>
                      <li>
                        <b>{SHIPPING_PRICE} :</b>
                        <span className={s.price}>
                          <b>{checkoutProducts.stringTotalShipmentAmount}</b>
                        </span>
                      </li>
                      <li>
                        <b>{DISCOUNT_CODE} :</b>
                        <span className={s.price}>
                          <b>
                            {checkoutProducts.stringTotalDiscountCodeAmount}
                          </b>
                        </span>
                      </li>
                      <li>
                        <b>{TOTAL_PRICE_TO_PAY} :</b>
                        <span className={s.price}>
                          <b>{checkoutProducts.stringPayableAmount}</b>
                          <small>({TOMAN})</small>
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              {checkoutProducts.payableAmount > 0 && (
                <div className={s.banks}>
                  <h4 className={s.paymentTitle}>{E_PAYMENT}</h4>
                  <b className={s.terminal}>{CHOOSE_YOUR_TERMINAL}</b>
                  <div className={s.radios}>
                    <Group onChange={this.onChange} value={this.state.value}>
                      {banks.map(item => (
                        <Radio
                          key={item.id}
                          value={item.id}
                          disabled={checkoutProducts.payableAmount <= 0}
                        >
                          <img
                            className={s.logo}
                            src={item.imageDto.url}
                            alt=""
                          />
                          {item.name}
                        </Radio>
                      ))}
                      <Radio
                        key={-1}
                        value={-1}
                        disabled={
                          userWallet.totalAmount <
                            checkoutProducts.payableAmount ||
                          checkoutProducts.payableAmount === 0
                        }
                      >
                        <img
                          className={s.logo}
                          src={`${baseCDN}/wallet.png`}
                          alt=""
                        />
                        <span>
                          {` ${WALLET} (${
                            userWallet.stringTotalAmount
                          } ${TOMAN})`}
                        </span>
                      </Radio>
                    </Group>
                  </div>
                </div>
              )}
              <CPButton
                disabled={
                  checkoutProducts &&
                  checkoutProducts.hasExpiredProduct &&
                  buttonDisable
                }
                className="confirmationBtn greenBtn"
                onClick={() => this.onCompletePayment()}
              >
                {buttonDisable ? (
                  <Spin spinning={buttonDisable} />
                ) : (
                  COMPLETE_PAYMENT
                )}
              </CPButton>
            </div>
          </div>
        ) : (
          <div className={s.formWrapper}>
            <div className={s.orders}>
              <div className={s.productDetail}>
                {checkoutProducts.userBasketHistoryDetailDtos.map(item => (
                  <CPCard
                    key={item.id}
                    className={
                      item.priceOption === 'Pickup'
                        ? s.mobileCardPickUp
                        : s.mobileCardDelivery
                    }
                  >
                    {item.expired && (
                      <span className={s.expired}>
                        <p>زمان ارسال / تحویل به اتمام رسید</p>
                      </span>
                    )}
                    <div className={s.details}>
                      <CPButton className={s.deleteAction} type="circle">
                        <i className="mf-close" />
                      </CPButton>
                      <CPAvatar
                        src={
                          item.vendorBranchProductDto.productDto.imageDtos
                            ? item.vendorBranchProductDto.productDto
                                .imageDtos[0].url
                            : ''
                        }
                      />
                      <span className={s.proDetail}>
                        <p className={s.productName}>
                          {item.vendorBranchProductDto.productDto.name}
                        </p>
                        <p>
                          {item.vendorBranchProductDto.vendorBranchDto.name}
                        </p>
                      </span>
                    </div>
                    <div className={s.mobileCounter}>
                      <b>تعداد : </b>
                      <div className={s.counterBtns}>{item.count}</div>
                    </div>
                    <div className={s.unitePrice}>
                      <b>{UNITE_PRICE} : </b>
                      <span className={s.priceContainer}>
                        <b className={s.priceValue}>
                          {item.priceOption === 'Pickup'
                            ? item.vendorBranchProductDto.productDto
                                .vendorBranchProductPriceDtos[0]
                                .stringPickupBasePrice
                            : item.vendorBranchProductDto.productDto
                                .vendorBranchProductPriceDtos[0]
                                .stringDeliveryBasePrice}{' '}
                        </b>
                        <b className={s.toman}>{TOMAN}</b>
                      </span>
                    </div>
                    <div className={s.unitePrice}>
                      <b>{SHIPPING_PRICE} : </b>
                      <b className={s.priceValue}>
                        {item.stringShipmentAmount}{' '}
                        <b className={s.toman}>{TOMAN}</b>
                      </b>
                    </div>
                    <div className={s.totalPrice}>
                      <b>{TOTAL_PRICE} : </b>
                      <b className={s.priceValue}>
                        {item.stringPayableAmount}{' '}
                        <b className={s.toman}>{TOMAN}</b>
                      </b>
                    </div>
                    {item.totalDiscount > 0 && (
                      <div className={s.mobileDiscount}>
                        <b>{DISCOUNT_SAVE} : </b>
                        <b>{item.stringTotalDiscount}</b>
                        <b className={s.toman}>{TOMAN}</b>
                      </div>
                    )}
                    <div className={s.description}>
                      <p className={s.type}>
                        <b>{DELIVERY_TYPE} : </b>
                        {item.priceOption === 'Pickup'
                          ? 'تحویل درب گل فروشی (Pick up)'
                          : 'تحویل در محل (Delivery)'}
                      </p>
                      <b>{THIS_PRODUCT_IN_DATE} </b>
                      <b className={s.date}> {item.requestedDate} </b>
                      <b>
                        {item.priceOption === 'Pickup' ? FROM_TIME : IN_TIME}{' '}
                      </b>
                      <b className={s.time}> {item.requestedHour} </b>
                      <b>
                        {' '}
                        {item.priceOption === 'Pickup' ? IN_SHOPPING : ''}{' '}
                      </b>
                      <b>
                        {item.priceOption === 'Pickup'
                          ? item.vendorBranchProductDto.vendorBranchDto.name
                          : ''}
                      </b>
                      <b>
                        {item.priceOption === 'Pickup'
                          ? READY_TO_DELIVER
                          : SEND_TO_YOU}{' '}
                      </b>
                    </div>
                    <div>
                      {!item.occasionTypeId ? (
                        <CPButton
                          className={s.btnEventApp}
                          onClick={() =>
                            this.showModal(
                              item.id,
                              '',
                              '',
                              '',
                              item.checkoutFullMessage,
                            )
                          }
                        >
                          یادداشت مناسبت <i className="mf-pen" />
                        </CPButton>
                      ) : (
                        <div className={s.btnEventAppOk}>
                          <i className="mf-check" />
                          پیام ثبت شد
                          <div>
                            <i
                              className="mf-trash"
                              onClick={() => this.showConfirm(item.id)}
                            />
                            <i
                              className="mf-pen"
                              onClick={() =>
                                this.showModal(
                                  item.id,
                                  item.occasionText,
                                  item.fromOccasionText,
                                  item.toOccasionText,
                                  item.checkoutFullMessage,
                                )
                              }
                            />
                          </div>
                        </div>
                      )}
                    </div>
                  </CPCard>
                ))}
              </div>
              <div className={cs(s.coupon, s.descriptionMobile)}>
                <div className={s.userDescription}>
                  <h4>
                    <img className={s.penImg} src={`${baseCDN}/pen.png`} />چنانچه
                    توضیحاتی برای فروشنده دارید، وارد نمایید.
                  </h4>
                  <CPInput
                    hintText="توضیحات"
                    onChange={value =>
                      this.handleInput('descriptionOrder', value.target.value)
                    }
                    value={descriptionOrder}
                    maxLength={500}
                  />
                </div>
              </div>
              <div className={s.coupon}>
                <div className={s.mask} />
                <div className={s.couponForm}>
                  <h4>{DO_YOU_HAVE_DISCOUNT}</h4>
                  {!activeDiscountCode && (
                    <div>
                      <input
                        className={messageDiscountCode ? s.inputError : s.input}
                        value={discountCode}
                        onChange={value =>
                          this.handleInput('discountCode', value.target.value)
                        }
                        placeholder={
                          messageDiscountCode ||
                          'لطفا کد تخفیف خود را وارد نمایید ...'
                        }
                      />

                      <CPButton
                        className="greenBtn discountBtn"
                        onClick={() => this.applyDiscountCode()}
                      >
                        {APPLY_CODE}
                      </CPButton>
                    </div>
                  )}
                  {activeDiscountCode && (
                    <CPButton
                      className={s.yellowBtn}
                      onClick={() => this.deleteDiscountCode()}
                    >
                      {DELETE_CODE}
                    </CPButton>
                  )}
                </div>
                <div className={s.gift}>
                  <img src={`${baseCDN}/gift.png`} alt="" />
                </div>
              </div>
              <div className={s.total}>
                <ul className={s.receiptPrices}>
                  <li>
                    <b>{VALUE_OF_ORDER} :</b>
                    <span className={s.price}>
                      <b>{checkoutProducts.stringTotalAmount} </b>
                    </span>
                  </li>
                  <li>
                    <b> {TOTAL_SAVE} :</b>
                    <span className={s.price}>
                      <b> {checkoutProducts.stringTotalDiscount}</b>
                    </span>
                  </li>
                  <li>
                    <b>{SHIPPING_PRICE} :</b>
                    <span className={s.price}>
                      <b>{checkoutProducts.stringTotalShipmentAmount}</b>
                    </span>
                  </li>
                  <li>
                    <b>{DISCOUNT_CODE} :</b>
                    <span className={s.price}>
                      <b> {checkoutProducts.stringTotalDiscountCodeAmount} </b>
                    </span>
                  </li>
                  <li>
                    <b>{TOTAL_PRICE_TO_PAY} :</b>
                    <span className={s.price}>
                      <b>{checkoutProducts.stringPayableAmount}</b>
                      <b className={s.toman}>({TOMAN})</b>
                    </span>
                  </li>
                </ul>
              </div>
            </div>

            {checkoutProducts.payableAmount > 0 && (
              <div className={s.banks}>
                <h4 className={s.paymentTitle}>{E_PAYMENT}</h4>
                <b className={s.terminal}>{CHOOSE_YOUR_TERMINAL}</b>
                <div className={s.radios}>
                  <Group onChange={this.onChange} value={this.state.value}>
                    {banks.map(item => (
                      <Radio
                        key={item.id}
                        value={item.id}
                        disabled={checkoutProducts.payableAmount <= 0}
                      >
                        <img
                          className={s.logo}
                          src={item.imageDto.url}
                          alt=""
                        />
                        {item.name}
                      </Radio>
                    ))}
                    <Radio
                      key={-1}
                      value={-1}
                      disabled={
                        userWallet.totalAmount <
                          checkoutProducts.payableAmount ||
                        checkoutProducts.payableAmount <= 0
                      }
                    >
                      <img
                        className={s.logo}
                        src={`${baseCDN}/wallet.png`}
                        alt=""
                      />
                      <span>
                        {` ${WALLET} (${
                          userWallet.stringTotalAmount
                        } ${TOMAN})`}
                      </span>
                    </Radio>
                  </Group>
                </div>
              </div>
            )}
            <CPButton
              disabled={
                checkoutProducts &&
                checkoutProducts.hasExpiredProduct &&
                buttonDisable
              }
              className="confirmationBtn greenBtn"
              onClick={() => this.onCompletePayment()}
            >
              {buttonDisable ? (
                <Spin spinning={buttonDisable} />
              ) : (
                COMPLETE_PAYMENT
              )}
            </CPButton>
          </div>
        )}
        <CPModal
          visible={visibleModal}
          handleOk={this.handleOkModal}
          handleCancel={this.handleCancelModal}
          textSave="تایید"
          customButton={
            <CPButton onClick={this.showPreviewModal} className={s.previewBtn}>
              {PREVIEW}
            </CPButton>
          }
        >
          <div className="bodyModal">
            <label>انتخاب مناسبت</label>
            <CPSelect
              onChange={value =>
                this.handleInputChange('occasionTypeId', value)
              }
              value={occasionTypeId}
            >
              {occasionTypeArray}
            </CPSelect>
          </div>
          <div className="bodyModal">
            <label>از طرف</label>
            <CPInput
              value={fromOccasion}
              isValidClass="hideITag"
              onChange={value =>
                this.handleInputChange('fromOccasion', value.target.value)
              }
            />
          </div>
          <div className="bodyModal">
            <label>به</label>
            <CPInput
              value={toOccasion}
              isValidClass="hideITag"
              onChange={value =>
                this.handleInputChange('toOccasion', value.target.value)
              }
            />
          </div>
          <div className="bodyModal">
            <label>متن ارسالی</label>
            <CPTextArea
              name="description"
              autoSize
              value={occasionText}
              onChange={value =>
                this.handleInputChange('occasionText', value.target.value)
              }
              placeholder={occasionPlaceHolder}
              className={s.textArea}
            />
          </div>
          {!validation && (
            <label className={s.validationOccasion}>{textValidation}</label>
          )}
        </CPModal>
        <CPModal
          visible={previewModal}
          handleCancel={this.cancelPreviewModal}
          textSave="تایید"
          showFooter={false}
          className="preview"
        >
          <div className={s.parentBody}>
            <div className={s.bodyPreview}>
              <div>{toOccasion}</div>
              <div>{occasionText}</div>
              <div> از طرف:{fromOccasion}</div>
            </div>
          </div>
        </CPModal>
        <CPConfirmation
          visible={visibleConfirm}
          handleOk={this.handleOkConfirm}
          handleCancel={this.handleCancelConfirm}
          content={ARE_YOU_SURE_WANT_TO_DELETE_THE_EVENT}
        />
        <CPConfirmation
          visible={visibleConfirmExistEvent}
          handleOk={this.handleOkConfirmExistEvent}
          handleCancel={this.handleCancelConfirmExistEvent}
          content={
            DO_NOT_SUBSCRIBE_TO_THE_PRODUCT_NOTES_DO_YOU_AGREE_TO_CONTINUE
          }
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  checkoutProducts: state.samCheckout.checkoutListData.items[0],
  banks: state.samBank.bankListData ? state.samBank.bankListData.items : [],
  occasionTypes: state.occasionType.occasionTypeListData
    ? state.occasionType.occasionTypeListData.items
    : [],
  userWallet:
    state.samWallet.walletListData &&
    state.samWallet.walletListData.items &&
    state.samWallet.walletListData.items.length > 0
      ? state.samWallet.walletListData.items[0]
      : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      checkoutListRequest,
      checkoutListSuccess,
      checkoutListFailure,
      basketListSuccess,
      occasionTypeListSuccess,
      occasionTypeListFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(CheckoutPayment));
