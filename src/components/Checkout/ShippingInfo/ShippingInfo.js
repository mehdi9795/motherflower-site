import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ShippingInfo.css';
import {
  ORDER_ADDRESS,
  ADD_NEW_ORDER_ADDRESS,
  ADD_NEW_ADDRESS,
  YES,
  NO,
  CONFIRM_AND_ISSUANCE,
  ADDRESS,
  RECEIVER_NAME,
  POSTAL_ADDRESS,
  ENTER_THE_EXACT_ADDRESS,
  DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP,
  ORDERING_INFORMATION,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_ADDRESS,
  SAVE,
  CANCEL,
} from '../../../Resources/Localization';
import CPButton from '../../CP/CPButton';
import CPTable from '../../CP/CPTable';
import CPPopConfirm from '../../CP/CPPopConfirm';
import CPRadio from '../../CP/CPRadio';
import CPModal from '../../CP/CPModal';
import CPInput from '../../CP/CPInput';
import CPSelect from '../../CP/CPSelect';
import { AddressDto } from '../../../dtos/identityDtos';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import {
  DeleteAddressUser,
  GetAddressUser,
  PostAddressUser,
  PutAddressUser,
} from '../../../services/identityApi';
import { getCookie } from '../../../utils';
import {
  addressDeleteFailure,
  addressDeleteRequest,
  addressDeleteSuccess,
  addressListFailure,
  addressListRequest,
  addressListSuccess,
  addressPostFailure,
  addressPostRequest,
  addressPostSuccess,
  addressPutFailure,
  addressPutRequest,
  addressPutSuccess,
} from '../../../redux/identity/action/address';
import CPMap from '../../CP/CPMap';
import { CityDto, DistrictDto } from '../../../dtos/commonDtos';
import { getCityApi, getDistrictApi } from '../../../services/commonApi';
import {
  districtOptionListSuccess,
  singleDistrictSuccess,
} from '../../../redux/common/action/district';
import { BasketDto, UserBasketHistoryDto } from '../../../dtos/catalogDtos';
import {
  GetUserBasketHistoriesApi,
  GetWalletsApi,
  PutUserBasketHistoriesApi,
} from '../../../services/samApi';
import {
  checkoutListFailure,
  checkoutListRequest,
  checkoutListSuccess,
} from '../../../redux/sam/action/checkout';
import CPCard from '../../CP/CPCard';
import LocationBox from '../../ProductDetail/LocationBox';
import history from '../../../history';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import CPValidator from '../../CP/CPValidator';
import { loginSuccess } from '../../../redux/identity/action/account';
import { cityListSuccess } from '../../../redux/common/action/city';
import {
  BaseCRUDDtoBuilder,
  BaseGetDtoBuilder,
} from '../../../dtos/dtoBuilder';
import {
  walletListFailure,
  walletListSuccess,
} from '../../../redux/sam/action/wallet';
import { getVendorBranchCityApi } from '../../../services/vendorApi';
import {
  VendorBranchCitiesDto,
  VendorBranchDto,
} from '../../../dtos/vendorDtos';

const { Option } = Select;

class ShippingInfo extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    addresses: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    districtOptions: PropTypes.arrayOf(PropTypes.object),
    basketProducts: PropTypes.objectOf(PropTypes.any),
    validationFalse: PropTypes.func,
    isValid: PropTypes.func,
    setPassedStep: PropTypes.func,
  };

  static defaultProps = {
    addresses: [],
    cities: [],
    districtOptions: [],
    basketProducts: {},
    validationFalse: () => {},
    isValid: () => {},
    setPassedStep: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      editAddressModal: false,
      addNewAddressModal: false,
      idSelected:
        props.basketProducts.hasDeliverProduct &&
        props.addresses &&
        props.addresses[0]
          ? props.addresses[0].id
          : 0,
      receiveName: '',
      phone: '',
      cityId:
        props.cities && props.cities[0] ? props.cities[0].id.toString() : '',
      districtId:
        props.districtOptions && props.districtOptions[0]
          ? `${props.districtOptions[0].id.toString()}*${
              props.districtOptions[0].lat
            }*${props.districtOptions[0].lng}`
          : '',
      content: '',
      contentEdit: '',
      editId: 0,
      lat:
        props.districtOptions && props.districtOptions[0]
          ? props.districtOptions[0].lat
          : 35.6891975,
      lng:
        props.districtOptions && props.districtOptions[0]
          ? props.districtOptions[0].lng
          : 51.3889736,
      disabled: false,
      showMessagePhone: false,
      messagePhoneNumber: '',
      phoneFinal: '',
      errorValid: false,
    };
    this.props.validationFalse();
    this.handleOkModal = this.handleOkModal.bind(this);
    this.handleOkEditModal = this.handleOkEditModal.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.addressConfirm = this.addressConfirm.bind(this);
    this.checkVendorBranchIsRepeat = this.checkVendorBranchIsRepeat.bind(this);
    this.vendorBranchName = [];
    this.changeMap = false;
    this.validation = [];
  }

  async componentWillMount() {
    /**
     * get all cities for first time with default clause
     */
    const responseCity = await getCityApi(
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );

    if (responseCity.status === 200) {
      this.props.actions.cityListSuccess(responseCity.data);

      const districtList = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityDto: new CityDto({ id: responseCity.data.items[0].id }),
          }),
        )
        .buildJson();

      const responseDistrict = await getDistrictApi(
        getDtoQueryString(districtList),
      );

      if (responseDistrict.status === 200) {
        this.props.actions.districtOptionListSuccess(responseDistrict.data);
      }
    }
  }

  async componentDidMount() {
    const token = getCookie('siteToken');
    if (
      !(
        this.props.basketProducts &&
        this.props.basketProducts.userBasketHistoryDetailDtos &&
        this.props.basketProducts.userBasketHistoryDetailDtos.length > 0
      )
    ) {
      setTimeout(() => {
        history.push('/');
      }, 3000);
    }

    if (this.props.basketProducts.multiVendor) {
      history.push('/card');
    }

    const responseWalletUser = await GetWalletsApi(token, { dto: {} });
    if (responseWalletUser.status === 200)
      this.props.actions.walletListSuccess(responseWalletUser.data);
    else this.props.actions.walletListFailure();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cities) {
      this.setState({
        cityId:
          nextProps.cities && nextProps.cities[0]
            ? nextProps.cities[0].id.toString()
            : '',
        districtId:
          nextProps.districtOptions && nextProps.districtOptions[0]
            ? `${nextProps.districtOptions[0].id.toString()}*${
                nextProps.districtOptions[0].lat
              }*${nextProps.districtOptions[0].lng}`
            : '',
      });
    }
  }

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  onChangeAddress = value => {
    this.setState({ idSelected: value });
  };

  async onDelete(id) {
    const token = getCookie('siteToken');
    this.props.actions.addressDeleteRequest();
    const response = await DeleteAddressUser(token, id);
    if (response.status === 200) {
      this.props.actions.addressDeleteSuccess();
      this.getAddress();
      showNotification('success', '', 'َآدرس موردنظر با موفقیت حذف شد.', 10);
    } else {
      this.props.actions.addressDeleteFailure();
      this.props.actions.showAlertRequest(response.data.errorMessage);
    }
  }

  async getAddress() {
    const token = getCookie('siteToken');
    this.props.actions.addressListRequest();

    const response = await GetAddressUser(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new AddressDto({ active: true }))
          .buildJson(),
      ),
    );
    if (response.status === 200) {
      this.props.actions.addressListSuccess(response.data);
      this.setState({
        disabled: response.data.items.length === 0,
        idSelected:
          response.data.items.length > 0 ? response.data.items[0].id : 0,
      });
    } else {
      this.props.actions.addressListFailure();
    }
  }

  setLatAndLng = value => {
    this.setState({ lat: value.lat, lng: value.lng });
  };

  async handleOkModal() {
    const {
      receiveName,
      phoneFinal,
      cityId,
      districtId,
      content,
      lat,
      lng,
      showMessagePhone,
    } = this.state;
    if (this.validation.length === 2 && !showMessagePhone) {
      const token = getCookie('siteToken');
      this.props.actions.addressPostRequest();

      const addressCrud = new BaseCRUDDtoBuilder()
        .dto(
          new AddressDto({
            content,
            receiverName: receiveName,
            phone: phoneFinal,
            cityId,
            districtId: districtId.split('*')[0],
            lat,
            lng,
          }),
        )
        .build();

      const response = await PostAddressUser(token, addressCrud);

      if (response.status === 200) {
        this.props.actions.addressPostSuccess();
        this.getAddress();
        showNotification('success', '', 'آدرس با موفقیت اضافه شد.', 10);
      } else {
        this.props.actions.addressPostFailure();
        this.props.actions.showAlertRequest(response.data.errorMessage);
      }
      this.setState({
        editAddressModal: false,
        addNewAddressModal: false,
      });
    } else {
      this.setState({ errorValid: true });
    }
  }

  handleCancelModal = () => {
    this.setState({
      editAddressModal: false,
      addNewAddressModal: false,
    });
  };

  editAddressModal(id, content) {
    this.setState({
      editAddressModal: true,
      contentEdit: content,
      editId: id,
    });
  }

  async handleOkEditModal() {
    const { contentEdit, editId } = this.state;
    const token = getCookie('siteToken');
    this.props.actions.addressPutRequest();

    const response = await PutAddressUser(
      token,
      new BaseCRUDDtoBuilder()
        .dto(
          new AddressDto({
            id: editId,
            content: contentEdit,
          }),
        )
        .build(),
    );
    if (response.status === 200) {
      this.props.actions.addressPutSuccess();
      this.getAddress();
      showNotification('success', '', 'آدرس با موفقیت ویرایش شد.', 10);
    } else {
      this.props.actions.addressPutFailure();
      this.props.actions.showAlertRequest(response.data.errorMessage);
    }
    this.setState({
      editAddressModal: false,
      addNewAddressModal: false,
    });
  }

  addNewAddressModal = () => {
    this.setState({
      addNewAddressModal: true,
    });
  };

  async addressConfirm() {
    const { basketProducts, addresses } = this.props;
    const token = getCookie('siteToken');
    const address = addresses.find(item => item.id === this.state.idSelected);

    const responseVBCities = await getVendorBranchCityApi(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new VendorBranchCitiesDto({
              vendorBranchDto: new VendorBranchDto({
                id:
                  basketProducts.userBasketHistoryDetailDtos[0]
                    .vendorBranchProductDto.vendorBranchDto.id,
              }),
              cityDto: new CityDto({ id: address.cityId }),
            }),
          )
          .buildJson(),
      ),
    );

    if (responseVBCities.data.items.length === 0) {
      showNotification(
        'error',
        '',
        `${
          basketProducts.userBasketHistoryDetailDtos[0].vendorBranchProductDto
            .vendorBranchDto.name
        } در حال حاضر در این آدرس سرویس دهی ندارد (بزودی)`,
        6,
        class {
          errorsBox;
        },
      );
      return;
    }
    this.props.actions.checkoutListRequest();

    if (basketProducts.hasDeliverProduct) {
      const basketCrud = new BaseCRUDDtoBuilder()
        .dto(
          new BasketDto({
            userBasketHistoryDto: new UserBasketHistoryDto({
              addressId: this.state.idSelected,
            }),
          }),
        )
        .build();

      const responsePut = await PutUserBasketHistoriesApi(basketCrud, token);

      if (responsePut.status === 200) {
        const basketList = new BaseGetDtoBuilder()
          .dto(new BasketDto({ fromCheckout: true }))
          .buildJson();

        const responseCheckout = await GetUserBasketHistoriesApi(
          getDtoQueryString(basketList),
          token,
        );
        if (responseCheckout.status === 200) {
          this.props.actions.checkoutListSuccess(responseCheckout.data);
          this.props.isValid('next');
        } else {
          this.props.actions.checkoutListFailure();
        }
      } else {
        this.props.actions.checkoutListFailure();
      }
    } else {
      const basketList = new BaseGetDtoBuilder()
        .dto(new BasketDto({ fromCheckout: true }))
        .buildJson();

      const responseCheckout = await GetUserBasketHistoriesApi(
        getDtoQueryString(basketList),
        token,
      );
      if (responseCheckout.status === 200) {
        this.props.actions.checkoutListSuccess(responseCheckout.data);
        this.props.isValid('next');
      } else if (responseCheckout.status === 401) {
        this.props.isValid('prev');
        this.props.setPassedStep([]);
      } else {
        this.props.actions.checkoutListFailure();
      }
    }
  }

  handleInput(inputName, value) {
    if (inputName === 'districtId') {
      const position = value.split('*');
      this.setState({
        lat: parseFloat(position[1]),
        lng: parseFloat(position[2]),
        [inputName]: value,
      });
    } else {
      this.setState({ [inputName]: value });
    }
  }

  /**
   * change city for automatic change district
   * @param inputName
   * @param value
   * @returns {Promise<void>}
   */
  async cityChange(inputName, value) {
    const response = await getDistrictApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(
            new DistrictDto({
              cityDto: new CityDto({ id: value }),
            }),
          )
          .buildJson(),
      ),
    );

    if (response.status === 200) {
      this.props.actions.districtOptionListSuccess(response.data);

      if (response.data.items.length === 0) {
        /**
         * get single city by id
         */

        const responseCity = await getCityApi(
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(
                new CityDto({
                  id: value,
                }),
              )
              .buildJson(),
          ),
        );

        if (responseCity.status === 200) {
          this.setState({
            lat: responseCity.data.items[0].lat,
            lng: responseCity.data.items[0].lng,
          });
        }
      } else {
        this.setState({
          districtId: `${response.data.items[0].id.toString()}*${
            response.data.items[0].lat
          }*${response.data.items[0].lng}`,
          lat: response.data.items[0].lat,
          lng: response.data.items[0].lng,
        });
      }

      this.setState({
        [inputName]: value,
      });
    }
  }

  checkVendorBranchIsRepeat(name) {
    const index = this.vendorBranchName.indexOf(name);
    if (index !== -1) return false;

    this.vendorBranchName.push(name);
    return true;
  }

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneFinal: number.replace(/ /g, ''),
        phone: value,
        messagePhoneNumber: status ? '' : 'لطفا شماره همراه را صحیح وارد کنید.',
        showMessagePhone: !status,
      });
    }
  };

  checkValidation = (name, isValid) => {
    const index = this.validation.indexOf(name);
    if (index !== -1) this.validation.splice(index, 1);
    if (isValid === 'true') this.validation.push(name);
  };

  render() {
    const { addresses, cities, districtOptions, basketProducts } = this.props;

    const {
      idSelected,
      receiveName,
      phone,
      cityId,
      districtId,
      content,
      contentEdit,
      lat,
      lng,
      disabled,
      showMessagePhone,
      messagePhoneNumber,
      errorValid,
    } = this.state;

    this.vendorBranchName = [];
    const addressDataSource = [];
    const citiesDataSource = [];
    const districtDataSource = [];

    addresses.map(item => {
      const obj = {
        value: item.id,
        name: item.content,
        key: item.id,
      };
      addressDataSource.push(obj);
      return null;
    });

    /**
     * map district for comboBox Datasource
     */
    districtOptions.map(item =>
      districtDataSource.push(
        <Option key={`${item.id}*${item.lat}*${item.lng}`}>{item.name}</Option>,
      ),
    );

    const columns = [
      {
        dataIndex: 'name',
        render: (text, record) => (
          <CPRadio
            model={[{ value: record.value, name: record.name }]}
            size="small"
            defaultValue={idSelected}
            onChange={value => this.onChangeAddress(value.target.value)}
          />
        ),
      },
      {
        title: '',
        dataIndex: 'operationOne',
        width: 50,
        render: (text, record) => (
          <CPPopConfirm
            title={ARE_YOU_SURE_WANT_TO_DELETE_THE_ADDRESS}
            okText={YES}
            cancelText={NO}
            okType="primary"
            onConfirm={() => this.onDelete(record.value)}
          >
            <CPButton className="delete_action">
              <i className="mf-trash" />
            </CPButton>
          </CPPopConfirm>
        ),
      },
      {
        title: '',
        dataIndex: 'operationTwo',
        width: 50,
        render: (text, record) => (
          <CPButton
            onClick={() => this.editAddressModal(record.value, record.name)}
            className="edit_action"
          >
            <i className="mf-edit" />
          </CPButton>
        ),
      },
    ];

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <div className={s.shippingForm}>
        {basketProducts &&
        basketProducts.userBasketHistoryDetailDtos &&
        basketProducts.userBasketHistoryDetailDtos.length > 0 ? (
          <div className={s.formWrapper}>
            {/* <i className="mf-pin" /> */}
            <h3 className={s.title}>{ORDERING_INFORMATION}</h3>
            {basketProducts &&
              basketProducts.hasDeliverProduct && (
                <div>
                  <div className={s.addNew}>
                    <p>{ORDER_ADDRESS}</p>
                    <b>{ADD_NEW_ORDER_ADDRESS}</b>
                    <CPButton
                      className={s.addNewBtn}
                      onClick={this.addNewAddressModal}
                    >
                      {ADD_NEW_ADDRESS}
                      <i className="mf-add" />
                    </CPButton>
                  </div>

                  {addresses.length > 0 && (
                    <CPTable
                      columns={columns}
                      data={addressDataSource}
                      footer={null}
                    />
                  )}
                </div>
              )}
            {basketProducts &&
              basketProducts.hasPickupProduct &&
              basketProducts.userBasketHistoryDetailDtos.map(
                item =>
                  item.priceOption === 'Pickup' &&
                  this.checkVendorBranchIsRepeat(
                    item.vendorBranchProductDto.vendorBranchDto.name,
                  ) && (
                    <CPCard
                      key={item.id}
                      title={`آدرس فروشگاه ${
                        item.vendorBranchProductDto.vendorBranchDto.name
                      }`}
                    >
                      <div className={s.location}>
                        <div className={s.addressBox}>
                          <LocationBox
                            vendorName={
                              item.vendorBranchProductDto.vendorBranchDto.name
                            }
                            rate={
                              item.vendorBranchProductDto.vendorBranchDto
                                .rateAverage
                            }
                            location={
                              item.vendorBranchProductDto.vendorBranchDto
                                .districtDto.name
                            }
                          />
                        </div>
                        <div className={s.addressBox}>
                          <span className={s.workingTime}>
                            <i className="mf-clock" />
                            <b>ساعت کاری</b>
                          </span>
                          <span className={s.workingTime}>
                            <p className={s.time}>
                              {
                                item.vendorBranchProductDto.vendorBranchDto
                                  .openingHour
                              }
                              -
                              {
                                item.vendorBranchProductDto.vendorBranchDto
                                  .closingHour
                              }
                            </p>
                          </span>
                        </div>
                        <div className={s.addressBox}>
                          <span
                            className={
                              item.vendorBranchProductDto.vendorBranchDto
                                .isOpenNow
                                ? s.isOpen
                                : s.isClose
                            }
                          >
                            <i className="mf-circle" />
                            <b>
                              {item.vendorBranchProductDto.vendorBranchDto
                                .isOpenNow
                                ? 'باز'
                                : 'بسته'}
                            </b>
                          </span>
                        </div>
                      </div>
                    </CPCard>
                  ),
              )}
            <CPButton
              disabled={
                disabled ||
                (addressDataSource.length === 0 &&
                  basketProducts.hasDeliverProduct)
              }
              className="confirmationBtn greenBtn"
              onClick={this.addressConfirm}
            >
              {CONFIRM_AND_ISSUANCE}
            </CPButton>
            <CPModal
              visible={this.state.editAddressModal}
              handleOk={this.handleOkEditModal}
              handleCancel={this.handleCancelModal}
            >
              <div className="modalContent">
                <b>{ADDRESS}</b>
                <CPInput
                  value={contentEdit}
                  onChange={value =>
                    this.handleInput('contentEdit', value.target.value)
                  }
                />
              </div>
            </CPModal>
            <CPModal
              className="max_modal"
              visible={this.state.addNewAddressModal}
              handleOk={this.handleOkModal}
              handleCancel={this.handleCancelModal}
              showFooter={false}
            >
              <div className={s.addNewAddress}>
                <div className="col-md-6 col-sm-12">
                  <h5 className={s.modalTitle}>{ENTER_THE_EXACT_ADDRESS}</h5>
                  <CPIntlPhoneInput
                    value={phone}
                    onChange={this.changeNumber}
                  />
                  <p
                    className={s.errorMessage}
                    style={{ display: `${showMessagePhone}` }}
                  >
                    {messagePhoneNumber}
                  </p>
                  <CPValidator
                    value={receiveName}
                    checkValidation={this.checkValidation}
                    minLength={3}
                    showMessage={errorValid}
                    name="receiveName"
                  >
                    <CPInput
                      label={RECEIVER_NAME}
                      value={receiveName}
                      onChange={value =>
                        this.handleInput('receiveName', value.target.value)
                      }
                    />
                  </CPValidator>
                  <div className={s.city}>
                    <CPSelect
                      onChange={value => this.cityChange('cityId', value)}
                      defaultValue={cityId}
                      showSearch
                    >
                      {citiesDataSource}
                    </CPSelect>
                  </div>
                  <div className={s.city}>
                    <CPSelect
                      onChange={value => this.handleInput('districtId', value)}
                      value={districtId}
                      showSearch
                    >
                      {districtDataSource}
                    </CPSelect>
                  </div>
                  <CPValidator
                    value={content}
                    checkValidation={this.checkValidation}
                    minLength={10}
                    showMessage={errorValid}
                    name="content"
                  >
                    <CPInput
                      label={POSTAL_ADDRESS}
                      value={content}
                      onChange={value =>
                        this.handleInput('content', value.target.value)
                      }
                    />
                  </CPValidator>
                  <div className="modalButton">
                    <CPButton onClick={this.handleOkModal}>{SAVE}</CPButton>

                    <CPButton onClick={this.handleCancelModal}>
                      {CANCEL}
                    </CPButton>
                  </div>
                </div>
                <div className="col-md-6 col-sm-12">
                  <h5 className={s.mapTitle}>
                    {DEFINE_THE_EXACT_ADDRESS_ON_THE_MAP}
                  </h5>
                  <CPMap
                    containerElement="500px"
                    // defaultCenter={{ lat, lng}}
                    defaultZoom={15}
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
                    mapElement="100%"
                    loadingElement="100%"
                    draggable
                    onDragEnd={value => this.setLatAndLng(value)}
                    lat={lat}
                    lng={lng}
                  />
                </div>
              </div>
            </CPModal>
          </div>
        ) : (
          <div>محصولی در سبد خرید شما نیست</div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  addresses: state.identityAddress.addressListData
    ? state.identityAddress.addressListData.items
    : [],
  cities: state.commonCity.cityListData
    ? state.commonCity.cityListData.items
    : [],
  districtOptions: state.commonDistrict.districtOptionData
    ? state.commonDistrict.districtOptionData.items
    : [],
  basketProducts: state.basket.basketListData
    ? state.basket.basketListData.items[0]
    : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      addressListSuccess,
      addressListRequest,
      addressListFailure,
      addressPostFailure,
      addressPostSuccess,
      addressPostRequest,
      addressPutFailure,
      addressPutSuccess,
      addressPutRequest,
      addressDeleteFailure,
      addressDeleteSuccess,
      addressDeleteRequest,
      districtOptionListSuccess,
      singleDistrictSuccess,
      checkoutListFailure,
      checkoutListSuccess,
      checkoutListRequest,
      loginSuccess,
      cityListSuccess,
      walletListSuccess,
      walletListFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ShippingInfo));
