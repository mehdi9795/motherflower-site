import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Tabs } from 'antd';
import s from './TabSection.css';
import {
  NEWEST,
  MOST_VIEWED,
  BEST_SELLERS,
} from '../../../Resources/Localization';
import CPCarouselSlider from '../../CP/CPCarouselSlider';
import ProductCard from '../../Shared/ProductCard';
import {
  bestSellingProductFailure,
  bestSellingProductSuccess,
  mostVisitedProductFailure,
  mostVisitedProductSuccess,
  newestProductFailure,
  newestProductSuccess,
} from '../../../redux/catalog/action/product';
import { ProductSearchDto } from '../../../dtos/catalogDtos';
import { getProductSearchApi } from '../../../services/catalogApi';
import { getDtoQueryString } from '../../../utils/helper';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';
import ContentLoader, { Instagram, List } from 'react-content-loader';
import { getCookie } from '../../../utils';

const { TabPane } = Tabs;

class TabSection extends React.Component {
  static propTypes = {
    newestProducts: PropTypes.arrayOf(PropTypes.object),
    bestSellingProducts: PropTypes.arrayOf(PropTypes.object),
    mostVisitedProducts: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    newestProducts: [],
    bestSellingProducts: [],
    mostVisitedProducts: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      activeKeyTab: '1',
      isWeb: false,
    };
    this.getProductForTab = this.getProductForTab.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.bestSellingProducts !== this.props.bestSellingProducts)
      this.setState({ activeKeyTab: '1' });
  }

  componentDidMount() {
    this.updatePredicate();
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth > 480 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth > 480 });
    });
  };

  preparingContainer = (sortId = 1) => {
    const cityId = getCookie('cityId');
    const productList = new BaseGetDtoBuilder()
      .dto(
        new ProductSearchDto({
          categoryIds: 0,
          vendorBranchIds: -578,
          sortOption: sortId,
          toPrice: '3000000',
          fromPrice: '2000',
          priceOption: '1',
          cityId,
        }),
      )
      .pageIndex(0)
      .pageSize(10)
      .disabledCount(true)
      .fromCache(true)
      .buildJson();

    return productList;
  };

  async getProductForTab(key) {
    const { newestProducts, mostVisitedProducts } = this.props;

    if (key === '2' && mostVisitedProducts.length === 0) {
      const mostVisitedProduct = await getProductSearchApi(
        getDtoQueryString(this.preparingContainer(2)),
      );

      if (mostVisitedProduct.status === 200) {
        this.props.actions.mostVisitedProductSuccess(mostVisitedProduct.data);
      } else this.props.actions.mostVisitedProductFailure();
    } else if (key === '3' && newestProducts.length === 0) {
      const bestSellingProduct = await getProductSearchApi(
        getDtoQueryString(this.preparingContainer(5)),
      );

      if (bestSellingProduct.status === 200) {
        this.props.actions.newestProductSuccess(bestSellingProduct.data);
      } else this.props.actions.newestProductFailure();
    }
    this.setState({ activeKeyTab: key });
  }

  render() {
    const {
      newestProducts,
      bestSellingProducts,
      mostVisitedProducts,
    } = this.props;
    const { activeKeyTab, isWeb } = this.state;

    return (
      <div className={s.tabSection}>
        <div className={s.tabRow}>
          <Tabs
            animated
            onTabClick={value => {
              this.getProductForTab(value);
            }}
            activeKey={activeKeyTab}
          >
            <TabPane tab={BEST_SELLERS} key={1}>
              <div className={s.products}>
                {bestSellingProducts.length > 0 ? (
                  <CPCarouselSlider
                    arrows
                    slidesToShow={6}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                    {bestSellingProducts.map(item => (
                      <div key={item.key} className="col-md-12">
                        <ProductCard
                          key={item.id}
                          url={`/product-details/${item.productId}/${
                            item.vendorBranchId
                          }`}
                          imageUrl1={
                            item.imageUrls.length > 0 ? item.imageUrls[0] : ''
                          }
                          imageUrl2={
                            item.imageUrls.length > 0 ? item.imageUrls[1] : ''
                          }
                          ProductName={item.name}
                          value={item.rateAverage}
                          price={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupBasePrice
                              : item.deliveryBasePrice
                          }
                          discount={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupPriceAfterDiscount
                              : item.deliveryPriceAfterDiscount
                          }
                          percent={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupPriceDiscount.toString()
                              : item.deliveryPriceDiscount.toString()
                          }
                        />
                      </div>
                    ))}
                  </CPCarouselSlider>
                ) : (
                  <CPCarouselSlider
                    arrows
                    slidesToShow={6}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                  </CPCarouselSlider>
                )}
              </div>
            </TabPane>
            {isWeb && 
              <TabPane tab={MOST_VIEWED} key="2">
              <div className={s.products}>
                {mostVisitedProducts.length > 0 ? (
                  <CPCarouselSlider
                    arrows
                    slidesToShow={6}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                    {mostVisitedProducts.map(item => (
                      <div key={item.key} className="col-md-12">
                        <ProductCard
                          key={item.id}
                          url={`/product-details/${item.productId}/${
                            item.vendorBranchId
                          }`}
                          imageUrl1={
                            item.imageUrls.length > 0 ? item.imageUrls[0] : ''
                          }
                          imageUrl2={
                            item.imageUrls.length > 0 ? item.imageUrls[1] : ''
                          }
                          ProductName={item.name}
                          value={item.rateAverage}
                          price={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupBasePrice
                              : item.deliveryBasePrice
                          }
                          discount={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupPriceAfterDiscount
                              : item.deliveryPriceAfterDiscount
                          }
                          percent={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupPriceDiscount.toString()
                              : item.deliveryPriceDiscount.toString()
                          }
                        />
                      </div>
                    ))}
                  </CPCarouselSlider>
                ) : (
                  <CPCarouselSlider
                    arrows
                    slidesToShow={6}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                  </CPCarouselSlider>
                )}
              </div>
            </TabPane>
          }
            {isWeb && 
            <TabPane tab={NEWEST} key="3">
              <div className={s.products}>
                {newestProducts.length > 0 ? (
                  <CPCarouselSlider
                    arrows
                    slidesToShow={6}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                    {newestProducts.map(item => (
                      <div key={item.id} className="col-md-12">
                        <ProductCard
                          key={item.id}
                          url={`/product-details/${item.productId}/${
                            item.vendorBranchId
                          }`}
                          imageUrl1={
                            item.imageUrls.length > 0 ? item.imageUrls[0] : ''
                          }
                          imageUrl2={
                            item.imageUrls.length > 0 ? item.imageUrls[1] : ''
                          }
                          ProductName={item.name}
                          value={item.rateAverage}
                          price={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupBasePrice
                              : item.deliveryBasePrice
                          }
                          discount={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupPriceAfterDiscount
                              : item.deliveryPriceAfterDiscount
                          }
                          percent={
                            item.deliveryPriceAfterDiscount >
                            item.pickupPriceAfterDiscount
                              ? item.pickupPriceDiscount.toString()
                              : item.deliveryPriceDiscount.toString()
                          }
                        />
                      </div>
                    ))}
                  </CPCarouselSlider>
                ) : (
                  <CPCarouselSlider
                    arrows
                    slidesToShow={6}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                    <ContentLoader
                      height={260}
                      className={s.contentLoader}
                      speed={1}
                      primaryColor="#ddd"
                      secondaryColor="#ccc"
                    >
                      <rect
                        x="0"
                        y="0"
                        rx="5"
                        ry="5"
                        width="90%"
                        height="260"
                      />
                    </ContentLoader>
                  </CPCarouselSlider>
                )}
              </div>
            </TabPane>
            }
          </Tabs>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  newestProducts: state.catalogProduct.newestProductData
    ? state.catalogProduct.newestProductData.items
    : null,
  bestSellingProducts: state.catalogProduct.bestSellingProductData
    ? state.catalogProduct.bestSellingProductData.items
    : null,
  mostVisitedProducts: state.catalogProduct.mostVisitedProductData
    ? state.catalogProduct.mostVisitedProductData.items
    : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      mostVisitedProductSuccess,
      mostVisitedProductFailure,
      newestProductSuccess,
      newestProductFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(TabSection));
