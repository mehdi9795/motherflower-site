import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AppInstall.css';
import CPButton from '../../CP/CPButton';
import { setCookie } from '../../../utils';

class AppInstall extends Component {
  static propTypes = {
    iosLink: PropTypes.string,
    androidLink: PropTypes.string,
  };

  static defaultProps = {
    iosLink: '',
    androidLink: '',
  };

  installApp = () => {
    const { androidLink, iosLink } = this.props;
    if (navigator.userAgent.match(/Android/i)) {
      setTimeout(() => {
        window.location = androidLink;
      }, 2500);
      window.location = `myapp://${androidLink} `;
      // window.open("market://details?id=com.motherflower","_system");
    } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
      setTimeout(() => {
        window.location = iosLink;
      }, 25);
      // window.location = ` myapp://${iosLink} `;
    }
  };

  close = () => {
    const element = document.getElementById('appInstall');
    element.style.display = 'none';
    setCookie('C-App', 'true', 8760);
  };

  render() {
    return (
      <div className={s.container} id="appInstall">
        <div className={s.background} />
        <CPButton className={s.closeButton} type="circle" onClick={this.close}>
          <img src="/images/close.png" alt="close" />
        </CPButton>
        <img
          height={70}
          src="/images/Layer9.png"
          alt="logo"
          onClick={this.installApp}
        />
        <div className={s.content}>
          <label onClick={this.installApp}>دانلود اپلیکیشن سفارش گل</label>
          <label onClick={this.installApp}>
            نزدیکترین گلفروشی در دستان شما
          </label>
        </div>
        <CPButton className={s.installButton} onClick={this.installApp}>
          نصب کن
        </CPButton>
      </div>
    );
  }
}

export default withStyles(s)(AppInstall);
