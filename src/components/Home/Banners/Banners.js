import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Banners.css';
import cs from 'classnames';
import Link from '../../Link';

class Banners extends React.Component {
  static propTypes = {
    banners: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    banners: [],
  };

  render() {
    const { banners } = this.props;
    console.log('banners',banners)
    return (
      <div className={s.bannerSection}>
      <div className="row">
      
        {banners.map(item => (
            <div className='col-sm-12 col-md-6 col-lg-3'>
              <div className={s.banners}>
                <Link to={item.link} className={s.bannerBox}>
                  <img src={item.imageDto ? item.imageDto.url : ''} alt="" />   
                </Link>
          
              </div>
            </div>
          ))}
          
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  banners: state.cmsBanner.bannerListData
    ? state.cmsBanner.bannerListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch),
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(Banners),
);
