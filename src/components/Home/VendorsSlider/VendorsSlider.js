import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorsSlider.css';
import Link from '../../Link';
import CPCarouselSlider from '../../CP/CPCarouselSlider';
import CPRate from '../../CP/CPRate';
import {baseCDN} from "../../../setting";

class VendorsSlider extends React.Component {
  static propTypes = {
    // data: PropTypes.array(PropTypes.object),
  };

  static defaultProps = {
    // data: [],
  };

  render() {
    // const { data } = this.props;
    const data = [
      {
        key: 1,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی بهرام',
        value: 0,
      },
      {
        key: 2,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی بهرام',
        value: 2,
      },
      {
        key: 3,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی زعیم',
        value: 1,
      },
      {
        key: 4,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی شهرام',
        value: 4,
      },
      {
        key: 5,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی ارکیده',
        value: 1,
      },
      {
        key: 6,
        imgUrl: `${baseCDN}SiteAsset/vendor.png`,
        vendorName: 'گل فروشی ملودی',
        value: 0,
      },
      {
        key: 7,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی نفس',
        value: 4,
      },
      {
        key: 8,
        imgUrl: `${baseCDN}/vendor.png`,
        vendorName: 'گل فروشی شب بو',
        value: 3,
      },
    ];

    return (
      <div className={s.vendorsSlider}>
        <h3 className={s.title}>گلفروشی ها</h3>
        <CPCarouselSlider
          dots
          slidesToShow={5}
          slidesToScroll={1}
          infinite={false}
          initialSlide={1}
          responsive={[
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ]}
        >
          {data.map(item => (
            <Link to="/#" className={s.slides} key={item.key}>
              <div className={s.vendorCard}>
                <img src={item.imgUrl} className={s.vendorImg} alt="" />
                <div className={s.details}>
                  <h5 className={s.vendorName}>{item.vendorName}</h5>
                  <span className={s.rate}>
                    <CPRate value={item.value} disabled />
                  </span>
                </div>
              </div>
            </Link>
          ))}
        </CPCarouselSlider>
      </div>
    );
  }
}

export default withStyles(s)(VendorsSlider);
