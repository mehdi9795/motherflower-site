import React, { Component } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Promotion.css';

class Promotion extends Component {
  render() {
    return (
      <div className={s.promotion}>
        <div />
        <img src="/images/winter.png" />
      </div>
    );
  }
}

export default withStyles(s)(Promotion);
