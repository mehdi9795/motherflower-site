import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Customer.css';
import Link from '../../Link/Link';
import CPCarouselSlider from '../../CP/CPCarouselSlider/CPCarouselSlider';
import CPRate from '../../CP/CPRate/CPRate';
import {baseCDN} from "../../../setting";

class VendorsSlider extends React.Component {
  static propTypes = {
    // data: PropTypes.array(PropTypes.object),
  };

  static defaultProps = {
    // data: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      isWeb: true,
    }
  }

  componentDidMount() {
    this.updatePredicate();
  }

  updatePredicate = () => {
    this.setState({
      isWeb: window.innerWidth >= 480,
    });
    window.addEventListener('resize', () => {
      this.setState({
        isWeb: window.innerWidth >= 480,
      });
    });
  };

  render() {
    const { isWeb } = this.state;
    const data = [
      {
        key: 1,
        imgUrl: `${baseCDN}/customer/aparat.jpg`,
        name: 'آپارات',
      },
      {
        key: 2,
        imgUrl: `${baseCDN}/customer/filimo.png`,
        name: 'فیلیمو',
      },
      {
        key: 3,
        imgUrl: `${baseCDN}/customer/madiran.jpg`,
        name: 'مادیران',
      },
      {
        key: 4,
        imgUrl: `${baseCDN}/customer/maliati.jpg`,
        name: 'سازمان امور مالیاتی کشور',
      },
      {
        key: 5,
        imgUrl: `${baseCDN}/customer/mashregh.jpg`,
        name: 'مشرق نیوز',
      },
      {
        key: 6,
        imgUrl: `${baseCDN}/customer/nafti.jpeg`,
        name: 'پخش فرآورده های نفتی',
      },
      {
        key: 4,
        imgUrl: `${baseCDN}/customer/naftKhazar.png`,
        name: 'پخش نفت پارس خزر',
      },
      {
        key: 5,
        imgUrl: `${baseCDN}/customer/saba.jpg`,
        name: 'صبا ویژن',
      }
    ];

    return (
      <div className={s.vendorsSlider}>
        <h3 className={s.title}>لیست مشتریان ما</h3>
        {isWeb ? 
        <div className="row" style={{maxWidth: '1000px', margin: '0 auto'}}>
          {data.map(item => (
            <div className="col-sm-3">
              <div className={s.slides}>
                <div className={s.borderSlide}>
                
                  <img src={item.imgUrl} className={s.customerImg} alt="" />
                
                </div>
                  <h5 className={s.customerName}>{item.name}</h5>
                  
              </div>
            </div>
          ))}
        </div>
        :
          <CPCarouselSlider
          dots
          slidesToShow={8}
          slidesToScroll={1}
          infinite={false}
          initialSlide={1}
          responsive={[
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 350,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ]}
        >
          {data.map(item => (
            <div to="/#" className={s.slides} key={item.key}>
              <div className={s.borderSlide}>
                <img src={item.imgUrl} className={s.customerImg} alt="" />
              </div>
                <h5 className={s.customerName}>{item.name}</h5>
            </div>
          ))}
        </CPCarouselSlider>
        }
      </div>
    );
  }
}

export default withStyles(s)(VendorsSlider);
