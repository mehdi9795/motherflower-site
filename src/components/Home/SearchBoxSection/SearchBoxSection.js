import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchBoxSection.css';
import SearchBox from '../SearchBox/SearchBox';
import PropTypes from "prop-types";

class SearchBoxSection extends React.Component {
  static propTypes = {
    city: PropTypes.string,
  };

  static defaultProps = {
    city: '',
  };

  render() {
    return (
      <section className={s.searchBoxSection}>
        <SearchBox city={this.props.city} />
      </section>
    );
  }
}

export default withStyles(s)(SearchBoxSection);
