import React from 'react';
import PropTypes from 'prop-types';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import { bindActionCreators } from 'redux';
import cs from 'classnames';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { Select, AutoComplete } from 'antd';
import s from './SearchBox.css';
import CPButton from '../../CP/CPButton/CPButton';
import { CityDto, DistrictDto, ZoneDto } from '../../../dtos/commonDtos';
import { districtOptionListRequest } from '../../../redux/common/action/district';
import history from '../../../history';
import { getDtoQueryString, showNotification } from '../../../utils/helper';
import { getVendorBranchZoneApi } from '../../../services/vendorApi';
import {
  getDistrictApi,
  getNearestLocationsApi,
} from '../../../services/commonApi';
import { VendorBranchZoneDto } from '../../../dtos/vendorDtos';
import { getCookie, setCookie } from '../../../utils';
import NavigationHomePage from '../../Template/NavigationHomePage';
import HeaderSocialMenuHomePage from '../../Template/HeaderSocialMenuHomePage';
import RoundLogo from '../../Template/RoundLogo';
import { BaseGetDtoBuilder } from '../../../dtos/dtoBuilder';
import { baseCDN } from '../../../setting';
import CPSelect from '../../CP/CPSelect';
import {
  bestSellingProductSuccess,
  mostVisitedProductSuccess,
  newestProductSuccess,
  specialProductSuccess,
} from '../../../redux/catalog/action/product';

const { Option } = Select;

class SearchBox extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    districts: PropTypes.arrayOf(PropTypes.object),
    categories: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    downloadBtn1: PropTypes.string,
    downloadBtn2: PropTypes.string,
    downloadBtn3: PropTypes.string,
    downloadBtn4: PropTypes.string,
    downloadBtn5: PropTypes.string,
    downloadBtn6: PropTypes.string,
    city: PropTypes.string,
  };

  static defaultProps = {
    districts: [],
    categories: [],
    cities: [],
    downloadBtn1: `${baseCDN}/download1.png`,
    downloadBtn2: `${baseCDN}/download2.png`,
    downloadBtn3: `${baseCDN}/download3.png`,
    downloadBtn4: `${baseCDN}/download4.png`,
    downloadBtn5: `${baseCDN}/download5.png`,
    downloadBtn6: `${baseCDN}/download6.png`,
    city: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      districtId: 0,
      // props.categories && props.categories[0] ? props.categories[0].id : 0,
      districtName: '',
      isAllowLocation: false,
      isWeb: false,
      categoryId: props.categories ? props.categories[0].id : 0,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.submitSearch = this.submitSearch.bind(this);
    this.showPosition = this.showPosition.bind(this);
    this.categoryArray = [];
  }

  componentDidMount() {
    this.geoLocation();
    this.props.actions.hideLoading();
    this.updatePredicate();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.districts) {
      const dataSourceArray = [];
      if (
        getCookie('districtId') !== undefined &&
        getCookie('districtId') !== null
      )
        nextProps.districts.map(item =>
          dataSourceArray.push({ text: item.name, value: item.id }),
        );
      if (this.state.isAllowLocation)
        dataSourceArray.push({
          text: (
            <span>
              انتخاب نزدیکترین محله به شما <i className="mf-compass" />
            </span>
          ),
          value: -1,
        });
      this.setState({ dataSource: dataSourceArray });
    }
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 769 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 769 });
    });
  };

  geoLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        this.showPosition,
        this.notAllowLocation,
      );
    }
  }

  onSelect = (value, option) => {
    if (value !== '-1') {
      setCookie('districtId', value, 8760);
      setCookie(
        'districtName',
        encodeURIComponent(option.props.children),
        8760,
      );
      this.setState(
        { districtId: value, districtName: option.props.children },
        () => this.submitSearch(),
      );
    } else {
      this.setState({
        districtName: getCookie('nearestLocation').split(',')[1],
        districtId: getCookie('nearestLocation').split(',')[0],
      });
      const newurl = `${window.location.protocol}//${
        window.location.host
      }/city/${getCookie('nearestLocation').split(',')[1]}`;
      window.history.pushState({ path: newurl }, '', newurl);
    }
  };

  async showPosition(position) {
    const { city, cities } = this.props;

    const lat = position.coords.latitude;
    const lng = position.coords.longitude;
    this.setState({ isAllowLocation: true });
    const exist = cities.find(item => item.name === city);
    const nearestLocation = getCookie('nearestLocation');
    if (nearestLocation && city && exist) {
      this.setState({
        cityId: exist.id,
        districtId:
          exist.id.toString() === nearestLocation.split(',')[2]
            ? nearestLocation.split(',')[0]
            : 0,
        districtName:
          exist.id.toString() === nearestLocation.split(',')[2]
            ? nearestLocation.split(',')[1]
            : '',
      });
    } else {
      const response = await getNearestLocationsApi(
        getDtoQueryString(
          JSON.stringify({ dto: { lat, lng }, fromCache: true }),
        ),
      );

      if (
        response.status === 200 &&
        response.data.items &&
        response.data.items.length > 0
      ) {
        const cityId =
          response.data.items.length > 0 && response.data.items[0].cityDto
            ? response.data.items[0].cityDto.id
            : '';

        const isActiveCity = this.props.cities.find(item => item.id === cityId);

        if (isActiveCity) {
          const districtId =
            response.data.items.length > 0 ? response.data.items[0].id : '';
          const districtName =
            response.data.items.length > 0 ? response.data.items[0].name : '';

          this.setState({ districtId, districtName, cityId });
          setCookie('districtId', districtId, 8760);
          setCookie('districtName', encodeURIComponent(districtName), 8760);
          setCookie('cityId', cityId, 8760);
          setCookie(
            'nearestLocation',
            [districtId, districtName, cityId],
            8760,
          );
          const newurl = `${window.location.protocol}//${
            window.location.host
          }/city/${isActiveCity.name}`;
          window.history.pushState({ path: newurl }, '', newurl);
        } else this.notAllowLocation();
      } else {
        this.notAllowLocation();
      }
    }
  }

  notAllowLocation = () => {
    this.setState({
      cityId: this.props.cities.length > 0 ? this.props.cities[0].id : '',
    });
  };

  async handleSearch(value) {
    this.setState({ districtName: value, districtId: 0 });
    if (value.length > 1) {
      const districtList = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            cityDto: new CityDto({
              id: this.state.cityId,
            }),
            name: value,
          }),
        )
        .fromCache(true)
        .buildJson();

      this.props.actions.districtOptionListRequest(districtList);
    } else if (
      getCookie('districtId') !== undefined &&
      getCookie('districtId') !== null
    ) {
      const dataSourceArray = [];
      if (this.state.isAllowLocation)
        dataSourceArray.push({
          text: 'انتخاب نزدیکترین محله به شما',
          value: -1,
        });
      this.setState({ dataSource: dataSourceArray, districtName: value });
    }
  }

  async submitSearch() {
    const { categoryId, districtId, districtName, cityId } = this.state;
    if (districtName.length === 0) return;
    this.props.actions.showLoading();

    if (parseInt(districtId, 0) > 0) {
      setCookie('districtId', districtId, 8760);
      setCookie('districtName', encodeURIComponent(districtName), 8760);
      /**
       * get vendor branch for product search
       */
      const VBZoneList = new BaseGetDtoBuilder()
        .dto(
          new VendorBranchZoneDto({
            zoneDto: new ZoneDto({
              districtDtos: [
                new DistrictDto({
                  name: districtName,
                  cityDto: new CityDto({ id: cityId }),
                }),
              ],
            }),
          }),
        )
        .searchMode(true)
        .fromCache(false)
        .buildJson();

      const vendorBranchData = await getVendorBranchZoneApi(
        getDtoQueryString(VBZoneList),
      );

      if (
        vendorBranchData.data &&
        vendorBranchData.data.items &&
        vendorBranchData.data.items.length > 0
      ) {
        let index = 0;
        for (let i = 0; i < vendorBranchData.data.items.length; i += 1) {
          if (vendorBranchData.data.items[i].vendorBranchDto.active) break;
          else index += 1;
        }

        history.push(
          `/search/${districtId}/${
            vendorBranchData.data.items[index].vendorBranchDto.id
          }?categoryIds=${categoryId}&sort=4`,
        );
      } else {
        showNotification(
          'error',
          '',
          'فروشگاهی در این محله موجود نیست',
          10,
          'errorsBox',
        );
        this.props.actions.hideLoading();
      }
    } else if (districtName.length > 1) {
      const districtList = new BaseGetDtoBuilder()
        .dto(
          new DistrictDto({
            name: districtName,
            searchSameName: true,
          }),
        )
        .fromCache(true)
        .buildJson();

      const district = await getDistrictApi(getDtoQueryString(districtList));

      if (district.data.items.length > 0) {
        setCookie('districtId', district.data.items[0].id, 8760);
        setCookie(
          'districtName',
          encodeURIComponent(district.data.items[0].name),
          8760,
        );
        /**
         * get vendor branch for product search
         */
        const VBZoneList = new BaseGetDtoBuilder()
          .dto(
            new VendorBranchZoneDto({
              zoneDto: new ZoneDto({
                districtDtos: [
                  new DistrictDto({
                    name: district.data.items[0].name,
                    cityDto: new CityDto({ id: cityId }),
                  }),
                ],
              }),
            }),
          )
          .fromCache(true)
          .searchMode(true)
          .buildJson();

        const vendorBranchData = await getVendorBranchZoneApi(
          getDtoQueryString(VBZoneList),
        );

        if (vendorBranchData.data && vendorBranchData.data.items.length > 0) {
          this.props.actions.hideLoading();
          history.push(
            `/search/${district.data.items[0].id}/${
              vendorBranchData.data.items[0].vendorBranchDto.id
            }?categoryIds=${categoryId}&sort=4`,
          );
        } else {
          showNotification(
            'error',
            '',
            'فروشگاهی در این محله موجود نیست',
            10,
            'errorsBox',
          );
          this.props.actions.hideLoading();
        }
      }
    }
  }

  handleChangeInput = (inputName, value) => {
    if (inputName === 'cityId') {
      window.scrollTo(0, 0);
      setCookie('cityId', value, 8760);
      this.props.actions.mostVisitedProductSuccess({
        items: [],
        count: 0,
      });
      this.props.actions.newestProductSuccess({
        items: [],
        count: 0,
      });
      this.props.actions.specialProductSuccess({
        items: [],
        count: 0,
      });
      this.props.actions.bestSellingProductSuccess({
        items: [],
        count: 0,
      });
      if (
        getCookie('nearestLocation') &&
        getCookie('nearestLocation').split(',')[2] === value.toString()
      )
        this.setState({
          districtId: getCookie('nearestLocation').split(',')[0],
          districtName: getCookie('nearestLocation').split(',')[1],
          dataSource: [],
          [inputName]: value,
        });
      else
        this.setState({
          districtId: 0,
          districtName: '',
          dataSource: [],
          [inputName]: value,
        });
      setTimeout(() => {
        const element = document
          .getElementsByClassName('cityBox')[0]
          .getElementsByClassName('ant-select-selection-selected-value')[0];

        const newurl = `${window.location.protocol}//${
          window.location.host
        }/city/${element.title}`;
        window.history.pushState({ path: newurl }, '', newurl);
      }, 50);
    } else this.setState({ [inputName]: value });
  };

  goTo = url => {
    window.open(url, '_blank');
  };

  render() {
    const { dataSource, cityId, categoryId, districtName, isWeb } = this.state;
    const {
      categories,
      cities,
    } = this.props;
    const cityArray = [];

    if (cities)
      cities.map(item =>
        cityArray.push(
          <Option key={item.id.toString()} value={item.id}>
            {item.name}
          </Option>,
        ),
      );
    console.log('isWeb',isWeb)
    return (
      <div>
        <div className={s.row}>
          <HeaderSocialMenuHomePage />
          <NavigationHomePage />
        </div>
        <div className={s.searchBox}>
        {/* {isWeb && <label className={s.title}>سفارش آنلاین گل از بهترین گلفروشی ها</label>} */}
        <div className={s.downloadAndSearchBox}>
        {isWeb && 
          <div className={s.downloadApp}>
            <img src="/images/download/cafe.png"
             onClick={() =>
                  this.goTo('http://bit.ly/2rWwLTi')
                }
              />
            <img src="/images/download/IOS.png"
            />
          </div>
        }
          <div className={s.searchBoxContainer}>
           <label className={s.title}>سفارش آنلاین گل از بهترین گلفروشی ها</label>
            <div className={s.dropDownSearch}>
              <CPSelect
                  value={cityId}
                  onChange={value =>
                    this.handleChangeInput('cityId', value)
                  }
                  showSearch
                  className="cityBox"
                >
                  {cityArray}
              </CPSelect>
              <AutoComplete
                        autoFocus
                        value={districtName}
                        className={s.areaInput}
                        dataSource={dataSource}
                        onSelect={this.onSelect}
                        onSearch={this.handleSearch}
                        placeholder="نام محله خود را وارد نمایید ..."
                      />
            </div>
            <div className={s.categoryBox}>
            {categories && categories.map(item => 
              <CPButton className={categoryId === item.id && s.activeCategory} onClick={() => this.handleChangeInput('categoryId', item.id)}>
              {item.name}</CPButton>
            )}
              
            </div>
            <CPButton className={s.searchBottom} onClick={this.submitSearch}>
              جستجو
            </CPButton>
          </div>
          </div>
        <div className={s.autoLocation}>
                <CPButton onClick={this.geoLocation}><i class="mf-compass" /></CPButton>
                <div >موقعیت یاب خودکار</div>
        </div>
        {!isWeb && 
          <div className={s.downloadApp}>
            <img src="/images/download/cafe.png" />
            <img src="/images/download/IOS.png" />
          </div>
        }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  districts: state.commonDistrict.districtOptionData
    ? state.commonDistrict.districtOptionData.items
    : [],
  categories: state.catalogCategory.categoryListData
    ? state.catalogCategory.categoryListData.items
    : null,
  cities: state.commonCity.cityListData
    ? state.commonCity.cityListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      districtOptionListRequest,
      showLoading,
      hideLoading,
      mostVisitedProductSuccess,
      newestProductSuccess,
      specialProductSuccess,
      bestSellingProductSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SearchBox));
