import React from 'react';
import { showLoading } from 'react-redux-loading-bar';
import Countdown from 'react-countdown-now';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SpecialOffers.css';
import {
  DELIVERY_TYPE,
  DISCOUNT,
  TOMAN,
  VENDOR,
} from '../../../../Resources/Localization';
import CPCarouselSlider from '../../../CP/CPCarouselSlider';
import Link from '../../../Link';
import SpecialProductCard from '../../../Shared/SpecialProductCard';
import { baseCDN } from '../../../../setting';

class SpecialOffers extends React.Component {
  static propTypes = {
    specialProducts: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    specialProducts: null,
  };

  constructor(props) {
    super(props);
    this.state = { isWeb: false };
    this.sliderData = [];

    if (props.specialProducts.length > 0) {
      props.specialProducts.map(item => {
        const dataSource = [];
        if (item.productSpecificationAttributeDtos) {
          item.productSpecificationAttributeDtos.map(attr => {
            if (attr.customValue !== 'true') {
              dataSource.push(
                `${attr.specificationAttributeDto.name} ${
                  attr.colorOptionDto.name
                } ${
                  attr.customValue ? `${attr.customValueDescription} عدد` : ''
                }`,
              );
            } else {
              dataSource.push(attr.specificationAttributeDto.name);
            }
            return null;
          });
        }
        const price = item.vendorBranchProductPriceDtos[0];
        this.sliderData.push({
          key: item.id,
          image: item.imageDtos.length > 0 ? item.imageDtos[0].url : '',
          productName: item.name,
          rateAverage: item.rateAverage,
          vendor: item.vendorBranchProductDtos[0].vendorBranchDto.name,
          priceOption:
            price.pickupPriceAfterDiscount < price.deliveryPriceAfterDiscount
              ? 'pickup'
              : 'delivery',
          time: price.pickupSummeryPromotions
            ? price.pickupSummeryPromotions.leftOverMilliSeconds
            : price.deliverySummeryPromotions.leftOverMilliSeconds,
          mainPrice:
            price.pickupPriceAfterDiscount < price.deliveryPriceAfterDiscount
              ? price.stringPickupBasePrice
              : price.stringDeliveryBasePrice,
          discountPrice:
            price.pickupPriceAfterDiscount < price.deliveryPriceAfterDiscount
              ? price.stringPickupPriceAfterDiscount
              : price.stringDeliveryPriceAfterDiscount,
          discount: price.pickupSummeryPromotions
            ? price.pickupSummeryPromotions.amount
            : price.deliverySummeryPromotions.amount,
          specification: dataSource,
          url: `/product-details/${item.id}/${
            item.vendorBranchProductDtos
              ? item.vendorBranchProductDtos[0].vendorBranchDto.id
              : ''
          }`,
          discountProfit:
            price.pickupPriceAfterDiscount < price.deliveryPriceAfterDiscount
              ? price.stringPickupDiscountProfit
              : price.stringDeliveryDiscountProfit,
        });
        return null;
      });
    }

    if (this.sliderData && this.sliderData[0]) {
      this.state = {
        productName: this.sliderData[0].productName,
        vendor: this.sliderData[0].vendor,
        priceOption: this.sliderData[0].priceOption,
        time: this.sliderData[0].time,
        mainPrice: this.sliderData[0].mainPrice,
        discountPrice: this.sliderData[0].discountPrice,
        discount: this.sliderData[0].discount,
        image: this.sliderData[0].image,
        specification: this.sliderData[0].specification,
        url: this.sliderData[0].url,
        discountProfit: this.sliderData[0].discountProfit,
      };
    }
    this.changeProduct = this.changeProduct.bind(this);
    this.second = 0;
    this.minute = 0;
    this.hour = 0;
  }

  componentDidMount() {
    this.updatePredicate();
  }

  updatePredicate = () => {
    this.setState({
      isWeb: window.innerWidth >= 1050,
      isTablet: window.innerWidth < 1050 && window.innerWidth >= 769,
      isMobile: window.innerWidth <= 768,
    });
    window.addEventListener('resize', () => {
      this.setState({
        isWeb: window.innerWidth >= 1050,
        isTablet: window.innerWidth < 1050 && window.innerWidth >= 769,
        isMobile: window.innerWidth <= 768,
      });
    });
  };

  changeProduct(e, record) {
    console.log('record', record);
    e.preventDefault();
    this.setState({
      productName: record.productName,
      vendor: record.vendor,
      priceOption: record.priceOption,
      mainPrice: record.mainPrice,
      discountPrice: record.discountPrice,
      discountProfit: record.discountProfit,
      discount: record.discount,
      image: record.image,
      specification: record.specification,
      time:
        (parseInt(this.hour, 0) * 60 * 60 +
          parseInt(this.minute, 0) * 60 +
          (parseInt(this.second, 0) + 1)) *
        1000,
      url: record.url,
    });
  }

  goToUrl = (e, url) => {
    e.preventDefault();
    // this.props.actions.showLoading();
    // history.push(url);
    window.open(url, '_blank');
  };

  render() {
    const {
      productName,
      vendor,
      priceOption,
      time,
      mainPrice,
      discountPrice,
      discount,
      image,
      specification,
      url,
      discountProfit,
    } = this.state;

    const { isWeb, isTablet, isMobile } = this.state;

    const renderer = ({ days, hours, minutes, seconds }) => {
      hours = parseInt(days * 24) + parseInt(hours);
      this.second = seconds;
      this.minute = minutes;
      this.hour = hours;

      const hoursOutput = [];
      const sHours = hours.toString();
      for (let i = 0, len = sHours.length; i < len; i += 1) {
        hoursOutput.push(+sHours.charAt(i));
      }

      const minutesOutput = [];
      const sMinutes = minutes.toString();
      for (let i = 0, len = sMinutes.length; i < len; i += 1) {
        minutesOutput.push(+sMinutes.charAt(i));
      }

      const secondsOutput = [];
      const sSeconds = seconds.toString();
      for (let i = 0, len = sSeconds.length; i < len; i += 1) {
        secondsOutput.push(+sSeconds.charAt(i));
      }

      {
        return (
          <span className={s.timerWrapper}>
            <div className={s.timerNum}>
              <span className={s.numbers}>{hoursOutput[0]}</span>
              <span className={s.numbers}>{hoursOutput[1]}</span>
            </div>
            <div className={s.comma}>
              <b>:</b>
            </div>
            <div className={s.timerNum}>
              <span className={s.numbers}>{minutesOutput[0]}</span>
              <span className={s.numbers}>{minutesOutput[1]}</span>
            </div>
            <div className={s.comma}>
              <b>:</b>
            </div>
            <div className={s.timerNum}>
              <span className={s.numbers}>{secondsOutput[0]}</span>
              <span className={s.numbers}>{secondsOutput[1]}</span>
            </div>
          </span>
        );
      }
    };
    
    return (
      <div className={s.specialOffersContainer}>
      <h3 className={s.title}>پیشنهادات شگفت انگیز روز</h3>
        <CPCarouselSlider
                    arrows
                    slidesToShow={4}
                    slidesToScroll={1}
                    infinite={false}
                    initialSlide={1}
                    rtl={false}
                    responsive={[
                      {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 4,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                    ]}
                  >
                  {this.sliderData.map(item => (
                    <Link
                          to="/"
                          onClick={e => this.changeProduct(e, item)}
                          key={item.key}
                          className={s.slides}
                        >
                     <SpecialProductCard
                          key={item.id}
                          url={item.url}
                          imageUrl1={
                            item.image.replace('480-480', '250-250')
                          }
                          imageUrl2={
                            item.image.replace('480-480', '250-250')
                          }
                          ProductName={item.productName}
                          value={item.rateAverage}
                          price={ item.mainPrice}
                          discount={
                            item.discountPrice}
                          percent={
                            item.discount  }
                        />
                        </Link>
                      ))}
                      
                  </CPCarouselSlider>
                            
        <div className={s.timer}>
              <Countdown
                date={Date.now() + time}
                intervalDelay={0}
                precision={3}
                renderer={renderer}
              />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  specialProducts: state.catalogProduct.specialProductData
    ? state.catalogProduct.specialProductData.items
    : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(SpecialOffers));
