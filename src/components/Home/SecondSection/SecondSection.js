import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SecondSection.css';
import SpecialOffers from './SpecialOffers';

class SecondSection extends React.Component {
  render() {
    return (
      <section className={s.secondSection}>
        <SpecialOffers />
      </section>
    );
  }
}

export default withStyles(s)(SecondSection);
