import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AppDownload.css';
import CPButton from '../../CP/CPButton/CPButton';
import CPIntlPhoneInput from '../../CP/CPIntlPhoneInput';
import CPInput from '../../CP/CPInput';
import { baseCDN } from '../../../setting';
import { showNotification } from '../../../utils/helper';
import { postDownloadApp } from '../../../services/commonApi';

class AppDownload extends Component {
  static propTypes = {
    downloadBtn1: PropTypes.string,
    downloadBtn2: PropTypes.string,
  };

  static defaultProps = {
    downloadBtn1: `${baseCDN}/google_play.png`,
    downloadBtn2: `${baseCDN}/apple.png`,
  };

  constructor(props) {
    super(props);
    this.state = {
      coverFlow: null,
      phoneNumber: '',
      isWeb: false,
      isTablet: false,
      titleSlider: 'انتخاب نزدیکترین گلفروشی',
      imgSplash: `${baseCDN}/splash/1.png`,
      phoneNumberFinal: '',
      phoneNumber: '',
      isValidPhoneNumber: false,
      countryCode: '',
      email: '',
    };
    this.sendToPhoneNumber = this.sendToPhoneNumber.bind(this);
    this.sendToEmail = this.sendToEmail.bind(this);
  }

  componentDidMount() {
    this.updatePredicate();
    // import('../../CP/CPCoverFlow').then(module =>
    //   this.setState({ coverFlow: module.default }),
    // );
    // const safari = navigator.userAgent.toLowerCase();
    // if (safari.indexOf('safari') !== -1) {
    //   if (safari.indexOf('chrome') > -1) {
    //     // Chrome
    //   } else {
    //     document.getElementById('appDownloadYa').classList.add('lineIos');
    //   }
    // }

    setInterval(() => {
      const element = document.getElementsByClassName('activeStep')[0];
      if (element) {
        if (element.nextElementSibling) element.nextElementSibling.click();
        else document.getElementsByClassName('firstSlide')[0].click();
      }
    }, 5000);
  }

  goTo = url => {
    window.open(url, '_blank');
  };

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 1024,isTablet: window.innerWidth >= 480 && window.innerWidth < 1024, });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 1024,isTablet: window.innerWidth >= 480 && window.innerWidth < 1024, });
    });
  };

  changeStep = (e, step, img) => {
    // const elementImg = document.getElementsByClassName('imgSlide')[0];
    // elementImg.classList.remove('opacity');
    // elementImg.classList.add('noOpacity');

    // setTimeout(() => {
    //   this.setState(
    //     { titleSlider: step, imgSplash: `${baseCDN}/splash/${img}.png` },
    //     () => elementImg.classList.remove('noOpacity'),
    //   );
    // }, 800);
    this.setState({
      titleSlider: step,
      imgSplash: `${baseCDN}/splash/${img}.png`,
    });

    const element = document.getElementsByClassName('activeStep')[0];
    element.classList.remove('activeStep');
    if (e.target.tagName === 'SPAN')
      e.target.parentNode.classList.add('activeStep');
    else e.target.classList.add('activeStep');
  };

  downloadSibApp = () => {
    // window.open('http://bit.ly/2GWM5c3', '_blank');
    window.open('https://motherflower.com/ios', '_blank');
  };

  downloadCafeBazar = () => {
    window.open('https://bit.ly/2ECzCbw', '_blank');
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneNumberFinal: number.replace(/ /g, ''),
        phoneNumber: value,
        isValidPhoneNumber: status,
        countryCode: countryData,
      });
    }
  };

  async sendToPhoneNumber() {
    const {
      phoneNumberFinal,
      phoneNumber,
      isValidPhoneNumber,
      countryCode,
    } = this.state;

    if (!isValidPhoneNumber) {
      showNotification(
        'error',
        '',
        'لطفا شماره همراه را صحیح وارد کنید',
        10,
        'errorBox',
      );
      return;
    }

    const response = await postDownloadApp({
      dto: {
        phone: phoneNumberFinal,
        countryCode: countryCode.dialCode,
      },
    });

    if (response.status === 200)
      showNotification(
        'success',
        '',
        `لینک دانلود اپلیکیشن به شماره ${phoneNumber} ارسال شد`,
        10,
      );
  }

  async sendToEmail() {
    const { email } = this.state;
    const re = /^([a-zA-Z0-9])([a-zA-Z0-9\._])*@(([a-zA-Z0-9])+(\.))+([a-zA-Z]{2,4})+$/;
    if (!re.test(email)) {
      showNotification(
        'error',
        '',
        'لطفا ایمیل خود را صحیح وارد کنید',
        10,
        'errorBox',
      );
      return;
    }

    const response = await postDownloadApp({
      dto: {
        email,
      },
    });

    if (response.status === 200)
      showNotification(
        'success',
        '',
        `لینک دانلود اپلیکیشن به ایمیل ${email} ارسال شد`,
        10,
      );
  }

  handleChange = (inputName, value) => {
    this.setState({ [inputName]: value });
  };

  render() {
    const { downloadBtn1, downloadBtn2 } = this.props;
    const {
      coverFlow: Component,
      phoneNumber,
      isWeb,
      isTablet,
      titleSlider,
      imgSplash,
      email,
    } = this.state;
    return (
      <div className={s.container}>
        <div className={s.appDownload} >
          <div>
          <h3 className={s.title}> دریافت اپلیکیشن اندروید و IOS</h3>
          </div>
          {isWeb || isTablet ? <img src="/images/home/layer3.png" className={s.background} /> 
          : <img src="/images/home/mobile.png" className={s.background} />}
              

              <div className={s.downloadBox} >
                <img src="/images/home/cafe.png" onClick={() => this.goTo('http://bit.ly/2rWwLTi')} />
                <img src="/images/home/google.png"  onClick={() => this.goTo('http://bit.ly/2VcdcUu')}/>
                <img src="/images/home/sibapp.png" />
                <img src="/images/home/myket.png" onClick={() =>
                    this.goTo('https://myket.ir/app/com.motherflower?lang=fa')
                  } />
              </div>
                <h2 className={s.content}>
                  برای دریافت لینک دانلود اپلیکیشن، شماره موبایل خود را
                  وارد نمایید
                </h2>
              
              <div className={s.phoneBox}>
                <CPIntlPhoneInput
                  placeholder="09121234567"
                  value={phoneNumber}
                  onChange={this.changeNumber}
                  className={s.phoneInput}
                />
                <CPButton
                  className={s.phoneBtn}
                  onClick={this.sendToPhoneNumber}
                >
                  دریافت لینک از طریق پیامک
                </CPButton>
              </div>
            </div>
            {isWeb &&
              <svg id="Layer_1" style={{position: 'absolute',bottom: '0px'}} data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1358.81 113.5"><title>Untitled-2</title><path d="M7878.29,7498.37H6519.48v-86.8A1768.22,1768.22,0,0,1,6847.82,7385c280.4,3.57,402.63,72.57,666.92,71.63,85.5-.3,210.73-8,363.55-45.07Z" transform="translate(-6519.48 -7384.87)" style={{fill:'#fff'}}/></svg>
            }
          </div>
    );
  }
}

export default withStyles(s)(AppDownload);
