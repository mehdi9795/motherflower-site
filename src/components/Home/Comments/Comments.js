import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Comments.css';
import cs from 'classnames';
import CPCarouselSlider from '../../CP/CPCarouselSlider';



class Comments extends React.Component {

  componentWillMount() {
    this.setState({
      children: ['https://cdn.motherflower.com:7090/SiteAsset/home/mob.jpg','https://cdn.motherflower.com:7090/SiteAsset/home/mob.jpg','https://cdn.motherflower.com:7090/SiteAsset/home/mob.jpg','https://cdn.motherflower.com:7090/SiteAsset/home/mob.jpg','https://cdn.motherflower.com:7090/SiteAsset/home/mob.jpg',],
      activeItemIndex: 0,
      isWeb: false,
    });

  }

  componentDidMount() {
    this.setState({ isWeb: window.innerWidth >= 480});
    if (window.innerWidth >= 780) {
      const btn1 = document.getElementsByClassName('commentSlider')[0].getElementsByClassName('slick-arrow')[0];
      const btn2 = document.getElementsByClassName('commentSlider')[0].getElementsByClassName('slick-arrow')[1];
      const thiz = this;
      btn1.onclick = function() {setTimeout(() => thiz.setAttributeToSlider(), 100)};
      btn2.onclick = function() {setTimeout(() => thiz.setAttributeToSlider(), 100)};
      this.setAttributeToSlider();
    }
  }

  setAttributeToSlider = () => {
    const centerElement = document.getElementsByClassName('commentSlider')[0].getElementsByClassName('slick-active')[1].getElementsByClassName('itemSlider')[0];
    const leftElement = document.getElementsByClassName('commentSlider')[0].getElementsByClassName('slick-active')[0].getElementsByClassName('itemSlider')[0];
    const rightElement = document.getElementsByClassName('commentSlider')[0].getElementsByClassName('slick-active')[2].getElementsByClassName('itemSlider')[0];
    const elementMain = document.getElementsByClassName('centerSlide');
    const elementOther = document.getElementsByClassName('otherSlide');
    
    if (elementOther.length) {
      elementOther[0].classList.remove('otherSlide');
    }

    if (elementMain.length) {
      elementMain[0].classList.add('otherSlide');
      elementMain[0].classList.remove('centerSlide');
    }
    leftElement.style.top = 0;
    rightElement.style.top = 0;
    centerElement.style.boxShadow = '-1px 2px 5px 2px rgba(230,230,230,0.68)';
    rightElement.style.boxShadow = '0px 0px 0px 0px rgba(230,230,230,0)';
    leftElement.style.boxShadow = '0px 0px 0px 0px rgba(230,230,230,0)';
    centerElement.classList.add("centerSlide");

  }

  render() {
    const {
      isWeb
    } = this.state;

    return (
      <div className={cs(s.container, 'commentSlider')}>
      <div className={s.sliderContainer}>
      {isWeb &&
      <div style={{position: 'relative'}}>
        <svg id="Layer_1" style={{position: 'absolute',top: '0',transform: 'rotate(180deg)'}} data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1358.81 113.5"><title>Untitled-2</title><path d="M7878.29,7498.37H6519.48v-86.8A1768.22,1768.22,0,0,1,6847.82,7385c280.4,3.57,402.63,72.57,666.92,71.63,85.5-.3,210.73-8,363.55-45.07Z" transform="translate(-6519.48 -7384.87)" style={{fill:'#fff'}}/></svg>
        <img src="https://cdn.motherflower.com:7090/SiteAsset/home/2.jpg" height={350} width="100%" />
      </div>
      }
      <CPCarouselSlider
        arrows
        slidesToShow={3}
        slidesToScroll={1}
        infinite={false}
        initialSlide={1}
        speed={1000}
        className={s.slider}
        rtl={false}
        responsive={[
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ]}
      >
        <div className={cs(s.itemSlider, 'itemSlider')}>
          <div style={{position: 'relative'}}>
            <label>شاهین طبری مدیر عامل چارگون</label>
            <img src="/images/avatar/10.jpg" />
            <small>,,</small>
          </div>
          <label>من هدیه دادن را خیلی دوس دارم. با گلستان به جای یک دسته گل، اشتراک یک ماهه ارائه میدم و طرف مقابل رو چهار بار سوپرایز میکنم</label>
        </div>
        <div className={cs(s.itemSlider, 'itemSlider')}>
        <div style={{position: 'relative'}}>
            <label>شاهین طبری مدیر عامل چارگون</label>
            <img src="/images/avatar/10.jpg" />
            <small>,,</small>
          </div>
          <label>من هدیه دادن را خیلی دوس دارم. با گلستان به جای یک دسته گل، اشتراک یک ماهه ارائه میدم و طرف مقابل رو چهار بار سوپرایز میکنم</label>
        </div>
        <div className={cs(s.itemSlider, 'itemSlider')}>
        <div style={{position: 'relative'}}>
            <label>شاهین طبری مدیر عامل چارگون</label>
            <img src="/images/avatar/10.jpg" />
            <small>,,</small>
          </div>
          <label>من هدیه دادن را خیلی دوس دارم. با گلستان به جای یک دسته گل، اشتراک یک ماهه ارائه میدم و طرف مقابل رو چهار بار سوپرایز میکنم</label>
        </div>
        <div className={cs(s.itemSlider, 'itemSlider')}>
        <div style={{position: 'relative'}}>
            <label>شاهین طبری مدیر عامل چارگون</label>
            <img src="/images/avatar/10.jpg" />
            <small>,,</small>
          </div>
          <label>من هدیه دادن را خیلی دوس دارم. با گلستان به جای یک دسته گل، اشتراک یک ماهه ارائه میدم و طرف مقابل رو چهار بار سوپرایز میکنم</label>
        </div>
        <div className={cs(s.itemSlider, 'itemSlider')}>
        <div style={{position: 'relative'}}>
            <label>شاهین طبری مدیر عامل چارگون</label>
            <img src="/images/avatar/10.jpg" />
            <small>,,</small>
          </div>
          <label>من هدیه دادن را خیلی دوس دارم. با گلستان به جای یک دسته گل، اشتراک یک ماهه ارائه میدم و طرف مقابل رو چهار بار سوپرایز میکنم</label>
        </div>
      </CPCarouselSlider>
      </div>
      </div>
    );  
  }
}

export default withStyles(s)(Comments);
