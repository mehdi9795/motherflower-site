import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AccountWizard.css';
import LoginForm from '../../Login/LoginForm';
import RegisterForm from '../../Login/RegisterForm';
import ConfirmationForm from '../../Login/ConfirmationForm';
import ForgetPassword from '../../Login/ForgetPassword';
import ChangePassword from '../../Login/ChangePassword';

class AccountWizard extends React.Component {
  static propTypes = {
    validationFalse: PropTypes.func,
    validationTrue: PropTypes.func,
    isValid: PropTypes.func,
    hideNext: PropTypes.func,
    hidePrev: PropTypes.func,
    loading: PropTypes.bool,
    fromCheckout: PropTypes.bool,
    selectedStep: PropTypes.string,
    fromUrl: PropTypes.string,
    reagentUser: PropTypes.string,
    // reagentUser: PropTypes.string,
    // actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    loading: false,
    fromCheckout: false,
    validationFalse: () => {},
    validationTrue: () => {},
    isValid: () => {},
    hideNext: () => {},
    hidePrev: () => {},
    selectedStep: '',
    fromUrl: '',
    reagentUser: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      stepWizardData: props.selectedStep,
    };
  }

  visibilityHandler = key => {
    const stepData = this.state.stepWizardData;
    return key === stepData;
  };

  changeStep = value => {
    this.setState({ stepWizardData: value });
  };

  render() {
    const {
      loading,
      validationFalse,
      validationTrue,
      isValid,
      hideNext,
      hidePrev,
      fromCheckout,
      fromUrl,
      reagentUser,
    } = this.props;
    return (
      <div className={s.accountWizard}>
        {this.visibilityHandler('login') && (
          <div className={loading ? 'showLogin' : 'hiddenLogin'}>
            <LoginForm
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              hideNext={hideNext}
              hidePrev={hidePrev}
              changeStep={this.changeStep}
              fromCheckout={fromCheckout}
              fromUrl={fromUrl}
            />
          </div>
        )}
        {this.visibilityHandler('register') && (
          <div className={loading ? 'hiddenRegister' : 'showRegister'}>
            <RegisterForm
              fromCheckout={fromCheckout}
              changeStep={this.changeStep}
              fromUrl={fromUrl}
              reagentUser={reagentUser}
            />
          </div>
        )}
        {this.visibilityHandler('verification') && (
          <div className={loading ? 'hiddenConfirmation' : 'showConfirmation'}>
            <ConfirmationForm
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              hideNext={hideNext}
              hidePrev={hidePrev}
              fromCheckout={fromCheckout}
              changeStep={this.changeStep}
            />
          </div>
        )}
        {this.visibilityHandler('forgetPassword') && (
          <div className={loading ? 'hiddenConfirmation' : 'showConfirmation'}>
            <ForgetPassword
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              hideNext={hideNext}
              hidePrev={hidePrev}
              changeStep={this.changeStep}
            />
          </div>
        )}
        {this.visibilityHandler('changePassword') && (
          <div className={loading ? 'hiddenConfirmation' : 'showConfirmation'}>
            <ChangePassword
              validationFalse={validationFalse}
              validationTrue={validationTrue}
              isValid={isValid}
              hideNext={hideNext}
              hidePrev={hidePrev}
            />
          </div>
        )}
      </div>
    );
  }
}

export default (  withStyles(s)(AccountWizard));
