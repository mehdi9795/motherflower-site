import firebase from 'firebase';
import { initializePush } from './initialize';
import { config } from './firebaseConfig';

export function getPushToken() {
  const isChrome = navigator.userAgent.indexOf('Chrome') > -1;
  const isExplorer = navigator.userAgent.indexOf('MSIE') > -1;
  const isFirefox = navigator.userAgent.indexOf('Firefox') > -1;
  const isOpera = navigator.userAgent.toLowerCase().indexOf('op') > -1;

  if (isChrome || isExplorer || isFirefox || isOpera) {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('/firebase-messaging-sw.js');
        initializePush();
      }
    }
  }
}
