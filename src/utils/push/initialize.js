import firebase from 'firebase';
import { notification } from 'antd';
import {
  authPostAppUserInfo,
  postAppUserInfo,
} from '../../services/identityApi';
import { BaseCRUDDtoBuilder } from '../../dtos/dtoBuilder';
import { UserAppInfo } from '../../dtos/identityDtos';
import { getCookie, setCookie } from '../index';
import React from 'react';
import Link from '../../components/Link';

export function initializePush() {
  const messaging = firebase.messaging();
  messaging.usePublicVapidKey(
    'BPJ9syq7vmIjvkDSNjToZKhDdfnupjJJnZgNJ9mL5Xs14ObNOzlue8WzuteBNQeb-CP201HdHx4T2MJzgFMc-iQ',
  );
  messaging
    .requestPermission()
    .then(() => messaging.getToken())
    .then(token => {
      console.log('token', token);
      setCookie('fcmToken', token, 200);
      const sitToken = getCookie('siteToken');
      if (sitToken)
        authPostAppUserInfo(
          new BaseCRUDDtoBuilder()
            .dto(new UserAppInfo({ token, os: 'web', appType: 'Customer' }))
            .build(),
          sitToken,
        );
      else
        postAppUserInfo(
          new BaseCRUDDtoBuilder()
            .dto(new UserAppInfo({ token, os: 'web', appType: 'Customer' }))
            .build(),
        );
    })
    .catch(error => {
      if (error.code === 'messaging/permission-blocked') {
        console.log('Please Unblock Notification Request Manually');
      } else {
        console.log('Error Occurred', error);
      }
    });

  messaging.onMessage(payload => {
    let title = '';
    const notif = JSON.parse(payload.data.custom_notification);
    console.log('payload', notif);
    if (notif.type === 'Subscription' || notif.type === 'Bill')
      title = (
        <div>
          {notif.large_icon && (
            <img src={notif.large_icon} width={80} height={80} />
          )}
          <span>{notif.title}</span>
        </div>
      );
    else if (notif.type === 'Product')
      title = (
        <Link
          to={`/product-details/${notif.meta_data.productId}/${
            notif.meta_data.vendorBranchId
          }`}
        >
          {notif.large_icon && (
            <img src={notif.large_icon} width={80} height={80} />
          )}
          <span>{notif.title}</span>
        </Link>
      );

    notification.config({
      placement: 'bottomRight',
      bottom: 50,
      duration: 300,
    });
    notification.open({
      message: title,
      description: notif.body,
    });
    // this is the function that gets triggered when you receive a
    // push notification while you’re on the page. So you can
    // create a corresponding UI for you to have the push
    // notification handled.
  });
}
