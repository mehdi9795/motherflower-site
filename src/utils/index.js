/**
 * Created by majid on 12/28/16.
 */

// import { notification } from 'antd';
// import coreFetch from '../core/fetch';
// import { loginUrl } from '../variables';

export function createConstants(...constants) {
  return constants.reduce((acc, constant) => {
    acc[constant] = constant;
    return acc;
  }, {});
}
export function createReducer(initialState, reducerMap) {
  return (state = initialState, action) => {
    const reducer = reducerMap[action.type];
    return reducer ? reducer(state, action.payload) : state;
  };
}
export function checkHttpStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}
export function parseJSON(response) {
  return response.json();
}

export function setCookie(name, value, hours) {
  if (process.env.BROWSER) {
    let expires = '';
    if (hours) {
      const date = new Date();
      date.setTime(date.getTime() + hours * 60 * 60 * 1000);
      expires = `; expires=${date.toGMTString()}`;
    }
    document.cookie = `${name}=${value}${expires}; path=/`;
  } else {
    console.log('You are not supposed to set a cookie at server');
  }
}

export function getCookie(name, cookie = null) {
  if (!cookie && process.env.BROWSER) {
    return readCookieFrom(document.cookie, name);
  } else if (cookie) {
    return readCookieFrom(cookie, name);
  }
  return null;
}

export function readCookieFrom(cookies, name) {
  const nameEQ = `${name}=`;
  const ca = cookies.split(';');
  let i = 0;
  const length = ca.length;
  for (; i < length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

export function deleteCookie(name) {
  setCookie(name, '', -1);
}

export const FetchData = async (url, fetch) => {
  const defaults = {
    method: 'GET',
  };
  const resp = await fetch(url, defaults);
  const data = await resp.json();
  if (data.code === 401 || data.code === 403) {
    if (process.env.BROWSER) {
      // window.location = loginUrl;
    }
  }
  return data;
};

export const FetchDataAuth = async (token, url, fetch) => {
  const defaults = {
    method: 'GET',
    headers: { token },
  };
  const resp = await fetch(url, defaults, fetch);
  const data = await resp.json();
  if (data.code === 401 || data.code === 403) {
    if (process.env.BROWSER) {
      // window.location = loginUrl;
    }
  }
  return data;
};

export const DeleteData = async (token, url, fetch) => {
  const defaults = {
    method: 'DELETE',
    headers: { token },
  };
  const resp = await fetch(url, defaults, fetch);
  const data = await resp.json();
  if (data.code === 401 || data.code === 403) {
    if (process.env.BROWSER) {
      // window.location = loginUrl;
    }
  }
  return data;
};

export const PostData = async (url, body, fetch) => {
  const defaults = {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(body),
  };

  const resp = await fetch(url, defaults);

  if (resp.status === 200) {
    const data = await resp.json();
    return data;
  }
  return resp;
};

export const PutData = async (token, url, body, fetch) => {
  const defaults = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      token,
    },
    body,
  };
  const resp = await fetch(url, defaults);
  const data = await resp.json();
  if (data.code === 401 || data.code === 403) {
    if (process.env.BROWSER) {
      // window.location = loginUrl;
    }
  }
  return data;
};

export const formatNum = (number) => {
  const num = number.toString();
  let formattedNum = '';
  for (let i = 1; i <= num.length; i += 1) {
    if (i % 3 === 0) {
      formattedNum = `,${num[num.length - i]}${formattedNum}`;
    } else {
      formattedNum = num[num.length - i] + formattedNum;
    }
  }
  if (num.length % 3 === 0) {
    formattedNum = formattedNum.substr(1);
  }
  return formattedNum;
}
