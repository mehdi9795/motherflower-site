import { notification } from 'antd';

export const getDtoQueryString = getDto => {
  const output = getDto.replace(/['"]+/g, '');
  return output;
};

export const showNotification = (
  type,
  title,
  description,
  duration = 100,
  className = 'successBox',
) => {
  notification[type]({
    bottom: 15,
    duration,
    className,
    placement: 'bottomRight',
    message: title,
    description,
  });
};

export const ellipsisString = (
  str,
  maxLength = 19,
  elliplsisSuffix = '...',
) => {
  const strArray = str.split(' ');
  const badCharArray = ['-'];
  let outPut = '';
  for (let i = 0; i < strArray.length; i++) {
    const item = strArray[i];
    if (!badCharArray.find(badItem => badItem === item)) {
      if (outPut.length <= maxLength) {
        if (item.length + outPut.length < maxLength) {
          outPut = `${outPut} ${item}`;
        } else {
          outPut = `${outPut} ${elliplsisSuffix}`;
          break;
        }
      }
    }
  }
  return outPut;
};

export const PersianNumber = (number) => {
  number += '';
  const x = number.split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? `.${x[1]}` : '';
  const rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  number = x1 + x2;
  return number;
};
