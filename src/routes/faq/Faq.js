import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Collapse } from 'antd';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Faq.css';

const { Panel } = Collapse;

class Faq extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    faqData: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    faqData: [],
  };

  render() {
    const { faqData } = this.props;
    return (
      <div className={s.root}>
        <div className={s.container}>
          <h2>{this.props.title}</h2>

          <Collapse accordion>
            {faqData.map(faq => (
              <Panel header={faq.question} key={faq.id}>
                <p>{faq.answer}</p>
              </Panel>
            ))}
          </Collapse>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  faqData: state.cmsFaq.faqListData ? state.cmsFaq.faqListData.items : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Faq));
