import React from 'react';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import Faq from './Faq';
import { FaqDto } from '../../dtos/cmsDtos';
import { getFaqApi } from '../../services/cmsApi';
import { getDtoQueryString } from '../../utils/helper';
import { faqListFailure, faqListSuccess } from '../../redux/cms/action/faq';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';

const title = 'سوالات متداول';

async function action({ store }) {
  const faqData = await getFaqApi(
    getDtoQueryString(
      new BaseGetDtoBuilder().dto(new FaqDto({ published: true })).buildJson(),
    ),
  );

  if (faqData.status === 200) store.dispatch(faqListSuccess(faqData.data));
  else store.dispatch(faqListFailure());

  return {
    chunks: ['faq'],
    title,
    component: (
      <Layout>
        <Faq title={title} />
      </Layout>
    ),
  };
}

export default action;
