import React from 'react';
import Search from './Search';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import {
  categoryListFailure,
  categoryListSuccess,
} from '../../redux/catalog/action/category';
import { SearchColorOptionDto } from '../../dtos/catalogDtos';
import { VendorBranchDto, VendorBranchZoneDto } from '../../dtos/vendorDtos';
import { getDtoQueryString } from '../../utils/helper';
import {
  getColorOptionApi,
  getSearchSpecificationAttributeApi,
  getSearchVendorBranchCategoriesApi,
  getTagVendorBranchApi,
} from '../../services/catalogApi';
import {
  searchColorOptionFailure,
  searchColorOptionSuccess,
} from '../../redux/catalog/action/searchColorOption';
import {
  searchSpecificationAttributeFailure,
  searchSpecificationAttributeSuccess,
} from '../../redux/catalog/action/searchSpecificationAttribute';
import {
  productSearchFailure,
  productShouldUpdateRequest,
  productShouldUpdateSuccess,
} from '../../redux/catalog/action/product';
import {CityDto, DistrictDto, ZoneDto} from '../../dtos/commonDtos';
import {getCityApi, getDistrictApi} from '../../services/commonApi';
import {
  singleDistrictFailure,
  singleDistrictSuccess,
} from '../../redux/common/action/district';
import {
  getVendorBranchApi,
  getVendorBranchZoneApi,
} from '../../services/vendorApi';
import { vendorBranchZoneListSuccess } from '../../redux/vendor/action/vendorBranchZone';
import { showAddToCardRequest } from '../../redux/shared/action/addToCard';
import { tagListFailure, tagListSuccess } from '../../redux/catalog/action/tag';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import {cityListSuccess} from "../../redux/common/action/city";

async function action({ store, params, query }) {
  try {
    store.dispatch(showAddToCardRequest());

    const district = await getDistrictApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new DistrictDto({ id: params.district }))
          .buildJson(),
      ),
    );

    const vendorBranch = await getVendorBranchApi(
      getDtoQueryString(
        new BaseGetDtoBuilder().dto(
          new VendorBranchDto({
            id: params.vendorBranch,
          }),
        ).buildJson(),
      ),
    );
    query.districtName = district.data.items[0].name;
    query.districtId = district.data.items[0].id;
    query.cityId = district.data.items[0].cityDto.id;
    query.vendorBranchName = vendorBranch.data.items[0].name;
    query.vendorBranchId = vendorBranch.data.items[0].id;

    const categoryData = await getSearchVendorBranchCategoriesApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new VendorBranchDto({ id: query.vendorBranchId }))
          .buildJson(),
      ),
    );
    const jsonList = new BaseGetDtoBuilder()
      .dto(
        new SearchColorOptionDto({
          categoryIds: query.categoryIds
            ? query.categoryIds.replace(/,/g, '-')
            : 0,
          vendorBranchId: query.vendorBranchId,
        }),
      )
      .fromCache(true)
      .buildJson();

    const searchColorOptionData = await getColorOptionApi(
      getDtoQueryString(jsonList),
    );
    const searchSpecificationAttributeData = await getSearchSpecificationAttributeApi(
      getDtoQueryString(jsonList),
    );

    /**
     * get vendor branch for product search
     */

    const VBZone = new BaseGetDtoBuilder()
      .dto(
        new VendorBranchZoneDto({
          zoneDto: new ZoneDto({
            districtDtos: [new DistrictDto({ name: query.districtName, cityDto: new CityDto({ id: district.data.items[0].cityDto.id }) })],
          }),
        }),
      )
      .searchMode(true)
      .fromCache(true)
      .buildJson();

    const vendorBranchData = await getVendorBranchZoneApi(
      getDtoQueryString(VBZone),
    );

    /**
     * get tag for product search
     */

    const tagData = await getTagVendorBranchApi(
      getDtoQueryString(
        JSON.stringify({ dto: { id: params.vendorBranch }, fromCache: true }),
      ),
    );
    if (
      categoryData.status === 200 &&
      searchColorOptionData.status === 200 &&
      searchSpecificationAttributeData.status === 200 &&
      vendorBranchData.status === 200 &&
      tagData.status === 200
    ) {
      /**
       * get product with data of query for search page
       */
      store.dispatch(productShouldUpdateRequest());

      const singleDistrict = await getDistrictApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new DistrictDto({
                name: query.districtName,
                searchSameName: true,
              }),
            )
            .fromCache(true)
            .buildJson(),
        ),
      );

      const responseCities = await getCityApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new CityDto({
                active: true,
              }),
            )
            .fromCache(true)
            .buildJson(),
        ),
      );

      if (responseCities.status === 200)
        store.dispatch(cityListSuccess(responseCities.data));


      if (singleDistrict.status === 200) {
        query.vendorBranchId = query.vendorBranchId;
        query.sort = query.sort;
        query.priceOption = query.priceOption ? query.priceOption : '2';

        store.dispatch(categoryListSuccess(categoryData.data));
        store.dispatch(searchColorOptionSuccess(searchColorOptionData.data));
        store.dispatch(
          searchSpecificationAttributeSuccess(
            searchSpecificationAttributeData.data,
          ),
        );
        // productSearchData.data.items = [];
        // store.dispatch(productSearchSuccess(productSearchData.data));
        store.dispatch(singleDistrictSuccess(singleDistrict.data));
        store.dispatch(vendorBranchZoneListSuccess(vendorBranchData.data));
        store.dispatch(tagListSuccess(tagData.data));
        store.dispatch(productShouldUpdateSuccess());
      } else {
        store.dispatch(productSearchFailure());
        store.dispatch(singleDistrictFailure());
      }
    } else {
      store.dispatch(categoryListFailure());
      store.dispatch(searchColorOptionFailure());
      store.dispatch(searchSpecificationAttributeFailure());
      store.dispatch(tagListFailure());
      store.dispatch(productShouldUpdateSuccess());
    }
  } catch (error) {
    store.dispatch(categoryListFailure());
    store.dispatch(searchColorOptionFailure());
    store.dispatch(searchSpecificationAttributeFailure());
    store.dispatch(productShouldUpdateSuccess());
  }

  return {
    chunks: ['search'],
    title: 'Search',
    component: (
      <Layout showFooter={false}>
        <Search params={query} />
      </Layout>
    ),
  };
}

export default action;
