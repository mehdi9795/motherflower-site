import React from 'react';
import { Select, Spin } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import InfiniteScroll from 'react-infinite-scroller';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import $ from 'jquery';
import PropTypes from 'prop-types';
import s from './Search.css';
import FilterSide from '../../components/Search/FilterSide';
import ProductCard from '../../components/Shared/ProductCard';
import {
  districtOptionListRequest,
  singleDistrictRequest,
} from '../../redux/common/action/district';
import { ProductSearchDto } from '../../dtos/catalogDtos';
import {
  isEmptyProductCardArray,
  isNotEmptyProductCardArray,
  productSearchRequest,
  productSearchSuccess,
} from '../../redux/catalog/action/product';
import {
  BEST_SELLER,
  LATEST,
  MOST_CHEAPEST,
  MOST_EXPENSIVE,
  MOST_VISITED,
} from '../../Resources/Localization';

import CPSelect from '../../components/CP/CPSelect';

import {
  loadingSearchHideRequest,
  loadingSearchShowRequest,
} from '../../redux/shared/action/loading';
import { getDtoQueryString } from '../../utils/helper';
import { getProductSearchApi } from '../../services/catalogApi';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { getPushToken } from '../../utils/push/getPushToken';
import CPButton from '../../components/CP/CPButton';
import history from '../../history';
import AppInstall from '../../components/Home/AppInstall';
import { getCookie } from '../../utils';
import {baseCDN} from "../../setting";

const { Option } = Select;

class Search extends React.Component {
  static propTypes = {
    params: PropTypes.objectOf(PropTypes.any).isRequired,
    productSearchCount: PropTypes.number,
    loadingSearch: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    productSearchCount: 0,
    loadingSearch: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      sort: props.params.sort,
      loading: false,
      hasMore: true,
      totalPageCount: props.productSearchCount,
      data: [],
      pageIndex: 0,
      isWeb: false,
    };
    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    getPushToken();
    /*  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition);
    }
  }

  async showPosition(position) {
    const lat = position.coords.latitude;
    const lng = position.coords.longitude; */
    this.props.actions.hideLoading();
    $(window).scroll(() => {
      this.fixedFilterSide();
    });

    this.updatePredicate();
  }

  async getData() {
    const { sort } = this.state;
    const { data } = this.state;
    const params = this.getParamsURL();
    const vendorBranchId = window.location.pathname.split('/')[3];
    this.setState({ loading: true });
    if (this.state.totalPageCount < this.state.pageIndex) {
      this.setState({
        hasMore: false,
        loading: false,
      });
      return;
    }

    /**
     * get product with data of query for search page
     */

    const productSearchList = new BaseGetDtoBuilder()
      .dto(
        new ProductSearchDto({
          categoryIds: params.categoryIds ? params.categoryIds.split(',') : 0,
          specificationAttributeIds: params.specificationIds
            ? params.specificationIds.split(',')
            : [],
          colorOptionIds: params.colorOptionIds
            ? params.colorOptionIds.split(',')
            : [],
          vendorBranchIds: window.location.pathname.split('/')[3],
          sortOption: sort,
          tagIds: [params.tagIds],
          toPrice: params.maxPrice ? params.maxPrice : '3000000',
          fromPrice: params.minPrice ? params.minPrice : '20000',
          priceOption: params.priceOption ? params.priceOption : '1',
        }),
      )
      .pageIndex(this.state.pageIndex)
      .pageSize(20)
      .disabledCount(true)
      .fromCache(true)
      .buildJson();

    const productSearchData = await getProductSearchApi(
      getDtoQueryString(productSearchList),
    );
    const thiz = this;
    let countDiv = 0;
    setTimeout(() => {
      productSearchData.data.items.map(item => {
        countDiv += 1;
        const css = {
          animationDelay: `${0.2 * countDiv}s`,
        };
        data.push(
          <div key={item.productId} className="col-lg-4 col-sm-6">
            <ProductCard
              styles={css}
              lazyLoading
              vendorBranchId={vendorBranchId}
              key={item.productId}
              url={`/product-details/${item.productId}/${vendorBranchId}`}
              imageUrl1={item.imageUrls[0]}
              imageUrl2={item.imageUrls[1]}
              ProductName={item.name}
              value={item.rateAverage}
              price={
                params.priceOption === '2'
                  ? item.deliveryBasePrice.toString()
                  : item.pickupBasePrice.toString()
              }
              discount={
                params.priceOption === '2'
                  ? item.deliveryPriceAfterDiscount.toString()
                  : item.pickupPriceAfterDiscount.toString()
              }
              percent={
                params.priceOption === '2'
                  ? item.deliveryPriceDiscount.toString()
                  : item.pickupPriceDiscount.toString()
              }
            >
              <style>{css}</style>
            </ProductCard>
          </div>,
        );
        return null;
      });
      thiz.setState({
        loading: false,
        totalPageCount: productSearchData.data.count / 20,
        pageIndex: thiz.state.pageIndex + 1,
      });
      thiz.props.actions.loadingSearchHideRequest();
      this.fixedFilterSide();
    }, 700);
  }

  getParamsURL = () => {
    const queryStringUrl = window.location.search.substring(1);
    const params = {};
    let temp;
    let i;
    let l;
    // Split into key/value pairs1
    const queries = queryStringUrl.split('&');
    // Convert the array of strings into an object
    for (i = 0, l = queries.length; i < l; i += 1) {
      temp = queries[i].split('=');
      params[temp[0]] = temp[1];
    }
    return params;
  };

  fixedFilterSide = () => {
    if (
      document.getElementById('header')
      // document.getElementById('footer')
    ) {
      const doc = document.documentElement;
      const heightHeader = document.getElementById('header').offsetHeight;
      // const heightFooter = 0;
      // const heightDocument = doc.scrollHeight;
      // const heightWindow = window.innerHeight;
      const widthWindow = window.innerWidth;
      if (widthWindow > 769) {
        if (doc.scrollTop > heightHeader)
          $('#filterSide').addClass('filterOptions col-md-4');
        else $('#filterSide').removeClass('filterOptions col-md-4');
        // if (
        //   doc.scrollTop + heightWindow + 20 > heightDocument - heightFooter &&
        //   document.getElementById('filterSide')
        // )
        //   document.getElementById('filterSide').style.height = '39%';
        // else if (document.getElementById('filterSide'))
        //   document.getElementById('filterSide').style.height = '90%';
      }
    }
  };
  applySearch = () => {
    setTimeout(() => {
      this.setState({
        pageIndex: 0,
        data: [],
        hasMore: true,
      });
    }, 500);
  };

  handleChangeInput = (inputName, value) => {
    const params = this.getParamsURL();
    this.setState({
      [inputName]: value,
    });
    let queryString = params.categoryIds
      ? `categoryIds=${params.categoryIds}&`
      : '';
    queryString = params.specificationIds
      ? `${queryString}specificationIds=${params.specificationIds}&`
      : queryString;
    queryString = params.colorOptionIds
      ? `${queryString}colorOptionIds=${params.colorOptionIds}&`
      : queryString;
    queryString = params.priceOption
      ? `${queryString}priceOption=${params.priceOption}&`
      : queryString;
    queryString = params.minPrice
      ? `${queryString}minPrice=${params.minPrice}&maxPrice=${params.maxPrice}&`
      : queryString;

    queryString = `${queryString}sort=${value}`;

    const newurl = `${window.location.protocol}//${window.location.host}${
      window.location.pathname
    }?${queryString}`;
    window.history.pushState({ path: newurl }, '', newurl);
    this.applySearch();
  };

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 992 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 992 });
    });
  };

  download = url => {
    window.open(url, '_blank');
  };

  render() {
    const { sort, hasMore, data, loading, isWeb } = this.state;
    const { params, loadingSearch } = this.props;

    return (
      <div>
        <Spin spinning={loadingSearch}>
          <div className={s.searchPage}>
            <div className={s.filterSide}>
              <FilterSide params={params} applySearch={this.applySearch} />
            </div>
            <div className={s.resultSide}>
              {isWeb ? (
                <div className={s.dropDowns}>
                  <img
                    src={`${baseCDN}/download2.png`}
                    height={32}
                    onClick={() => this.download('http://bit.ly/2GDBZNG')}
                  />
                  <img
                    src={`${baseCDN}/download3.png`}
                    height={32}
                    onClick={() => this.download('http://bit.ly/2TacaXe')}
                  />
                  <img
                    src={`${baseCDN}/download1.png`}
                    height={32}
                    onClick={() => this.download('http://bit.ly/2LBQmRB')}
                  />
                  <div className={s.orderDropDown}>
                    <b className={s.order}>ترتیب بر اساس :</b>
                    <CPSelect
                      value={sort}
                      onChange={value => this.handleChangeInput('sort', value)}
                    >
                      <Option value="1" key="1">
                        {BEST_SELLER}
                      </Option>
                      <Option value="2" key="2">
                        {MOST_VISITED}
                      </Option>
                      <Option value="3" key="3">
                        {MOST_EXPENSIVE}
                      </Option>
                      <Option value="4" key="4">
                        {MOST_CHEAPEST}
                      </Option>
                      <Option value="5" key="5">
                        {LATEST}
                      </Option>
                    </CPSelect>
                  </div>
                </div>
              ) : (
                <div className={s.dropDowns}>
                  <CPButton
                    className="greenBtn downloadBtn"
                    onClick={() => {
                      history.push('/app');
                    }}
                  >
                    دانلود اپلیکیشن<i className="mf-android" />
                    <i className="mf-apple" />
                  </CPButton>
                  <div className={s.orderDropDown}>
                    <b className={s.order}>ترتیب بر اساس :</b>
                    <CPSelect
                      value={sort}
                      onChange={value => this.handleChangeInput('sort', value)}
                    >
                      <Option value="1" key="1">
                        {BEST_SELLER}
                      </Option>
                      <Option value="2" key="2">
                        {MOST_VISITED}
                      </Option>
                      <Option value="3" key="3">
                        {MOST_EXPENSIVE}
                      </Option>
                      <Option value="4" key="4">
                        {MOST_CHEAPEST}
                      </Option>
                      <Option value="5" key="5">
                        {LATEST}
                      </Option>
                    </CPSelect>
                  </div>
                </div>
              )}

              <InfiniteScroll
                pageStart={0}
                loadMore={this.getData}
                hasMore={!loading && hasMore}
              >
                <div className="row">{data}</div>
              </InfiniteScroll>
              {!data.length &&
                !hasMore && (
                  <div className={s.noExistProduct}>
                    <i className="mf-search">
                      <i className="mf-close" />
                    </i>
                    <div>محصول مورد نظر شما یافت نشد!</div>
                  </div>
                )}
              <div className={s.loading}>
                <Spin spinning={loading && !loadingSearch} />
              </div>
            </div>
          </div>
        </Spin>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loadingSearch: state.loadingSearch.loadingSearch,
  vendorBranches: state.vendorVendorBranchZone.vendorBranchZoneData
    ? state.vendorVendorBranchZone.vendorBranchZoneData.items
    : [],
  productSearches: state.catalogProduct.productSearchData
    ? state.catalogProduct.productSearchData.items
    : [],
  productSearchCount: state.catalogProduct.productSearchData
    ? state.catalogProduct.productSearchData.count
    : [],
  productShouldUpdate: state.catalogProduct.productShouldUpdateLoading,
  isEmptyProductCardArray: state.catalogProduct.emptyProductArray
    ? state.catalogProduct.emptyProductArray
    : [],
  districts: state.commonDistrict.districtOptionData
    ? state.commonDistrict.districtOptionData.items
    : [],
  district:
    state.commonDistrict.singleDistrictData &&
    state.commonDistrict.singleDistrictData.length > 0
      ? state.commonDistrict.singleDistrictData.items[0]
      : '',
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      districtOptionListRequest,
      singleDistrictRequest,
      productSearchRequest,
      isNotEmptyProductCardArray,
      isEmptyProductCardArray,
      loadingSearchHideRequest,
      loadingSearchShowRequest,
      productSearchSuccess,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Search));
