import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Login.css';
import AccountWizard from '../../components/Account/AccountWizard/AccountWizard';

class Login extends React.Component {
  static propTypes = {
    // actions: PropTypes.objectOf(PropTypes.func).isRequired,
    fromUrl: PropTypes.string,
  };

  static defaultProps = {
    fromUrl: '',
  };

  toggleMeOne = () => {
    setTimeout(() => {
      this.setState({ register: !this.state.register });
    }, 500);
    setTimeout(() => {
      this.setState({ login: !this.state.login });
    }, 500);
  };

  toggleMeTwo = () => {
    setTimeout(() => {
      this.setState({ register: !this.state.register });
    }, 500);
    setTimeout(() => {
      this.setState({ confirmation: !this.state.confirmation });
    }, 500);
  };

  render() {
    const { fromUrl } = this.props;
    return (
      <div className={s.root}>
        <div className={s.loginWrap}>
          <AccountWizard selectedStep="forgetPassword" fromUrl={fromUrl} />
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Login);
