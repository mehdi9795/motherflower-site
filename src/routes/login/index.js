import React from 'react';
import LoginLayout from '../../components/Template/DefaultLayout/LoginLayout';
import Login from './Login';

const title = 'Log In';

async function action({ query }) {
  return {
    chunks: ['login'],
    title,
    component: (
      <LoginLayout>
        <Login fromUrl={query.from || ''} />
      </LoginLayout>
    ),
  };
}

export default action;
