import React from 'react';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import DownloadIOS from './downloadIOS';

const title = 'Contact Us';

function action() {
  return {
    chunks: ['IOS'],
    title: 'دانلود مستقیم IOS',
    component: (
      <Layout>
        <DownloadIOS title={title} />
      </Layout>
    ),
  };
}

export default action;
