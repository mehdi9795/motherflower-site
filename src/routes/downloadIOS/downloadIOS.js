import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './downloadIOS.css';
import { baseCDN } from '../../setting';
import CPButton from "../../components/CP/CPButton";

class DownloadIOS extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isWeb: false,
    };
  }

  componentDidMount() {
    this.updatePredicate();
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 780 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 780 });
    });
  };

  download = () => {
    setTimeout(() => {
      window.open('http://bit.ly/2GWM5c3', '_blank');
    }, 25);
  }

  render() {
    return this.state.isWeb ? (
      <div className={s.container}>
        <h2>نصب مستقیم اپلیکیشن مادِرفِلاور روی IOS</h2>
        <br />
        <label>
          برای استفاده از نسخه ios مادِرفِلاور ابتدا نسخه ی پیشین را از گوشی خود
          حذف کرده، سپس نسخه جدید را از اینجا دریافت کنید و مراحل زیر را انجام
          دهید
        </label>
        <br />
        <CPButton onClick={this.download}>
          <img
            width={204}
            src="/images/ios/botton.png" />
        </CPButton>
        <div className={s.steps}>
          <h2>گام اول</h2>
          <br />
          <p>
            پس از دانلود و اجرای برنامه، پیغام Untrusted Enterprise Developer را
            خواهید دید. گزینه Cancel را انتخاب کنید.
          </p>
          <div className="row">
            <div className="col-sm-4">
              <img src="/images/ios/1.png" />
            </div>
            <div className="col-sm-4">
              <img src="/images/ios/1.1.png" />
            </div>
          </div>
        </div>
        <hr />
        <div className={s.steps}>
          <div className="row">
            <div className="col-sm-4">
              <h2>گام دوم</h2>
              <br />
              <p>
                ​​​​وارد تنظیمات (Setting) گوشی خود شوید و گزینه General را
                انتخاب کنید.
              </p>
              <img src="/images/ios/2.png" />
            </div>
            <div className="col-sm-4">
              <h2>گام سوم</h2>
              <br />
              <p>​​سپس وارد Profile & Device Management شوید.</p>
              <img src="/images/ios/3.png" />
            </div>
            <div className="col-sm-4">
              <h2>گام چهارم</h2>
              <br />
              <p>​​گزینه Seeb Co OU را انتخاب کنید.</p>
              <img src="/images/ios/4.png" style={{ marginTop: '22px' }} />
            </div>
          </div>
        </div>
        <hr />
        <div className={s.steps}>
          <h2>گام پنجم</h2>
          <br />
          <p>
            ​​عبارت ​​Trust Seeb Co OU ​​را لمس کنید ​​و در انتها Trust را
            انتخاب کنید.
          </p>
          <div className="row">
            <div className="col-sm-4">
              <img src="/images/ios/5.png" />
            </div>
            <div className="col-sm-4">
              <img src="/images/ios/5.1.png" />
            </div>
          </div>
        </div>
      </div>
    ) : (
      <div className={s.container}>
        <h2>نصب مستقیم اپلیکیشن مادِرفِلاور روی IOS</h2>
        <br />
        <label>
          برای استفاده از نسخه ios مادِرفِلاور ابتدا نسخه ی پیشین را از گوشی خود
          حذف کرده، سپس نسخه جدید را از اینجا دریافت کنید و مراحل زیر را انجام
          دهید
        </label>
        <br />
        <CPButton onClick={this.download}>
          <img
            width={204}
            src="/images/ios/botton.png" />
        </CPButton>
        <div className={s.steps}>
          <h2>گام اول</h2>
          <br />
          <p>
            پس از دانلود و اجرای برنامه، پیغام Untrusted Enterprise Developer را
            خواهید دید. گزینه Cancel را انتخاب کنید.
          </p>
          <div>
            <img src="/images/ios/1.png" />
            <img src="/images/ios/1.1.png" />
          </div>
        </div>
        <div className={s.steps}>
          <h2>گام دوم</h2>
          <br />
          <p>
            ​​​​وارد تنظیمات (Setting) گوشی خود شوید و گزینه General را انتخاب
            کنید.
          </p>
          <div>
            <img src="/images/ios/2.png" />
          </div>
        </div>
        <div className={s.steps}>
          <h2>گام سوم</h2>
          <br />
          <p>​​سپس وارد Profile & Device Management شوید.</p>
          <div>
            <img src="/images/ios/3.png" />
          </div>
        </div>
        <div className={s.steps}>
          <h2>گام چهارم</h2>
          <br />
          <p>​​گزینه Seeb Co OU را انتخاب کنید.</p>
          <div>
            <img src="/images/ios/4.png" />
          </div>
        </div>
        <div className={s.steps}>
          <h2>گام پنجم</h2>
          <br />
          <p>
            ​​عبارت ​​Trust Seeb Co OU ​​را لمس کنید ​​و در انتها Trust را
            انتخاب کنید.
          </p>
          <div>
            <img src="/images/ios/5.png" />
            <img src="/images/ios/5.1.png" />
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(DownloadIOS);
