import React from 'react';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import Cooperation from './Cooperation';

const title = 'Contact Us';

function action() {
  return {
    chunks: ['cooperation'],
    title: 'همکاری با ما',
    component: (
      <Layout>
        <Cooperation title={title} />
      </Layout>
    ),
  };
}

export default action;
