import React from 'react';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Cooperation.css';
import CPButton from '../../components/CP/CPButton';
import CPModal from '../../components/CP/CPModal';

class Cooperation extends React.Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }

  showModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <div className={s.container}>
        <h2>فرصت های شغلی (استخدام)</h2>
        <div className="row">
          <div className={cs('col-sm-12 offset-md-2 col-md-5', s.cooperation)}>
            <h4>کارشناس مرکز تماس ( Call Center )</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>فن بیان و روابط عمومی بالا</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>قدرت ارائه و آموزش بالا </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  انعطاف‌پذیری زمانی و مکانی
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>روابط عمومی خوب</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>باهوش</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>با پشتکار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تحمل استرس بالا</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دقت و نظم</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>متعهد و مشتری مدار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی با نرم افزارهای آفیس و نرم افزارهای تلفن های هوشمند</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>توانایی انجام همزمان چند کار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>پیگیر و صبور در برابر مشتریان</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>افرادی که سابقه فعالیت در مرکز تماس و پشتیبانی را دارند در الویت میباشند</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 col-md-5', s.cooperation)}>
            <h4>کارشناس دیجیتال مارکتینگ</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>تحقیق روی بازار و تمایلات مشتریان به منظور شفاف شدن نیاز بازار و تدوین استراتژی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>قابلیت تحلیل داده و ارائه گزارشات </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  آشنایی کامل با کسب و کارهای آنلاین
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>پیدا کردن اسپانسر و شرکای تجاری جدید</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>برگزاری نمایشگاه ها و گردهمایی ها</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>ارتباط با مشتریان، تامین کنندگان و رسانه ها</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تهیه کلیپ های تبلیغاتی جهت انتشار در روزنامه ها، مجلات، تلویزیون یا فضای مجازی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تسلط به مفاهیم بازاریابی و تبلیغات</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تسلط به طراحی ، بودجه بندی ، برنامه ریزی و مدیریت اجرای کمپین های تبلیغاتی دیجیتال</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> تسلط به مفاهیم و کاربرد انواع سرویس ها و ابزار های دیجیتال مارکتینگ مثل بازاریابی شبکه های اجتماعی ، تبلیغات کلیکی و موبایلی ، بهینه سازی وب سایت ، Adwords ، بازاریابی ایمیلی و ...</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تسلط به تحلیل بازار و مخاطب هدف و همچنین توانایی طراحی پرسونای مخاطبان</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>توانایی کار با ابزار های تحلیلی مثل google Analyze و Similar web و …..</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>توانایی پرزنت پروپوزال و برگزاری جلسات دفاع</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>روحیه کار تیمی ، تعامل و همکاری خوب با همکاران در محیط کار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>وانایی برنامه‌ریزی برای انواع Affiliate Marketing</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تسلط به Google Adwords و تبلیغات PPC</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تسلط به Google Analytics</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تسلط روی SEO</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 offset-md-2 col-md-5', s.cooperation)}>
            <h4>کارشناس فروش حضوری</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>روابط عمومی بالا</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> قدرت مذاکره</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>  توانایی برقراری ارتباط با مشتری</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مدیریت ارتباط با مشتری</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دانش روانشناسی مشتری</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>فن بیان</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>حداکثر سن: ۳۰ سال</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>جنسیت: آقا</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> داشتن سابقه فروش الزامی است</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 col-md-5', s.cooperation)}>
            <h4>برنامه نویس موبایل (React Native)</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>مسلط به React ,React Native </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی با Redux</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> مسلط به ES6 و ES5</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به پیاده سازی طرح به صورت Pixel Perfect</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>توانایی نوشتن کاستوم سرویس ها در React-native</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>اشنا به مفاهیم UI & UX</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به RESTful API</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی با کتابخانه های پرکاربرد</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تجربه کار با Git </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دارای روحیه کار تیمی </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>خلاق و با پشتکار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>نمونه کار قابل ارائه</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  فرصت بیشتر برای مطالعه و یادگیری تکنولوژی‌های متنوع با توجه به
                  ماهیت فعالیت کار
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>درک کامل اکوسیستم‌های Android و iOS</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به Agile و تکنیک های سریع توسعه نرم افزار</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 offset-md-2 col-md-5', s.cooperation)}>
            <h4>برنامه نویس React) Front End)</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>
                  مسلط به html, css , flex, JavaScript, jQuery, responsive
                  design
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی با Redux </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به RESTful API</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی با Build Tools, Task Runners </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به Webpack, Babel</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تجربه کار با Git</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دارای روحیه کار تیمی </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>خلاق و با پشتکار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  فرصت بیشتر برای مطالعه و یادگیری تکنولوژی‌های متنوع با توجه به
                  ماهیت فعالیت کار
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به Agile و تکنیک های سریع توسعه نرم افزار</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 col-md-5', s.cooperation)}>
            <h4>برنامه نویس C# - .Net) Back End)</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>مسلط به #C </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به EntityFramework</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  مسلط به مفاهیم پیاده سازی بانک‌های اطلاعاتی با MS SQL Server
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به T-SQL</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به Rest Web API</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی با Design Patterns امتیاز محسوب می شود</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تجربه کار با Git</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دارای روحیه کار تیمی </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>خلاق و با پشتکار</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  فرصت بیشتر برای مطالعه و یادگیری تکنولوژی‌های متنوع با توجه به
                  ماهیت فعالیت کار
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به Agile و تکنیک های سریع توسعه نرم افزار</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 offset-md-2 col-md-5', s.cooperation)}>
            <h4>کارشناس پشتیبان شبکه</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>
                  مسلط به مفاهیم CCNA
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به مفاهیم Domain controller، Group Policy، DHCP و قدرت مدیریت و رفع مشکل در شبکه هایی با تعداد 50 الی 100 کاربر </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به رفع مشکلات سیستم های عامل ویندوزی 7،8،10 تحت شبکه </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به حل مشکلات نرم افزار های اداری از قبیل Office و غیره، تحت شبکه </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به حل مشکلات سخت افزاری از قبیل پرینتر و دستگاه های جانبی، تحت شبکه</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به مفاهیم VOIP و مرکز تماس های مبتنی بر Asterisk</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به مفاهیم مجازی سازی VMWare </label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 col-md-5', s.cooperation)}>
            <h4>استخدام متخصص SEO (سئو) Google</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label> مسلط به موتور جستجو Google</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به تمامی update های اخیر و پیگیر تغییرات الگوریتمی گوگل </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  مسلط به Social Media مارکتینگ و انواع روشهای ارتقای ranking در گوگل
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به مفاهیم و ابزارهای سئو</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مسلط به زبان انگلیسی و توانایی نوشتن مقاله های لازم</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> سئو (On-page SEO)</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> سئو (Off-page SEO)</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>سئو (SEO tools)</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آنالیز کننده کلمات کلیدی (عنوان - تگ و تیکت ها) فارسی و لاتین و تحلیل گر وزن کلمات کلیدی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> معرفی سایت به موتورهای جستجو و سایت های دایرکتوری</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آنالیز رفتار کاربران و بازدیدکنندگان سایت در هنگام بازدید</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>مانیتور کردن رتبه سایت ها و افزایش رتبه سایت  </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>ارائه گزارش های مختلف از رتبه</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دارای مدرك تحصیل کارشناسی در یکی از رشته های مرتبط متخصص سئو </label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 offset-md-2 col-md-5', s.cooperation)}>
            <h4>کارشناس مالی</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>
                  ثبت اسناد حسابداری
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تهیه گزارشات در زمان مقرر </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  انجام محاسبات مربوط به حقوق و دستمزد
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنا به مباحث مالیات و ارزش افزوده</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تجربۀ کاری در زمینۀ حسابداری فروش</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> کنترل صورت وضعیت پیمانکاران</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>محاسبه بهای تمام شده</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تهیه صورت مغایرت های مالی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تهیه گزارشات مالی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> آشنایی با پرداخت بیمه و مالیات، مفاصا حساب</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی به امور حساب رسی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>سابقه حداقل 3ساله  </label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
          <div className={cs('col-sm-12 col-md-5', s.cooperation)}>
            <h4>استخدام طراح گرافیک</h4>
            <label>مهارت های مورد نیاز:</label>
            <ul>
              <li className="mf-arrowsLeft">
                <label>تسلط بر مفاهیم مختلف طراحی ، تصویرسازی و تبلیغات ، هویت برند و گرافیک در فضای وب</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>آشنایی بالا با مفاهیم تبلیغات و بازاریابی، کسب و کارهای آنلاین و مفاهیم دیجیتال مارکتینگ </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>
                  دارای روحیه کار تیمی، خلاق، با پشتکار و انعطاف پذیر
                </label>
              </li>
              <li className="mf-arrowsLeft">
                <label>دارا بودن قابلیت مدیریت انجام چند پروژه به صورت هم زمان</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> توانایی انجام کار تیمی و همکاری با واحدهای مختلف از قبیل ایده پردازی و مدیران پروژه</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>طراحی و تولید محتوای بصری (بنر، عکس، گیف و..)</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> طراحی و ادیت بخش ویژوال کمپین های دیجیتال مارکتینگ</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>تولید و ادیت موشن گرافیک و ویدئو های کوتاه جهت انتشار در شبکه های اجتماعی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label> مانیتورینگ، پیگیری و برنامه ریزی انتشار محتوا در شبکه های اجتماعی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>جستجوی تصاویر و محتوای بصری متناسب با متن جهت انتشار در شبکه های اجتماعی</label>
              </li>
              <li className="mf-arrowsLeft">
                <label>طراحی لوگو</label>
              </li>
            </ul>
            <CPButton onClick={() => this.showModal()}>ارسال رزومه</CPButton>
          </div>
        </div>
        <CPModal
          title="ارسال رزومه"
          handleCancel={this.closeModal}
          textClose="بستن"
          iconClose=""
          hideOkButton
          visible={this.state.showModal}
        >
          ارسال رزومه به ایمیل job@motherFlower.com
        </CPModal>
      </div>
    );
  }
}

export default withStyles(s)(Cooperation);
