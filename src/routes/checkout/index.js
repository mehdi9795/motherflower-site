import React from 'react';
import Checkout from './Checkout';
import CheckoutLayout from '../../components/Template/DefaultLayout/CheckoutLayout';
import { getDtoQueryString } from '../../utils/helper';
import { GetAddressUser } from '../../services/identityApi';
import { AddressDto } from '../../dtos/identityDtos';
import { deleteCookie, getCookie } from '../../utils';
import { addressListSuccess } from '../../redux/identity/action/address';
import { cityListSuccess } from '../../redux/common/action/city';
import { getCityApi, getDistrictApi } from '../../services/commonApi';
import { basketListSuccess } from '../../redux/catalog/action/basket';
import { BasketDto } from '../../dtos/catalogDtos';
import {GetBankApi, GetUserBasketHistoriesApi, GetWalletsApi} from '../../services/samApi';
import { bankListSuccess } from '../../redux/sam/action/bank';
import { CityDto, DistrictDto } from '../../dtos/commonDtos';
import { districtOptionListSuccess } from '../../redux/common/action/district';
import { loginSuccess } from '../../redux/identity/action/account';
import history from '../../history';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import {walletListFailure, walletListSuccess} from "../../redux/sam/action/wallet";

const title = 'Checkout';
async function action({ store }) {
  const { token } = store.getState().account.loginData; // getCookie('siteToken');

  this.addresses = [];
  if (token) {
    const responseBasket = await GetUserBasketHistoriesApi(
      getDtoQueryString(
        new BaseGetDtoBuilder().dto(new BasketDto()).buildJson(),
      ),
      token,
    );
    if (responseBasket.status === 200) {
      if (responseBasket.data.items && responseBasket.data.items.length > 0) {
        responseBasket.data.items[0].userBasketHistoryDetailDtos.map(item => {
          if (item.expired) history.push('/card');
          return null;
        });
      }
      store.dispatch(basketListSuccess(responseBasket.data));
    }

    const response = await GetAddressUser(
      token,
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new AddressDto({ active: true }))
          .buildJson(),
      ),
    );
    if (response.status === 200) {
      this.addresses = response.data;
      store.dispatch(addressListSuccess(response.data));
    } else if (response.status === 401) {
      store.dispatch(loginSuccess({}));
      deleteCookie('siteToken');
      deleteCookie('siteUserName');
      deleteCookie('siteDisplayName');
    }

    /**
     * get all cities for first time with default clause
     */
    const responseCity = await getCityApi(
      getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
    );

    if (responseCity.status === 200) {
      store.dispatch(cityListSuccess(responseCity.data));

      const responseDistrict = await getDistrictApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(
              new DistrictDto({
                cityDto: new CityDto({ id: responseCity.data.items[0].id }),
              }),
            )
            .buildJson(),
        ),
      );

      if (responseDistrict.status === 200) {
        store.dispatch(districtOptionListSuccess(responseDistrict.data));
      }
    }
  } else {
    const guestKey = getCookie('guestKey');

    if (guestKey) {
      const response = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder().dto(new BasketDto({ guestKey })).buildJson(),
        ),
        token,
      );

      if (response.status === 200) {
        store.dispatch(basketListSuccess(response.data));
      }
    }
  }

  const response = await GetBankApi(
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new BasketDto({ published: true }))
        .buildJson(),
    ),
  );

  if (response.status === 200) {
    store.dispatch(bankListSuccess(response.data));
  }

  return {
    chunks: ['checkout'],
    title,
    component: (
      <CheckoutLayout>
        <Checkout addresses={this.addresses} />
      </CheckoutLayout>
    ),
  };
}

export default action;
