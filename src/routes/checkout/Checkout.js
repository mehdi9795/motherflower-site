import React from 'react';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Checkout.css';
import ShippingInfo from '../../components/Checkout/ShippingInfo';
import CPWizard from '../../components/CP/CPWizard';
import CheckoutPayment from '../../components/Checkout/CheckoutPayment';
import AccountWizard from '../../components/Account/AccountWizard';
import Header from '../../components/Template/Header';

class Checkout extends React.Component {
  static propTypes = {
    userData: PropTypes.objectOf(PropTypes.any),
    addresses: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    userData: {},
    addresses: {},
  };

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  render() {
    const data = [
      {
        key: 1,
        title: 'ورود',
        className: s.login,
        components: <AccountWizard fromCheckout selectedStep="forgetPassword" />,
      },
      {
        key: 2,
        title: 'اطلاعات ارسال سفارش',
        className: s.shipping,
        components: <ShippingInfo addresses={this.props.addresses} />,
      },
      {
        key: 3,
        title: 'پرداخت',
        className: s.payment,
        components: <CheckoutPayment />,
      },
    ];
    const dd = this.props.userData.displayName ? 2 : 1;
    return (
      <div className={s.root}>
        <Header noBackground />
        <CPWizard
          steps={data}
          selectedStep={dd}
          availableSteps={[1, 2, 3]}
          passedSteps={this.props.userData.displayName ? [1] : []}
          disableSteps={this.props.userData.displayName ? [1] : []}
        />
        {/* <AddToCard count={basketProductCount} /> */}
        {/*<Chat />*/}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  userData: state.account.loginData,
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(s)(Checkout),
);
