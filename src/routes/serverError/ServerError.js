/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ServerError.css';
import RoundLogo from '../../components/Template/RoundLogo';
import CPButton from '../../components/CP/CPButton';
import history from '../../history';
import {baseCDN} from "../../setting";

class ServerError extends React.Component {

  render() {
    return (
      <div>
        <RoundLogo className={s.pos} />
        <CPButton
          className={s.backButton}
          onClick={() => {
            history.push('/');
          }}
        >
          بازگشت به صفحه اصلی
        </CPButton>
        <img src={`${baseCDN}/500.png`} style={{ width: '100%', height: '732px' }} />
      </div>
    );
  }
}

export default withStyles(s)(ServerError);
