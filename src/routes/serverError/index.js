import React from 'react';
import ServerError from './ServerError';
import s from './ServerError.css';

const title = 'serverError';

function action() {
  return {
    chunks: ['serverError'],
    title,
    component: (
      <div className={s.content}>
        <ServerError title={title} />
      </div>
    ),
  };
}

export default action;
