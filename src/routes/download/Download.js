import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Download.css';
import CPButton from '../../components/CP/CPButton/CPButton';
import CPIntlPhoneInput from '../../components/CP/CPIntlPhoneInput';
import history from '../../history';
import { baseCDN } from '../../setting';
import { showNotification } from '../../utils/helper';
import { postDownloadApp } from '../../services/commonApi';

class AppDownload extends Component {
  static propTypes = {
    downloadBtn1: PropTypes.string,
    downloadBtn2: PropTypes.string,
  };

  static defaultProps = {
    downloadBtn1: `${baseCDN}/cafe.png`,
    // downloadBtn1: `${baseCDN}/google_play.png`,
    downloadBtn2: `${baseCDN}/apple.png`,
  };

  constructor(props) {
    super(props);
    this.state = {
      coverFlow: null,
      isWeb: false,
      titleSlider: 'انتخاب نزدیکترین گلفروشی',
      imgSplash: `${baseCDN}/splash/1.png`,
      phoneNumberFinal: '',
      phoneNumber: '',
      isValidPhoneNumber: false,
      countryCode: '',
    };

    this.sendToPhoneNumber = this.sendToPhoneNumber.bind(this);
  }

  componentDidMount() {
    this.updatePredicate();

    if (!this.state.isWeb)
      setInterval(() => {
        const element = document.getElementsByClassName('activeStep')[0];
        if (element) {
          if (element.nextElementSibling) element.nextElementSibling.click();
          else document.getElementsByClassName('firstSlide')[0].click();
        }
      }, 5000);
  }

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 480 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 480 });
    });
  };

  changeStep = (e, step, img) => {
    this.setState({
      titleSlider: step,
      imgSplash: `${baseCDN}/splash/${img}.png`,
    });

    const element = document.getElementsByClassName('activeStep')[0];
    element.classList.remove('activeStep');
    if (e.target.tagName === 'SPAN')
      e.target.parentNode.classList.add('activeStep');
    else e.target.classList.add('activeStep');
  };

  downloadSibApp = () => {
    // window.open('http://bit.ly/2GWM5c3', '_blank');
    window.open('https://motherflower.com/ios', '_blank');
  };

  downloadCafeBazar = () => {
    window.open('https://cafebazaar.ir/app/com.motherflower/?l=fa', '_blank');
  };

  changeNumber = (status, value, countryData, number) => {
    const re = /^[0-9\b]+$/;
    if (value === '' || re.test(value)) {
      this.setState({
        phoneNumberFinal: number.replace(/ /g, ''),
        phoneNumber: value,
        isValidPhoneNumber: status,
        countryCode: countryData,
      });
    }
  };

  async sendToPhoneNumber() {
    const {
      phoneNumberFinal,
      phoneNumber,
      isValidPhoneNumber,
      countryCode,
    } = this.state;

    if (!isValidPhoneNumber) {
      showNotification(
        'error',
        '',
        'لطفا شماره همراه را صحیح وارد کنید',
        10,
        'errorBox',
      );
      return;
    }

    const response = await postDownloadApp({
      dto: {
        phone: phoneNumberFinal,
        countryCode: countryCode.dialCode,
      },
    });

    if (response.status === 200)
      showNotification(
        'success',
        '',
        `لینک دانلود اپلیکیشن به شماره ${phoneNumber} ارسال شد`,
        10,
      );
  }

  render() {
    const { downloadBtn1, downloadBtn2 } = this.props;
    const {
      coverFlow: Component,
      phoneNumber,
      isWeb,
      titleSlider,
      imgSplash,
    } = this.state;

    return isWeb ? (
      <div>
        <div className={s.mobileHeader}>
          <img className={s.headerImage} />
          <div className={s.logo}>
            <img
              src={`${baseCDN}/logo.png`}
              onClick={() => {
                history.push('/');
              }}
            />
          </div>
          <div
            className="col-sm-12 col-md-6 rightSection"
            style={{ direction: 'ltr' }}
          >
            <div className={s.receiveApp}>
              <h4 className={s.content}>
                برای دریافت لینک دانلود اپلیکیشن شماره موبایل خود را وارد نمایید
              </h4>
            </div>
            <div className={s.phoneInput}>
              <CPIntlPhoneInput
                placeholder="09121234567"
                value={phoneNumber}
                onChange={this.changeNumber}
              />
              <CPButton
                className="phoneBtn greenBtn"
                onClick={this.sendToPhoneNumber}
              >
                دریافت لینک از طریق پیامک
              </CPButton>
            </div>

            <div className={cs(s.buttons, 'row')}>
              {/* <CPButton
                className="mailBtn downloadBtn cafeBazar"
                onClick={this.downloadCafeBazar}
              >
                دانلود از کافه بازار <label className={s.soon}> بزودی </label>
                <img src={downloadBtn1} alt="" />
              </CPButton> */}
              <CPButton
                className="mailBtn downloadBtn cafeBazar"
                onClick={this.downloadCafeBazar}
              >
                دانلود از کافه بازار
                <img src={downloadBtn1} alt="" />
              </CPButton>
              <CPButton
                className="mailBtn downloadBtn sibApp"
                onClick={this.downloadSibApp}
              >
                IOS دانلود مستقیم 
                <img src={downloadBtn2} alt="" />
              </CPButton>
            </div>
          </div>
        </div>
        <div className={s.appDownload}>
          <div className={s.wrapper}>
            <div>
              <div className={s.working}>نحوه کار با اپلیکیشن MotherFlower</div>
              <div className="row">
                <div className="col-xs-12 tabletView">
                  <img
                    src={`${baseCDN}/splash/1.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-2 webView">
                  <img
                    src={`${baseCDN}/splash/1.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-9 description">
                  <h4>1. انتخاب نزدیکترین گلفروشی</h4>
                  <div>
                    کاربر پس از ورود به اپلیکیشن می تواند محله مورد نظر خود را
                    به صورت دستی تایپ کند تا به بتواند به پروفایل نزدیک ترین
                    گلفروشی دسترسی داشته باشد . برای مثال : اگر شما در محله
                    "کامرانیه" در تهران زندگی می کنید و علاقه دارید پروفایل
                    گلفروشی های محله خود را ببینید می توانید با وارد نمودن محله
                    " کامرانیه " در فیلد انتخاب محله ، به تمام گلفروش های منطقه
                    کامرانیه دسترسی کامل داشته باشید . همچنین اپلیکیشن دارای
                    تکنولوژی "موقعیت یابی خودکار" است و این امکان را فراهم می
                    کند موقعیت کاربر را مبتنی بر جی پی اس شناسایی کرده و
                    نزدیکترین محله را نمایش می دهد .
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 tabletView">
                  <img
                    src={`${baseCDN}/splash/2.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-9 description">
                  <h4>2. سفارش گل</h4>
                  <div>
                    در این مرحله تمام کالای یک گلفروشی نمایش داده می شود و کاربر
                    می تواند با انتخاب محصول مورد نظر خود وارد صفحه جزییات کالا
                    شود . در قسمت نمایش کل محصولات، امکان فیلتر پیشرفته در بالای
                    اپلیکیشن فراهم است و کاربر می تواند دسته بندی کالا ها مانند
                    دسته گل ، تاج گل ، باکس گل ، سبد گل و ... را به راحتی انتخاب
                    کند همچنین این امکان برای کاربر فراهم است که در سهولت کامل
                    رنگ و نوع گل مورد علاقه خود را فیلتر کند . در این صفحه امکان
                    بیشتری مانند تغییر گل فروشی ، تغییر محله ، نمایش کالا به
                    صورت پنجره ای و حتی رده بندی، آخرین کالا مبتنی بر پر فروش
                    ترین ، ارزان ترین، گران ترین فراهم شده تا کاربر مناسب ترین
                    محصول را انتخاب کند.
                  </div>
                </div>
                <div className="col-xs-12 col-lg-2 webView">
                  <img
                    src={`${baseCDN}/splash/2.png`}
                    className="imgSlideWeb"
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 tabletView">
                  <img
                    src={`${baseCDN}/splash/3.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-2 webView">
                  <img
                    src={`${baseCDN}/splash/3.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-9 description">
                  <h4>3. انتخاب نحوه تحویل</h4>
                  <div>
                    در این مرحله کاربر می تواند قیمت کالا را در دو حالت پی کاپ (
                    تحویل درب گلفروشی) یا به صورت دلیوری ( تحویل گل درب آدرس
                    تعیین شده ) ببیند ، درصورتی کالا دارای تخفیف ویژه در نوع
                    تحویل باشد در قسمت فوق به نمایش در می آید . در قسمت پایین
                    محصول ، کالای های مرتبط ، جزییات محصول و نظرات کاربران نمایش
                    داده می شود که مشتری بتواند کالای خود را انتخاب و خرید را
                    انجام دهد .
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 tabletView">
                  <img
                    src={`${baseCDN}/splash/4.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-9 description">
                  <h4>4. پرداخت آنلاین</h4>
                  <div>
                    مرحله پرداخت آنلاین ، آخرین مرحله ای است که کاربر می تواند
                    سفارش خود را به صورت کامل ثبت نماید . در این مرحله جزییات
                    سفارش مثل ارزش کالا ، هزینه حمل ، مجموع تخفیفات به نمایش در
                    می آید تا کاربر بتواند تمام جزییات سفارش را مشاهده نماید .
                    لازم به ذکر است در انتهای صفحه قیمت نهایی و انتخاب درگاه
                    پرداخت بانک سامان نمایش داده می شود و مشتری می تواند با کلیک
                    بر روی دکمه سبز رنگ به درگاه بانک سامان متصل و پرداخت خود را
                    انجام نماید .
                  </div>
                </div>
                <div className="col-xs-12 col-lg-2 webView">
                  <img
                    src={`${baseCDN}/splash/4.png`}
                    className="imgSlideWeb"
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-xs-12 tabletView">
                  <img
                    src={`${baseCDN}/splash/5.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-2 webView">
                  <img
                    src={`${baseCDN}/splash/5.png`}
                    className="imgSlideWeb"
                  />
                </div>
                <div className="col-xs-12 col-lg-9 description">
                  <h4>5. تحویل سفارش</h4>
                  <div>
                    سیستم پس از تحویل سفارش به مشتری ، پیامی شبیه پیام" مشتری
                    عزیز ، دسته گل شما در ساعت 16 تحویل همسرتان شد " به سفارش
                    دهنده به صورت خودکار ارسال می کند.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={s.footer}>
          <div className={cs(s.buttons, 'row')}>
            {/* <CPButton
              className="mailBtn downloadBtn cafeBazar"
              onClick={this.downloadCafeBazar}
            >
              دانلود از کافه بازار <label className={s.soon}> بزودی </label>
              <img src={downloadBtn1} alt="" />
            </CPButton> */}
            <CPButton
              className="mailBtn downloadBtn cafeBazar"
              onClick={this.downloadCafeBazar}
            >
              دانلود از کافه بازار
              <img src={downloadBtn1} alt="" />
            </CPButton>
            <CPButton
              className="mailBtn downloadBtn sibApp"
              onClick={this.downloadSibApp}
            >
              IOS دانلود مستقیم 
              <img src={downloadBtn2} alt="" />
            </CPButton>
          </div>
        </div>
      </div>
    ) : (
      <div>
        <div className={s.mobileHeader}>
          <img className={s.headerImage} />
          <div className={s.logo}>
            <img
              src={`${baseCDN}/logo.png`}
              onClick={() => {
                history.push('/');
              }}
            />
          </div>
          <div
            className="col-sm-12 col-md-6 rightSection"
            style={{ direction: 'ltr' }}
          >
            <div className={s.receiveApp}>
              <h4 className={s.content}>برای دریافت لینک دانلود اپلیکیشن</h4>
              <h4 className={s.content}>شماره موبایل خود را وارد نمایید</h4>
            </div>
            <div className={s.phoneInput}>
              <CPIntlPhoneInput placeholder="09121234567" value={phoneNumber} />
              <CPButton className="phoneBtn greenBtn">
                دریافت لینک از طریق پیامک
              </CPButton>
            </div>

            <div className={cs(s.buttons, 'row')}>
              {/* <CPButton
                className="mailBtn downloadBtn cafeBazar"
                onClick={this.downloadCafeBazar}
              >
                دانلود از کافه بازار <label className={s.soon}> بزودی </label>
                <img src={downloadBtn1} alt="" />
              </CPButton> */}
              <CPButton
                className="mailBtn downloadBtn cafeBazar"
                onClick={this.downloadCafeBazar}
              >
                دانلود از کافه بازار
                <img src={downloadBtn1} alt="" />
              </CPButton>
              <CPButton
                className="mailBtn downloadBtn sibApp"
                onClick={this.downloadSibApp}
              >
                IOS دانلود مستقیم 
                <img src={downloadBtn2} alt="" />
              </CPButton>
            </div>
          </div>
        </div>
        <div className={s.appDownload}>
          <div className={s.wrapper}>
            <div className={s.sliderMobile}>
              <div className={s.working}>نحوه کار با اپلیکیشن</div>
              <div className={s.working}>MotherFlower</div>
              <ul className={s.lineCircle}>
                <li
                  onClick={e =>
                    this.changeStep(e, 'انتخاب نزدیکترین گلفروشی', '1')
                  }
                  className="activeStep firstSlide"
                >
                  1
                </li>
                <li onClick={e => this.changeStep(e, 'سفارش گل', '2')}>2</li>
                <li onClick={e => this.changeStep(e, 'انتخاب نحوه تحویل', '3')}>
                  3
                </li>
                <li onClick={e => this.changeStep(e, 'پرداخت آنلاین', '4')}>
                  4
                </li>
                <li onClick={e => this.changeStep(e, 'تحویل سفارش', '5')}>5</li>
              </ul>
              <div className={s.titleStep}>{titleSlider}</div>
              <img src={imgSplash} className="imgSlide" />
              <div className={s.description}>
                {/* لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله
                در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد
                نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان
                جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را
                برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در
                زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و
                دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد. */}
              </div>
            </div>
          </div>
        </div>
        <div className={s.footer}>
          <div className={cs(s.buttons, 'row')}>
            {/* <CPButton
              className="mailBtn downloadBtn cafeBazar"
              onClick={this.downloadCafeBazar}
            >
              دانلود از کافه بازار <label className={s.soon}> بزودی </label>
              <img src={downloadBtn1} alt="" />
            </CPButton> */}
            <CPButton
              className="mailBtn downloadBtn cafeBazar"
              onClick={this.downloadCafeBazar}
            >
              دانلود از کافه بازار
              <img src={downloadBtn1} alt="" />
            </CPButton>
            <CPButton
              className="mailBtn downloadBtn sibApp"
              onClick={this.downloadSibApp}
            >
              IOS دانلود مستقیم 
              <img src={downloadBtn2} alt="" />
            </CPButton>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(AppDownload);
