import React from 'react';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import Download from './Download';

const title = 'دانلود اپلیکیشن';

function action() {
  return {
    chunks: ['download'],
    title,
    component: <Download title={title} />,
  };
}

export default action;
