import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cs from 'classnames';
import s from './Card.css';
import CPButton from '../../components/CP/CPButton';
import {
  RELATED_PRODUCTS,
  FORWARD_TO_SHOP,
  CONTINUE_SUBMIT_ORDER,
  TOTAL_PRICE_TO_PAY,
  READY_TO_DELIVER,
  FROM_TIME,
  DISCOUNT,
  VALUE_OF_ORDER,
  TOMAN,
  THIS_PRODUCT_IN_DATE,
  PRODUCT,
  TOTAL_PRICE,
  UNITE_PRICE,
  IN_SHOPPING,
  DELETE,
  DISCOUNT_SAVE,
  DELIVERY_TYPE,
  COUNT,
  SEND_TO_YOU,
  ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT,
  LOGIN_TO_ACCOUNT,
  REGISTER,
  MOST_VIEWED,
  IN_PLACE,
  IN_TIME,
} from '../../Resources/Localization';
import CPAvatar from '../../components/CP/CPAvatar';
import ProductCard from '../../components/Shared/ProductCard';
import CPCard from '../../components/CP/CPCard';
import {
  basketListFailure,
  basketListRequest,
  basketListSuccess,
} from '../../redux/catalog/action/basket';

import { getDtoQueryString, showNotification } from '../../utils/helper';
import {
  DeleteUserBasketHistoriesApi,
  GetUserBasketHistoriesApi,
  PutUserBasketHistoriesApi,
} from '../../services/samApi';
import { getCookie } from '../../utils';
import { BasketDto } from '../../dtos/catalogDtos';
import Link from '../../components/Link';
import CPConfirmation from '../../components/CP/CPConfirmation';
import CPCarouselSlider from '../../components/CP/CPCarouselSlider';
import history from '../../history';
import { BaseCRUDDtoBuilder, BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { baseCDN } from '../../setting';

class Card extends React.Component {
  static propTypes = {
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
    basketProducts: PropTypes.objectOf(PropTypes.any),
    loginData: PropTypes.objectOf(PropTypes.any),
    mostVisitedProducts: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    basketProducts: {},
    loginData: {},
    mostVisitedProducts: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      webView: false,
      visible: false,
      productBasketId: 0,
    };
    this.changeCount = this.changeCount.bind(this);
    this.deleteOnBasket = this.deleteOnBasket.bind(this);
  }

  componentDidMount() {
    const { basketProducts } = this.props;
    this.props.actions.hideLoading();
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
    if (basketProducts.hasExpiredProduct) {
      showNotification(
        'error',
        'اخطار',
        'لطفا محصولی که زمان ارسال یا تحویل آن به اتمام رسیده است را از سبد خرید خود حذف کنید.',
        7,
      );
    }
    setTimeout(() => {
      if (basketProducts.multiVendor) {
        showNotification(
          'error',
          'اخطار',
          'محصولات سبد خرید شما باید از یک فروشگاه سفارش داده شوند.',
          7,
        );
      }
    }, 500);
  }

  async changeCount(id, count, type) {
    let token = getCookie('siteToken');
    const guestKey = getCookie('guestKey');
    this.props.actions.basketListRequest();

    const basketCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BasketDto({
          guestKey: !token ? guestKey : undefined,
          id,
          count: type === 'increment' ? count + 1 : count - 1,
        }),
      )
      .build();

    const responsePut = await PutUserBasketHistoriesApi(basketCrud, token);

    if (responsePut.status === 200 || responsePut.status === 401) {
      token = getCookie('siteToken');

      const response = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(new BasketDto({ guestKey: !token ? guestKey : undefined }))
            .buildJson(),
        ),
        token,
      );

      if (response.status === 200) {
        this.props.actions.basketListSuccess(response.data);
      } else {
        this.props.actions.basketListFailure();
      }
    } else {
      this.props.actions.basketListFailure();
    }
  }
  updatePredicate = () => {
    this.setState({ webView: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ webView: window.innerWidth >= 768 });
    });
  };

  async deleteOnBasket(id) {
    const token = getCookie('siteToken');
    const guestKey = getCookie('guestKey');

    const response = await DeleteUserBasketHistoriesApi(id);
    if (response.status === 200) {
      const responseGet = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(new BasketDto({ guestKey: !token ? guestKey : undefined }))
            .buildJson(),
        ),
        token,
      );
      if (responseGet.status === 200) {
        this.props.actions.basketListSuccess(responseGet.data);
      }
    }
  }

  showModal = id => {
    this.setState({
      visible: true,
      productBasketId: id,
    });
  };

  handleOk = () => {
    this.deleteOnBasket(this.state.productBasketId);

    this.setState({
      visible: false,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  goToCheckout = e => {
    const { basketProducts } = this.props;
    e.preventDefault();
    if (basketProducts.hasExpiredProduct) {
      showNotification(
        'error',
        'اخطار',
        'لطفا محصولی که زمان ارسال یا تحویل آن به اتمام رسیده است را از سبد خرید خود حذف کنید.',
      );
    }
    setTimeout(() => {
      if (basketProducts.multiVendor) {
        showNotification(
          'error',
          'اخطار',
          'محصولات سبد خرید شما باید از یک فروشگاه سفارش داده شوند.',
          10,
        );
      }

      if (!basketProducts.hasExpiredProduct && !basketProducts.multiVendor) {
        this.props.actions.showLoading();
        history.push('/checkout');
      }
    }, 500);
  };

  goToUrl = (e, url) => {
    e.preventDefault();
    this.props.actions.showLoading();
    history.push(url);
  };

  goToLogin = () => {
    history.push('/login?from=Card');
  };

  gotToRegister = () => {
    history.push('/register?from=Card');
  };

  render() {
    const { webView } = this.state;
    const { basketProducts, mostVisitedProducts, loginData } = this.props;
    const relatedProductArray = [];

    if (basketProducts && basketProducts.relatedProductDtos) {
      basketProducts.relatedProductDtos.map(item => {
        const priceData = item.vendorBranchProductPriceDtos[0];
        const images = [];
        let price = '';
        let discount = '';
        let percent = '';

        item.imageDtos.map(image => {
          if (image.isCover) images.push(`${image.url}`);
          return null;
        });

        if (
          priceData.deliveryPriceAfterDiscount <
          priceData.pickupPriceAfterDiscount
        ) {
          price = priceData.stringDeliveryBasePrice;
          discount = priceData.stringDeliveryPriceAfterDiscount;
          percent = priceData.deliveryPriceDiscount;
        }

        if (
          priceData.pickupPriceAfterDiscount <=
          priceData.deliveryPriceAfterDiscount
        ) {
          price = priceData.stringPickupBasePrice;
          discount = priceData.stringPickupPriceAfterDiscount;
          percent = priceData.pickupPriceDiscount;
        }

        const obj = {
          url: `/product-details/${
            item.id
          }/${item.vendorBranchProductPriceDtos &&
            item.vendorBranchProductPriceDtos[0].vendorBranchDto.id}`,
          imageUrl1: images[0],
          imageUrl2: images.length > 1 ? images[1] : images[0],
          ProductName: item.name,
          value: item.rateAverage,
          price,
          discount,
          percent: `${percent}`,
          key: item.id,
        };

        relatedProductArray.push(obj);
        return null;
      });
    }
    return basketProducts &&
      basketProducts.userBasketHistoryDetailDtos &&
      basketProducts.userBasketHistoryDetailDtos.length > 0 ? (
      <div className={s.checkOutPage}>
        {webView ? (
          <div className={s.orders}>
            <div className={s.productDetail}>
              <ul className={s.productHeader}>
                <li className="col-md-3">
                  <b>{PRODUCT}</b>
                </li>
                <li className="col-md-2">
                  <b>{COUNT}</b>
                </li>
                <li className="col-md-2">
                  <b>{UNITE_PRICE}</b>
                </li>
                <li className="col-md-2">
                  <b>{DISCOUNT}</b>
                </li>
                <li className="col-md-2">
                  <b>{TOTAL_PRICE}</b>
                </li>
                <li className="col-md-1">
                  <b>{DELETE}</b>
                </li>
              </ul>
              {basketProducts &&
                basketProducts.userBasketHistoryDetailDtos &&
                basketProducts.userBasketHistoryDetailDtos.map(item => (
                  <div
                    key={item.id}
                    className={
                      item.priceOption === 'Pickup'
                        ? s.pickupList
                        : s.deliveryList
                    }
                  >
                    {item.expired && (
                      <span className={s.expired}>
                        <p>زمان دریافت به اتمام رسید</p>
                      </span>
                    )}
                    <ul className={s.productList}>
                      <li className="col-md-3">
                        <Link
                          to={`/product-details/${
                            item.vendorBranchProductDto.productDto.id
                          }/${item.vendorBranchProductDto.vendorBranchDto.id}`}
                          target="_blank"
                        >
                          <CPAvatar
                            src={
                              item.vendorBranchProductDto.productDto.imageDtos
                                ? item.vendorBranchProductDto.productDto
                                    .imageDtos[0].url
                                : ''
                            }
                          />
                        </Link>
                        <p className={s.productName}>
                          {item.vendorBranchProductDto.productDto.name}
                        </p>
                        <p>
                          {item.vendorBranchProductDto.vendorBranchDto.name}
                        </p>
                        <p className={s.type}>
                          {item.priceOption === 'Pickup'
                            ? 'تحویل درب گل فروشی (Pick up)'
                            : 'تحویل در محل (Delivery)'}
                        </p>
                      </li>
                      <li className="col-md-2">
                        <div className={s.counter}>
                          <CPButton
                            type="circle"
                            className={s.increment}
                            onClick={() =>
                              this.changeCount(item.id, item.count, 'increment')
                            }
                          >
                            <i className="mf-add" />
                          </CPButton>
                          {item.count}
                          {/* <input className={s.count} value={this.state.count} /> */}
                          <CPButton
                            type="circle"
                            className={s.decrement}
                            disabled={item.count === 1}
                            onClick={() =>
                              this.changeCount(item.id, item.count, 'decrement')
                            }
                          >
                            <i className="mf-minus" />
                          </CPButton>
                        </div>
                      </li>
                      <li className="col-md-2">
                        <span className={s.unitePrice}>
                          <b>
                            {item.priceOption === 'Pickup'
                              ? item.vendorBranchProductDto.productDto
                                  .vendorBranchProductPriceDtos[0]
                                  .stringPickupBasePrice
                              : item.vendorBranchProductDto.productDto
                                  .vendorBranchProductPriceDtos[0]
                                  .stringDeliveryBasePrice}
                          </b>
                        </span>
                      </li>
                      <li className="col-md-2">
                        <span className={s.save}>
                          <b>{item.stringTotalDiscount}</b>
                        </span>
                      </li>
                      <li className="col-md-2">
                        <b>{item.stringPayableAmount}</b>
                        <small>({TOMAN})</small>
                      </li>
                      <li className="col-md-1">
                        <CPButton
                          className={cs(s.deleteAction)}
                          type="circle"
                          onClick={() => this.showModal(item.id)}
                        >
                          <i className="mf-close" />
                        </CPButton>
                      </li>
                    </ul>
                    <div className={s.description}>
                      <img
                        className={s.giftImg}
                        src={
                          item.priceOption === 'Pickup'
                            ? `${baseCDN}/present1.png`
                            : `${baseCDN}/gps.png`
                        }
                        alt=""
                      />
                      <b>{THIS_PRODUCT_IN_DATE} </b>
                      <b className={s.date}> {item.requestedDate} </b>
                      <b>
                        {item.priceOption === 'Pickup' ? FROM_TIME : IN_TIME}
                      </b>
                      <b className={s.time}> {item.requestedHour} </b>
                      <b>{item.priceOption === 'Pickup' ? IN_SHOPPING : ''}</b>
                      <b>
                        {item.priceOption === 'Pickup'
                          ? item.vendorBranchProductDto.vendorBranchDto.name
                          : ''}{' '}
                      </b>
                      <b>
                        {item.priceOption === 'Pickup'
                          ? READY_TO_DELIVER
                          : SEND_TO_YOU}
                      </b>
                    </div>
                  </div>
                ))}
              <div className={s.totalPay}>
                <div className={s.disValue}>
                  <span className={s.price}>
                    <b>{VALUE_OF_ORDER} :</b>
                    <b>
                      {basketProducts ? basketProducts.stringTotalAmount : 0}
                    </b>
                  </span>
                  <span className={s.price}>
                    <b>{DISCOUNT} :</b>
                    <b>
                      {basketProducts ? basketProducts.stringTotalDiscount : 0}
                    </b>
                  </span>
                </div>
                <div className={s.final}>
                  <span className={s.price}>
                    <b>{TOTAL_PRICE_TO_PAY} :</b>
                    <b className={s.value}>
                      {basketProducts ? basketProducts.stringPayableAmount : 0}{' '}
                      <b className={s.toman}>({TOMAN})</b>
                    </b>
                  </span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div>
            {basketProducts &&
              basketProducts.userBasketHistoryDetailDtos &&
              basketProducts.userBasketHistoryDetailDtos.map(item => (
                <CPCard
                  key={item.id}
                  className={
                    item.priceOption === 'Pickup'
                      ? s.mobileCardPickUp
                      : s.mobileCardDelivery
                  }
                >
                  {item.expired && (
                    <span className={s.expired}>
                      <p>زمان ارسال / تحویل به اتمام رسید</p>
                    </span>
                  )}
                  <div className={s.details}>
                    <CPButton
                      className={s.deleteAction}
                      type="circle"
                      onClick={() => this.showModal(item.id)}
                    >
                      <i className="mf-close" />
                    </CPButton>
                    <CPAvatar
                      src={
                        item.vendorBranchProductDto.productDto.imageDtos
                          ? item.vendorBranchProductDto.productDto.imageDtos[0]
                              .url
                          : ''
                      }
                    />
                    <span className={s.proDetail}>
                      <p className={s.productName}>
                        {item.vendorBranchProductDto.productDto.name}
                      </p>
                      <p>{item.vendorBranchProductDto.vendorBranchDto.name}</p>
                      {/* {item.priceOption === 'Pickup'
                      ? 'تحویل درب گل فروشی (Pick up)'
                      : 'تحویل در محل (Delivery)'} */}
                    </span>
                  </div>
                  <div className={s.mobileCounter}>
                    <b>تعداد : </b>
                    <div className={s.counterBtns}>
                      <CPButton
                        type="circle"
                        className={s.increment}
                        onClick={() =>
                          this.changeCount(item.id, item.count, 'increment')
                        }
                      >
                        <i className="mf-add" />
                      </CPButton>
                      {/* <input className={s.count} value={this.state.count} /> */}
                      {item.count}
                      <CPButton
                        type="circle"
                        className={s.decrement}
                        disabled={item.count === 1}
                        onClick={() =>
                          this.changeCount(item.id, item.count, 'decrement')
                        }
                      >
                        <i className="mf-minus" />
                      </CPButton>
                    </div>
                  </div>
                  <div className={s.unitePrice}>
                    <b>{UNITE_PRICE} : </b>
                    <span>
                      <b>
                        {item.priceOption === 'Pickup'
                          ? item.vendorBranchProductDto.productDto
                              .vendorBranchProductPriceDtos[0]
                              .stringPickupBasePrice
                          : item.vendorBranchProductDto.productDto
                              .vendorBranchProductPriceDtos[0]
                              .stringDeliveryBasePrice}{' '}
                        <b className={s.toman}>{TOMAN}</b>
                      </b>
                      {/* <div>
                      <del>{item.unitPrice}</del>
                      <b className={s.label}>{TOMAN}</b>
                    </div> */}
                    </span>
                  </div>
                  {/* <div className={s.unitePrice}>
                  <b>{DISCOUNT} : </b>
                  <b>
                    {item.stringTotalDiscount} <b className={s.toman}>{TOMAN}</b>
                  </b>
                </div> */}
                  <div className={s.totalPrice}>
                    <b>{TOTAL_PRICE} : </b>
                    <b>
                      {item.stringPayableAmount}{' '}
                      <b className={s.toman}>{TOMAN}</b>
                    </b>
                  </div>
                  {item.totalDiscount > 0 && (
                    <div className={s.mobileDiscount}>
                      <b>{DISCOUNT_SAVE} : </b>
                      <b>{item.stringTotalDiscount}</b>
                      <b className={s.toman}> {TOMAN} </b>
                    </div>
                  )}
                  <div className={s.description}>
                    <p className={s.type}>
                      <b>{DELIVERY_TYPE} : </b>
                      {item.priceOption === 'Pickup'
                        ? 'تحویل درب گل فروشی (Pick up)'
                        : 'تحویل در محل (Delivery)'}
                    </p>
                    <b>{THIS_PRODUCT_IN_DATE} </b>
                    <b className={s.date}> {item.requestedDate} </b>
                    <b>
                      {item.priceOption === 'Pickup' ? FROM_TIME : IN_TIME}{' '}
                    </b>
                    <b className={s.time}> {item.requestedHour} </b>
                    <b>
                      {item.priceOption === 'Pickup' ? IN_SHOPPING : IN_PLACE}
                    </b>
                    <b> {item.vendorBranchProductDto.vendorBranchDto.name} </b>
                    <b>
                      {' '}
                      {item.priceOption === 'Pickup'
                        ? READY_TO_DELIVER
                        : SEND_TO_YOU}{' '}
                    </b>
                  </div>
                </CPCard>
              ))}
            <div className={s.totalPay}>
              <div className={s.disValue}>
                <span className={s.price}>
                  <b>{VALUE_OF_ORDER} :</b>
                  <b>{basketProducts ? basketProducts.stringTotalAmount : 0}</b>
                </span>
                <span className={s.price}>
                  <b>{DISCOUNT} :</b>
                  <b>
                    {basketProducts ? basketProducts.stringTotalDiscount : 0}
                  </b>
                </span>
              </div>
              <div className={s.final}>
                <span className={s.price}>
                  <b>{TOTAL_PRICE_TO_PAY} :</b>
                  <b className={s.value}>
                    {basketProducts ? basketProducts.stringPayableAmount : 0}{' '}
                    <b className={s.toman}>({TOMAN})</b>
                  </b>
                </span>
              </div>
            </div>
          </div>
        )}
        <CPButton className={s.return} onClick={e => this.goToUrl(e, '/')}>
          {FORWARD_TO_SHOP}
          {/* <Link to="/" onClick={e => this.goToUrl(e, '/')}>{FORWARD_TO_SHOP}</Link> */}
        </CPButton>
        <CPButton className={s.continue} onClick={e => this.goToCheckout(e)}>
          {CONTINUE_SUBMIT_ORDER}
          {/* <Link onClick={e => this.goToCheckout(e)} to="/checkout">
            {CONTINUE_SUBMIT_ORDER}
          </Link> */}
        </CPButton>

        <CPConfirmation
          visible={this.state.visible}
          handleOk={this.handleOk}
          handleCancel={this.handleCancel}
          content={ARE_YOU_SURE_WANT_TO_DELETE_THE_PRODUCT}
        />

        <div className="col-sm-12 text-center">
          <h3 className={s.relatedPro}>{RELATED_PRODUCTS}</h3>
        </div>
        {relatedProductArray.length > 4 ? (
          <CPCarouselSlider
            arrows
            slidesToShow={6}
            slidesToScroll={1}
            infinite={false}
            responsive={[
              {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
            ]}
          >
            {relatedProductArray.map(item => (
              <div className="col-md-12">
                <ProductCard
                  url={item.url}
                  imageUrl1={item.imageUrl1}
                  imageUrl2={item.imageUrl2}
                  ProductName={item.ProductName}
                  value={item.value}
                  price={item.price}
                  discount={item.discount}
                  percent={item.percent}
                />
              </div>
            ))}
          </CPCarouselSlider>
        ) : (
          <div className={s.relatedProRow}>
            {relatedProductArray.map(item => (
              <div key={item.key} className="col-lg-3 col-sm-6">
                <ProductCard
                  url={item.url}
                  imageUrl1={item.imageUrl1}
                  imageUrl2={item.imageUrl2}
                  ProductName={item.ProductName}
                  value={item.value}
                  price={item.price}
                  discount={item.discount}
                  percent={item.percent}
                />
              </div>
            ))}
          </div>
        )}
      </div>
    ) : (
      <div className={s.checkOutPage}>
        <div className={s.emptyBasket}>
          <i className="mf-addToCard">
            <i className="mf-close-o" />
          </i>
          <div>سبد خرید شما خالی است !</div>
        </div>
        {!loginData.token && (
          <div>
            <div className={s.divLogin}>
              <CPButton
                onClick={this.goToLogin}
                className={cs('loginBtn greenBtn', s.btnLogin)}
              >
                {LOGIN_TO_ACCOUNT}
              </CPButton>
            </div>
            <div className={s.divRegister}>
              <CPButton className={s.btnRegister} onClick={this.gotToRegister}>
                {REGISTER}
              </CPButton>
            </div>
          </div>
        )}
        <div className={s.title}>
          <label>{MOST_VIEWED}</label>
        </div>
        <div className={cs(s.products, 'row')}>
          {mostVisitedProducts.map(item => (
            <div key={item.id} className="col-md-3 col-sm-12">
              <ProductCard
                key={item.id}
                url={`/product-details/${item.productId}/${
                  item.vendorBranchId
                }`}
                imageUrl1={item.imageUrls.length > 0 ? item.imageUrls[0] : ''}
                imageUrl2={item.imageUrls.length > 0 ? item.imageUrls[1] : ''}
                ProductName={item.name}
                value={item.rateAverage}
                price={
                  item.deliveryPriceAfterDiscount >
                  item.pickupPriceAfterDiscount
                    ? item.pickupBasePrice
                    : item.deliveryBasePrice
                }
                discount={
                  item.deliveryPriceAfterDiscount >
                  item.pickupPriceAfterDiscount
                    ? item.pickupPriceAfterDiscount
                    : item.deliveryPriceAfterDiscount
                }
                percent={
                  item.deliveryPriceAfterDiscount >
                  item.pickupPriceAfterDiscount
                    ? item.pickupPriceDiscount.toString()
                    : item.deliveryPriceDiscount.toString()
                }
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  basketProducts:
    state.basket.basketListData && state.basket.basketListData.items
      ? state.basket.basketListData.items[0]
      : null,
  mostVisitedProducts: state.catalogProduct.mostVisitedProductData
    ? state.catalogProduct.mostVisitedProductData.items
    : null,
  loginData: state.account.loginData ? state.account.loginData : {},
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      basketListSuccess,
      basketListFailure,
      basketListRequest,
      showLoading,
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Card));
