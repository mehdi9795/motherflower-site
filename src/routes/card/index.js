import React from 'react';
import Card from './Card';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import { getCookie } from '../../utils';
import { getDtoQueryString } from '../../utils/helper';
import { GetUserBasketHistoriesApi } from '../../services/samApi';
import { basketListSuccess } from '../../redux/catalog/action/basket';
import { BasketDto, ProductSearchDto } from '../../dtos/catalogDtos';
import { getProductSearchApi } from '../../services/catalogApi';
import {
  mostVisitedProductFailure,
  mostVisitedProductSuccess,
} from '../../redux/catalog/action/product';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';

async function action({ store, params }) {
  const token = getCookie('siteToken');
  if (token !== '' && token !== null) {
    const response = await GetUserBasketHistoriesApi(
      getDtoQueryString(
        new BaseGetDtoBuilder().dto(new BasketDto()).buildJson(),
      ),
      token,
    );
    if (response.status === 200) {
      store.dispatch(basketListSuccess(response.data));
    } else if (response.status === 401) {
      const guestKey = getCookie('guestKey');

      if (guestKey) {
        const response2 = await GetUserBasketHistoriesApi(
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(new BasketDto({ guestKey }))
              .buildJson(),
          ),
          token,
        );

        if (response2.status === 200) {
          store.dispatch(basketListSuccess(response2.data));
        }
      }
    }
  } else {
    const guestKey = getCookie('guestKey');

    if (guestKey) {
      const response = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder().dto(new BasketDto({ guestKey })).buildJson(),
        ),
        token,
      );

      if (response.status === 200) {
        store.dispatch(basketListSuccess(response.data));
      }
    }
  }

  const productList = new BaseGetDtoBuilder()
    .dto(
      new ProductSearchDto({
        categoryIds: 0,
        vendorBranchId: -578,
        toPrice: '3000000',
        fromPrice: '2000',
        priceOption: '1',
        sortOption: 2,
      }),
    )
    .pageSize(4)
    .pageIndex(0)
    .disabledCount(true)
    .fromCache(true)
    .buildJson();

  const mostVisitedProduct = await getProductSearchApi(
    getDtoQueryString(productList),
  );

  if (mostVisitedProduct.status === 200) {
    store.dispatch(mostVisitedProductSuccess(mostVisitedProduct.data));
  } else store.dispatch(mostVisitedProductFailure());

  return {
    chunks: ['card'],
    title: 'Card',
    component: (
      <Layout>
        <Card params={params} />
      </Layout>
    ),
  };
}

export default action;
