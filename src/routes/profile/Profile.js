import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Profile.css';
import Sidebar from '../../components/profile/Sidebar';
import CPButton from '../../components/CP/CPButton';
import BankAccount from '../../components/profile/BankAccount';
import EditInfo from '../../components/profile/EditInfo';
import Reminding from '../../components/profile/Reminding';
import AddressList from '../../components/profile/AddressList';
import Orders2 from '../../components/profile/Orders2';
import ChangePassword from '../../components/profile/ChangePassword';
import CPImageEditor from '../../components/CP/CPImageEditor';
import Event from '../../components/profile/Event';
import Wallet from '../../components/profile/Wallet';

class Profile extends React.Component {
  static propTypes = {
    onClick: PropTypes.func,
    selectedMenu: PropTypes.string,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    onClick: () => {},
    selectedMenu: 'editInfo',
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      menuSelected: props.selectedMenu ? props.selectedMenu : 'wallet',
    };
  }

  componentDidMount() {
    this.props.actions.hideLoading();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedMenu !== nextProps.selectedMenu) {
      this.setState({ menuSelected: nextProps.selectedMenu });
    }
  }

  onOpenChange = () => {
    this.setState({ open: !this.state.open });
  };

  onClick = () => {
    this.props.onClick();
  };

  changeMenu = value => {
    this.setState({ menuSelected: value });
  };
  render() {
    const { menuSelected } = this.state;

    return (
      <div className={s.profilePage}>
        <div className={s.sideBar}>
          <Sidebar />
        </div>
        <div className="contentProfile">
          <CPButton
            type="circle"
            className={s.sideBarBtn}
            onClick={this.onClick}
          >
            <i className="mf-menu" />
          </CPButton>
          {menuSelected === 'password' && <ChangePassword />}
          {menuSelected === 'address' && <AddressList />}
          {menuSelected === 'reminding' && <Reminding />}
          {menuSelected === 'editInfo' && <EditInfo />}
          {menuSelected === 'order' && <Orders2 />}
          {menuSelected === 'accountNumber' && <BankAccount />}
          {menuSelected === 'event' && <Event />}
          {menuSelected === 'wallet' && <Wallet />}
          {/* <Orders /> */}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  selectedMenu:
    state.sharedChangeMenu.value === ''
      ? undefined
      : state.sharedChangeMenu.value,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Profile));
