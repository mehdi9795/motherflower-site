import React from 'react';
import Profile from './Profile';
import { BillDto } from '../../dtos/samDtos';
import { getCookie } from '../../utils';
import { getDtoQueryString } from '../../utils/helper';
import { BillApi, GetBankApi, GetWalletsApi } from '../../services/samApi';
import {
  inProgressOrderListSuccess,
  inProgressOrderListFailure,
} from '../../redux/sam/action/order';
import ProfileLayout from '../../components/Template/DefaultLayout/ProfileLayout';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { getCityApi } from '../../services/commonApi';
import {
  cityListFailure,
  cityListSuccess,
} from '../../redux/common/action/city';
import {
  walletListFailure,
  walletListSuccess,
} from '../../redux/sam/action/wallet';
import { changeMenuProfileSuccess } from '../../redux/shared/action/changeMenuProfile';
import { GetUser } from '../../services/identityApi';
import { UserDto } from '../../dtos/identityDtos';
import {
  singleUserFailure,
  singleUserSuccess,
} from '../../redux/identity/action/user';
import { BasketDto } from '../../dtos/catalogDtos';
import { bankListSuccess } from '../../redux/sam/action/bank';

async function action({ store, query }) {
  const token = getCookie('siteToken');
  let isAuth = true;

  store.dispatch(changeMenuProfileSuccess(query.selectTab));

  const response = await GetBankApi(
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(new BasketDto({ published: true }))
        .buildJson(),
    ),
  );

  if (response.status === 200) {
    store.dispatch(bankListSuccess(response.data));
  }

  const responseInProgressOrder = await BillApi(
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new BillDto({
            searchBillItemStatus: [
              'None',
              'Confirmed',
              'Prepared',
              'ReadyDeliveryToDriver',
              'DeliveryToDriver',
            ],
          }),
        )
        .includes(['addressDto', 'billItemDtos', 'vendorBranchDto'])
        .pageSize(4)
        .pageIndex(0)
        .buildJson(),
    ),
    token,
  );

  const cities = await getCityApi(
    getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
  );
  if (responseInProgressOrder.status === 200) {
    store.dispatch(inProgressOrderListSuccess(responseInProgressOrder.data));
    if (cities.status === 200) store.dispatch(cityListSuccess(cities.data));
    else store.dispatch(cityListFailure());
  } else if (responseInProgressOrder.status === 401) {
    isAuth = false;
  } else store.dispatch(inProgressOrderListFailure());

  const responseUser = await GetUser(
    token,
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new UserDto({
            fromMobile: true,
          }),
        )
        .includes(['imageDto'])
        .buildJson(),
    ),
    true,
  );

  if (responseUser.status === 200)
    store.dispatch(singleUserSuccess(responseUser.data));
  else store.dispatch(singleUserFailure());

  this.onClick = () => {
    this.child.onOpenChange();
  };

  const responseWalletUser = await GetWalletsApi(token, { dto: {} });
  if (responseWalletUser.status === 200)
    store.dispatch(walletListSuccess(responseWalletUser.data));
  else store.dispatch(walletListFailure());
  return {
    chunks: ['profile'],
    component: (
      <ProfileLayout
        onRef={ref => {
          this.child = ref;
          return false;
        }}
      >
        <Profile onClick={this.onClick} />
      </ProfileLayout>
    ),
  };
}

export default action;
