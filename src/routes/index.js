/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable global-require */

// The top-level (parent) route
const routes = {
  path: '',

  // Keep in mind, routes are evaluated in order
  children: [
    // The home route is added to client.js to make sure shared components are
    // added to client.js as well and not repeated in individual each route chunk.
    {
      path: '/',
      load: () => import(/* webpackMode: 'eager' */ './home'),
    },
    {
      path: '/city/:city',
      load: () => import(/* webpackMode: 'eager' */ './home'),
    },
    {
      path: '/search/:district/:vendorBranch',
      load: () => import(/* webpackChunkName: 'search' */ './search'),
    },
    {
      path: '/card',
      load: () => import(/* webpackChunkName: 'card' */ './card'),
    },
    {
      path: '/profile',
      load: () => import(/* webpackChunkName: 'profile' */ './profile'),
    },
    {
      path: '/app',
      load: () => import(/* webpackChunkName: 'download' */ './download'),
    },
    {
      path: '/product-details/:id/:vendorBranchId',
      load: () =>
        import(/* webpackChunkName: 'product-details' */ './product-details'),
    },
    {
      path: '/contact',
      load: () => import(/* webpackChunkName: 'contact' */ './contact'),
    },
    {
      path: '/login',
      load: () => import(/* webpackChunkName: 'login' */ './login'),
    },
    {
      path: '/register/:reagentUser?',
      load: () => import(/* webpackChunkName: 'register' */ './register'),
    },
    {
      path: '/checkout',
      load: () => import(/* webpackChunkName: 'checkout' */ './checkout'),
    },
    {
      path: '/about',
      load: () => import(/* webpackChunkName: 'about' */ './about'),
    },
    {
      path: '/returnBank/:status/:TrackingId/:fromApp/:os/:fromWallet',
      load: () => import(/* webpackChunkName: 'returnBank' */ './returnBank'),
    },
    {
      path: '/privacy',
      load: () => import(/* webpackChunkName: 'privacy' */ './privacy'),
    },
    {
      path: '/admin',
      load: () => import(/* webpackChunkName: 'admin' */ './admin'),
    },
    {
      path: '/rules',
      load: () => import(/* webpackChunkName: 'rules' */ './rules'),
    },
    {
      path: '/cooperation',
      load: () => import(/* webpackChunkName: 'cooperation' */ './cooperation'),
    },
    {
      path: '/vendorAffiliate',
      load: () => import(/* webpackChunkName: 'shop-introduction' */ './vendorAffiliate'),
    },
    {
      path: '/faq',
      load: () => import(/* webpackChunkName: 'faq' */ './faq'),
    },
    {
      path: '/serverError',
      load: () => import(/* webpackChunkName: 'serverError' */ './serverError'),
    },
    {
      path: '/IOS',
      load: () => import(/* webpackChunkName: 'IOS' */ './downloadIOS'),
    },
    // Wildcard routes, e.g. { path: '(.*)', ... } (must go last)
    {
      path: '(.*)',
      load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
    },
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();

    // Provide default values for title, description etc.
    route.title = `${route.title || 'MotherFlower'}`;
    route.description = route.description || '';

    return route;
  },
};

// The error page is available by permanent url for development mode
if (__DEV__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  });
}

export default routes;
