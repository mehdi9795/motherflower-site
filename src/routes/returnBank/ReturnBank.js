import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ReturnBank.css';
import CPButton from '../../components/CP/CPButton';
import history from '../../history';

class returnBank extends React.Component {
  static propTypes = {
    params: PropTypes.objectOf(PropTypes.any),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    params: {},
  };

  redirectToHome = () => {
    const { params } = this.props;
    this.props.actions.showLoading();

    if (params.fromWallet === 'False') history.push('/');
    else history.push('/profile');
  };

  getUrlIOS = () => {
    const { params } = this.props;
    let url = '';

    if (params.status === 'failure') {
      url = 'app.motherflower.tech://returnbank/?status=0';
      if (params.fromWallet === 'True')
        url = `app.motherflower.tech://returnbank/?status=2`;
    } else {
      url = 'app.motherflower.tech://returnbank/?status=1';
      if (params.fromWallet === 'True')
        url = `app.motherflower.tech://returnbank/?status=3`;
    }
    return url;
  };

  getUrlAndroid = () => {
    const { params } = this.props;
    let url = '';

    if (params.status === 'failure') {
      url = 'http://app.motherflower.tech/returnbank/?status=0';
      if (params.fromWallet === 'True')
        url = `http://app.motherflower.tech/returnbank/?status=2`;
    } else {
      url = 'http://app.motherflower.tech/returnbank/?status=1';
      if (params.fromWallet === 'True')
        url = `http://app.motherflower.tech/returnbank/?status=3`;
    }
    return url;
  };

  render() {
    const { params } = this.props;
    return (
      <div className={s.root}>
        {params.status === 'failure' && (
          <div className={s.checkoutMessage}>
            <h3 className={s.messageTitle}>
              <i className="mf-close-o" />خطا
              {/* <a href="app.motherflower.tech://returnbank/?status=1">Open Android App</a> */}
            </h3>
            <div className={s.messageContent}>
              {params.TrackingId === '-1' ? (
                <p className={s.errorText}>انصراف توسط کاربر</p>
              ) : (
                <div>
                  <p className={s.errorText}>
                    متاسفانه سفارش شما بعلت بروز خطا ثبت نشد.
                  </p>
                  <p className={s.errorText}>
                    شناسه پرداخت: {params.TrackingId}
                  </p>
                </div>
              )}
              {params.fromApp === 'True' ? (
                <a
                  className={s.backBtn}
                  href={
                    params.os === 'ios'
                      ? this.getUrlIOS()
                      : this.getUrlAndroid()
                  }
                >
                  بازگشت به اپلیکیشن
                </a>
              ) : (
                <CPButton className={s.backBtn} onClick={this.redirectToHome}>
                  {params.fromWallet === 'False' ? (
                    <span>بازگشت به صفحه اصلی</span>
                  ) : (
                    <span>بازگشت به پروفایل</span>
                  )}
                </CPButton>
              )}
            </div>
          </div>
        )}
        {params.status === 'success' && (
          <div className={s.checkoutMessage}>
            <h3 className={s.messageTitle}>
              <i className="mf-check" />پرداخت موفق
            </h3>
            <div className={s.messageContent}>
              <p className={s.successText}>سفارش شما با موفقیت ثبت شد.</p>
              <p className={s.successText}>
                {' '}
                شناسه پرداخت: {params.TrackingId}{' '}
              </p>
              <p className={s.lastText}>
                سفارش شما در سامانه ثبت گردید. برای پیگیری سفارش خود با پشتیبانی
                در ارتباط باشید.
              </p>
              {params.fromApp === 'True' ? (
                <a
                  className={s.backBtn}
                  href={
                    params.os === 'ios'
                      ? this.getUrlIOS()
                      : this.getUrlAndroid()
                  }
                >
                  بازگشت به اپلیکیشن
                </a>
              ) : (
                <CPButton className={s.backBtn} onClick={this.redirectToHome}>
                  {params.fromWallet === 'False' ? (
                    <span>بازگشت به صفحه اصلی</span>
                  ) : (
                    <span>بازگشت به پروفایل</span>
                  )}
                </CPButton>
              )}
            </div>
          </div>
        )}
        {params.status === 'invalid' && (
          <div className={s.checkoutMessage}>
            <h3 className={s.messageTitle}>
              <i className="mf-close-o" />خطا
            </h3>
            <div className={s.messageContent}>
              <p className={s.errorText}>
                invalid متاسفانه سفارش شما بعلت بروز خطا ثبت نشد.
              </p>
              <p className={s.lastText}>
                سفارش شما در سامانه ثبت گردید. برای پیگیری سفارش خود با پشتیبانی
                در ارتباط باشید.
              </p>
              {params.fromApp === 'True' ? (
                <a
                  className={s.backBtn}
                  href={
                    params.os === 'ios'
                      ? this.getUrlIOS()
                      : this.getUrlAndroid()
                  }
                >
                  بازگشت به اپلیکیشن
                </a>
              ) : (
                <CPButton className={s.backBtn} onClick={this.redirectToHome}>
                  {params.fromWallet === 'False' ? (
                    <span>بازگشت به صفحه اصلی</span>
                  ) : (
                    <span>بازگشت به پروفایل</span>
                  )}
                </CPButton>
              )}
            </div>
          </div>
        )}
        {params.status === 'expire' && (
          <div className={s.checkoutMessage}>
            <h3 className={s.messageTitle}>
              <i className="mf-close-o" />خطا
            </h3>
            <div className={s.messageContent}>
              <p className={s.errorText}>زمان پرداخت به پایان رسید.</p>
              {params.fromApp === 'True' ? (
                <a
                  className={s.backBtn}
                  href={
                    params.os === 'ios'
                      ? this.getUrlIOS()
                      : this.getUrlAndroid()
                  }
                >
                  بازگشت به اپلیکیشن
                </a>
              ) : (
                <CPButton className={s.backBtn} onClick={this.redirectToHome}>
                  {params.fromWallet === 'False' ? (
                    <span>بازگشت به صفحه اصلی</span>
                  ) : (
                    <span>بازگشت به پروفایل</span>
                  )}
                </CPButton>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  userData: state.account.loginData,
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      showLoading,
    },
    dispatch,
  ),
  dispatch,
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(returnBank));
