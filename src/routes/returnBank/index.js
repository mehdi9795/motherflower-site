import React from 'react';
import ReturnBank from './ReturnBank';
import CheckoutLayout from '../../components/Template/DefaultLayout/CheckoutLayout';
import { getCookie } from '../../utils';
import { loginSuccess } from '../../redux/identity/action/account';
import { IsAuthenticated } from '../../services/identityApi';

const title = 'MotherFlower';
async function action({ store, params }) {
  const token = getCookie('siteToken');
  // const userName = getCookie('siteUserName');
  // const displayName = getCookie('siteDisplayName');

  const response = await IsAuthenticated(token);

  if (response.status === 200) {
    if (token) {
      const obj = {
        token,
        userName: response.data.userAuthName,
        displayName: response.data.displayName,
      };

      store.dispatch(loginSuccess(obj));
    }
  }
  return {
    chunks: ['returnBank'],
    title,
    component: (
      <CheckoutLayout>
        <ReturnBank params={params} />
      </CheckoutLayout>
    ),
  };
}

export default action;
