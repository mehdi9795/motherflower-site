import React from 'react';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import Contact from './Contact';

const title = 'Contact Us';

function action() {
  return {
    chunks: ['contact'],
    title,
    component: (
      <Layout>
        <Contact title={title} />
      </Layout>
    ),
  };
}

export default action;
