import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Contact.css';
import Link from '../../components/Link';

class Contact extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  goTo = url => {
    window.open(url, '_blank');
  };

  render() {
    return (
      <div className={s.container}>
        <h2>تماس با ما</h2>
        <div className={s.content}>
          <div className={s.contactInfo}>
            <ul className={s.infoList}>
              <li>
                <i className="mf-pin" />
                <label>آدرس : </label>
                <p>
                  تهران، جردن، خیابان آرش، روبروی اسفندیار، ساختمان رومه رون،
                  پلاک 24، طبقه دوم، واحد 1
                </p>
              </li>
              <li className={s.rotate}>
                <i className="mf-call" />
                <label>امور مشتریان (تهران) : </label>
                <p>1876</p>
              </li>
              <li className={s.rotate}>
                <i className="mf-call" />
                <label>امور مشتریان (شهرستان ها) : </label>
                <p>91001877 - 021</p>
              </li>
              <li>
                <i className="mf-tablet" />
                <label>شماره پیامک : </label>
                <p>20001876</p>
              </li>
              <li>
                <i className="mf-email" />
                <label>ایمیل : </label>
                <p>Inquery@motheflower.com</p>
              </li>
              <li className={s.rotateShare}>
                <i className="mf-share" />
                <label>شبکه های اجتماعی : </label>
                <div className={s.socials}>
                  <Link
                    onClick={() =>
                      this.goTo(
                        'https://www.facebook.com/motherflower.official/',
                      )
                    }
                    to="https://www.facebook.com/motherflower.official/"
                  >
                    <i className="mf-facebook" />
                  </Link>
                  <Link to="/#">
                    <i className="mf-twitter" />
                  </Link>
                  <Link
                    to="https://www.instagram.com/motherflower.official/"
                    className="mdl-button mdl-js-button mdl-button--icon"
                  >
                    <i className="mf-instagram" />
                  </Link>
                  <Link
                    onClick={() =>
                      this.goTo(
                        'https://www.telegram.com/motherflower_official/',
                      )
                    }
                    to="https://www.telegram.com/motherflower_official/"
                  >
                    <i className="mf-telegram" />
                  </Link>
                </div>
              </li>
              <li>
                <i className="mf-mailBox" />
                <label>کد پستی : </label>
                <p>466171719</p>
              </li>
            </ul>
          </div>
          <div className={s.map} />
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Contact);
