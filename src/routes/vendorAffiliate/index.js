import React from 'react';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import VendorAffiliate from './VendorAffiliate';
import { getCityApi } from '../../services/commonApi';
import { getDtoQueryString } from '../../utils/helper';
import { cityListSuccess } from '../../redux/common/action/city';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';

async function action({ store }) {
  /**
   * get all cities for first time with default clause
   */
  const responseCity = await getCityApi(
    getDtoQueryString(new BaseGetDtoBuilder().all(true).buildJson()),
  );

  if (responseCity.status === 200) {
    store.dispatch(cityListSuccess(responseCity.data));
  }

  return {
    chunks: ['cooperation'],
    title: 'معرفی گلفروشی',
    component: (
      <Layout>
        <VendorAffiliate title="معرفی گلفروشی" />
      </Layout>
    ),
  };
}

export default action;
