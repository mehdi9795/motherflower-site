import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import cs from 'classnames';
import { Select } from 'antd';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './VendorAffiliate.css';
import CPButton from '../../components/CP/CPButton';
import CPInput from '../../components/CP/CPInput';
import {
  ADDRESS,
  CITY,
  DESCRIPTION,
  PHONE_NUMBER,
  SUBMIT,
} from '../../Resources/Localization';
import CPMap from '../../components/CP/CPMap';
import { cityListSuccess } from '../../redux/common/action/city';
import CPSelect from '../../components/CP/CPSelect';
import { CityDto } from '../../dtos/commonDtos';
import { showNotification } from '../../utils/helper';
import { VendorAffiliateDto } from '../../dtos/vendorDtos';
import { PostVendorAffiliateApi } from '../../services/vendorApi';
import { BaseCRUDDtoBuilder } from '../../dtos/dtoBuilder';
import { baseCDN } from '../../setting';

const { Option } = Select;

class VendorAffiliate extends React.Component {
  static propTypes = {
    cities: PropTypes.arrayOf(PropTypes.object),
    title: PropTypes.string,
  };

  static defaultProps = {
    cities: [],
    title: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      cityId:
        props.cities && props.cities[0] ? props.cities[0].id.toString() : '',
      telegram: '',
      fullName: '',
      phone: '',
      instagram: '',
      vendorName: '',
      address: '',
      description: '',
      lat: 35.6891975,
      lng: 51.3889736,
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cities) {
      this.setState({
        cityId:
          nextProps.cities && nextProps.cities[0]
            ? nextProps.cities[0].id.toString()
            : '',
      });
    }
  }

  handleInput(inputName, value) {
    this.setState({ [inputName]: value, mapRender: false });
  }

  /**
   * change city for automatic change district
   * @param inputName
   * @param value
   * @returns {Promise<void>}
   */
  async cityChange(inputName, value) {
    this.setState({
      [inputName]: value,
    });
  }

  async onSubmit() {
    const {
      cityId,
      telegram,
      fullName,
      phone,
      instagram,
      vendorName,
      address,
      description,
      lat,
      lng,
    } = this.state;

    const vendorAffiliateCrud = new BaseCRUDDtoBuilder()
      .dto(
        new VendorAffiliateDto({
          cityDto: new CityDto({ id: cityId }),
          telegram,
          fullName,
          phone,
          instagram,
          vendorName,
          description,
          address,
          lat,
          lng,
        }),
      )
      .build();

    const response = await PostVendorAffiliateApi(vendorAffiliateCrud);

    if (response.data.isSuccess) {
      showNotification(
        'success',
        '',
        'اطلاعات فروشگاه موردنظر با موفقیت ثبت شد. ',
        5,
      );
    } else {
      showNotification(
        'error',
        '',
        'اطلاعات فروشگاه موردنظر با خطا روبرو شد. ',
        5,
        'errorBox',
      );
    }
    this.setState({
      cityId:
        this.props.cities && this.props.cities[0]
          ? this.props.cities[0].id.toString()
          : '',
      telegram: '',
      fullName: '',
      phone: '',
      instagram: '',
      vendorName: '',
      address: '',
      description: '',
      lat: 35.6891975,
      lng: 51.3889736,
    });
  }

  setLatAndLng = value => {
    this.setState({ lat: value.lat, lng: value.lng });
  };

  render() {
    const { cities, title } = this.props;
    const {
      cityId,
      telegram,
      fullName,
      phone,
      instagram,
      vendorName,
      address,
      description,
      lat,
      lng,
    } = this.state;

    const citiesDataSource = [];

    /**
     * map cities for comboBox Datasource
     */
    cities.map(item =>
      citiesDataSource.push(<Option key={item.id}>{item.name}</Option>),
    );

    return (
      <div className={s.container}>
        <h2>{title}</h2>
        <div>
          {/* <h4>هدف مادرفلاور</h4>
          لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
          از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و
          سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای
          متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه
          درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با
          نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان
          خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید
          داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به
          پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی
          سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد. */}
        </div>
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <div className={cs('row', s.form)}>
              <div className="col-xs-12">
                <h4 className="row">ثبت مشخصات گلفروشی</h4>
              </div>
              <div className="col-xs-12 col-sm-6 marginTop">
                <CPInput
                  label="نام و نام خانوادگی ثبت کننده"
                  onChange={value =>
                    this.handleInput('fullName', value.target.value)
                  }
                  value={fullName}
                />
              </div>
              <div className="col-xs-12 col-sm-6 marginTop">
                <CPInput
                  label={PHONE_NUMBER}
                  onChange={value =>
                    this.handleInput('phone', value.target.value)
                  }
                  value={phone}
                />
              </div>
              <div className="col-xs-12 col-sm-6 marginTop">
                <CPInput
                  label="نام گلفروشی"
                  onChange={value =>
                    this.handleInput('vendorName', value.target.value)
                  }
                  value={vendorName}
                />
              </div>
              <div className="col-xs-12 col-sm-6 comboBox marginTop">
                <label>{CITY}</label>
                <CPSelect
                  onChange={value => this.cityChange('cityId', value)}
                  defaultValue={cityId}
                  showSearch
                >
                  {citiesDataSource}
                </CPSelect>
              </div>
              <div className="col-xs-12 col-sm-6 marginTop">
                <CPInput
                  label="اینستاگرام"
                  onChange={value =>
                    this.handleInput('instagram', value.target.value)
                  }
                  value={instagram}
                />
              </div>

              <div className="col-xs-12 col-sm-6 comboBox marginTop">
                <CPInput
                  label="تلگرام"
                  onChange={value =>
                    this.handleInput('telegram', value.target.value)
                  }
                  value={telegram}
                />
              </div>
              <div className="col-xs-12 marginTop">
                <CPInput
                  label={ADDRESS}
                  onChange={value =>
                    this.handleInput('address', value.target.value)
                  }
                  value={address}
                />
              </div>
              <div className="col-xs-12 marginTop">
                <CPInput
                  label={DESCRIPTION}
                  onChange={value =>
                    this.handleInput('description', value.target.value)
                  }
                  value={description}
                />
              </div>
              <label className={s.mapTitle}>
                مکان گلفروشی خود را روی نقشه مشخص کنید
              </label>
              <div className="col-xs-12">
                <CPMap
                  containerElement="215px"
                  defaultCenter={{ lat, lng }}
                  defaultZoom={10}
                  googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZvHdH8X3VuIsB2N97DP8rRVxpvRKclfI&v=3.exp&libraries=geometry,drawing,places"
                  mapElement="100%"
                  loadingElement="100%"
                  draggable
                  onDragEnd={value => this.setLatAndLng(value)}
                  lat={lat}
                  lng={lng}
                  searchBox
                />
              </div>
              <div className={cs('col-xs-12', s.submit)}>
                <CPButton onClick={this.onSubmit}>{SUBMIT}</CPButton>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-6 image">
            <img src={`${baseCDN}/vendor-affiliate.jpg`} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cities: state.commonCity.cityListData
    ? state.commonCity.cityListData.items
    : [],
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      cityListSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(VendorAffiliate));
