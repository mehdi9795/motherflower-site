import React from 'react';
import ProductDetails from './ProductDetails';
import Layout from '../../components/Template/DefaultLayout/Layout/Layout';
import {
  productDetailsFailure,
  productDetailsSuccess,
} from '../../redux/catalog/action/product';
import { getProductApi } from '../../services/catalogApi';
import { ProductDto } from '../../dtos/catalogDtos';
import {
  AgentDto,
  CalendarExceptionDto,
  VendorBranchDto,
} from '../../dtos/vendorDtos';
import { getDtoQueryString } from '../../utils/helper';
import { getCalendarExceptionApi } from '../../services/vendorApi';
import { calendarExceptionListSuccess } from '../../redux/vendor/action/calendarException';
import { showAddToCardRequest } from '../../redux/shared/action/addToCard';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';

async function action({ store, params }) {
  try {
    store.dispatch(showAddToCardRequest());

    /**
     * GET PRODUCT DETAILS FOR FIRST TIME WITH PRODUCTID AND VENDORBRANCHID
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, searchMode, removeChildtoParentList}}
     */
    const productList = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          vendorDto: new AgentDto({
            vendorBranchDtos: [
              new VendorBranchDto({ id: params.vendorBranchId }),
            ],
          }),
          id: params.id,
          published: true,
        }),
      )
      .searchMode(true)
      .includes([
        'vendorBranchProductPriceDtos',
        'productSpecificationAttributeDtos',
        'openCalendarContainerDto',
        'productTagDtos',
        'vendorBranches',
        'imageDtos',
        // 'commentDtos',
        'rateAverageDto',
        // 'relatedProductDtos',
        'productCategoryDtos',
        'vendorDto',
        // 'deepCode',
      ])
      .fromCache(true)
      .buildJson();

    const productDetials = await getProductApi(getDtoQueryString(productList));

    /**
     * get product faq for first time where published = true
     * @type {{dto, pageIndex, pageSize, disabledCount, all, orderByFields, orderByDescending, ids, notExpectedIds, includes, searchMode, removeChildtoParentList}}
     */
    /* const faqData = await getFaqApi(
      getDtoQueryString(
        new BaseGetDtoBuilder()
          .dto(new FaqDto({ published: true }))
          .buildJson(),
      ),
    ); */

    /**
     * get product calendar excpetion
     */
    const calendarException = await getCalendarExceptionApi(
      getDtoQueryString(
        JSON.stringify(
          new CalendarExceptionDto({
            productDto: new ProductDto({
              id: params.id,
              vendorDto: new AgentDto({
                vendorBranchDtos: [
                  new VendorBranchDto({ id: params.vendorBranchId }),
                ],
              }),
            }),
          }),
        ),
      ),
    );

    if (productDetials && calendarException) {
      store.dispatch(productDetailsSuccess(productDetials.data));
      // store.dispatch(faqListSuccess(faqData.data));
      store.dispatch(calendarExceptionListSuccess(calendarException.data));
    } else {
      store.dispatch(productDetailsFailure());
    }
  } catch (error) {
    store.dispatch(productDetailsFailure());
  }

  return {
    chunks: ['product-details'],
    // title: 'Product Details',
    component: (
      <Layout>
        <ProductDetails params={params} />
      </Layout>
    ),
  };
}

export default action;
