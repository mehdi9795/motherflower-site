import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import $ from 'jquery';
import { connect } from 'react-redux';
import { hideLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Tabs } from 'antd';
import s from './ProductDetails.css';
import ProductCard from '../../components/Shared/ProductCard';
import CPBreadcrumb from '../../components/CP/CPBreadcrumb';
import CPCarousel from '../../components/CP/CPCarousel';
import CPTag from '../../components/CP/CPTag';
import LocationBox from '../../components/ProductDetail/LocationBox';
import Faq from '../../components/ProductDetail/Faq';
import Comments from '../../components/ProductDetail/Comments';
import CommentForm from '../../components/ProductDetail/CommentForm';
import CPRate from '../../components/CP/CPRate/CPRate';
import CPButton from '../../components/CP/CPButton/CPButton';
import PriceBox from '../../components/ProductDetail/PriceBox/PriceBox';
import TimepickerBox from '../../components/ProductDetail/TimepickerBox/TimepickerBox';
import ShareBox from '../../components/ProductDetail/ShareBox/ShareBox';
import {
  SIMILAR_PRODUCTS,
  PRODUCT_DETAILS,
  FAQ,
  COMMENTS,
  ADD_TO_CARD,
} from '../../Resources/Localization';
import CPSelect from '../../components/CP/CPSelect';
import {
  BasketDto,
  ProductDto,
  VendorBranchProductDto,
} from '../../dtos/catalogDtos';
import { getCookie } from '../../utils';
import { AgentDto, VendorBranchDto } from '../../dtos/vendorDtos';
import { availableProductShiftRequest } from '../../redux/vendor/action/availableProductShift';
import {
  GetUserBasketHistoriesApi,
  UserBasketHistoriesApi,
} from '../../services/samApi';
import {
  basketListFailure,
  basketListRequest,
  basketListSuccess,
} from '../../redux/catalog/action/basket';
import {
  commentProductSuccess,
  relatedProductSuccess,
} from '../../redux/catalog/action/product';
import { getDtoQueryString } from '../../utils/helper';
import { showAlertRequest } from '../../redux/shared/action/errorAlert';
import ProductDetail from '../../components/ProductDetail/ProductDetail';
import CPCarouselSlider from '../../components/CP/CPCarouselSlider';
import history from '../../history';
import { BaseCRUDDtoBuilder, BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { getPushToken } from '../../utils/push/getPushToken';
import { getProductApi } from '../../services/catalogApi';
import { Helmet } from 'react-helmet';

const { TabPane } = Tabs;

class ProductDetails extends React.Component {
  static propTypes = {
    productDetails: PropTypes.objectOf(PropTypes.any),
    relatedProduct: PropTypes.objectOf(PropTypes.any),
    commentProduct: PropTypes.objectOf(PropTypes.any),
    faqData: PropTypes.arrayOf(PropTypes.object),
    calendarException: PropTypes.objectOf(PropTypes.any),
    params: PropTypes.objectOf(PropTypes.any),
    navigationLoading: PropTypes.bool,
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    productDetails: null,
    relatedProduct: null,
    commentProduct: null,
    faqData: null,
    navigationLoading: false,
    calendarException: {},
    params: {},
  };

  constructor(props) {
    super(props);
    const keywords = [];
    const description = [];

    if (
      props.productDetails &&
      props.productDetails.productCategoryDtos &&
      props.productDetails.productCategoryDtos.length > 0
    ) {
      keywords.push(
        props.productDetails.productCategoryDtos[0].categoryDto.description.split(
          '>',
        )[1],
      );
      keywords.push(props.productDetails.name);
    }

    if (props.productDetails && props.productDetails.productTagDtos)
      props.productDetails.productTagDtos.map(item => {
        keywords.push(item.tagDto.title);
        return null;
      });

    if (
      props.productDetails &&
      props.productDetails.productSpecificationAttributeDtos
    )
      props.productDetails.productSpecificationAttributeDtos.map(item => {
        keywords.push(item.specificationAttributeDto.name);
        if (item.customValue !== 'true')
          description.push(`${item.customValueDescription} عدد ${item.specificationAttributeDto.name} ${item.colorOptionDto.name}`);
        else
          description.push(item.specificationAttributeDto.name);

        return null;
      });

    this.state = {
      priceType:
        props.productDetails &&
        props.productDetails.vendorBranchProductDtos &&
        props.productDetails.vendorBranchProductDtos.length > 0 &&
        props.productDetails.vendorBranchProductDtos[0].vendorBranchDto
          .pickupEnabled
          ? 'pickUp'
          : 'delivery',
      timePickerArray: [],
      hour: '',
      day: '',
      clearDate: false,
      isWeb: false,
      keywords,
      description,
    };
    this.addToBasket = this.addToBasket.bind(this);
    this.flyToElement = this.flyToElement.bind(this);
    const districtId =
      props.productDetails && props.productDetails.vendorBranchProductDtos
        ? props.productDetails.vendorBranchProductDtos[0].vendorBranchDto
            .districtDto.id
        : 0;
    this.breadcrumbData = [
      { key: 1, name: 'صفحه نخست', url: '/' },
      {
        key: 2,
        name:
          props.productDetails && props.productDetails.vendorBranchProductDtos
            ? props.productDetails.vendorBranchProductDtos[0].vendorBranchDto
                .name
            : '',
        url: `/search/${districtId}/${props.params.vendorBranchId}?sort=1`,
      },
      { key: 3, name: 'جزئیات محصول', url: '' },
    ];
    if (props.productDetails && props.productDetails.name)
    document.title = props.productDetails.name;
    document.getElementsByTagName('meta')["description"].content = `${props.productDetails && props.productDetails.name} ${description.length > 0 ? 'شامل' : ''} ${description}`;
  }

  componentDidMount() {
    // if (navigator.geolocation) {
    //   navigator.geolocation.getCurrentPosition(this.showPosition);
    // }
    this.props.actions.hideLoading();
    this.updatePredicate();
    window.addEventListener('resize', this.updatePredicate);
    getPushToken();
    this.getProductRelatedAndComments();
  }

  async getProductRelatedAndComments() {
    const { params } = this.props;
    const productRelated = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          vendorDto: new AgentDto({
            vendorBranchDtos: [
              new VendorBranchDto({ id: params.vendorBranchId }),
            ],
          }),
          id: params.id,
        }),
      )
      .includes(['justRelatedProductDtos'])
      .fromCache(true)
      .buildJson();

    const responseProductRelated = await getProductApi(
      getDtoQueryString(productRelated),
    );

    if (responseProductRelated.status === 200) {
      this.props.actions.relatedProductSuccess(responseProductRelated.data);
    }

    const productComments = new BaseGetDtoBuilder()
      .dto(
        new ProductDto({
          vendorDto: new AgentDto({
            vendorBranchDtos: [
              new VendorBranchDto({ id: params.vendorBranchId }),
            ],
          }),
          id: params.id,
        }),
      )
      .includes(['justCommentDtos'])
      .fromCache(true)
      .buildJson();

    const responseProductComments = await getProductApi(
      getDtoQueryString(productComments),
    );

    if (responseProductComments.status === 200) {
      this.props.actions.commentProductSuccess(responseProductComments.data);
    }
  }

  /* showPosition = (position) => {
    console.log('Latitude', position.coords.latitude);
    console.log('Longitude', position.coords.longitude);
  } */

  updatePredicate = () => {
    this.setState({ isWeb: window.innerWidth >= 768 });
    window.addEventListener('resize', () => {
      this.setState({ isWeb: window.innerWidth >= 768 });
    });
  };

  priceChange = value => {
    this.setState({
      priceType: value,
      timePickerArray: [],
      hour: '',
      clearDate: true,
    });
  };

  datePickerClick = (value, day = '') => {
    this.setState({
      timePickerArray: value,
      hour: value[0].props.value,
      day,
      clearDate: false,
    });
  };

  timePickerClick = value => {
    this.setState({ hour: value });
  };

  flyToElement(flyer, flyingTo) {
    const { isWeb } = this.state;
    const divider = 12;
    const flyerClone = $(flyer).clone();
    $(flyerClone).css({
      position: 'absolute',
      top: `${$(flyer).offset().top + 20}px`,
      left: isWeb ? `58%` : '20px',
      opacity: 1,
      'z-index': 1000,
      width: '30%',
      borderRadius: 20,
    });
    $('body').append($(flyerClone));
    const gotoX =
      $(flyingTo).offset().left +
      $(flyingTo).width() / 2 -
      $(flyer).width() / divider / 2 -
      5;
    const gotoY =
      $(flyingTo).offset().top +
      $(flyingTo).height() / 2 -
      $(flyer).height() / divider / 2 +
      5;

    $(flyerClone).animate(
      {
        opacity: 0.6,
        left: isWeb ? gotoX + 80 : 20,
        top: isWeb ? gotoY - 60 : gotoY - 110,
        width: $(flyer).width() / divider,
        height: $(flyer).height() / divider,
      },
      700,
      () => {
        setTimeout(() => {
          $(flyerClone).animate(
            {
              opacity: 0.6,
              left: gotoX,
              top: isWeb ? gotoY : gotoY - 10,
              width: $(flyer).width() / divider,
              height: $(flyer).height() / divider,
            },
            100,
            () => {
              $(flyingTo).fadeOut('fast', () => {
                $(flyingTo).fadeIn('fast', () => {
                  $(flyerClone).fadeOut('fast', () => {
                    $(flyerClone).remove();
                  });
                });
              });
            },
          );
          const addShakeClass = 'rubberBand';
          document
            .getElementsByClassName('addToCard')[0]
            .classList.add(addShakeClass);
          setTimeout(() => {
            document
              .getElementsByClassName('addToCard')[0]
              .classList.remove(addShakeClass);
          }, 1000);
        }, 200);
      },
    );
  }

  preparingCrudContainer = guestKey => {
    const { priceType, day, hour } = this.state;
    const { params } = this.props;

    const basketCrud = new BaseCRUDDtoBuilder()
      .dto(
        new BasketDto({
          count: 1,
          guestKey,
          priceOption: priceType,
          requestedDate: day,
          requestedHour: hour.replace(' ساعت ', ''),
          vendorBranchProductDto: new VendorBranchProductDto({
            productDto: new ProductDto({ id: params.id }),
            vendorBranchDto: new VendorBranchDto({ id: params.vendorBranchId }),
          }),
        }),
      )
      .build();

    return basketCrud;
  };

  async addToBasket() {
    const guestKey = getCookie('guestKey');
    let token = getCookie('siteToken');
    this.props.actions.basketListRequest();

    const responsePost = await UserBasketHistoriesApi(
      this.preparingCrudContainer(!token ? guestKey : undefined),
      token,
    );
    if (responsePost.status === 200) {
      const itemImg = $('#image-details')
        .find('img')
        .eq(0);
      this.flyToElement(itemImg, $('.add-to-card-icon'));

      token = getCookie('siteToken');

      const response = await GetUserBasketHistoriesApi(
        getDtoQueryString(
          new BaseGetDtoBuilder()
            .dto(new BasketDto({ guestKey: !token ? guestKey : undefined }))
            .buildJson(),
        ),
        token,
      );
      if (response.status === 200) {
        this.props.actions.basketListSuccess(response.data);
      }
    } else if (responsePost.status === 401) {
      const response2 = await UserBasketHistoriesApi(
        this.preparingCrudContainer(guestKey),
        '',
      );

      if (response2.status === 200) {
        token = getCookie('siteToken');

        const responseGet = await GetUserBasketHistoriesApi(
          getDtoQueryString(
            new BaseGetDtoBuilder()
              .dto(new BasketDto({ guestKey: !token ? guestKey : undefined }))
              .buildJson(),
          ),
          token,
        );
        if (responseGet.status === 200) {
          this.props.actions.basketListSuccess(responseGet.data);
        }
      }
    } else {
      this.props.actions.basketListFailure();
      this.props.actions.showAlertRequest(responsePost.data.errorMessage);
    }
  }

  selectTag = value => {
    const { productDetails } = this.props;
    history.push(
      `/search/${
        productDetails.vendorBranchProductDtos[0].vendorBranchDto.districtId
      }/${
        productDetails.vendorBranchProductDtos[0].vendorBranchDto.id
      }?tagIds=${value}&sort=1`,
    );
  };

  render() {
    const {
      priceType,
      timePickerArray,
      hour,
      clearDate,
      keywords,
      description,
    } = this.state;
    const {
      productDetails,
      relatedProduct,
      commentProduct,
      calendarException,
      params,
    } = this.props;
    const dataImage = [];
    const tagData = [];
    const productRelatedArray = [];
    const commentArray = [];

    if (productDetails) {
      productDetails.imageDtos.map(item => {
        const srcSetArray = [];
        srcSetArray.push({ src: `${item.url}`, vw: '500w' });
        srcSetArray.push({ src: `${item.url}`, vw: '1426w' });
        const obj = {
          small: `${item.url}`,
          large: `${item.url}`,
          key: item.id,
          // srcSet: srcSetArray, TODO: It was commented on the error
        };
        dataImage.push(obj);
        return null;
      });

      productDetails.productTagDtos.map(item =>
        tagData.push({ name: item.tagDto.title, key: item.tagDto.id }),
      );

      if (relatedProduct && relatedProduct.relatedProductDtos) {
        relatedProduct.relatedProductDtos.map(item => {
          const priceData = item.vendorBranchProductPriceDtos[0];
          const images = [];
          let price = '';
          let discount = '';
          let percent = '';

          item.imageDtos.map(image => {
            if (image.isCover) images.push(`${image.url}`);
            return null;
          });

          if (
            priceData.deliveryPriceAfterDiscount <
            priceData.pickupPriceAfterDiscount
          ) {
            price = priceData.stringDeliveryBasePrice;
            discount = priceData.stringDeliveryPriceAfterDiscount;
            percent = priceData.deliveryPriceDiscount;
          }

          if (
            priceData.pickupPriceAfterDiscount <=
            priceData.deliveryPriceAfterDiscount
          ) {
            price = priceData.stringPickupBasePrice;
            discount = priceData.stringPickupPriceAfterDiscount;
            percent = priceData.pickupPriceDiscount;
          }

          const obj = {
            url: `/product-details/${
              item.id
            }/${item.vendorBranchProductPriceDtos &&
              item.vendorBranchProductPriceDtos[0].vendorBranchDto.id}`,
            imageUrl1: images[0],
            imageUrl2: images.length > 1 ? images[1] : images[0],
            ProductName: item.name,
            value: item.rateAverage,
            price,
            discount,
            percent: `${percent}`,
            key: item.id,
          };

          productRelatedArray.push(obj);
          return null;
        });
      }

      if (commentProduct && commentProduct.commentDtos)
        commentProduct.commentDtos.map(item => {
          const obj = {
            userName: item.userFullName,
            value: item.rateDto.value,
            date: item.persianCreatedOnUtc,
            comment: item.body,
            key: item.id,
          };

          commentArray.push(obj);
          return null;
        });
    }
    return (
      <div>
        <Helmet>
          
          <meta name="keywords" content={keywords.toString()} />
        </Helmet>
        <div className={s.productDetailPage}>
          <div className={s.productDetails}>
            <div className={s.breadCrumb}>
              <CPBreadcrumb links={this.breadcrumbData} />
            </div>
            <div className={s.gallery}>
              <CPCarousel data={dataImage} width={700} height={700} />
            </div>

            <div className={s.details}>
              <h1 className={s.productName}>
                {productDetails && productDetails.name}
                {/* <i className={s.productCode}> */}
                {/* ({productDetails && productDetails.code}) */}
                {/* </i> */}
              </h1>
              <CPRate
                value={productDetails && productDetails.rateAverage}
                disabled
              />
              {productDetails &&
                productDetails.rateAverage > 0 && (
                  <b className="rate-text">
                    ({productDetails && productDetails.rateAverage})
                  </b>
                )}
              {productDetails && (
                <div className={s.parentPriceBox}>
                  <PriceBox
                    priceData={
                      productDetails &&
                      productDetails.vendorBranchProductPriceDtos[0]
                    }
                    onChange={this.priceChange}
                    pickupEnable={
                      ProductDetails &&
                      productDetails.vendorBranchProductDtos &&
                      productDetails.vendorBranchProductDtos.length > 0
                        ? productDetails.vendorBranchProductDtos[0]
                            .vendorBranchDto.pickupEnabled
                        : false
                    }
                    deliveryEnable={
                      ProductDetails &&
                      productDetails.vendorBranchProductDtos &&
                      productDetails.vendorBranchProductDtos.length > 0
                        ? productDetails.vendorBranchProductDtos[0]
                            .vendorBranchDto.deliveryEnabled
                        : false
                    }
                  />
                  {!productDetails.vendorBranchProductDtos[0]
                    .inventoryStatus && (
                    <div className={s.notExist}>موجود نیست</div>
                  )}
                </div>
              )}
              {productDetails && (
                <TimepickerBox
                  onClick={this.datePickerClick}
                  priceType={priceType}
                  data={productDetails.openCalendarContainerDto}
                  calendarData={calendarException}
                  params={params}
                  clearDate={clearDate}
                  inventoryStatus={
                    productDetails.vendorBranchProductDtos[0].inventoryStatus
                  }
                />
              )}
              <div className={s.wrapp}>
                <div className={s.selectTime}>
                  <CPSelect
                    value={hour ? ` ساعت ${hour}` : []}
                    placeholder="انتخاب ساعت تحویل"
                    onChange={value => this.timePickerClick(value)}
                  >
                    {timePickerArray}
                  </CPSelect>
                </div>
                <CPButton
                  disabled={
                    !(timePickerArray.length > 0) ||
                    (productDetails.vendorBranchProductDtos &&
                      !productDetails.vendorBranchProductDtos[0]
                        .inventoryStatus)
                  }
                  className="addToBasket greenBtn"
                  onClick={this.addToBasket}
                >
                  {ADD_TO_CARD}
                </CPButton>
              </div>
            </div>
          </div>

          <div className={s.shareTag}>
            <div className={s.tag}>
              <CPTag
                onChange={value => this.selectTag(value)}
                data={tagData}
                tagLabel
              />
            </div>
            <div className={s.shareBox}>
              <ShareBox />
            </div>
          </div>

          <div className={s.tabRow}>
            <Tabs defaultActiveKey="1" animated>
              <TabPane tab={SIMILAR_PRODUCTS} key="1">
                {productRelatedArray.length > 4 ? (
                  <div className={s.relatedPro}>
                    <CPCarouselSlider
                      arrows
                      slidesToShow={6}
                      slidesToScroll={1}
                      infinite={false}
                      initialSlide={1}
                      responsive={[
                        {
                        breakpoint: 1200,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 5,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                        },
                      },
                      {
                        breakpoint: 400,
                        settings: {
                          slidesToShow: 1,
                          slidesToScroll: 1,
                        },
                      },
                      ]}
                    >
                      {productRelatedArray.map(item => (
                        <div key={item.key} className="col-md-12">
                          <ProductCard
                            key={item.key}
                            url={item.url}
                            imageUrl1={item.imageUrl1}
                            imageUrl2={item.imageUrl2}
                            ProductName={item.ProductName}
                            value={item.value}
                            price={item.price}
                            discount={item.discount}
                            percent={item.percent}
                          />
                        </div>
                      ))}
                    </CPCarouselSlider>
                  </div>
                ) : (
                  <div className="row">
                    {productRelatedArray.map(item => (
                      <div key={item.key} className="col-md-3 col-sm-6">
                        <ProductCard
                          key={item.key}
                          url={item.url}
                          imageUrl1={item.imageUrl1}
                          imageUrl2={item.imageUrl2}
                          ProductName={item.ProductName}
                          value={item.value}
                          price={item.price}
                          discount={item.discount}
                          percent={item.percent}
                        />
                      </div>
                    ))}
                  </div>
                )}
              </TabPane>
              <TabPane tab={PRODUCT_DETAILS} key="2">
                <ProductDetail
                  specificationAttribute={
                    productDetails &&
                    productDetails.productSpecificationAttributeDtos
                  }
                />
              </TabPane>
              {/* <TabPane tab={FAQ} key="3">
                <Faq data={faqData} />
              </TabPane> */}
              <TabPane tab={COMMENTS} key="4">
                <div className={s.commentContainer}>
                  <div className="col-sm-12">
                    <LocationBox
                      location={
                        productDetails && productDetails.vendorBranchProductDtos
                          ? productDetails.vendorBranchProductDtos[0]
                              .vendorBranchDto.districtDto.name
                          : ''
                      }
                      rate={
                        productDetails && productDetails.vendorBranchProductDtos
                          ? productDetails.vendorBranchProductDtos[0]
                              .vendorBranchDto.rateAverage
                          : 0
                      }
                      vendorName={
                        productDetails && productDetails.vendorBranchProductDtos
                          ? productDetails.vendorBranchProductDtos[0]
                              .vendorBranchDto.name
                          : ''
                      }
                    />
                  </div>
                  <div className={s.form}>
                    <CommentForm
                      entityId={productDetails && productDetails.id}
                    />
                  </div>
                  <div className={s.comments}>
                    <Comments data={commentArray} />
                  </div>
                </div>
              </TabPane>
              {/* <TabPane tab={RULES} key="5" /> */}
            </Tabs>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productDetails:
    state.catalogProduct.productDetailsData &&
    state.catalogProduct.productDetailsData.items
      ? state.catalogProduct.productDetailsData.items[0]
      : null,
  relatedProduct:
    state.catalogProduct.relatedProductData &&
    state.catalogProduct.relatedProductData.items
      ? state.catalogProduct.relatedProductData.items[0]
      : null,
  commentProduct:
    state.catalogProduct.commentProductData &&
    state.catalogProduct.commentProductData.items
      ? state.catalogProduct.commentProductData.items[0]
      : null,
  faqData: state.cmsFaq.faqListData ? state.cmsFaq.faqListData.items : [],
  calendarException: state.vendorCalendarException.calendarExceptionListData,
  navigationLoading: state.loadingSearch.navigationLoading,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      availableProductShiftRequest,
      basketListSuccess,
      basketListFailure,
      basketListRequest,
      showAlertRequest,
      hideLoading,
      relatedProductSuccess,
      commentProductSuccess,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(ProductDetails));
