import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Register.css';
import AddToCard from '../../components/Shared/AddToCard/AddToCard';
import AccountWizard from '../../components/Account/AccountWizard/AccountWizard';

class Register extends React.Component {
  static propTypes = {
    basketProductCount: PropTypes.number,
    reagentUser: PropTypes.string,
  };

  static defaultProps = {
    basketProductCount: 0,
    reagentUser: '',
  };

  /* toggleMeOne = () => {
    setTimeout(() => {
      this.setState({ register: !this.state.register });
    }, 500);
    setTimeout(() => {
      this.setState({ login: !this.state.login });
    }, 500);
  };

  toggleMeTwo = () => {
    setTimeout(() => {
      this.setState({ register: !this.state.register });
    }, 500);
    setTimeout(() => {
      this.setState({ confirmation: !this.state.confirmation });
    }, 500);
  }; */

  render() {
    const { basketProductCount, reagentUser } = this.props;
    return (
      <div className={s.root}>
        {/* <CoverHeader noBackground /> */}
        <div className={s.loginWrap}>
          <AccountWizard selectedStep="register" reagentUser={reagentUser} />
        </div>
        <AddToCard count={basketProductCount} />
        {/* <Chat /> */}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  basketProductCount: state.basket.basketListData
    ? state.basket.basketListData.count
    : 0,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Register));
