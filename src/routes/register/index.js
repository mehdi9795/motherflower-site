import React from 'react';
import LoginLayout from '../../components/Template/DefaultLayout/LoginLayout';
import Register from './Register';

const title = 'Register';

async function action({ params }) {
  return {
    chunks: ['register'],
    title,
    component: (
      <LoginLayout>
        <Register reagentUser={params.reagentUser || ''} />
      </LoginLayout>
    ),
  };
}

export default action;
