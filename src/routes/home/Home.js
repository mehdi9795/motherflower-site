import React from 'react';
import { bindActionCreators } from 'redux';
import { hideLoading } from 'react-redux-loading-bar';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.css';
import SearchBoxSection from '../../components/Home/SearchBox';
import SecondSection from '../../components/Home/SecondSection';
import TabSection from '../../components/Home/TabSection';
import Banners from '../../components/Home/Banners';
import AppDownload from '../../components/Home/AppDownload';
import Comments from '../../components/Home/Comments';
import { getPushToken } from '../../utils/push/getPushToken';
import { getCookie, setCookie } from '../../utils';
import { UserGuestKeyApi } from '../../services/identityApi';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { ProductDto, ProductSearchDto, BasketDto } from '../../dtos/catalogDtos';
import { getProductApi, getProductSearchApi } from '../../services/catalogApi';
import { getDtoQueryString } from '../../utils/helper';
import {
  bestSellingProductFailure,
  bestSellingProductSuccess,
  newestProductFailure,
  newestProductSuccess,
  specialProductFailure,
  specialProductSuccess,
} from '../../redux/catalog/action/product';
import { getBannerApi } from '../../services/cmsApi';
import {
  bannerListFailure,
  bannerListSuccess,
} from '../../redux/cms/action/banner';
import AppInstall from '../../components/Home/AppInstall';
import Customer from '../../components/Home/Customer';
// import registerServiceWorker from '../../registerServiceWorker';

class Home extends React.Component {
  static propTypes = {
    city: PropTypes.string,
    config: PropTypes.arrayOf(PropTypes.object),
    specialProducts: PropTypes.arrayOf(PropTypes.object),
    banners: PropTypes.arrayOf(PropTypes.object),
    actions: PropTypes.objectOf(PropTypes.func).isRequired,
  };

  static defaultProps = {
    config: [],
    banners: [],
    specialProducts: null,
    city: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      showAppDownload: false,
      showAppInstall: false,
      os: '',
      showComments: false,
    };
    this.scrolling = this.scrolling.bind(this);
    this.isScroll = false;
  }

  componentDidMount() {
    const mapLib = require('../../registerServiceWorker');
    mapLib.register();
    this.props.actions.hideLoading();
    getPushToken();
    // window.onscroll = this.scrolling;
    // this.checkIsOpenFromMobile();
    this.checkInstallApp();
    this.scrolling();
  }

  componentWillReceiveProps(nextProps) {
    // if (nextProps.bestSellingProducts && nextProps.bestSellingProducts.length <= 0)
    //   this.isScroll = false;
  }

  checkInstallApp = () => {
    if (navigator.userAgent.match(/Android/i)) {
      this.setState({
        os: 'android',
        showAppInstall: getCookie('C-App') !== 'true',
      });
    } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
      this.setState({
        os: 'ios',
        showAppInstall: false,
        // showAppInstall: getCookie('C-App') !== 'true',
      });
    }
  };

  async scrolling() {
    // if (this.isScroll) {
      this.isScroll = true;
      this.setState({ showAppDownload: true });
      const token = getCookie('siteToken');
      const guestKey = getCookie('guestKey');
      const cityId = getCookie('cityId');
      if (!token && !guestKey) {
        const response = await UserGuestKeyApi();

        setCookie('guestKey', response.data.guestKey, 8760);
      }

      const productSearchList = new BaseGetDtoBuilder()
        .dto(
          new ProductSearchDto({
            categoryIds: 0,
            vendorBranchIds: -578,
            sortOption: 1,
            toPrice: '3000000',
            fromPrice: '2000',
            priceOption: '1',
            cityId,
          }),
        )
        .pageSize(10)
        .pageIndex(0)
        .disabledCount(true)
        .fromCache(true)
        .buildJson();
      const newestProduct = await getProductSearchApi(
        getDtoQueryString(productSearchList),
      );

      if (newestProduct.status === 200) {
        this.props.actions.bestSellingProductSuccess(newestProduct.data);
      } else this.props.actions.bestSellingProductFailure();

      const productList = new BaseGetDtoBuilder()
        .dto(
          new ProductDto({
            published: true,
            showOnHomePage: true,
            searchCityId: cityId,
          }),
        )
        .searchMode(true)
        .includes([
          'vendorBranchProductPriceDtos',
          'vendorBranches',
          'imageDtos',
          'rateAverageDto',
          'productSpecificationAttributeDtos',
        ])
        .fromCache(true)
        .buildJson();

      const specialProducts = await getProductApi(
        getDtoQueryString(productList),
      );

      if (specialProducts.status === 200) {
        this.props.actions.specialProductSuccess(specialProducts.data);
      } else this.props.actions.specialProductFailure();

      const banerDto = new BaseGetDtoBuilder().dto(new ProductDto({
        published: true
      })).includes('imageDtos').buildJson();

      const banners = await getBannerApi(
        getDtoQueryString(banerDto),
      );

      if (banners.status === 200) {
        this.props.actions.bannerListSuccess(banners.data);
      } else this.props.actions.bannerListFailure();
    // }
  }

  render() {
    const { specialProducts, banners, city } = this.props;
    const { showAppDownload, os, showAppInstall } = this.state;
    const isBannerShow = banners.length > 0;
    let isSpecialShow = false;

    if (
      specialProducts.length > 0 &&
      specialProducts[0].vendorBranchProductPriceDtos.length > 0 &&
      (specialProducts[0].vendorBranchProductPriceDtos[0]
        .pickupSummeryPromotions ||
        specialProducts[0].vendorBranchProductPriceDtos[0]
          .deliverySummeryPromotions)
    )
      isSpecialShow = true;
    // config.map(item => {
    //   if (item.showHomePageBanner === true) isBannerShow = true;
    //   return null;
    // });

    return (
      <div>
        <Helmet>
          <meta
            name="keywords"
            content="تاج گل، دسته گل، باکس گل، سبد گل، گلدان، گیاه، گیاه آپارتمانی، سبد گل فانتزی، سبد گل کلاسیک، سبد گل پایه دار، گلفروشی، گل، "
          />
        </Helmet>
        {showAppInstall &&
          os !== '' && (
            <AppInstall
              androidLink="http://bit.ly/2CSWe6i"
              iosLink=""
            />
          )}
        {/* <Promotion /> */}

        <SearchBoxSection city={city} />
        <Banners />
        {isSpecialShow && <SecondSection />}
        <TabSection />
        <AppDownload />
        <Comments />
        <Customer />
        {/* <VendorsSlider /> */}
        {/* <AddToCard count={5} onClick={this.onOpenChange} /> */}
        {/* <Chat /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  config: state.commonConfig.configListData
    ? state.commonConfig.configListData.items
    : [],
  specialProducts: state.catalogProduct.specialProductData
    ? state.catalogProduct.specialProductData.items
    : null,
  navigationLoading: state.loadingSearch.navigationLoading,
  banners: state.cmsBanner.bannerListData
    ? state.cmsBanner.bannerListData.items
    : [],
  bestSellingProducts: state.catalogProduct.bestSellingProductData
    ? state.catalogProduct.bestSellingProductData.items
    : null,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      hideLoading,
      bestSellingProductSuccess,
      bestSellingProductFailure,
      specialProductSuccess,
      specialProductFailure,
      newestProductSuccess,
      newestProductFailure,
      bannerListSuccess,
      bannerListFailure,
    },
    dispatch,
  ),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(s)(Home));
