import React from 'react';
import Home from './Home';
import HomeLayout from '../../components/HomeLayout';
import { getCityApi } from '../../services/commonApi';
import { getCategoryApi } from '../../services/catalogApi';
import { BaseGetDtoBuilder } from '../../dtos/dtoBuilder';
import { CityDto } from '../../dtos/commonDtos';
import { cityListSuccess } from '../../redux/common/action/city';
import { categoryListSuccess } from '../../redux/catalog/action/category';
import { getDtoQueryString } from '../../utils/helper';
import { CategoryDto } from '../../dtos/catalogDtos';

async function action({ store, params }) {
  const responseCities = await getCityApi(
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new CityDto({
            active: true,
          }),
        )
        .fromCache(true)
        .buildJson(),
    ),
  );

  if (responseCities.status === 200) {
    store.dispatch(cityListSuccess(responseCities.data));
  }

  const responseCategory = await getCategoryApi(
    getDtoQueryString(
      new BaseGetDtoBuilder()
        .dto(
          new CategoryDto({
            published: true,
            showOnHomePage: true,
          }),
        )
        .fromCache(true)
        .buildJson(),
    ),
  );

  if (responseCategory.status === 200) {
    store.dispatch(categoryListSuccess(responseCategory.data));
  }

  return {
    title: 'MotherFlower',
    component: (
      <HomeLayout>
        <Home city={params && params.city} />
      </HomeLayout>
    ),
  };
}

export default action;
