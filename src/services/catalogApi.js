import { BaseGet } from './baseApi';

// <editor-fold dsc="Product Apis">
export const getProductApi = dto => BaseGet('/Catalog/Product/Products', dto);

export const getProductSearchApi = dto =>
  BaseGet('Catalog/Product/ProductSearch', dto);

export const getCategoryApi = dto =>
  BaseGet('/Catalog/Category/Categories', dto);

export const getColorOptionApi = dto =>
  BaseGet('/Catalog/SearchColorOption/SearchColorOptions', dto);

export const getSearchSpecificationAttributeApi = dto =>
  BaseGet(
    '/Catalog/SearchSpecificationAttribute/SearchSpecificationAttributes',
    dto,
  );

export const getTagApi = dto => BaseGet('/Catalog/Tag/Tags', dto);
export const getTagVendorBranchApi = dto =>
  BaseGet('/Catalog/Tag/GetSearchVendorBranchTags', dto);

export const getSearchVendorBranchCategoriesApi = dto =>
  BaseGet(
    '/Catalog/SearchVendorBranchCategory/SearchVendorBranchCategories',
    dto,
  );
// </editor-fold>
