import {
  BaseAuthDelete,
  BaseAuthGet,
  BaseAuthPost,
  BaseAuthPut,
  BaseDelete,
  BaseGet,
  BasePost,
  BasePut,
} from './baseApi';
import { WalletDepositHistoriesDto } from '../dtos/samDtos';

// <editor-fold dsc="basket Apis">

export const UserBasketHistoriesApi = (dto, token) => {
  if (token) {
    return BaseAuthPost(
      token,
      '/SAM/UserBasketHistory/UserBasketHistories',
      dto,
    );
  }
  return BasePost('/SAM/UserBasketHistory/UserBasketHistories', dto);
};
export const GetUserBasketHistoriesApi = (dto, token) => {
  if (token) {
    return BaseAuthGet(
      token,
      '/SAM/UserBasketHistory/UserBasketHistories',
      dto,
    );
  }
  return BaseGet('/SAM/UserBasketHistory/UserBasketHistories', dto);
};

export const PutUserBasketHistoriesApi = (dto, token) => {
  if (token) {
    return BaseAuthPut(
      token,
      '/SAM/UserBasketHistory/UserBasketHistories',
      dto,
    );
  }
  return BasePut('/SAM/UserBasketHistory/UserBasketHistories', dto);
};

export const DeleteUserBasketHistoriesApi = id =>
  BaseDelete(`/SAM/UserBasketHistory/UserBasketHistories/${id}`);

// </editor-fold>
export const GetBankApi = dto => BaseGet('/SAM/Bank/Banks', dto);

export const CheckUserBasketHistoryValidity = (dto, token) => {
  if (token) {
    return BaseAuthGet(
      token,
      '/SAM/UserBasketHistory/CheckUserBasketHistoryValidity',
      dto,
    );
  }
  return BaseGet('/SAM/UserBasketHistory/CheckUserBasketHistoryValidity', dto);
};

export const BankPaymentRequestsApi = (dto, token) =>
  BaseAuthPost(token, '/SAM/Payment/BankPaymentRequests', dto);

export const UserBankHistoriesApi = (dto, token) =>
  BaseAuthGet(token, '/SAM/UserBankHistory/UserBankHistories', dto, true);

export const PostUserBankHistoriesApi = (dto, token) =>
  BaseAuthPost(token, '/SAM/UserBankHistory', dto, true);

export const PutUserBankHistoriesApi = (dto, token) =>
  BaseAuthPut(token, '/SAM/UserBankHistory', dto, true);

export const DeleteUserBankHistoriesApi = (id, token) =>
  BaseAuthDelete(token, `/SAM/UserBankHistory/${id}`, true);

export const BankApi = (dto, token) =>
  BaseAuthGet(token, '/SAM/Bank/Banks', dto);

export const BillApi = (dto, token) =>
  BaseAuthGet(token, '/SAM/Bill/Bills', dto, true);

export const PutUserBasketHistoryDetailOccasionTypesApi = (dto, token) =>
  BaseAuthPut(
    token,
    '/SAM/UserBasketHistory/UserBasketHistoryDetailOccasionTypes',
    dto,
    true,
  );

export const PostWalletDepositHistoriesApi = (token, dto) =>
  BaseAuthPost(token, '/SAM/WalletDepositHistories', dto);

export const GetWalletsApi = (token, dto) =>
  BaseAuthGet(token, '/SAM/Wallet/Wallets', dto);

export const BankPaymentWalletRequestsApi = (dto, token) =>
  BaseAuthPost(token, '/SAM/Payment/BankPaymentWalletRequest', dto);
