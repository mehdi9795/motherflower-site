import {
  BaseAuthDelete,
  BaseAuthGet,
  BaseAuthPost,
  BaseAuthPut,
} from './baseApi';

export const GetNotificationApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/UserMessage/UserMessages', dto);
export const PostNotificationApi = (token, dto) =>
  BaseAuthPost(token, '/Notification/Message/Messages', dto);
export const PutNotificationApi = (token, dto) =>
  BaseAuthPut(token, '/Notification/Message/Messages', dto);
export const DeleteNotificationApi = (token, id) =>
  BaseAuthDelete(token, `/Notification/Message/Messages/${id}`);

export const GetRequestTypeApi = (token, dto) =>
  BaseAuthGet(token, '/Notification/RequestType/RequestTypes', dto);
