import {BaseGet, BaseGenericDto, BaseAuthPost, BasePost, BaseAuthGet} from './baseApi';

// <editor-fold dsc="vendor Apis">
export const getCalendarExceptionApi = dto =>
  BaseGenericDto('/Vendor/CalendarException', dto);

export const getAvailableProductShiftApi = dto =>
  BaseGenericDto('/Vendor/AvailableProductShift', dto);

export const getVendorBranchApi = dto =>
  BaseGet('/Vendor/VendorBranch/VendorBranches', dto);

// </editor-fold>
// <editor-fold dsc="Vendor Branch zone">
export const getVendorBranchZoneApi = dto =>
  BaseGet('Vendor/VendorBranchZone/VendorBranchZones', dto);

// </editor-fold>


export const PostVendorAffiliateApi = (dto) =>
  BasePost('Vendor/VendorAffiliate/VendorAffiliates', dto, true);

export const getVendorBranchCityApi = (token, dto) =>
  BaseAuthGet(token, 'Vendor/VendorBranchCity/VendorBranchCities', dto);
