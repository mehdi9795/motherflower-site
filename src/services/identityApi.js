import {
  BaseAuthDelete,
  BaseAuthGet,
  BaseAuthPost,
  BaseAuthPut,
  BaseGet,
  BasePost,
} from './baseApi';

export const postAuthenticateApi = dto => BasePost('authenticate', dto);

export const getPermissionApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/Permission/Permissions', dto);

export const SignUpApi = dto => BasePost('/Identity/Account/SignUp', dto);

export const UserVerificationApi = dto =>
  BasePost('/Identity/Account/UserVerification', dto);

export const SendVerificationCodeApi = dto =>
  BasePost('/Identity/Account/SendVerificationCode', dto);

export const ChangePasswordApi = dto =>
  BasePost('/Identity/Account/ChangePassword', dto);

export const ChangePasswordAuthApi = (token, dto, force = false) =>
  BaseAuthPost(token, '/Identity/Account/ChangePassword', dto, force);

export const LogOutApi = () => BasePost('/auth/logout');

export const UserGuestKeyApi = () => BaseGet('/Identity/Account/UserGuestKey');

export const GetAddressUser = (token, dto, force = false) =>
  BaseAuthGet(token, '/Identity/Address/Addresses', dto, force);
export const PostAddressUser = (token, dto, force = false) =>
  BaseAuthPost(token, '/Identity/Address/Addresses', dto, force);
export const PutAddressUser = (token, dto, force = false) =>
  BaseAuthPut(token, '/Identity/Address/Addresses', dto, force);
export const DeleteAddressUser = (token, id, force = false) =>
  BaseAuthDelete(token, `/Identity/Address/Addresses/${id}`, force);

export const GetUser = (token, dto, force = false) =>
  BaseAuthGet(token, `Identity/User/Users`, dto, force);

export const PutUser = (token, dto) =>
  BaseAuthPut(token, `Identity/User/Users`, dto, true);

export const IsAuthenticated = token =>
  BaseAuthGet(token, `Identity/Account/IsAuthenticated`);

export const postAppUserInfo = dto =>
  BasePost('Identity/UserAppInfo/UserAppInfos', dto);

export const authPostAppUserInfo = (dto, token) =>
  BaseAuthPost(token, 'Identity/UserAppInfo/UserAppInfos', dto);

export const getUserEventApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserEvent/UserEvents', dto);

export const postUserEventApi = (token, dto) =>
  BaseAuthPost(token, '/Identity/UserEvent/UserEvent', dto);

export const putUserEventApi = (token, dto) =>
  BaseAuthPut(token, '/Identity/UserEvent/UserEvent', dto);

export const deleteUserEventApi = (token, id) =>
  BaseAuthDelete(token, `/Identity/UserEvent/UserEvent/${id}`);

export const getUserEventTypeApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserEventType/UserEventTypes', dto);

export const getUserRelationShipApi = (token, dto) =>
  BaseAuthGet(token, '/Identity/UserRelationShip/UserRelationShips', dto);



export const postTestApi = () =>
  BasePost('/session-to-token');
