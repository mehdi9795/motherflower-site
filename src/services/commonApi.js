import {BaseAuthGet, BaseGet, BasePost} from './baseApi';

// <editor-fold dsc="district Apis">
export const getDistrictApi = dto => BaseGet('Common/District/Districts', dto);
// </editor-fold>
// <editor-fold dsc="city Apis">
export const getCityApi = dto => BaseGet('Common/City/Cities', dto);
// </editor-fold>

export const getConfigApi = dto => BaseGet('/Common/Config/Configs', dto);

export const getNearestLocationsApi = dto =>
  BaseGet('/Common/District/NearestLocations', dto);

export const getOccasionTypeApi = dto =>
  BaseGet('/Common/OccasionType/OccasionTypes', dto);

export const postDownloadApp = dto =>
  BasePost('/Common/DownloadApp/DownloadApps', dto);
