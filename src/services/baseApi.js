import apisauce from 'apisauce';
import { message } from 'antd';
import { deleteCookie, getCookie } from '../utils';

// const baseURL = 'http://192.168.110.12:4060/';
// const baseURL = 'http://104.227.248.180:4060/';
//   const baseURL = 'http://localhost:50454/';
const baseURL = 'https://api.motherflower.com/';
// const baseURL = 'http://192.168.120.12:4070/';

// export const authHeader = () => {
//   const session = JSON.parse(localStorage.getItem('session'));
//   if (session && session.token) {
//     return `Bearer ${session.token}`;
//   }
//   return '';
// };

export const baseApi = apisauce.create({
  // base URL is read from the "constructor"
  baseURL,
  // here are some default headers
  headers: { 'Content-Type': 'application/json' },
  // 10 second timeout...
  timeout: 10000,
});

export const BaseGetPromise = async (url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `null`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.get(url, { container: dto });

    resolve(response);
  });
  return func;
};
export const BaseGet = async (url, dto) => {
  const rs = await BaseGetPromise(url, dto);
  if (rs.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (rs.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }

  return rs;
};
export const BaseGenericDto = async (url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      'Authorization': `null`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.get(url, { dto });

    resolve(response);
  });
  return func;
};
export const BaseAuthGetPromise = async (token, url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.get(url, { container: dto });

    resolve(response);
  });

  return func;
};
export const BaseAuthGet = async (token, url, dto, force = false) => {
  const response = await BaseAuthGetPromise(token, url, dto);

  if (response.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }

  if (force) {
    if (response.status === 401) {
      if (typeof window !== 'undefined') {
        deleteCookie('siteToken');
        deleteCookie('siteUserName');
        deleteCookie('siteDisplayName');

        window.location = '/login';
      }
    }
  }
  return response;
};

export const BasePostPromise = async (url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `null`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.post(url, JSON.stringify(dto));

    resolve(response);
  });
  return func;
};
export const BasePost = async (url, dto) => {
  const response = await BasePostPromise(url, dto);
  if (response.status === 500) {
    message.error(response.data.errorMessage);

    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }
  return response;
};
export const BaseAuthPostPromise = async (token, url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.post(url, JSON.stringify(dto));

    resolve(response);
  });
  return func;
};
export const BaseAuthPost = async (token, url, dto, force = false) => {
  const response = await BaseAuthPostPromise(token, url, dto);

  if (response.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }

  if (response.status === 401) {
    if (typeof window !== 'undefined') {
      deleteCookie('siteToken');
      deleteCookie('siteUserName');
      deleteCookie('siteDisplayName');
      if (force) {
        window.location = '/login';
      }
    }
  }

  return response;
};

export const BasePutPromise = async (url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `null`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.put(url, JSON.stringify(dto));

    resolve(response);
  });
  return func;
};
export const BasePut = async (url, dto) => {
  const response = await BasePutPromise(url, dto);
  if (response.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }
  return response;
};
export const BaseAuthPutPromise = async (token, url, dto) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.put(url, JSON.stringify(dto));

    resolve(response);
  });
  return func;
};
export const BaseAuthPut = async (token, url, dto, force = false) => {
  const response = await BaseAuthPutPromise(token, url, dto);

  if (response.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }

  if (response.status === 401) {
    if (typeof window !== 'undefined') {
      deleteCookie('siteToken');
      deleteCookie('siteUserName');
      deleteCookie('siteDisplayName');
      if (force) {
        window.location = '/login';
      }
    }
  }

  return response;
};

export const BaseDeletePromise = async url => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `null`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.delete(url);

    resolve(response);
  });

  return func;
};
export const BaseDelete = async url => {
  const response = await BaseDeletePromise(url);
  if (response.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }
  return response;
};
export const BaseAuthDeletePromise = async (token, url) => {
  const func = new Promise(resolve => {
    baseApi.setHeaders({
      Authorization: `Bearer ${token}`,
      // 'ss-id': `${token}`,
      'Content-Type': 'application/json',
    });
    const response = baseApi.delete(url);

    resolve(response);
  });
  return func;
};
export const BaseAuthDelete = async (token, url, force = false) => {
  const response = await BaseAuthDeletePromise(token, url);

  if (response.status === 500) {
    if (typeof window !== 'undefined') {
      window.location = '/serverError';
    }
  } else if (response.status === 404) {
    if (typeof window !== 'undefined') {
      window.location = '/notFound';
    }
  }

  if (response.status === 401) {
    if (typeof window !== 'undefined') {
      deleteCookie('siteToken');
      deleteCookie('siteUserName');
      deleteCookie('siteDisplayName');
      if (force) {
        window.location = '/login';
      }
    }
  }

  return response;
};
