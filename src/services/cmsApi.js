import {
  BaseAuthPost,
  BaseAuthPut,
  BaseAuthDelete,
  BaseGet,
  BasePost, BaseAuthGet,
} from './baseApi';

// <editor-fold dsc="Cms Apis">
export const getFaqApi = dto => BaseGet('/CMS/FAQ/FAQs', dto);
export const postCommentApi = dto => BasePost('/CMS/Comment', dto);
export const postAuthCommentApi = (token, dto) =>
  BaseAuthPost(token, '/CMS/Comment', dto);

export const getBannerApi = dto =>
  BaseGet('/CMS/HomePageBanner/HomePageBanners', dto);

export const getImageApi = (token, dto) =>
  BaseAuthGet(token, '/CMS/Image/Images', dto);
export const postImageApi = (token, dto) =>
  BaseAuthPost(token, '/CMS/Image', dto);
export const putImageApi = (token, dto) =>
  BaseAuthPut(token, '/CMS/Image', dto);
export const deleteImageApi = (token, id) =>
  BaseAuthDelete(token, `/CMS/Image/${id}`);
// </editor-fold>
