export class VendorBranchDto {
  constructor({
    name = undefined,
    code = undefined,
    vendorBranchGrade = undefined,
    deliveryEnabled = undefined,
    pickupEnabled = undefined,
    description = undefined,
    adminComment = undefined,
    active = undefined,
    deleted = undefined,
    displayOrder = undefined,
    address = undefined,
    phone = undefined,
    fax = undefined,
    cityId = undefined,
    districtId = undefined,
    lat = undefined,
    lng = undefined,
    vendorId = undefined,
    vendorDto = undefined, // instance of vendorDto
    districtDto = undefined, // instance of commonDtos>districtDto
    vendorCategories = undefined, // array of vendorCategoryDto
    vendorFcilities = undefined, // array of vendorFcilityDto
    openingHour = undefined,
    closingHour = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.code = code;
    this.vendorBranchGrade = vendorBranchGrade;
    this.deliveryEnabled = deliveryEnabled;
    this.pickupEnabled = pickupEnabled;
    this.description = description;
    this.adminComment = adminComment;
    this.active = active;
    this.deleted = deleted;
    this.displayOrder = displayOrder;
    this.address = address;
    this.phone = phone;
    this.fax = fax;
    this.cityId = cityId;
    this.districtId = districtId;
    this.lat = lat;
    this.lng = lng;
    this.vendorId = vendorId;
    this.vendorDto = vendorDto; // instance of vendorDto
    this.districtDto = districtDto; // instance of commonDtos>districtDto
    this.vendorCategories = vendorCategories; // array of vendorCategoryDto
    this.vendorFcilities = vendorFcilities; // array of vendorFcilityDto
    this.openingHour = openingHour;
    this.closingHour = closingHour;
    this.id = id;
  }
}
export class AgentDto {
  constructor({
    name = undefined,
    code = undefined,
    email = undefined,
    description = undefined,
    pictureId = undefined,
    adminComment = undefined,
    active = undefined,
    vendorBranchDtos = undefined, // array of vendorBrancheDto
    vendorProductTypeDtos = undefined, // array of vendorProductTypeDto
    imageDtos = undefined, // instance of camDtos>imageDto
    id = undefined,
  } = {}) {
    this.name = name;
    this.code = code;
    this.email = email;
    this.description = description;
    this.pictureId = pictureId;
    this.adminComment = adminComment;
    this.active = active;
    this.vendorBranchDtos = vendorBranchDtos; // array of vendorBrancheDto
    this.vendorProductTypeDtos = vendorProductTypeDtos; // array of vendorProductTypeDto
    this.imageDtos = imageDtos; // instance of camDtos>imageDto
    this.id = id;
  }
}
export class CalendarExceptionDto {
  constructor({
    monthExceptions = undefined,
    persianFromDate = undefined,
    fromDate = undefined,
    toDate = undefined,
    persianToDate = undefined,
    productDto = undefined, // instance of catalogDto>productDto
    fromMobile = undefined,
    id = undefined,
  } = {}) {
    this.monthExceptions = monthExceptions;
    this.persianFromDate = persianFromDate;
    this.fromDate = fromDate;
    this.toDate = toDate;
    this.persianToDate = persianToDate;
    this.productDto = productDto; // instance of catalogDto>productDto
    this.fromMobile = fromMobile;
    this.id = id;
  }
}
export class AvailableProductShiftDto {
  constructor({
    persianSelectedDate = undefined,
    pickupItems = undefined,
    deliveryItems = undefined,
    id = undefined,
    productDto = undefined,
  } = {}) {
    this.persianSelectedDate = persianSelectedDate;
    this.pickupItems = pickupItems;
    this.deliveryItems = deliveryItems;
    this.id = id;
    this.productDto = productDto;
  }
}
export class VendorBranchZoneDto {
  constructor({
    vendorBranchDto = undefined,
    zoneDto = undefined,
    deletable = undefined,
    id = undefined,
  } = {}) {
    this.vendorBranchDto = vendorBranchDto;
    this.zoneDto = zoneDto;
    this.deletable = deletable;
    this.id = id;
  }
}
export class VendorAffiliateDto {
  constructor({
    cityDto = undefined,
    telegram = undefined,
    fullName = undefined,
    phone = undefined,
    instagram = undefined,
    address = undefined,
    description = undefined,
    vendorName = undefined,
    lat = undefined,
    lng = undefined,
    id = undefined,
  } = {}) {
    this.cityDto = cityDto;
    this.telegram = telegram;
    this.fullName = fullName;
    this.phone = phone;
    this.instagram = instagram;
    this.address = address;
    this.description = description;
    this.vendorName = vendorName;
    this.lat = lat;
    this.lng = lng;
    this.id = id;
  }
}

export class VendorBranchCitiesDto {
  constructor({
    vendorBranchDto = undefined,
    cityDto = undefined,
    searchVendorBranchIds = undefined,
    fromCurrentVendorBranch = undefined,
    id = undefined,
  } = {}) {
    this.vendorBranchDto = vendorBranchDto;
    this.cityDto = cityDto;
    this.fromCurrentVendorBranch = fromCurrentVendorBranch;
    this.searchVendorBranchIds = searchVendorBranchIds;
    this.id = id;
  }
}
