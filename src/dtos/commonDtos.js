export class DistrictDto {
  constructor({
    name = undefined,
    lat = undefined,
    lng = undefined,
    displayOrder = undefined,
    cityId = undefined,
    cityDto = undefined, // instance of cityDto
    id = undefined,
    grade = undefined,
    expectedZoneIds = undefined,
    districtZoneDtos = undefined,
    districtZoneStatusType = undefined,
    searchSameName = undefined,
  } = {}) {
    this.name = name;
    this.lat = lat;
    this.lng = lng;
    this.displayOrder = displayOrder;
    this.cityId = cityId;
    this.cityDto = cityDto; // instance of cityDto
    this.id = id;
    this.grade = grade;
    this.expectedZoneIds = expectedZoneIds;
    this.districtZoneDtos = districtZoneDtos;
    this.districtZoneStatusType = districtZoneStatusType;
    this.searchSameName = searchSameName;
  }
}
export class CityDto {
  constructor({
    name = undefined,
    displayOrder = undefined,
    active = undefined,
    lat = undefined,
    lng = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.displayOrder = displayOrder;
    this.active = active;
    this.lat = lat;
    this.lng = lng;
    this.id = id;
  }
}
export class ZoneDto {
  constructor({
    name = undefined,
    active = undefined,
    zoneGrade = undefined,
    description = undefined,
    districts = undefined,
    cityDto = undefined,
    items = undefined,
    count = undefined,
    id = undefined,
    districtDtos = undefined,
  } = {}) {
    this.name = name;
    this.active = active;
    this.zoneGrade = zoneGrade;
    this.description = description;
    this.districts = districts;
    this.cityDto = cityDto;
    this.items = items;
    this.count = count;
    this.id = id;
    this.districtDtos = districtDtos;
  }
}
export class OccasionTypeDto {
  constructor({
    id = undefined,
    name = undefined,
    occasionTypeDto = undefined,
    published = undefined,
  } = {}) {
    this.id = id;
    this.name = name;
    this.occasionTypeDto = occasionTypeDto;
    this.published = published;
  }
}
