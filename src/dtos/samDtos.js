export class BankDto {
  constructor({
    name = undefined,
    imageDto = undefined,
    published = undefined,
    usedForProfile = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.imageDto = imageDto;
    this.published = published;
    this.usedForProfile = usedForProfile;
    this.id = id;
  }
}
export class BankPaymentRequestsDto {
  constructor({ bankId = undefined, amount = undefined } = {}) {
    this.bankId = bankId;
    this.amount = amount;
  }
}
export class BillDto {
  constructor({
    userDto = undefined,
    code = undefined,
    addressDto = undefined,
    totalAmount = undefined,
    stringTotalAmount = undefined,
    totalDiscount = undefined,
    stringTotalDiscount = undefined,
    totalDiscountCodeAmount = undefined,
    stringTotalDiscountCodeAmount = undefined,
    totalShipmentAmount = undefined,
    stringTotalShipmentAmount = undefined,
    payableAmount = undefined,
    payableBillingAmount = undefined,
    stringPayableBillingAmount = undefined,
    stringPayableAmount = undefined,
    discountCode = undefined,
    billStatus = undefined,
    searchBillItemStatus = undefined, // array of string,
    searchBillStatus = undefined,
    billItemDtos = undefined, // array of ,
    searchFromDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    persianSearchFromDateTimeUtc = undefined,
    persianSearchToDateTimeUtc = undefined,
    fromPanel = undefined,
    id = undefined,
    searchHasDiscountCode = undefined,
    saerchHasPromotion = undefined,
    searchVendorBranchDtos = undefined,
  } = {}) {
    this.userDto = userDto;
    this.code = code;
    this.addressDto = addressDto;
    this.totalAmount = totalAmount;
    this.totalDiscount = totalDiscount;
    this.stringTotalDiscount = stringTotalDiscount;
    this.totalDiscountCodeAmount = totalDiscountCodeAmount;
    this.stringTotalDiscountCodeAmount = stringTotalDiscountCodeAmount;
    this.totalShipmentAmount = totalShipmentAmount;
    this.stringTotalShipmentAmount = stringTotalShipmentAmount;
    this.payableAmount = payableAmount;
    this.payableBillingAmount = payableBillingAmount;
    this.stringPayableBillingAmount = stringPayableBillingAmount;
    this.stringPayableAmount = stringPayableAmount;
    this.discountCode = discountCode;
    this.billStatus = billStatus;
    this.searchBillItemStatus = searchBillItemStatus; // array of string;
    this.searchBillStatus = searchBillStatus;
    this.billItemDtos = billItemDtos; // array of ;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.persianSearchFromDateTimeUtc = persianSearchFromDateTimeUtc;
    this.persianSearchToDateTimeUtc = persianSearchToDateTimeUtc;
    this.fromPanel = fromPanel;
    this.id = id;
    this.searchHasDiscountCode = searchHasDiscountCode;
    this.saerchHasPromotion = saerchHasPromotion;
    this.searchVendorBranchDtos = searchVendorBranchDtos;
    this.stringTotalAmount = stringTotalAmount;
  }
}
export class UserBankHistoryDto {
  constructor({
    name = undefined,
    shabaNo = undefined,
    cardNo = undefined,
    userDto = undefined,
    bankDto = undefined,
    fromPanel = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.shabaNo = shabaNo;
    this.cardNo = cardNo;
    this.userDto = userDto;
    this.bankDto = bankDto;
    this.fromPanel = fromPanel;
    this.id = id;
  }
}
export class UserBasketHistoryDetailOccasionTypesDto {
  constructor({
    id = undefined,
    occasionTypeId = undefined,
    occasionText = undefined,
    fromOccasionText = undefined,
    toOccasionText = undefined,
  } = {}) {
    this.id = id;
    this.occasionTypeId = occasionTypeId;
    this.occasionText = occasionText;
    this.fromOccasionText = fromOccasionText;
    this.toOccasionText = toOccasionText;
  }
}

export class WalletDepositHistoriesDto {
  constructor({ discountCode = undefined } = {}) {
    this.discountCode = discountCode;
  }
}
