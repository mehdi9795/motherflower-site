export class CategoryDto {
  constructor({
    name = undefined,
    description = undefined,
    metaKeywords = undefined,
    metaDescription = undefined,
    metaTitle = undefined,
    parentCategoryId = undefined,
    pictureId = undefined,
    priceRanges = undefined,
    showOnHomePage = undefined,
    includeundefinedopMenu = undefined,
    published = undefined,
    justParent = undefined,
    justChild = undefined,
    secondaryLevel = undefined,
    makeTree = undefined,
    displayOrder = undefined,
    children = undefined, // array of categoryDto
    // id = undefined,
    id = undefined,
    vendorBranchId = undefined,
  } = {}) {
    this.name = name;
    this.description = description;
    this.metaKeywords = metaKeywords;
    this.metaDescription = metaDescription;
    this.metaTitle = metaTitle;
    this.parentCategoryId = parentCategoryId;
    this.pictureId = pictureId;
    this.priceRanges = priceRanges;
    this.showOnHomePage = showOnHomePage;
    this.includeundefinedopMenu = includeundefinedopMenu;
    this.published = published;
    this.justParent = justParent;
    this.justChild = justChild;
    this.secondaryLevel = secondaryLevel;
    this.makeTree = makeTree;
    this.displayOrder = displayOrder;
    this.children = children; // array of categoryDto
    this.id = id;
    this.vendorBranchId = vendorBranchId;
  }
}
export class ProductDto {
  constructor({
    name = undefined,
    code = undefined,
    marked = undefined,
    description = undefined,
    metaKeywords = undefined,
    metaDescription = undefined,
    metaTitle = undefined,
    adminComment = undefined,
    approvedRatingSum = undefined,
    notApprovedRatingSum = undefined,
    approvedTotalReviews = undefined,
    notApprovedTotalReviews = undefined,
    isGiftCard = undefined,
    showOnHomePage = undefined,
    markAsNew = undefined,
    takesTime = undefined,
    markAsNewStartPersianDateTimeUtc = undefined,
    markAsNewEndPersianDateTimeUtc = undefined,
    availableStartPersianDateTimeUtc = undefined,
    availableEndPersianDateTimeUtc = undefined,
    searchFromDateTimeUtc = undefined,
    searchToDateTimeUtc = undefined,
    inventoryStatus = undefined,
    inventoryStartPersianDateTimeUtc = undefined,
    inventoryEndPersianDateTimeUtc = undefined,
    published = undefined,
    displayOrder = undefined,
    persianCreatedOnUtc = undefined,
    persianUpdatedOnUtc = undefined,
    createdUerDto = undefined, // instance of identotyDtos>userDto
    updatedUserDto = undefined, // instance of identotyDtos>userDto
    confirmedUserDto = undefined, // instance of identotyDtos>userDto
    confirmedOnUtc = undefined,
    productTypeDto = undefined, // instance of productTypeDto,
    vendorDto = undefined, // instance of vendorDtos>agentDto
    productCategoryDtos = undefined, // instance of productCategoryDto array
    vendorBranchProductDtos = undefined, // instance from vendorDros>vendorBranchProductDto
    productRelatedCategoryDto = undefined, // instance from productRelatedCategoryDto array
    productTagDtos = undefined, // instance from productTagDto array
    vendorBranchProductPriceDtos = undefined, // instance from samDtos>vendorBranchProductPriceDto array
    productSpecificationAttributeDtos = undefined, // instance from productSpecificationAttributeDto array
    imageDtos = undefined, // instance from cmsDtos>imageDto array
    openCalendarContainerDto = undefined, // instance from vendorDtos> openCalendarContainerDto
    commentDtos = undefined, // instance from cmdDtos>commentDtos
    searchFromBasePrice = undefined,
    searchToBasePrice = undefined,
    searchIsPickupMode = undefined,
    siteCode = undefined,
    id = undefined,
    vendorBranchDto = undefined,
    disablePublishedFilter = undefined,
    searchCityId = undefined,
  } = {}) {
    this.name = name;
    this.code = code;
    this.marked = marked;
    this.description = description;
    this.metaKeywords = metaKeywords;
    this.metaDescription = metaDescription;
    this.metaTitle = metaTitle;
    this.adminComment = adminComment;
    this.approvedRatingSum = approvedRatingSum;
    this.notApprovedRatingSum = notApprovedRatingSum;
    this.approvedTotalReviews = approvedTotalReviews;
    this.notApprovedTotalReviews = notApprovedTotalReviews;
    this.isGiftCard = isGiftCard;
    this.showOnHomePage = showOnHomePage;
    this.markAsNew = markAsNew;
    this.takesTime = takesTime;
    this.markAsNewStartPersianDateTimeUtc = markAsNewStartPersianDateTimeUtc;
    this.markAsNewEndPersianDateTimeUtc = markAsNewEndPersianDateTimeUtc;
    this.availableStartPersianDateTimeUtc = availableStartPersianDateTimeUtc;
    this.availableEndPersianDateTimeUtc = availableEndPersianDateTimeUtc;
    this.searchFromDateTimeUtc = searchFromDateTimeUtc;
    this.searchToDateTimeUtc = searchToDateTimeUtc;
    this.inventoryStatus = inventoryStatus;
    this.inventoryStartPersianDateTimeUtc = inventoryStartPersianDateTimeUtc;
    this.inventoryEndPersianDateTimeUtc = inventoryEndPersianDateTimeUtc;
    this.published = published;
    this.displayOrder = displayOrder;
    this.persianCreatedOnUtc = persianCreatedOnUtc;
    this.persianUpdatedOnUtc = persianUpdatedOnUtc;
    this.createdUerDto = createdUerDto; // instance of identotyDtos>userDto
    this.updatedUserDto = updatedUserDto; // instance of identotyDtos>userDto
    this.confirmedUserDto = confirmedUserDto; // instance of identotyDtos>userDto
    this.confirmedOnUtc = confirmedOnUtc;
    this.productTypeDto = productTypeDto; // instance of productTypeDto;
    this.vendorDto = vendorDto; // instance of vendorDtos>agentDto
    this.productCategoryDtos = productCategoryDtos; // instance of productCategoryDto array
    this.vendorBranchProductDtos = vendorBranchProductDtos; // instance from vendorDros>vendorBranchProductDto
    this.productRelatedCategoryDto = productRelatedCategoryDto; // instance from productRelatedCategoryDto array
    this.productTagDtos = productTagDtos; // instance from productTagDto array
    this.vendorBranchProductPriceDtos = vendorBranchProductPriceDtos; // instance from samDtos>vendorBranchProductPriceDto array
    this.productSpecificationAttributeDtos = productSpecificationAttributeDtos; // instance from productSpecificationAttributeDto array
    this.imageDtos = imageDtos; // instance from cmsDtos>imageDto array
    this.openCalendarContainerDto = openCalendarContainerDto; // instance from vendorDtos> openCalendarContainerDto
    this.commentDtos = commentDtos; // instance from cmdDtos>commentDtos
    this.searchFromBasePrice = searchFromBasePrice;
    this.searchToBasePrice = searchToBasePrice;
    this.searchIsPickupMode = searchIsPickupMode;
    this.siteCode = siteCode;
    this.id = id;
    this.vendorBranchDto = vendorBranchDto;
    this.disablePublishedFilter = disablePublishedFilter;
    this.searchCityId = searchCityId;
  }
}
export class VendorBranchProductDto {
  constructor({
    productDto = undefined,
    vendorBranchDto = undefined,
    id = undefined,
  } = {}) {
    this.productDto = productDto; // instance from productDto
    this.vendorBranchDto = vendorBranchDto; // instance from vendorDtos>vendorBranchDto
    this.id = id;
  }
}
export class SpecificationAttributeDto {
  constructor({
    name = undefined,
    allowFiltering = undefined,
    showOnProductPage = undefined,
    specificationAttributeType = undefined,
    displayOrder = undefined,
    id = undefined,
    imageDtos = undefined,
  } = {}) {
    this.name = name;
    this.allowFiltering = allowFiltering;
    this.showOnProductPage = showOnProductPage;
    this.specificationAttributeType = specificationAttributeType;
    this.displayOrder = displayOrder;
    this.id = id;
    this.imageDtos = imageDtos;
  }
}
export class ColorOptionDto {
  constructor({
    name = undefined,
    attribute = undefined,
    displayOrder = undefined,
    id = undefined,
  } = {}) {
    this.name = name;
    this.attribute = attribute;
    this.displayOrder = displayOrder;
    this.id = id;
  }
}
export class SearchColorOptionDto {
  constructor({
    categoryIds = undefined,
    vendorBranchId = undefined,
    id = undefined,
  } = {}) {
    this.categoryIds = categoryIds;
    this.vendorBranchId = vendorBranchId;
    this.id = id;
  }
}
export class SearchSpecificationAttributeDto {
  constructor({
    categoryIds = undefined,
    vendorBranchId = undefined,
    id = undefined,
  } = {}) {
    this.categoryIds = categoryIds;
    this.vendorBranchId = vendorBranchId;
    this.id = id;
  }
}
export class ProductSearchDto {
  constructor({
    specificationAttributeIds = undefined,
    colorOptionIds = undefined,
    vendorBranchIds = undefined,
    toPrice = undefined,
    fromPrice = undefined,
    priceOption = undefined,
    categoryIds = undefined,
    sortOption = undefined,
    vendorBranchName = undefined,
    districtName = undefined,
    tagIds = undefined,
    fromMobile = undefined,
    vendorBranchId = undefined,
    cityId = undefined,
  } = {}) {
    this.specificationAttributeIds = specificationAttributeIds;
    this.colorOptionIds = colorOptionIds;
    this.vendorBranchIds = vendorBranchIds;
    this.toPrice = toPrice;
    this.fromPrice = fromPrice;
    this.priceOption = priceOption;
    this.categoryIds = categoryIds;
    this.sortOption = sortOption;
    this.vendorBranchName = vendorBranchName;
    this.districtName = districtName;
    this.tagIds = tagIds;
    this.fromMobile = fromMobile;
    this.vendorBranchId = vendorBranchId;
    this.cityId = cityId;
  }
}
export class BasketDto {
  constructor({
    count = undefined,
    guestKey = undefined,
    vendorBranchProductDto = undefined,
    userBasketHistoryDto = undefined,
    priceOption = undefined,
    requestedDate = undefined,
    requestedHour = undefined,
    fromCheckout = undefined,
    published = undefined,
    id = undefined,
  } = {}) {
    this.count = count;
    this.guestKey = guestKey;
    this.vendorBranchProductDto = vendorBranchProductDto;
    this.userBasketHistoryDto = userBasketHistoryDto;
    this.priceOption = priceOption;
    this.requestedDate = requestedDate;
    this.requestedHour = requestedHour;
    this.fromCheckout = fromCheckout;
    this.published = published;
    this.id = id;
  }
}
export class UserBasketHistoryDto {
  constructor({
    addressId = undefined,
    deleteDiscountCode = undefined,
    discountCode = undefined,
    description = undefined,
  } = {}) {
    this.addressId = addressId;
    this.deleteDiscountCode = deleteDiscountCode;
    this.discountCode = discountCode;
    this.description = description;
  }
}
