export class UserDto {
  constructor({
    firstName = undefined,
    lastName = undefined,
    email = undefined,
    password = undefined,
    phone = undefined,
    genderType = undefined,
    active = undefined,
    deleted = undefined,
    newsLetter = undefined,
    vendorId = undefined,
    persianDateOfBirth = undefined,
    userName = undefined,
    addressDto = undefined, // array of addressDto
    userRoleDto = undefined, // array of userRoleDto
    id = undefined,
    verify = undefined,
    countryCode = undefined,
    fromMobile = undefined,
    cityId = undefined,
    cityDto = undefined,
    dayOfBirthDate = undefined,
    monthOfBirthDate = undefined,
    reagentUserDto = undefined,
  } = {}) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.phone = phone;
    this.genderType = genderType;
    this.active = active;
    this.deleted = deleted;
    this.newsLetter = newsLetter;
    this.vendorId = vendorId;
    this.persianDateOfBirth = persianDateOfBirth;
    this.userName = userName;
    this.addressDto = addressDto; // array of addressDto
    this.userRoleDto = userRoleDto; // array of userRoleDto
    this.id = id;
    this.verify = verify;
    this.countryCode = countryCode;
    this.fromMobile = fromMobile;
    this.cityId = cityId;
    this.cityDto = cityDto;
    this.dayOfBirthDate = dayOfBirthDate;
    this.monthOfBirthDate = monthOfBirthDate;
    this.reagentUserDto = reagentUserDto;
  }
}
export class AddressDto {
  constructor({
    fromPanel = undefined,
    userDto = undefined,
    receiverName = undefined,
    lat = undefined,
    lng = undefined,
    content = undefined,
    userId = undefined,
    phone = undefined,
    fax = undefined,
    cityId = undefined,
    districtId = undefined,
    id = undefined,
    active = undefined,
  } = {}) {
    this.fromPanel = fromPanel;
    this.userDto = userDto;
    this.receiverName = receiverName;
    this.lat = lat;
    this.lng = lng;
    this.content = content;
    this.userId = userId;
    this.phone = phone;
    this.fax = fax;
    this.cityId = cityId;
    this.districtId = districtId;
    this.id = id;
    this.active = active;
  }
}
export class AuthenticateDto {
  constructor({
    UserName = undefined,
    Password = undefined,
    nc = undefined,
    UseTokenCookie = undefined,
    State = undefined,
  } = {}) {
    this.UserName = UserName;
    this.Password = Password;
    this.nc = nc;
    this.UseTokenCookie = UseTokenCookie;
    this.State = State;
  }
}
export class PermissionDto {
  constructor({
    parentId = undefined,
    resourceTitle = undefined,
    resourceKey = undefined,
    resourceValue = undefined,
    attribute = undefined,
    permissionStatus = undefined,
    resourceType = undefined,
    uiResourceStatus = undefined,
    children = undefined, // array of permissionDto
    id = undefined,
  } = {}) {
    this.parentId = parentId;
    this.resourceTitle = resourceTitle;
    this.resourceKey = resourceKey;
    this.resourceValue = resourceValue;
    this.attribute = attribute;
    this.permissionStatus = permissionStatus;
    this.resourceType = resourceType;
    this.uiResourceStatus = uiResourceStatus;
    this.children = children; // array of permissionDto
    this.id = id;
  }
}
export class UserVerificationDto {
  constructor({
    verificationCode = undefined,
    verificationType = undefined,
    userName = undefined,
    id = undefined,
    fromWeb = undefined,
  } = {}) {
    this.verificationCode = verificationCode;
    this.verificationType = verificationType;
    this.userName = userName;
    this.id = id;
    this.fromWeb = fromWeb;
  }
}
export class SendVerificationCodeDto {
  constructor({
    userDto = undefined, // instance userDto
    sendViaEmail = undefined, // instance roleDto
    verificationType = undefined,
    resend = undefined,
    id = undefined,
  } = {}) {
    this.userDto = userDto; // instance userDto
    this.sendViaEmail = sendViaEmail; // instance roleDto
    this.verificationType = verificationType;
    this.resend = resend;
    this.id = id;
  }
}
export class ChangePasswordDto {
  constructor({
    userName = undefined,
    currentPassword = undefined,
    newPassword = undefined,
    newConfirmPassword = undefined,
    id = undefined,
    fromWeb = undefined,
  } = {}) {
    this.userName = userName;
    this.currentPassword = currentPassword;
    this.newPassword = newPassword;
    this.newConfirmPassword = newConfirmPassword;
    this.id = id;
    this.fromWeb = fromWeb;
  }
}

export class UserAppInfo {
  constructor({ token = undefined, os = undefined, appType = undefined } = {}) {
    this.token = token;
    this.os = os;
    this.appType = appType;
  }
}

export class UserEvent {
  constructor({
    userDto = undefined,
    userEventTypeDto = undefined,
    userRelationShipDto = undefined,
    eventMonth = undefined,
    eventDay = undefined,
    name = undefined,
    customRelationShip = undefined,
    fromPanel = undefined,
    id = undefined,
    phone = undefined,
  } = {}) {
    this.userDto = userDto;
    this.userEventTypeDto = userEventTypeDto;
    this.userRelationShipDto = userRelationShipDto;
    this.eventMonth = eventMonth;
    this.eventDay = eventDay;
    this.name = name;
    this.customRelationShip = customRelationShip;
    this.fromPanel = fromPanel;
    this.id = id;
    this.phone = phone;
  }
}

export class UserEventType {
  constructor({ id = undefined } = {}) {
    this.id = id;
  }
}

export class UserRelationShip {
  constructor({ id = undefined } = {}) {
    this.id = id;
  }
}
