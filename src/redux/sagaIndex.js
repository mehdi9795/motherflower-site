import { fork } from 'redux-saga/effects';
import sagaCmsIndex from './cms/saga';
import sagaVendorIndex from './vendor/saga';
import sagaCategoryIndex from './catalog/saga';
import sagaCommonIndex from './common/saga';

export default function* sagas() {
  yield [fork(sagaCmsIndex)];
  yield [fork(sagaVendorIndex)];
  yield [fork(sagaCategoryIndex)];
  yield [fork(sagaCommonIndex)];
}
