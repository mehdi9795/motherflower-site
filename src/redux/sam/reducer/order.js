import { getResponseModel } from '../../../utils/model';
import {
  CANCEL_ORDER_LIST_REQUEST,
  CANCEL_ORDER_LIST_SUCCESS,
  CANCEL_ORDER_LIST_FAILURE,
  DELIVERED_ORDER_LIST_REQUEST,
  DELIVERED_ORDER_LIST_SUCCESS,
  DELIVERED_ORDER_LIST_FAILURE,
  IN_PROGRESS_ORDER_LIST_REQUEST,
  IN_PROGRESS_ORDER_LIST_SUCCESS,
  IN_PROGRESS_ORDER_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  cancelOrderListLoading: false,
  cancelOrderListData: getResponseModel,
  cancelOrderListError: false,

  deliveredOrderListLoading: false,
  deliveredOrderListData: getResponseModel,
  deliveredOrderListError: false,

  inProgressOrderListLoading: false,
  inProgressOrderListData: getResponseModel,
  inProgressOrderListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="cancel order List">
    case CANCEL_ORDER_LIST_REQUEST:
      return {
        ...state,
        cancelOrderListLoading: true,
        cancelOrderListError: false,
      };
    case CANCEL_ORDER_LIST_SUCCESS:
      return {
        ...state,
        cancelOrderListData: action.data,
        cancelOrderListLoading: false,
        cancelOrderListError: false,
      };
    case CANCEL_ORDER_LIST_FAILURE:
      return {
        ...state,
        cancelOrderListError: true,
        cancelOrderListLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="delivered order List">
    case DELIVERED_ORDER_LIST_REQUEST:
      return {
        ...state,
        deliveredOrderListLoading: true,
        deliveredOrderListError: false,
      };
    case DELIVERED_ORDER_LIST_SUCCESS:
      return {
        ...state,
        deliveredOrderListData: action.data,
        deliveredOrderListLoading: false,
        deliveredOrderListError: false,
      };
    case DELIVERED_ORDER_LIST_FAILURE:
      return {
        ...state,
        deliveredOrderListError: true,
        deliveredOrderListLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="inProgress order List">
    case IN_PROGRESS_ORDER_LIST_REQUEST:
      return {
        ...state,
        inProgressOrderListLoading: true,
        inProgressOrderListError: false,
      };
    case IN_PROGRESS_ORDER_LIST_SUCCESS:
      return {
        ...state,
        inProgressOrderListData: action.data,
        inProgressOrderListLoading: false,
        inProgressOrderListError: false,
      };
    case IN_PROGRESS_ORDER_LIST_FAILURE:
      return {
        ...state,
        inProgressOrderListError: true,
        inProgressOrderListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
