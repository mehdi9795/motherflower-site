import { getResponseModel } from '../../../utils/model';
import {
  CHECKOUT_LIST_REQUEST,
  CHECKOUT_LIST_SUCCESS,
  CHECKOUT_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  checkoutListLoading: false,
  checkoutListData: getResponseModel,
  checkoutListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="checkout List">
    case CHECKOUT_LIST_REQUEST:
      return {
        ...state,
        checkoutListLoading: true,
        checkoutListError: false,
      };
    case CHECKOUT_LIST_SUCCESS:
      return {
        ...state,
        checkoutListData: action.data,
        checkoutListLoading: false,
        checkoutListError: false,
      };
    case CHECKOUT_LIST_FAILURE:
      return {
        ...state,
        checkoutListError: true,
        checkoutListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
