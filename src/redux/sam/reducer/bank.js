import { getResponseModel } from '../../../utils/model';
import {
  BANK_LIST_REQUEST,
  BANK_LIST_SUCCESS,
  BANK_LIST_FAILURE,
  CARD_NUMBER_LIST_REQUEST,
  CARD_NUMBER_LIST_SUCCESS,
  CARD_NUMBER_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  bankListLoading: false,
  bankListData: getResponseModel,
  bankListError: false,

  cardNumberListLoading: false,
  cardNumberListData: getResponseModel,
  cardNumberListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="bank List">
    case BANK_LIST_REQUEST:
      return {
        ...state,
        bankListLoading: true,
        bankListError: false,
      };
    case BANK_LIST_SUCCESS:
      return {
        ...state,
        bankListData: action.data,
        bankListLoading: false,
        bankListError: false,
      };
    case BANK_LIST_FAILURE:
      return {
        ...state,
        bankListError: true,
        bankListLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="card number List">
    case CARD_NUMBER_LIST_REQUEST:
      return {
        ...state,
        cardNumberListLoading: true,
        cardNumberListError: false,
      };
    case CARD_NUMBER_LIST_SUCCESS:
      return {
        ...state,
        cardNumberListData: action.data,
        cardNumberListLoading: false,
        cardNumberListError: false,
      };
    case CARD_NUMBER_LIST_FAILURE:
      return {
        ...state,
        cardNumberListError: true,
        cardNumberListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
