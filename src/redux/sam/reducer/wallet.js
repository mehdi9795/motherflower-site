import { getResponseModel } from '../../../utils/model';
import {
  WALLET_LIST_REQUEST,
  WALLET_LIST_SUCCESS,
  WALLET_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  walletListLoading: false,
  walletListData: getResponseModel,
  walletListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="wallet List">
    case WALLET_LIST_REQUEST:
      return {
        ...state,
        walletListLoading: true,
        walletListError: false,
      };
    case WALLET_LIST_SUCCESS:
      return {
        ...state,
        walletListData: action.data,
        walletListLoading: false,
        walletListError: false,
      };
    case WALLET_LIST_FAILURE:
      return {
        ...state,
        walletListError: true,
        walletListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
