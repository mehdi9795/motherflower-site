import {
  CHECKOUT_LIST_REQUEST,
  CHECKOUT_LIST_SUCCESS,
  CHECKOUT_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="checkout list">
export function checkoutListRequest(data) {
  return {
    type: CHECKOUT_LIST_REQUEST,
    data,
  };
}

export function checkoutListSuccess(data) {
  return {
    type: CHECKOUT_LIST_SUCCESS,
    data,
  };
}

export function checkoutListFailure() {
  return {
    type: CHECKOUT_LIST_FAILURE,
  };
}
// </editor-fold>
