import {
  CANCEL_ORDER_LIST_REQUEST,
  CANCEL_ORDER_LIST_SUCCESS,
  CANCEL_ORDER_LIST_FAILURE,
  DELIVERED_ORDER_LIST_REQUEST,
  DELIVERED_ORDER_LIST_SUCCESS,
  DELIVERED_ORDER_LIST_FAILURE,
  IN_PROGRESS_ORDER_LIST_REQUEST,
  IN_PROGRESS_ORDER_LIST_SUCCESS,
  IN_PROGRESS_ORDER_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="cancel order list">
export function cancelOrderListRequest(data) {
  return {
    type: CANCEL_ORDER_LIST_REQUEST,
    data,
  };
}

export function cancelOrderListSuccess(data) {
  return {
    type: CANCEL_ORDER_LIST_SUCCESS,
    data,
  };
}

export function cancelOrderListFailure() {
  return {
    type: CANCEL_ORDER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="delivered order list">
export function deliveredOrderListRequest(data) {
  return {
    type: DELIVERED_ORDER_LIST_REQUEST,
    data,
  };
}

export function deliveredOrderListSuccess(data) {
  return {
    type: DELIVERED_ORDER_LIST_SUCCESS,
    data,
  };
}

export function deliveredOrderListFailure() {
  return {
    type: DELIVERED_ORDER_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="in progress order list">
export function inProgressOrderListRequest(data) {
  return {
    type: IN_PROGRESS_ORDER_LIST_REQUEST,
    data,
  };
}

export function inProgressOrderListSuccess(data) {
  return {
    type: IN_PROGRESS_ORDER_LIST_SUCCESS,
    data,
  };
}

export function inProgressOrderListFailure() {
  return {
    type: IN_PROGRESS_ORDER_LIST_FAILURE,
  };
}
// </editor-fold>
