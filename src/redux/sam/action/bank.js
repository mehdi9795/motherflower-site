import {
  BANK_LIST_REQUEST,
  BANK_LIST_SUCCESS,
  BANK_LIST_FAILURE,
  CARD_NUMBER_LIST_REQUEST,
  CARD_NUMBER_LIST_SUCCESS,
  CARD_NUMBER_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="bank list">
export function bankListRequest(data) {
  return {
    type: BANK_LIST_REQUEST,
    data,
  };
}

export function bankListSuccess(data) {
  return {
    type: BANK_LIST_SUCCESS,
    data,
  };
}

export function bankListFailure() {
  return {
    type: BANK_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="card number list">
export function cardNumberListRequest(data) {
  return {
    type: CARD_NUMBER_LIST_REQUEST,
    data,
  };
}

export function cardNumberListSuccess(data) {
  return {
    type: CARD_NUMBER_LIST_SUCCESS,
    data,
  };
}

export function cardNumberListFailure() {
  return {
    type: CARD_NUMBER_LIST_FAILURE,
  };
}
// </editor-fold>
