import {
  WALLET_LIST_REQUEST,
  WALLET_LIST_SUCCESS,
  WALLET_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="wallet list">
export function walletListRequest(data) {
  return {
    type: WALLET_LIST_REQUEST,
    data,
  };
}

export function walletListSuccess(data) {
  return {
    type: WALLET_LIST_SUCCESS,
    data,
  };
}

export function walletListFailure() {
  return {
    type: WALLET_LIST_FAILURE,
  };
}
// </editor-fold>
