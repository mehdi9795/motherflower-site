import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import runtime from './runtime/reducer/runtime';
import catalogProduct from './catalog/reducer/product';
import vendorCalendarException from './vendor/reducer/calendarException';
import vendorVendorBranch from './vendor/reducer/vendorBranch';
import vendorVendorBranchZone from './vendor/reducer/vendorBranchZone';
import availableProductShift from './vendor/reducer/availableProductShift';
import catalogCategory from './catalog/reducer/category';
import catalogTag from './catalog/reducer/tag';
import searchColorOption from './catalog/reducer/searchColorOption';
import searchSpecificationAttribute from './catalog/reducer/searchSpecificationAttribute';
import cmsFaq from './cms/reducer/faq';
import account from './identity/reducer/account';
import identityUser from './identity/reducer/user';
import commonDistrict from './common/reducer/district';
import commonCity from './common/reducer/city';
import loadingSearch from './shared/reducer/loading';
import stopScroll from './shared/reducer/stopScroll';
import errorAlert from './shared/reducer/errorAlert';
import basket from './catalog/reducer/basket';
import identityAddress from './identity/reducer/address';
import samCheckout from './sam/reducer/checkout';
import samWallet from './sam/reducer/wallet';
import samBank from './sam/reducer/bank';
import sharedAddToCard from './shared/reducer/addToCard';
import samOrder from './sam/reducer/order';
import sharedChangeMenu from './shared/reducer/changeMenuProfile';
import cmsBanner from './cms/reducer/banner';
import commonConfig from './common/reducer/config';
import occasionType from './common/reducer/occasionType';

export default combineReducers({
  runtime,
  loadingBar: loadingBarReducer,
  catalogProduct,
  catalogCategory,
  searchColorOption,
  searchSpecificationAttribute,
  cmsFaq,
  vendorCalendarException,
  availableProductShift,
  commonDistrict,
  commonCity,
  vendorVendorBranch,
  vendorVendorBranchZone,
  account,
  loadingSearch,
  stopScroll,
  errorAlert,
  basket,
  identityAddress,
  samCheckout,
  samWallet,
  samBank,
  sharedAddToCard,
  samOrder,
  sharedChangeMenu,
  catalogTag,
  cmsBanner,
  commonConfig,
  occasionType,
  identityUser,
});
