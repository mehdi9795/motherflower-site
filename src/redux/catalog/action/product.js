import {
  PRODUCT_DETAILS_REQUEST,
  PRODUCT_DETAILS_SUCCESS,
  PRODUCT_DETAILS_FAILURE,
  PRODUCT_SEARCH_REQUEST,
  PRODUCT_SEARCH_SUCCESS,
  PRODUCT_SEARCH_FAILURE,
  PRODUCT_SHOULD_UPDATE_REQUEST,
  PRODUCT_SHOULD_UPDATE_SUCCESS,
  PRODUCT_SHOULD_UPDATE_FAILURE,
  IS_EMPTY_PRODUCT_CARD_ARRAY,
  IS_NOT_EMPTY_PRODUCT_CARD_ARRAY,
  SPECIAL_PRODUCT_REQUEST,
  SPECIAL_PRODUCT_SUCCESS,
  SPECIAL_PRODUCT_FAILURE,
  MOST_VISITED_PRODUCT_REQUEST,
  MOST_VISITED_PRODUCT_SUCCESS,
  MOST_VISITED_PRODUCT_FAILURE,
  BEST_SELLING_PRODUCT_REQUEST,
  BEST_SELLING_PRODUCT_SUCCESS,
  BEST_SELLING_PRODUCT_FAILURE,
  NEWEST_PRODUCT_REQUEST,
  NEWEST_PRODUCT_SUCCESS,
  NEWEST_PRODUCT_FAILURE,
  RELATED_PRODUCT_REQUEST,
  RELATED_PRODUCT_SUCCESS,
  RELATED_PRODUCT_FAILURE,
  COMMENT_PRODUCT_REQUEST,
  COMMENT_PRODUCT_SUCCESS,
  COMMENT_PRODUCT_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Product">
export function productDetailsRequest() {
  return {
    type: PRODUCT_DETAILS_REQUEST,
  };
}

export function productDetailsSuccess(data) {
  return {
    type: PRODUCT_DETAILS_SUCCESS,
    data,
  };
}

export function productDetailsFailure() {
  return {
    type: PRODUCT_DETAILS_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product search">
export function productSearchRequest(data) {
  return {
    type: PRODUCT_SEARCH_REQUEST,
    data,
  };
}

export function productSearchSuccess(data) {
  return {
    type: PRODUCT_SEARCH_SUCCESS,
    data,
  };
}

export function productSearchFailure() {
  return {
    type: PRODUCT_SEARCH_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="special Product">
export function specialProductRequest() {
  return {
    type: SPECIAL_PRODUCT_REQUEST,
  };
}

export function specialProductSuccess(data) {
  return {
    type: SPECIAL_PRODUCT_SUCCESS,
    data,
  };
}

export function specialProductFailure() {
  return {
    type: SPECIAL_PRODUCT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="Product should update">
export function productShouldUpdateRequest() {
  return {
    type: PRODUCT_SHOULD_UPDATE_REQUEST,
  };
}

export function productShouldUpdateSuccess(data) {
  return {
    type: PRODUCT_SHOULD_UPDATE_SUCCESS,
    data,
  };
}

export function productShouldUpdateFailure() {
  return {
    type: PRODUCT_SHOULD_UPDATE_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="empty product card array in search">
export function isEmptyProductCardArray() {
  return {
    type: IS_EMPTY_PRODUCT_CARD_ARRAY,
  };
}

export function isNotEmptyProductCardArray() {
  return {
    type: IS_NOT_EMPTY_PRODUCT_CARD_ARRAY,
  };
}
// </editor-fold>

// <editor-fold dsc="most visited Product">
export function mostVisitedProductRequest() {
  return {
    type: MOST_VISITED_PRODUCT_REQUEST,
  };
}

export function mostVisitedProductSuccess(data) {
  return {
    type: MOST_VISITED_PRODUCT_SUCCESS,
    data,
  };
}

export function mostVisitedProductFailure() {
  return {
    type: MOST_VISITED_PRODUCT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="best selling Product">
export function bestSellingProductRequest() {
  return {
    type: BEST_SELLING_PRODUCT_REQUEST,
  };
}

export function bestSellingProductSuccess(data) {
  return {
    type: BEST_SELLING_PRODUCT_SUCCESS,
    data,
  };
}

export function bestSellingProductFailure() {
  return {
    type: BEST_SELLING_PRODUCT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="newest Product">
export function newestProductRequest() {
  return {
    type: NEWEST_PRODUCT_REQUEST,
  };
}

export function newestProductSuccess(data) {
  return {
    type: NEWEST_PRODUCT_SUCCESS,
    data,
  };
}

export function newestProductFailure() {
  return {
    type: NEWEST_PRODUCT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="related Product">
export function relatedProductRequest() {
  return {
    type: RELATED_PRODUCT_REQUEST,
  };
}

export function relatedProductSuccess(data) {
  return {
    type: RELATED_PRODUCT_SUCCESS,
    data,
  };
}

export function relatedProductFailure() {
  return {
    type: RELATED_PRODUCT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="comment Product">
export function commentProductRequest() {
  return {
    type: COMMENT_PRODUCT_REQUEST,
  };
}

export function commentProductSuccess(data) {
  return {
    type: COMMENT_PRODUCT_SUCCESS,
    data,
  };
}

export function commentProductFailure() {
  return {
    type: COMMENT_PRODUCT_FAILURE,
  };
}
// </editor-fold>
