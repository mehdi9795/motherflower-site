import {
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Product">
export function categoryListRequest(data) {
  return {
    type: CATEGORY_LIST_REQUEST,
    data,
  };
}

export function categoryListSuccess(data) {
  return {
    type: CATEGORY_LIST_SUCCESS,
    data,
  };
}

export function categoryListFailure() {
  return {
    type: CATEGORY_LIST_FAILURE,
  };
}
// </editor-fold>
