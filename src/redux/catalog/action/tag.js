import {
  TAG_LIST_REQUEST,
  TAG_LIST_SUCCESS,
  TAG_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="tag list">
export function tagListRequest(data) {
  return {
    type: TAG_LIST_REQUEST,
    data,
  };
}

export function tagListSuccess(data) {
  return {
    type: TAG_LIST_SUCCESS,
    data,
  };
}

export function tagListFailure() {
  return {
    type: TAG_LIST_FAILURE,
  };
}
// </editor-fold>
