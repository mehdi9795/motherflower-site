import {
  SEARCH_COLOR_OPTION_REQUEST,
  SEARCH_COLOR_OPTION_SUCCESS,
  SEARCH_COLOR_OPTION_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Product">
export function searchColorOptionRequest(data) {
  return {
    type: SEARCH_COLOR_OPTION_REQUEST,
    data,
  };
}

export function searchColorOptionSuccess(data) {
  return {
    type: SEARCH_COLOR_OPTION_SUCCESS,
    data,
  };
}

export function searchColorOptionFailure() {
  return {
    type: SEARCH_COLOR_OPTION_FAILURE,
  };
}
// </editor-fold>
