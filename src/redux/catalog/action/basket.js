import {
  BASKET_LIST_REQUEST,
  BASKET_LIST_SUCCESS,
  BASKET_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Product">
export function basketListRequest(data) {
  return {
    type: BASKET_LIST_REQUEST,
    data,
  };
}

export function basketListSuccess(data) {
  return {
    type: BASKET_LIST_SUCCESS,
    data,
  };
}

export function basketListFailure() {
  return {
    type: BASKET_LIST_FAILURE,
  };
}
// </editor-fold>
