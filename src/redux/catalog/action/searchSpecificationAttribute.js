import {
  SEARCH_SPECIFICATION_ATTRIBUTE_REQUEST,
  SEARCH_SPECIFICATION_ATTRIBUTE_SUCCESS,
  SEARCH_SPECIFICATION_ATTRIBUTE_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Product">
export function searchSpecificationAttributeRequest(data) {
  return {
    type: SEARCH_SPECIFICATION_ATTRIBUTE_REQUEST,
    data,
  };
}

export function searchSpecificationAttributeSuccess(data) {
  return {
    type: SEARCH_SPECIFICATION_ATTRIBUTE_SUCCESS,
    data,
  };
}

export function searchSpecificationAttributeFailure() {
  return {
    type: SEARCH_SPECIFICATION_ATTRIBUTE_FAILURE,
  };
}
// </editor-fold>
