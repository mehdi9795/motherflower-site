import { getResponseModel } from '../../../utils/model';
import {
  PRODUCT_DETAILS_REQUEST,
  PRODUCT_DETAILS_SUCCESS,
  PRODUCT_DETAILS_FAILURE,
  PRODUCT_SEARCH_REQUEST,
  PRODUCT_SEARCH_SUCCESS,
  PRODUCT_SEARCH_FAILURE,
  PRODUCT_SHOULD_UPDATE_REQUEST,
  PRODUCT_SHOULD_UPDATE_SUCCESS,
  PRODUCT_SHOULD_UPDATE_FAILURE,
  IS_EMPTY_PRODUCT_CARD_ARRAY,
  IS_NOT_EMPTY_PRODUCT_CARD_ARRAY,
  SPECIAL_PRODUCT_REQUEST,
  SPECIAL_PRODUCT_SUCCESS,
  SPECIAL_PRODUCT_FAILURE,
  MOST_VISITED_PRODUCT_REQUEST,
  MOST_VISITED_PRODUCT_SUCCESS,
  MOST_VISITED_PRODUCT_FAILURE,
  BEST_SELLING_PRODUCT_REQUEST,
  BEST_SELLING_PRODUCT_SUCCESS,
  BEST_SELLING_PRODUCT_FAILURE,
  NEWEST_PRODUCT_REQUEST,
  NEWEST_PRODUCT_SUCCESS,
  NEWEST_PRODUCT_FAILURE,
  RELATED_PRODUCT_REQUEST,
  RELATED_PRODUCT_SUCCESS,
  RELATED_PRODUCT_FAILURE,
  COMMENT_PRODUCT_REQUEST,
  COMMENT_PRODUCT_SUCCESS,
  COMMENT_PRODUCT_FAILURE,
} from '../../../constants';

const initialState = {
  productDetailsLoading: false,
  productDetailsData: getResponseModel,
  productDetailsError: false,

  specialProductLoading: false,
  specialProductData: getResponseModel,
  specialProductError: false,

  mostVisitedProductLoading: false,
  mostVisitedProductData: getResponseModel,
  mostVisitedProductError: false,

  bestSellingProductLoading: false,
  bestSellingProductData: getResponseModel,
  bestSellingProductError: false,

  newestProductLoading: false,
  newestProductData: getResponseModel,
  newestProductError: false,

  relatedProductLoading: false,
  relatedProductData: getResponseModel,
  relatedProductError: false,

  commentProductLoading: false,
  commentProductData: getResponseModel,
  commentProductError: false,

  productSearchLoading: false,
  productSearchData: getResponseModel,
  productSearchError: false,

  productShouldUpdateLoading: false,
  productShouldUpdateError: false,

  emptyProductArray: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Product List ">
    case PRODUCT_DETAILS_REQUEST:
      return {
        ...state,
        productDetailsLoading: true,
        productDetailsError: false,
      };
    case PRODUCT_DETAILS_SUCCESS:
      return {
        ...state,
        productDetailsData: action.data,
        productDetailsLoading: false,
        productDetailsError: false,
      };
    case PRODUCT_DETAILS_FAILURE:
      return {
        ...state,
        productDetailsError: true,
        productDetailsLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product search">
    case PRODUCT_SEARCH_REQUEST:
      return {
        ...state,
        productSearchLoading: true,
        productSearchError: false,
      };
    case PRODUCT_SEARCH_SUCCESS:
      return {
        ...state,
        productSearchData: action.data,
        productSearchLoading: false,
        productSearchError: false,
      };
    case PRODUCT_SEARCH_FAILURE:
      return {
        ...state,
        productSearchError: true,
        productSearchLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="special Product  ">
    case SPECIAL_PRODUCT_REQUEST:
      return {
        ...state,
        specialProductLoading: true,
        specialProductError: false,
      };
    case SPECIAL_PRODUCT_SUCCESS:
      return {
        ...state,
        specialProductData: action.data,
        specialProductLoading: false,
        specialProductError: false,
      };
    case SPECIAL_PRODUCT_FAILURE:
      return {
        ...state,
        specialProductError: true,
        specialProductLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="Product should update">
    case PRODUCT_SHOULD_UPDATE_REQUEST:
      return {
        ...state,
        productShouldUpdateLoading: true,
        productShouldUpdateError: false,
      };
    case PRODUCT_SHOULD_UPDATE_SUCCESS:
      return {
        ...state,
        productShouldUpdateLoading: false,
        productShouldUpdateError: false,
      };
    case PRODUCT_SHOULD_UPDATE_FAILURE:
      return {
        ...state,
        productShouldUpdateError: true,
        productShouldUpdateLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="empty product card array in search">
    case IS_EMPTY_PRODUCT_CARD_ARRAY:
      return {
        ...state,
        emptyProductArray: true,
      };
    case IS_NOT_EMPTY_PRODUCT_CARD_ARRAY:
      return {
        ...state,
        emptyProductArray: false,
      };
    // </editor-fold>

    // <editor-fold dsc="most visited Product  ">
    case MOST_VISITED_PRODUCT_REQUEST:
      return {
        ...state,
        mostVisitedProductLoading: true,
        mostVisitedProductError: false,
      };
    case MOST_VISITED_PRODUCT_SUCCESS:
      return {
        ...state,
        mostVisitedProductData: action.data,
        mostVisitedProductLoading: false,
        mostVisitedProductError: false,
      };
    case MOST_VISITED_PRODUCT_FAILURE:
      return {
        ...state,
        mostVisitedProductError: true,
        mostVisitedProductLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="best selling Product  ">
    case BEST_SELLING_PRODUCT_REQUEST:
      return {
        ...state,
        bestSellingProductLoading: true,
        bestSellingProductError: false,
      };
    case BEST_SELLING_PRODUCT_SUCCESS:
      return {
        ...state,
        bestSellingProductData: action.data,
        bestSellingProductLoading: false,
        bestSellingProductError: false,
      };
    case BEST_SELLING_PRODUCT_FAILURE:
      return {
        ...state,
        bestSellingProductError: true,
        bestSellingProductLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="newest Product  ">
    case NEWEST_PRODUCT_REQUEST:
      return {
        ...state,
        newestProductLoading: true,
        newestProductError: false,
      };
    case NEWEST_PRODUCT_SUCCESS:
      return {
        ...state,
        newestProductData: action.data,
        newestProductLoading: false,
        newestProductError: false,
      };
    case NEWEST_PRODUCT_FAILURE:
      return {
        ...state,
        newestProductError: true,
        newestProductLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="related Product  ">
    case RELATED_PRODUCT_REQUEST:
      return {
        ...state,
        relatedProductLoading: true,
        relatedProductError: false,
      };
    case RELATED_PRODUCT_SUCCESS:
      return {
        ...state,
        relatedProductData: action.data,
        relatedProductLoading: false,
        relatedProductError: false,
      };
    case RELATED_PRODUCT_FAILURE:
      return {
        ...state,
        relatedProductError: true,
        relatedProductLoading: false,
      };
    // </editor-fold>

    // <editor-fold dsc="comment Product  ">
    case COMMENT_PRODUCT_REQUEST:
      return {
        ...state,
        commentProductLoading: true,
        commentProductError: false,
      };
    case COMMENT_PRODUCT_SUCCESS:
      return {
        ...state,
        commentProductData: action.data,
        commentProductLoading: false,
        commentProductError: false,
      };
    case COMMENT_PRODUCT_FAILURE:
      return {
        ...state,
        commentProductError: true,
        commentProductLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
