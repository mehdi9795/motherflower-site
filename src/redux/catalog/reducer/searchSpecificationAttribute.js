import { getResponseModel } from '../../../utils/model';
import {
  SEARCH_SPECIFICATION_ATTRIBUTE_REQUEST,
  SEARCH_SPECIFICATION_ATTRIBUTE_SUCCESS,
  SEARCH_SPECIFICATION_ATTRIBUTE_FAILURE,
} from '../../../constants';

const initialState = {
  searchSpecificationAttributeLoading: false,
  searchSpecificationAttributeData: getResponseModel,
  searchSpecificationAttributeError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Search Specification Attribute">
    case SEARCH_SPECIFICATION_ATTRIBUTE_REQUEST:
      return {
        ...state,
        searchSpecificationAttributeLoading: true,
        searchSpecificationAttributeError: false,
      };
    case SEARCH_SPECIFICATION_ATTRIBUTE_SUCCESS:
      return {
        ...state,
        searchSpecificationAttributeData: action.data,
        searchSpecificationAttributeLoading: false,
        searchSpecificationAttributeError: false,
      };
    case SEARCH_SPECIFICATION_ATTRIBUTE_FAILURE:
      return {
        ...state,
        searchSpecificationAttributeError: true,
        searchSpecificationAttributeLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
