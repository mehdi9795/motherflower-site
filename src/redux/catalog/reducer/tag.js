import { getResponseModel } from '../../../utils/model';
import {
  TAG_LIST_REQUEST,
  TAG_LIST_SUCCESS,
  TAG_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  tagListLoading: false,
  tagListData: getResponseModel,
  tagListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="tag List">
    case TAG_LIST_REQUEST:
      return {
        ...state,
        tagListLoading: true,
        tagListError: false,
      };
    case TAG_LIST_SUCCESS:
      return {
        ...state,
        tagListData: action.data,
        tagListLoading: false,
        tagListError: false,
      };
    case TAG_LIST_FAILURE:
      return {
        ...state,
        tagListError: true,
        tagListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
