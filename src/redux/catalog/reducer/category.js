import { getResponseModel } from '../../../utils/model';
import {
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  categoryListLoading: false,
  categoryListData: getResponseModel,
  categoryListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Category List">
    case CATEGORY_LIST_REQUEST:
      return {
        ...state,
        categoryListLoading: true,
        productDetailsError: false,
      };
    case CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        categoryListData: action.data,
        categoryListLoading: false,
        categoryListError: false,
      };
    case CATEGORY_LIST_FAILURE:
      return {
        ...state,
        categoryListError: true,
        categoryListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
