import { getResponseModel } from '../../../utils/model';
import {
  SEARCH_COLOR_OPTION_REQUEST,
  SEARCH_COLOR_OPTION_SUCCESS,
  SEARCH_COLOR_OPTION_FAILURE,
} from '../../../constants';

const initialState = {
  searchColorOptionLoading: false,
  searchColorOptionData: getResponseModel,
  searchColorOptionError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Search Color Option">
    case SEARCH_COLOR_OPTION_REQUEST:
      return {
        ...state,
        searchColorOptionLoading: true,
        searchColorOptionError: false,
      };
    case SEARCH_COLOR_OPTION_SUCCESS:
      return {
        ...state,
        searchColorOptionData: action.data,
        searchColorOptionLoading: false,
        searchColorOptionError: false,
      };
    case SEARCH_COLOR_OPTION_FAILURE:
      return {
        ...state,
        searchColorOptionError: true,
        searchColorOptionLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
