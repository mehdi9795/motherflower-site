import { getResponseModel } from '../../../utils/model';
import {
  BASKET_LIST_REQUEST,
  BASKET_LIST_SUCCESS,
  BASKET_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  basketListLoading: false,
  basketListData: getResponseModel,
  basketListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="basket List">
    case BASKET_LIST_REQUEST:
      return {
        ...state,
        basketListLoading: true,
        basketListError: false,
      };
    case BASKET_LIST_SUCCESS:
      return {
        ...state,
        basketListData: action.data,
        basketListLoading: false,
        basketListError: false,
      };
    case BASKET_LIST_FAILURE:
      return {
        ...state,
        basketListError: true,
        basketListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
