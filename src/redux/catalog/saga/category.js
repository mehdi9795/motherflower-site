import { call, put } from 'redux-saga/effects';
import {
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
} from '../../../constants';

export function* categoryListRequest(
  getDtoQueryString,
  getCategoryApi,
  action,
) {
  const container = getDtoQueryString(action.data);
  const response = yield call(getCategoryApi, container);
  if (response) {
    yield put({ type: CATEGORY_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: CATEGORY_LIST_FAILURE });
  }
}
