import { call, put } from 'redux-saga/effects';
import {
  SEARCH_SPECIFICATION_ATTRIBUTE_SUCCESS,
  SEARCH_SPECIFICATION_ATTRIBUTE_FAILURE,
} from '../../../constants';

export function* searchSpecificationAttributeRequest(
  getDtoQueryString,
  getSearchSpecificationAttributeApi,
  action,
) {
  const container = getDtoQueryString(action.data);
  const response = yield call(getSearchSpecificationAttributeApi, container);
  if (response) {
    yield put({
      type: SEARCH_SPECIFICATION_ATTRIBUTE_SUCCESS,
      data: response.data,
    });
  } else {
    yield put({ type: SEARCH_SPECIFICATION_ATTRIBUTE_FAILURE });
  }
}
