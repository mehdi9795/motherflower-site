import { call, put } from 'redux-saga/effects';
import {
  SEARCH_COLOR_OPTION_SUCCESS,
  SEARCH_COLOR_OPTION_FAILURE,
} from '../../../constants';

export function* searchColorOptionRequest(
  getDtoQueryString,
  getColorOptionApi,
  action,
) {
  const container = getDtoQueryString(action.data);
  const response = yield call(getColorOptionApi, container);
  if (response) {
    yield put({ type: SEARCH_COLOR_OPTION_SUCCESS, data: response.data });
  } else {
    yield put({ type: SEARCH_COLOR_OPTION_FAILURE });
  }
}
