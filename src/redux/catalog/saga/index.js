import { all, takeLatest } from 'redux-saga/effects';
import { getDtoQueryString } from '../../../utils/helper';
import {
  CATEGORY_LIST_REQUEST,
  PRODUCT_SEARCH_REQUEST,
  SEARCH_COLOR_OPTION_REQUEST,
  SEARCH_SPECIFICATION_ATTRIBUTE_REQUEST,
} from '../../../constants';
import {
  getCategoryApi,
  getColorOptionApi,
  getProductSearchApi,
  getSearchSpecificationAttributeApi,
} from '../../../services/catalogApi';
import { categoryListRequest } from './category';
import { searchColorOptionRequest } from './searchColorOption';
import { searchSpecificationAttributeRequest } from './searchSpecificationAttribute';
import { productSearchRequest } from './product';

// main saga generators
export default function* sagaCategoryIndex() {
  yield all([
    // <editor-fold dsc="Permission">
    takeLatest(
      CATEGORY_LIST_REQUEST,
      categoryListRequest,
      getDtoQueryString,
      getCategoryApi,
    ),

    takeLatest(
      SEARCH_COLOR_OPTION_REQUEST,
      searchColorOptionRequest,
      getDtoQueryString,
      getColorOptionApi,
    ),

    takeLatest(
      SEARCH_SPECIFICATION_ATTRIBUTE_REQUEST,
      searchSpecificationAttributeRequest,
      getDtoQueryString,
      getSearchSpecificationAttributeApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="product ">
    /*takeLatest(
      PRODUCT_SEARCH_REQUEST,
      productSearchRequest,
      getDtoQueryString,
      getProductSearchApi,
    ),*/
    // </editor-fold>
  ]);
}
