import { call, put } from 'redux-saga/effects';
import {
  PRODUCT_SEARCH_SUCCESS,
  PRODUCT_SEARCH_FAILURE,
  LOADING_SEARCH_HIDE_REQUEST,
} from '../../../constants';

export function* productSearchRequest(
  getDtoQueryString,
  getProductSearchApi,
  action,
) {
  const container = getDtoQueryString(action.data);
  const response = yield call(getProductSearchApi, container);
  if (response) {
    yield put({ type: LOADING_SEARCH_HIDE_REQUEST });
    yield put({ type: PRODUCT_SEARCH_SUCCESS, data: response.data });
  } else {
    yield put({ type: PRODUCT_SEARCH_FAILURE });
  }
}
