import {
  SINGLE_USER_REQUEST,
  SINGLE_USER_SUCCESS,
  SINGLE_USER_FAILURE,
  USER_EVENT_LIST_REQUEST,
  USER_EVENT_LIST_SUCCESS,
  USER_EVENT_LIST_FAILURE,
  USER_EVENT_TYPE_LIST_REQUEST,
  USER_EVENT_TYPE_LIST_SUCCESS,
  USER_EVENT_TYPE_LIST_FAILURE,
  USER_RELATION_SHIP_LIST_REQUEST,
  USER_RELATION_SHIP_LIST_SUCCESS,
  USER_RELATION_SHIP_LIST_FAILURE,
} from '../../../constants';
// <editor-fold dsc="single user">
export function singleUserRequest(data) {
  return {
    type: SINGLE_USER_REQUEST,
    data,
  };
}

export function singleUserSuccess(data) {
  return {
    type: SINGLE_USER_SUCCESS,
    data,
  };
}

export function singleUserFailure() {
  return {
    type: SINGLE_USER_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User Event">
export function userEventRequest(data) {
  return {
    type: USER_EVENT_LIST_REQUEST,
    data,
  };
}

export function userEventSuccess(data) {
  return {
    type: USER_EVENT_LIST_SUCCESS,
    data,
  };
}

export function userEventFailure() {
  return {
    type: USER_EVENT_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User Event Type">
export function userEventTypeRequest(data) {
  return {
    type: USER_EVENT_TYPE_LIST_REQUEST,
    data,
  };
}

export function userEventTypeSuccess(data) {
  return {
    type: USER_EVENT_TYPE_LIST_SUCCESS,
    data,
  };
}

export function userEventTypeFailure() {
  return {
    type: USER_EVENT_TYPE_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="User relation ship">
export function userRelationShipRequest(data) {
  return {
    type: USER_RELATION_SHIP_LIST_REQUEST,
    data,
  };
}

export function userRelationShipSuccess(data) {
  return {
    type: USER_RELATION_SHIP_LIST_SUCCESS,
    data,
  };
}

export function userRelationShipFailure() {
  return {
    type: USER_RELATION_SHIP_LIST_FAILURE,
  };
}
// </editor-fold>
