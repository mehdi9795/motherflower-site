import {
  ADDRESS_LIST_REQUEST,
  ADDRESS_LIST_SUCCESS,
  ADDRESS_LIST_FAILURE,
  ADDRESS_POST_REQUEST,
  ADDRESS_POST_SUCCESS,
  ADDRESS_POST_FAILURE,
  ADDRESS_PUT_REQUEST,
  ADDRESS_PUT_SUCCESS,
  ADDRESS_PUT_FAILURE,
  ADDRESS_DELETE_REQUEST,
  ADDRESS_DELETE_SUCCESS,
  ADDRESS_DELETE_FAILURE,
} from '../../../constants';

// <editor-fold dsc="address list">

export function addressListRequest() {
  return {
    type: ADDRESS_LIST_REQUEST,
  };
}

export function addressListSuccess(data) {
  return {
    type: ADDRESS_LIST_SUCCESS,
    data,
  };
}

export function addressListFailure() {
  return {
    type: ADDRESS_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="address post">

export function addressPostRequest() {
  return {
    type: ADDRESS_POST_REQUEST,
  };
}

export function addressPostSuccess(data) {
  return {
    type: ADDRESS_POST_SUCCESS,
    data,
  };
}

export function addressPostFailure() {
  return {
    type: ADDRESS_POST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="address put">

export function addressPutRequest() {
  return {
    type: ADDRESS_PUT_REQUEST,
  };
}

export function addressPutSuccess(data) {
  return {
    type: ADDRESS_PUT_SUCCESS,
    data,
  };
}

export function addressPutFailure() {
  return {
    type: ADDRESS_PUT_FAILURE,
  };
}
// </editor-fold>

// <editor-fold dsc="address delete">

export function addressDeleteRequest() {
  return {
    type: ADDRESS_DELETE_REQUEST,
  };
}

export function addressDeleteSuccess(data) {
  return {
    type: ADDRESS_DELETE_SUCCESS,
    data,
  };
}

export function addressDeleteFailure() {
  return {
    type: ADDRESS_DELETE_FAILURE,
  };
}
// </editor-fold>
