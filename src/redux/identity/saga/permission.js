import { call, put, select } from 'redux-saga/effects';
import {
  PERMISSION_LIST_SUCCESS,
  PERMISSION_LIST_FAILURE,
} from '../../../constants';
import { setCookie } from '../../../utils/index';

const getCurrentSession = state => state.account.loginData;

export function* permissionListRequest(
  getDtoQueryString,
  getPermissionApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);

  const container = getDtoQueryString(action.data);
  const response = yield call(
    getPermissionApi,
    currentSession.token,
    container,
  );

  if (response) {
    const authenticationCookieExpireHours = 8760;
    setCookie('permission', response.data, authenticationCookieExpireHours);

    yield put({ type: PERMISSION_LIST_SUCCESS, data: response.data.listDto });
  } else {
    yield put({ type: PERMISSION_LIST_FAILURE });
  }
}
