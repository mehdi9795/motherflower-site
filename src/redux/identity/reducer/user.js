import {
  SINGLE_USER_REQUEST,
  SINGLE_USER_SUCCESS,
  SINGLE_USER_FAILURE,
  USER_EVENT_LIST_REQUEST,
  USER_EVENT_LIST_SUCCESS,
  USER_EVENT_LIST_FAILURE,
  USER_EVENT_TYPE_LIST_REQUEST,
  USER_EVENT_TYPE_LIST_SUCCESS,
  USER_EVENT_TYPE_LIST_FAILURE,
  USER_RELATION_SHIP_LIST_REQUEST,
  USER_RELATION_SHIP_LIST_SUCCESS,
  USER_RELATION_SHIP_LIST_FAILURE,
} from '../../../constants/index';

const initialState = {
  singleUserRequest: false,
  singleUserData: {},
  singleUserFailure: false,

  userEventRequest: false,
  userEventData: {},
  userEventFailure: false,

  userEventTypeRequest: false,
  userEventTypeData: {},
  userEventTypeFailure: false,

  userRelationShipRequest: false,
  userRelationShipData: {},
  userRelationShipFailure: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="user get">
    case SINGLE_USER_REQUEST:
      return {
        ...state,
        singleUserRequest: true,
        singleUserFailure: false,
      };
    case SINGLE_USER_SUCCESS:
      return {
        ...state,
        singleUserData: action.data,
        singleUserRequest: false,
        singleUserFailure: false,
      };
    case SINGLE_USER_FAILURE:
      return {
        ...state,
        singleUserFailure: true,
        singleUserRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="user event">
    case USER_EVENT_LIST_REQUEST:
      return {
        ...state,
        userEventRequest: true,
        userEventFailure: false,
      };
    case USER_EVENT_LIST_SUCCESS:
      return {
        ...state,
        userEventData: action.data,
        userEventRequest: false,
        userEventFailure: false,
      };
    case USER_EVENT_LIST_FAILURE:
      return {
        ...state,
        userEventFailure: true,
        userEventRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="user event type">
    case USER_EVENT_TYPE_LIST_REQUEST:
      return {
        ...state,
        userEventTypeRequest: true,
        userEventTypeFailure: false,
      };
    case USER_EVENT_TYPE_LIST_SUCCESS:
      return {
        ...state,
        userEventTypeData: action.data,
        userEventTypeRequest: false,
        userEventTypeFailure: false,
      };
    case USER_EVENT_TYPE_LIST_FAILURE:
      return {
        ...state,
        userEventTypeFailure: true,
        userEventTypeRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="user relation ship">
    case USER_RELATION_SHIP_LIST_REQUEST:
      return {
        ...state,
        userRelationShipRequest: true,
        userRelationShipFailure: false,
      };
    case USER_RELATION_SHIP_LIST_SUCCESS:
      return {
        ...state,
        userRelationShipData: action.data,
        userRelationShipRequest: false,
        userRelationShipFailure: false,
      };
    case USER_RELATION_SHIP_LIST_FAILURE:
      return {
        ...state,
        userRelationShipFailure: true,
        userRelationShipRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
