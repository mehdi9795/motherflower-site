import {
  PERMISSION_LIST_REQUEST,
  PERMISSION_LIST_SUCCESS,
  PERMISSION_LIST_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  permissionListRequest: false,
  permissionListData: getResponseModel,
  permissionListFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Role List new">
    case PERMISSION_LIST_REQUEST:
      return {
        ...state,
        permissionListRequest: true,
        permissionListFailure: false,
      };
    case PERMISSION_LIST_SUCCESS:
      return {
        ...state,
        permissionListData: action.data,
        permissionListRequest: false,
        permissionListFailure: false,
      };
    case PERMISSION_LIST_FAILURE:
      return {
        ...state,
        permissionListFailure: true,
        permissionListRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
