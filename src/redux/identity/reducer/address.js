import {
  ADDRESS_LIST_REQUEST,
  ADDRESS_LIST_SUCCESS,
  ADDRESS_LIST_FAILURE,
  ADDRESS_POST_REQUEST,
  ADDRESS_POST_SUCCESS,
  ADDRESS_POST_FAILURE,
  ADDRESS_PUT_REQUEST,
  ADDRESS_PUT_SUCCESS,
  ADDRESS_PUT_FAILURE,
  ADDRESS_DELETE_REQUEST,
  ADDRESS_DELETE_SUCCESS,
  ADDRESS_DELETE_FAILURE,
} from '../../../constants/index';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  addressListRequest: false,
  addressListData: getResponseModel,
  addressListFailure: false,

  addressPostData: getResponseModel,
  addressPostRequest: false,
  addressPostFailure: false,

  addressPutData: getResponseModel,
  addressPutRequest: false,
  addressPutFailure: false,

  addressDeleteData: getResponseModel,
  addressDeleteRequest: false,
  addressDeleteFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="address List ">
    case ADDRESS_LIST_REQUEST:
      return {
        ...state,
        addressListRequest: true,
        addressListFailure: false,
      };
    case ADDRESS_LIST_SUCCESS:
      return {
        ...state,
        addressListData: action.data,
        addressListRequest: false,
        addressListFailure: false,
      };
    case ADDRESS_LIST_FAILURE:
      return {
        ...state,
        addressListFailure: true,
        addressListRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="address Post ">
    case ADDRESS_POST_REQUEST:
      return {
        ...state,
        addressPostRequest: true,
        addressPostFailure: false,
      };
    case ADDRESS_POST_SUCCESS:
      return {
        ...state,
        addressPostData: action.data,
        addressPostRequest: false,
        addressPostFailure: false,
      };
    case ADDRESS_POST_FAILURE:
      return {
        ...state,
        addressPostFailure: true,
        addressPostRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="address Put ">
    case ADDRESS_PUT_REQUEST:
      return {
        ...state,
        addressPutRequest: true,
        addressPutFailure: false,
      };
    case ADDRESS_PUT_SUCCESS:
      return {
        ...state,
        addressPutData: action.data,
        addressPutRequest: false,
        addressPutFailure: false,
      };
    case ADDRESS_PUT_FAILURE:
      return {
        ...state,
        addressPutFailure: true,
        addressPutRequest: false,
      };
    // </editor-fold>

    // <editor-fold dsc="address delete ">
    case ADDRESS_DELETE_REQUEST:
      return {
        ...state,
        addressDeleteRequest: true,
        addressDeleteFailure: false,
      };
    case ADDRESS_DELETE_SUCCESS:
      return {
        ...state,
        addressDeleteData: action.data,
        addressDeleteRequest: false,
        addressDeleteFailure: false,
      };
    case ADDRESS_DELETE_FAILURE:
      return {
        ...state,
        addressDeleteFailure: true,
        addressDeleteRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
