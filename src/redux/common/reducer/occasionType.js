import {
  OCCASION_TYPE_LIST_REQUEST,
  OCCASION_TYPE_LIST_SUCCESS,
  OCCASION_TYPE_LIST_FAILURE,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  occasionTypeListRequest: false,
  occasionTypeListData: getResponseModel,
  occasionTypeListFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="City List new">
    case OCCASION_TYPE_LIST_REQUEST:
      return {
        ...state,
        occasionTypeListRequest: true,
        occasionTypeListFailure: false,
      };
    case OCCASION_TYPE_LIST_SUCCESS:
      return {
        ...state,
        occasionTypeListData: action.data,
        occasionTypeListRequest: false,
        occasionTypeListFailure: false,
      };
    case OCCASION_TYPE_LIST_FAILURE:
      return {
        ...state,
        occasionTypeListFailure: true,
        occasionTypeListRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
