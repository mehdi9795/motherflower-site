import {
  DISTRICT_OPTION_LIST_REQUEST,
  DISTRICT_OPTION_LIST_SUCCESS,
  DISTRICT_OPTION_LIST_FAILURE,
  SINGLE_DISTRICT_REQUEST,
  SINGLE_DISTRICT_SUCCESS,
  SINGLE_DISTRICT_FAILURE,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  districtOptionRequest: false,
  districtOptionData: getResponseModel,
  districtOptionError: false,

  singleDistrictRequest: false,
  singleDistrictData: getResponseModel,
  singleDistrictError: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="District List">
    case DISTRICT_OPTION_LIST_REQUEST:
      return {
        ...state,
        districtOptionRequest: true,
      };
    case DISTRICT_OPTION_LIST_SUCCESS:
      return {
        ...state,
        districtOptionRequest: false,
        districtOptionData: action.data,
      };
    case DISTRICT_OPTION_LIST_FAILURE:
      return {
        ...state,
        districtOptionRequest: false,
        districtOptionError: true,
      };
    // </editor-fold>

    // <editor-fold desc="District sigle">
    case SINGLE_DISTRICT_REQUEST:
      return {
        ...state,
        singleDistrictRequest: true,
      };
    case SINGLE_DISTRICT_SUCCESS:
      return {
        ...state,
        singleDistrictRequest: false,
        singleDistrictData: action.data,
      };
    case SINGLE_DISTRICT_FAILURE:
      return {
        ...state,
        singleDistrictRequest: false,
        singleDistrictError: true,
      };
    // </editor-fold>
    default:
      return state;
  }
}
