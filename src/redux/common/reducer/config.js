import {
  CONFIG_LIST_REQUEST,
  CONFIG_LIST_SUCCESS,
  CONFIG_LIST_FAILURE,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  configListRequest: false,
  configListData: getResponseModel,
  configListError: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="config List">
    case CONFIG_LIST_REQUEST:
      return {
        ...state,
        configListRequest: true,
      };
    case CONFIG_LIST_SUCCESS:
      return {
        ...state,
        configListRequest: false,
        configListData: action.data,
      };
    case CONFIG_LIST_FAILURE:
      return {
        ...state,
        configListRequest: false,
        configListError: true,
      };
    // </editor-fold>

    default:
      return state;
  }
}
