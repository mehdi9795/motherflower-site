import {
  OCCASION_TYPE_LIST_REQUEST,
  OCCASION_TYPE_LIST_SUCCESS,
  OCCASION_TYPE_LIST_FAILURE,
} from '../../../constants';

// <editor-fold desc="city List">
export function occasionTypeListRequest(data) {
  return {
    type: OCCASION_TYPE_LIST_REQUEST,
    data,
  };
}
export function occasionTypeListSuccess(data) {
  return {
    type: OCCASION_TYPE_LIST_SUCCESS,
    data,
  };
}
export function occasionTypeListFailure() {
  return {
    type: OCCASION_TYPE_LIST_FAILURE,
  };
}
// </editor-fold>
