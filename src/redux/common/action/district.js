import {
  DISTRICT_OPTION_LIST_REQUEST,
  DISTRICT_OPTION_LIST_SUCCESS,
  DISTRICT_OPTION_LIST_FAILURE,
  SINGLE_DISTRICT_REQUEST,
  SINGLE_DISTRICT_SUCCESS,
  SINGLE_DISTRICT_FAILURE,
} from '../../../constants';

// <editor-fold desc="District option List">
export function districtOptionListRequest(data) {
  return {
    type: DISTRICT_OPTION_LIST_REQUEST,
    data,
  };
}

export function districtOptionListSuccess(data) {
  return {
    type: DISTRICT_OPTION_LIST_SUCCESS,
    data,
  };
}

export function districtOptionListFailure() {
  return {
    type: DISTRICT_OPTION_LIST_FAILURE,
  };
}
// </editor-fold>

// <editor-fold desc="District  single">
export function singleDistrictRequest(data) {
  return {
    type: SINGLE_DISTRICT_REQUEST,
    data,
  };
}

export function singleDistrictSuccess(data) {
  return {
    type: SINGLE_DISTRICT_SUCCESS,
    data,
  };
}

export function singleDistrictFailure() {
  return {
    type: SINGLE_DISTRICT_FAILURE,
  };
}
// </editor-fold>
