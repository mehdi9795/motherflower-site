import {
  CONFIG_LIST_REQUEST,
  CONFIG_LIST_SUCCESS,
  CONFIG_LIST_FAILURE,
} from '../../../constants';

// <editor-fold desc="config List">
export function configListRequest(data) {
  return {
    type: CONFIG_LIST_REQUEST,
    data,
  };
}

export function configListSuccess(data) {
  return {
    type: CONFIG_LIST_SUCCESS,
    data,
  };
}

export function configListFailure() {
  return {
    type: CONFIG_LIST_FAILURE,
  };
}
// </editor-fold>
