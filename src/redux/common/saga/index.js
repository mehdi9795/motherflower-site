import { all, takeLatest } from 'redux-saga/effects';
import { getDtoQueryString } from '../../../utils/helper';
import {
  DISTRICT_OPTION_LIST_REQUEST,
  SINGLE_DISTRICT_REQUEST,
} from '../../../constants';
import { districtOptionListRequest, singleDistrictRequest } from './district';
import { getDistrictApi } from '../../../services/commonApi';

export default function* sagaCommonIndex() {
  // <editor-fold dsc="District">
  yield all([
    takeLatest(
      DISTRICT_OPTION_LIST_REQUEST,
      districtOptionListRequest,
      getDtoQueryString,
      getDistrictApi,
    ),
  ]);

  yield all([
    takeLatest(
      SINGLE_DISTRICT_REQUEST,
      singleDistrictRequest,
      getDtoQueryString,
      getDistrictApi,
    ),
  ]);
  // </editor-fold>
}
