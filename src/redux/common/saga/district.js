import { call, put } from 'redux-saga/effects';
import {
  DISTRICT_OPTION_LIST_SUCCESS,
  DISTRICT_OPTION_LIST_FAILURE,
  SINGLE_DISTRICT_SUCCESS,
  SINGLE_DISTRICT_FAILURE,
} from '../../../constants';

// eslint-disable-next-line import/prefer-default-export
export function* districtOptionListRequest(
  getDtoQueryString,
  getDistrictApi,
  action,
) {
  const container = getDtoQueryString(action.data);

  const response = yield call(getDistrictApi, container);
  if (response) {
    yield put({ type: DISTRICT_OPTION_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: DISTRICT_OPTION_LIST_FAILURE });
  }
}

export function* singleDistrictRequest(
  getDtoQueryString,
  getDistrictApi,
  action,
) {
  const container = getDtoQueryString(action.data);

  const response = yield call(getDistrictApi, container);
  if (response) {
    yield put({ type: SINGLE_DISTRICT_SUCCESS, data: response.data });
  } else {
    yield put({ type: SINGLE_DISTRICT_FAILURE });
  }
}
