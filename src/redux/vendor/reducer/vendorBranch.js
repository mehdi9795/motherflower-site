import {
  VENDOR_BRANCH_LIST_REQUEST,
  VENDOR_BRANCH_LIST_SUCCESS,
  VENDOR_BRANCH_LIST_FAILURE,
} from '../../../constants';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchListRequest: false,
  vendorBranchListData: getResponseModel,
  vendorBranchListFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="vendor branch list">
    case VENDOR_BRANCH_LIST_REQUEST:
      return {
        ...state,
        vendorBranchListRequest: true,
        vendorBranchListFailure: false,
      };
    case VENDOR_BRANCH_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchListData: action.data,
        vendorBranchListRequest: false,
        vendorBranchListFailure: false,
      };
    case VENDOR_BRANCH_LIST_FAILURE:
      return {
        ...state,
        vendorBranchListFailure: true,
        vendorBranchListRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
