import {
  VENDORBRANCH_ZONE_LIST_REQUEST,
  VENDORBRANCH_ZONE_LIST_SUCCESS,
  VENDORBRANCH_ZONE_LIST_FAILURE,
} from '../../../constants/index';

import { getResponseModel } from '../../../utils/model';

const initialState = {
  vendorBranchZoneRequest: false,
  vendorBranchZoneData: getResponseModel,
  vendorBranchZoneError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold desc="vendor branch zone list">
    case VENDORBRANCH_ZONE_LIST_REQUEST:
      return {
        ...state,
        vendorBranchZoneRequest: true,
        vendorBranchZoneError: false,
      };
    case VENDORBRANCH_ZONE_LIST_SUCCESS:
      return {
        ...state,
        vendorBranchZoneData: action.data,
        vendorBranchZoneRequest: false,
        vendorBranchZoneError: false,
      };
    case VENDORBRANCH_ZONE_LIST_FAILURE:
      return {
        ...state,
        vendorBranchZoneError: true,
        vendorBranchZoneRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
