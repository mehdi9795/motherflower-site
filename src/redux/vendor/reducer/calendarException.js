import {
  CALENDAR_EXCEPTION_LIST_REQUEST,
  CALENDAR_EXCEPTION_LIST_SUCCESS,
  CALENDAR_EXCEPTION_LIST_FAILURE,
} from '../../../constants';
import { getResponseModel } from '../../../utils/model';

const initialState = {
  calendarExceptionListRequest: false,
  calendarExceptionListData: getResponseModel,
  calendarExceptionListFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Role List new">
    case CALENDAR_EXCEPTION_LIST_REQUEST:
      return {
        ...state,
        calendarExceptionListRequest: true,
        calendarExceptionListFailure: false,
      };
    case CALENDAR_EXCEPTION_LIST_SUCCESS:
      return {
        ...state,
        calendarExceptionListData: action.data,
        calendarExceptionListRequest: false,
        calendarExceptionListFailure: false,
      };
    case CALENDAR_EXCEPTION_LIST_FAILURE:
      return {
        ...state,
        calendarExceptionListFailure: true,
        calendarExceptionListRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
