import {
  AVAILABLE_PRODUCT_SHIFT_REQUEST,
  AVAILABLE_PRODUCT_SHIFT_SUCCESS,
  AVAILABLE_PRODUCT_SHIFT_FAILURE,
} from '../../../constants';

const initialState = {
  availableProductShiftRequest: false,
  availableProductShiftData: null,
  availableProductShiftFailure: false,
};
export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="available product shift">
    case AVAILABLE_PRODUCT_SHIFT_REQUEST:
      return {
        ...state,
        availableProductShiftRequest: true,
        availableProductShiftFailure: false,
      };
    case AVAILABLE_PRODUCT_SHIFT_SUCCESS:
      return {
        ...state,
        availableProductShiftData: action.data,
        availableProductShiftRequest: false,
        availableProductShiftFailure: false,
      };
    case AVAILABLE_PRODUCT_SHIFT_FAILURE:
      return {
        ...state,
        availableProductShiftFailure: true,
        availableProductShiftRequest: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
