import { call, put } from 'redux-saga/effects';
import {
  AVAILABLE_PRODUCT_SHIFT_SUCCESS,
  AVAILABLE_PRODUCT_SHIFT_FAILURE,
} from '../../../constants';

export function* availableProductShiftRequest(
  getDtoQueryString,
  getAvailableProductShiftApi,
  action,
) {
  const container = getDtoQueryString(action.data);
  const response = yield call(getAvailableProductShiftApi, container);
  if (response) {
    yield put({ type: AVAILABLE_PRODUCT_SHIFT_SUCCESS, data: response.data });
  } else {
    yield put({ type: AVAILABLE_PRODUCT_SHIFT_FAILURE });
  }
}
