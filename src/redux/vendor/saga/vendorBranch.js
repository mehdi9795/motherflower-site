import { call, put } from 'redux-saga/effects';
import {
  VENDOR_BRANCH_LIST_SUCCESS,
  VENDOR_BRANCH_LIST_FAILURE,
} from '../../../constants';

export function* vendorBranchListRequest(
  getDtoQueryString,
  getVendorBranchListApi,
  action,
) {
  const container = getDtoQueryString(action.data);
  const response = yield call(getVendorBranchListApi, container);
  if (response) {
    yield put({ type: VENDOR_BRANCH_LIST_SUCCESS, data: response.data });
  } else {
    yield put({ type: VENDOR_BRANCH_LIST_FAILURE });
  }
}
