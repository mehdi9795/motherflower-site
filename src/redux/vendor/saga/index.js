import { all, takeLatest } from 'redux-saga/effects';
import { getDtoQueryString } from '../../../utils/helper';
import {
  AVAILABLE_PRODUCT_SHIFT_REQUEST,
  VENDOR_BRANCH_LIST_REQUEST,
} from '../../../constants';
import {
  getAvailableProductShiftApi,
  getVendorBranchApi,
} from '../../../services/vendorApi';
import { availableProductShiftRequest } from './availableProductShift';
import { vendorBranchListRequest } from './vendorBranch';

// main saga generators
export default function* sagaVendorIndex() {
  yield all([
    // <editor-fold dsc="available product ">
    takeLatest(
      AVAILABLE_PRODUCT_SHIFT_REQUEST,
      availableProductShiftRequest,
      getDtoQueryString,
      getAvailableProductShiftApi,
    ),
    // </editor-fold>

    // <editor-fold dsc="vendor branch ">
    takeLatest(
      VENDOR_BRANCH_LIST_REQUEST,
      vendorBranchListRequest,
      getDtoQueryString,
      getVendorBranchApi,
    ),
    // </editor-fold>
  ]);
}
