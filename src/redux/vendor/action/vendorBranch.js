import {
  VENDOR_BRANCH_LIST_REQUEST,
  VENDOR_BRANCH_LIST_SUCCESS,
  VENDOR_BRANCH_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Role new">

export function vendorBranchListRequest() {
  return {
    type: VENDOR_BRANCH_LIST_REQUEST,
  };
}

export function vendorBranchListSuccess(data) {
  return {
    type: VENDOR_BRANCH_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchListFailure() {
  return {
    type: VENDOR_BRANCH_LIST_FAILURE,
  };
}
// </editor-fold>
