import {
  VENDORBRANCH_ZONE_LIST_REQUEST,
  VENDORBRANCH_ZONE_LIST_SUCCESS,
  VENDORBRANCH_ZONE_LIST_FAILURE,
} from '../../../constants/index';

// <editor-fold dsc="vendor Branch Zone List">
export function vendorBranchZoneListRequest(data) {
  return {
    type: VENDORBRANCH_ZONE_LIST_REQUEST,
    data,
  };
}

export function vendorBranchZoneListSuccess(data) {
  return {
    type: VENDORBRANCH_ZONE_LIST_SUCCESS,
    data,
  };
}

export function vendorBranchZoneListFailure() {
  return {
    type: VENDORBRANCH_ZONE_LIST_FAILURE,
  };
}
// </editor-fold>
