import {
  CALENDAR_EXCEPTION_LIST_REQUEST,
  CALENDAR_EXCEPTION_LIST_SUCCESS,
  CALENDAR_EXCEPTION_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Role new">

export function calendarExceptionListRequest() {
  return {
    type: CALENDAR_EXCEPTION_LIST_REQUEST,
  };
}

export function calendarExceptionListSuccess(data) {
  return {
    type: CALENDAR_EXCEPTION_LIST_SUCCESS,
    data,
  };
}

export function calendarExceptionListFailure() {
  return {
    type: CALENDAR_EXCEPTION_LIST_FAILURE,
  };
}
// </editor-fold>
