import {
  AVAILABLE_PRODUCT_SHIFT_REQUEST,
  AVAILABLE_PRODUCT_SHIFT_SUCCESS,
  AVAILABLE_PRODUCT_SHIFT_FAILURE,
} from '../../../constants';

// <editor-fold dsc="Role new">

export function availableProductShiftRequest(data) {
  return {
    type: AVAILABLE_PRODUCT_SHIFT_REQUEST,
    data,
  };
}

export function availableProductShiftSuccess(data) {
  return {
    type: AVAILABLE_PRODUCT_SHIFT_SUCCESS,
    data,
  };
}

export function availableProductShiftFailure() {
  return {
    type: AVAILABLE_PRODUCT_SHIFT_FAILURE,
  };
}
// </editor-fold>
