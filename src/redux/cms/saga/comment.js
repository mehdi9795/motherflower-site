import { call, put, select } from 'redux-saga/effects';
import { COMMENT_POST_SUCCESS, COMMENT_POST_FAILURE } from '../../../constants';
import {
  COMMENT_CREATE_IS_FAILURE,
  COMMENT_CREATE_IS_SUCCESS,
} from '../../../Resources/Localization';
import { showNotification } from '../../../utils/helper';

const getCurrentSession = state => state.account.loginData;

export function* commentPostRequest(
  postCommentApi,
  postAuthCommentApi,
  action,
) {
  const currentSession = yield select(getCurrentSession);
  let response = null;
  if (action.data.isLogin)
    response = yield call(postAuthCommentApi, currentSession.token, action.data.data);
  else response = yield call(postCommentApi, action.data.data);
  if (response.status === 200) {
    yield put({
      type: COMMENT_POST_SUCCESS,
    });
  } else {
    yield put({
      type: COMMENT_POST_FAILURE,
    });
  }
}
export function commentPostSuccess() {
  showNotification('success', '', COMMENT_CREATE_IS_SUCCESS, 8);
}

export function commentPostFailure() {
  showNotification('error', '', COMMENT_CREATE_IS_FAILURE, 8, 'errorsBox');
}
