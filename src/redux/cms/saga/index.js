import { all, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import {
  COMMENT_POST_REQUEST,
  COMMENT_POST_SUCCESS,
  COMMENT_POST_FAILURE,
} from '../../../constants';

import {postAuthCommentApi, postCommentApi} from '../../../services/cmsApi';
import {
  commentPostFailure,
  commentPostRequest,
  commentPostSuccess,
} from './comment';

export default function* sagaCommonIndex() {
  yield all([
    takeLatest(COMMENT_POST_REQUEST, commentPostRequest, postCommentApi, postAuthCommentApi),
    takeLatest(COMMENT_POST_SUCCESS, commentPostSuccess, message),
    takeLatest(COMMENT_POST_FAILURE, commentPostFailure, message),
  ]);
}
