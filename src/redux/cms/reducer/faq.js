import { getResponseModel } from '../../../utils/model';
import {
  FAQ_LIST_REQUEST,
  FAQ_LIST_SUCCESS,
  FAQ_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  faqListLoading: false,
  faqListData: getResponseModel,
  faqListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Faq List">
    case FAQ_LIST_REQUEST:
      return {
        ...state,
        faqListLoading: true,
        faqListError: false,
      };
    case FAQ_LIST_SUCCESS:
      return {
        ...state,
        faqListData: action.data,
        faqListLoading: false,
        faqListError: false,
      };
    case FAQ_LIST_FAILURE:
      return {
        ...state,
        faqListError: true,
        faqListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
