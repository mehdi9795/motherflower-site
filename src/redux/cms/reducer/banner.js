import { getResponseModel } from '../../../utils/model';
import {
  BANNER_LIST_REQUEST,
  BANNER_LIST_SUCCESS,
  BANNER_LIST_FAILURE,
} from '../../../constants';

const initialState = {
  bannerListLoading: false,
  bannerListData: getResponseModel,
  bannerListError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="banner List">
    case BANNER_LIST_REQUEST:
      return {
        ...state,
        bannerListLoading: true,
        bannerListError: false,
      };
    case BANNER_LIST_SUCCESS:
      return {
        ...state,
        bannerListData: action.data,
        bannerListLoading: false,
        bannerListError: false,
      };
    case BANNER_LIST_FAILURE:
      return {
        ...state,
        bannerListError: true,
        bannerListLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
