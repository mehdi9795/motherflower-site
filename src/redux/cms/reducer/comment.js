import { getResponseModel } from '../../../utils/model';
import {
  COMMENT_POST_REQUEST,
  COMMENT_POST_SUCCESS,
  COMMENT_POST_FAILURE,
} from '../../../constants';

const initialState = {
  commentPostLoading: false,
  commentPostData: getResponseModel,
  commentPostError: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Faq List">
    case COMMENT_POST_REQUEST:
      return {
        ...state,
        commentPostLoading: true,
        commentPostError: false,
      };
    case COMMENT_POST_SUCCESS:
      return {
        ...state,
        commentPostData: action.data,
        commentPostLoading: false,
        commentPostError: false,
      };
    case COMMENT_POST_FAILURE:
      return {
        ...state,
        commentPostError: true,
        commentPostLoading: false,
      };
    // </editor-fold>
    default:
      return state;
  }
}
