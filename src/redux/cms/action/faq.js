import {
  FAQ_LIST_REQUEST,
  FAQ_LIST_SUCCESS,
  FAQ_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="faq">
export function faqListRequest() {
  return {
    type: FAQ_LIST_REQUEST,
  };
}

export function faqListSuccess(data) {
  return {
    type: FAQ_LIST_SUCCESS,
    data,
  };
}

export function faqListFailure() {
  return {
    type: FAQ_LIST_FAILURE,
  };
}
// </editor-fold>
