import {
  BANNER_LIST_REQUEST,
  BANNER_LIST_SUCCESS,
  BANNER_LIST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="banner">
export function bannerListRequest(data) {
  return {
    type: BANNER_LIST_REQUEST,
    data,
  };
}

export function bannerListSuccess(data) {
  return {
    type: BANNER_LIST_SUCCESS,
    data,
  };
}

export function bannerListFailure() {
  return {
    type: BANNER_LIST_FAILURE,
  };
}
// </editor-fold>
