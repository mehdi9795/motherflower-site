import {
  COMMENT_POST_REQUEST,
  COMMENT_POST_SUCCESS,
  COMMENT_POST_FAILURE,
} from '../../../constants';

// <editor-fold dsc="comment">
export function commentPostRequest(data) {
  return {
    type: COMMENT_POST_REQUEST,
    data,
  };
}

export function commentPostSuccess(data) {
  return {
    type: COMMENT_POST_SUCCESS,
    data,
  };
}

export function commentPostFailure() {
  return {
    type: COMMENT_POST_FAILURE,
  };
}
// </editor-fold>
