import {
  LOADING_SEARCH_SHOW_REQUEST,
  LOADING_SEARCH_HIDE_REQUEST,
  NAVIGATION_LOADING_REQUEST,
  NAVIGATION_LOADING_SUCCESS
} from '../../../constants/index';

const initialState = {
  loadingSearch: false,
  navigationLoading: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="Public Loading">
    case LOADING_SEARCH_SHOW_REQUEST:
      return {
        ...state,
        loadingSearch: true,
      };
    case LOADING_SEARCH_HIDE_REQUEST:
      return {
        ...state,
        loadingSearch: false,
      };

    case NAVIGATION_LOADING_REQUEST:
      return {
        ...state,
        navigationLoading: true,
      };
    case NAVIGATION_LOADING_SUCCESS:
      return {
        ...state,
        navigationLoading: false,
      };

    // </editor-fold>

    default:
      return state;
  }
}
