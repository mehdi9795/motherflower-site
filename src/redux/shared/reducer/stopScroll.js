import {
  STOP_SCROLL_ACTIVE_REQUEST,
  STOP_SCROLL_DEACTIVE_REQUEST,
} from '../../../constants/index';

const initialState = {
  stopScroll: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="stop scroll">
    case STOP_SCROLL_ACTIVE_REQUEST:
      return {
        ...state,
        stopScroll: true,
      };
    case STOP_SCROLL_DEACTIVE_REQUEST:
      return {
        ...state,
        stopScroll: false,
      };

    // </editor-fold>

    default:
      return state;
  }
}
