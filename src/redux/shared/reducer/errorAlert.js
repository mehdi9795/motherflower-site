import {
  SHOW_ALERT_REQUEST,
  HIDE_ALERT_REQUEST,
} from '../../../constants/index';

const initialState = {
  showAlertRequest: false,
  message: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="alert error">
    case SHOW_ALERT_REQUEST:
      return {
        ...state,
        showAlertRequest: true,
        message: action.data,
      };
    case HIDE_ALERT_REQUEST:
      return {
        message: '',
        showAlertRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
