import {
  SHOW_ADD_TO_CARD_REQUEST,
  HIDE_ADD_TO_CARD_REQUEST,
} from '../../../constants/index';

const initialState = {
  showAddToCardRequest: false,
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="alert error">
    case SHOW_ADD_TO_CARD_REQUEST:
      return {
        ...state,
        showAddToCardRequest: true,
      };
    case HIDE_ADD_TO_CARD_REQUEST:
      return {
        ...state,
        showAddToCardRequest: false,
      };
    // </editor-fold>

    default:
      return state;
  }
}
