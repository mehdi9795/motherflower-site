import { CHANGE_MENU_PROFILE } from '../../../constants/index';

const initialState = {
  value: '',
};

export default function runtime(state = initialState, action) {
  switch (action.type) {
    // <editor-fold dsc="alert error">
    case CHANGE_MENU_PROFILE:
      return {
        ...state,
        value: action.data,
      };
    // </editor-fold>

    default:
      return state;
  }
}
