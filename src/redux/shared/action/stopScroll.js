import {
  STOP_SCROLL_ACTIVE_REQUEST,
  STOP_SCROLL_DEACTIVE_REQUEST,
} from '../../../constants/index';

// <editor-fold dsc="Stop scroll">
export function stopScrollActiveRequest() {
  return {
    type: STOP_SCROLL_ACTIVE_REQUEST,
  };
}

export function stopScrollDeActiveRequest() {
  return {
    type: STOP_SCROLL_DEACTIVE_REQUEST,
  };
}

// </editor-fold>
