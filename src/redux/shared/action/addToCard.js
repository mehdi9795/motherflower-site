import {
  SHOW_ADD_TO_CARD_REQUEST,
  HIDE_ADD_TO_CARD_REQUEST,
} from '../../../constants/index';

export function showAddToCardRequest(data) {
  return {
    type: SHOW_ADD_TO_CARD_REQUEST,
    data,
  };
}

export function hideAddToCardRequest() {
  return {
    type: HIDE_ADD_TO_CARD_REQUEST,
  };
}
