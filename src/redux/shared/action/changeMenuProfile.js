import { CHANGE_MENU_PROFILE } from '../../../constants/index';

export function changeMenuProfileSuccess(data) {
  return {
    type: CHANGE_MENU_PROFILE,
    data,
  };
}
