import {
  LOADING_SEARCH_SHOW_REQUEST,
  LOADING_SEARCH_HIDE_REQUEST,
  NAVIGATION_LOADING_REQUEST,
  NAVIGATION_LOADING_SUCCESS
} from '../../../constants/index';

// <editor-fold dsc="Public Loading">
export function loadingSearchShowRequest() {
  return {
    type: LOADING_SEARCH_SHOW_REQUEST,
  };
}

export function loadingSearchHideRequest() {
  return {
    type: LOADING_SEARCH_HIDE_REQUEST,
  };
}

export function navigationLoadingRequest() {
  return {
    type: NAVIGATION_LOADING_REQUEST,
  };
}

export function navigationLoadingSuccess() {
  return {
    type: NAVIGATION_LOADING_SUCCESS,
  };
}

// </editor-fold>
