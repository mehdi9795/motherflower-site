import {
  SHOW_ALERT_REQUEST,
  HIDE_ALERT_REQUEST,
} from '../../../constants/index';

export function showAlertRequest(data) {
  return {
    type: SHOW_ALERT_REQUEST,
    data,
  };
}

export function hideAlertRequest() {
  return {
    type: HIDE_ALERT_REQUEST,
  };
}
